##############################################################
#
# File name:	replace_swe_dot_book_names_list.txt
# Date:		2020-11-09 
# Rev:		PA1
# Ststus:		Production
#
####################### Text ###################################
#
1 Mos. 	1 Mos 
2 Mos. 	2 Mos 
3 Mos. 	3 Mos 
4 Mos.	4 Mos 
5 Mos.	5 Mos 
Jos. 	Jos 
Dom. 	Dom 
Rut. 	Rut 
1 Sam. 	1 Sam 
2 Sam. 	2 Sam 
1 Kung. 	1 Kung 
2 Kung. 	2 Kung 
1 Krön. 	1 Krön 
2 Krön. 	2 Krön 
Esra 	Esra 
Neh. 	Neh 
Est. 	Est 
Job 	Job 
Ps. 	Ps 
Ords. 	Ords 
Pred. 	Pred 
Hög. V. 	Höga V 
Jes. 	Jes 
Jer. 	Jer 
Klag. 	Klag 
Hes. 	Hes 
Dan. 	Dan 
Hos. 	Hos 
Joel 	Joel 
Amos 	Amos 
Ob. 	Ob 
Jon. 	Jona 
Mik. 	Mika 
Nah.	Nah 
Hab. 	Hab 
Sef. 	Sef 
Hagg. 	Hagg 
Sak. 	Sak 
Mal. 	Mal 
Matt. 	Matt 
Mark. 	Mark 
Luk. 	Luk 
Joh. 	Joh 
Apg. 	Apg 
Rom. 	Rom 
1 Kor. 	1 Kor 
2 Kor. 	2 Kor 
Gal. 	Gal 
Ef. 	Ef 
Fil. 	Fil 
Kol. 	Kol 
1 Thess. 	1 Thess 
2 Thess. 	2 Thess 
1 Tess. 	1 Thess 
2 Tess. 	2 Thess 
1 Tim. 	1 Tim 
2 Tim. 	2 Tim 
Tit. 	Tit 
Filem. 	Filem 
Hebr. 	Hebr 
Jak. 	Jak 
1 Petr. 	1 Pet 
2 Petr. 	2 Pet 
1 Joh. 	1 Jh 
2 Joh. 	2 Jh 
3 Joh. 	3 Jh 
Jud. 	Jud 
Upp. 	Upp 
#
####################### End ###################################