@echo off
::
:: 
::############################################################################################################
::
:: Purpose
::
:: Starting Bible Reference Finder with an ENGLISH Input txt-file.
::
:: Bible Reference Finder will store result in "Output_Bible_RF_Final.txt" Result will be opened by BAT file.
::
:: Inputs to Bat-file: 
::	%1. A document ending with ".TXT".
::
:: Language for Bible References are: English
::
:: Created by: Sune Gustafsson
::
:: Updated: 2019-10-10 
::
:: Test status: Working
::
::############################################################################################################
::
:: Set Shared variables
::
set pythonExe="C:\Users\SuneG\AppData\Local\Programs\Python\Python39\python.exe"
::
:: Set Bible Reference Finder variables
::
set BRFpython="C:\References\Python\bible-reference-finder\biblereferencefindermodule\biblereferencefinder.py"
set BRFlogFolder="Logfiles"
set BRFinputLang="english_text"

set BRFinput="C:\References\Python\bible-reference-finder\input\biblereferencefinder_input.txt"
set BRFOut01NormChapVer="C:\References\Python\bible-reference-finder\output\biblereferencefinder_output01_NormChapAndVer.txt"
set BRFOut02NormBook="C:\References\Python\bible-reference-finder\output\biblereferencefinder_output02_NormBookNames.txt"
set BRFOut03="C:\References\Python\bible-reference-finder\output\biblereferencefinder_output03_No_split.txt"
set BRFOut04="C:\References\Python\bible-reference-finder\output\biblereferencefinder_output04_Split.txt"
set BRFOut05="C:\References\Python\bible-reference-finder\output\biblereferencefinder_output05_Real.txt"

:: For PDF and Word as input
:: set BRFinfileEncoding="utf-8"
:: Does not work with PDF as input 
set BRFinfileEncoding="iso-8859-1"
:: Does not work with PDF as input
:: BRFinfileEncoding="latin-1"
set BRFinputFromSpeech="True"
:: set BRFinputFromSpeech="False"
set BRFinputFolder="VOID"

::
::############################################################################################################
::
:: Now, run the program
::
::  Index
::  0. BRFpython: 		py-file
::  1. %1:			Input file
::  2. BRFlogFolder:		Sub-folder for logfiles
::  3. BRFinputLang:		Language
::  4. BRFinfileEncoding: 	Input file encoding
::  5. BRFinputFolder: 		Folder for multiple files
::  6. BRFinputFromSpeech:	Input from speech
::
:: @echo on
:: echo Running ******Bible-RF-(.txt-speech)-Eng.bat******
::
:: @echo on
%pythonExe% %BRFpython% %1 %BRFlogFolder% %BRFinputLang% %BRFinfileEncoding% %BRFinputFolder% %BRFinputFromSpeech%
::
::############################################################################################################
::
:: Open intermediate files for test purposes
::
:: %BRFOut01NormChapVer%
:: %BRFOut02NormBook%
:: %BRFOut03%
::
:: Open Output result file
::
%BRFOut04%
::
pause
::
::############################################################################################################
::
