#!/usr/bin/env python3
from setuptools import setup
#

setup(
        name='biblereferencefinder',
        version='0.1',
        entry_points={
            'console_scripts':[
                # 'webquotegetter=website.web:run',
                'biblereferencefinder=biblereferencefindermodule.cli:cli',
                ],
            },
        packages=[
            'biblereferencefindermodule',
            # 'quotegetter',
            ],
        install_requires=[
            'python-docx',
            'python-pptx',
            'PyPDF2',
            'click',
            'pudb',
            ],
        )
