10

Lesson

*August 31�September 6

Living the Gospel

Sabbath Afternoon

Read for This Week�s Study: Rom. 8:20�23; John 3:16, 17; 
Matt. 9:36; Eph. 2:8�10; 1 John 3:16, 17; Rev. 14:6, 7.

Memory Text: �For by grace you have been saved through faith, 
and that not of yourselves; it is the gift of God, not of works, lest 
anyone should boast. For we are His workmanship, created in Christ 
Jesus for good works, which God prepared beforehand that we 
should walk in them� (Ephesians 2:8�10, NKJV). 

As soon as we talk about God�s commands, requirements, or 
instructions, we run the risk�or even face the temptation�of 
thinking that somehow what we do can earn or contribute to 
our salvation or otherwise gain favor with God. But the Bible tells us 
repeatedly that we are sinners saved by God�s grace through Jesus and 
His substitutionary death for us on the cross. What could we possibly 
add to this in any way? Or, as Ellen G. White has written: �If you would 
gather together everything that is good and holy and noble and lovely in 
man and then present the subject to the angels of God as acting a part 
in the salvation of the human soul or in merit, the proposition would be 
rejected as treason.��Faith and Works, p. 24.

Thus, too, even our works of mercy and compassion toward those in 
need should not be seen as legalistic. On the contrary, as we grow in 
our understanding and appreciation of salvation, the link between God�s 
love and His concern for the poor and oppressed will be passed on to 
us, recipients of His love. We have received, so we will give. When we 
see how God so loved us, we also see how much He loves others and 
calls us to love them, as well.

* Study this week�s lesson to prepare for Sabbath, September 7.

(page 80 of Standard Edition)


124



Sunday

September 1

�For God So Loved . . .� 

John 3:16 says, �For God so loved the world . . .� (NIV; emphasis 
supplied)�and the original Greek word is kosmos, meaning �the world 
as a created, organized entity.��The SDA Bible Commentary, vol. 5, 
p. 929. This verse is about salvation for humanity, but the plan of salvation 
has implications for the whole of creation too. 

Read Romans 8:20�23. What does this teach about the broader issues 
in the plan of salvation?

_____________________________________________________

_____________________________________________________

Of course, on one level, salvation is about each one of us in our personal 
relationship with the Lord. But there�s more. Justification is really not 
just about getting our sins forgiven. Ideally, it also should be about how, 
through Jesus and the power of the Holy Spirit, the Lord creates the family 
of God, the members of which celebrate their forgiveness and assurance 
of salvation by, among other things, being witnesses to the world through 
their good works. 

Read John 3:16, 17. How does verse 17 contribute to a broader understanding 
of verse 16?

_____________________________________________________

_____________________________________________________

We can accept that God loves people other than just ourselves. He 
loves those we love, and we rejoice in that. He also loves those we 
reach out to, and our recognition of this truth is often our motivation 
for our own reaching out to them. But He also loves those whom we 
are uncomfortable with, or even afraid of. God loves all people, everywhere, 
even those whom we might not particularly like. 

Creation is one way we see this demonstrated. The Bible consistently 
points to the world around us as evidence of God�s goodness: � �He 
causes his sun to rise on the evil and the good, and sends rain on the 
righteous and the unrighteous� � (Matt. 5:45, NIV). Even life itself is a 
gift from God, and regardless of the individual�s response or attitude to 
God, every person is a recipient of that gift.

How should it change our attitude toward others and their circumstances 
when we recognize them as beings created and loved by God?

_____________________________________________________

(page 81 of Standard Edition)

125



Monday

September 2

Compassion and Repentance

The intermingled stories of salvation and the great controversy call 
us to acknowledge a truth about life that is foundational for our understanding 
of our world and ourselves, and that is: we and our world are 
fallen, broken, and sinful. Our world is not what it was created to be, 
and though we still bear the image of the God who created us, we are 
part of the world�s brokenness. The sin in our lives is of the same nature 
as the evil that causes so much pain, oppression, and exploitation all 
over the world. 

Thus, it is right for us to feel the hurt, discomfort, sorrow, and 
tragedy 
of the world and of the lives around us. We would have to 
be robots not to feel the pain of life here. The laments in the book of 
Psalms, the sorrows of Jeremiah and the other prophets, and the tears 
and compassion of Jesus demonstrate the appropriateness of this kind 
of response to the world and its evil, and particularly to those who are 
so often hurt by that evil.

Read Matthew 9:36; 14:14; Luke 19:41, 42; and John 11:35. What was 
it in each of these verses that moved Jesus with compassion? How 
can we have a heart that is softened to the pain around us?

_____________________________________________________

_____________________________________________________

We also need to remember that sin and evil are not just �out there,� or 
the result of someone else�s brokenness: �If we claim to be without sin, 
we deceive ourselves and the truth is not in us� (1 John 1:8, NIV). In the 
understanding of the biblical prophets, sin was a tragedy not primarily 
because someone had broken �the rules,� but because sin has broken 
the relationship between God and His people, and also because our sin 
hurts other people. This may take place on a small or large scale, but 
it is the same evil. 

Selfishness, greed, meanness, prejudice, ignorance, and carelessness are 
at the root of all the world�s evil, injustice, poverty, and oppression. And 
confessing our sinfulness is a first step in addressing this evil, as well as a 
first step toward allowing the love of God to take its rightful place in our 
hearts: �If we confess our sins, he is faithful and just and will forgive us our 
sins and purify us from all unrighteousness� (1 John 1:9, NIV). 

Look at yourself (but neither too closely nor for too long). In 
what ways are you broken and part of the bigger problem? 
What�s the only answer and the only place to look?

_____________________________________________________

(page 82 of Standard Edition)

126



Tuesday

September 3

Grace and Good Works 

Summarize Ephesians 2:8�10 in your own words. What do these 
verses tell us about the relationship between grace and good works?

_____________________________________________________

_____________________________________________________

_____________________________________________________

The Bible tells us that among other things, we were created to worship 
God and to serve others. Only in our imagination can we try to 
understand what these acts would be like in a sinless environment.

For now, because of sin, we know only a broken and fallen world. 
Fortunately for us, God�s grace, expressed and enacted in Jesus� 
sacrifice for the sins of the world, opens the way for forgiveness and 
healing. Thus, even amid this broken existence our lives become 
more fully God�s workmanship, and God uses us to partner with Him 
to seek to heal and restore the damage and hurt in the lives of others 
(see Eph. 2:10). �Those who receive are to impart to others. From 
every direction are coming calls for help. God calls upon men to 
minister gladly to their fellow men.��Ellen G. White, The Ministry 
of Healing, p. 103.

Again, we do not do good works�care for the poor, lift up the 
oppressed, feed the hungry�in order to earn salvation or standing with 
God. In Christ, by faith, we have all the standing with God we will ever 
need. Rather, we recognize ourselves as both sinners and victims of sin 
who are, nonetheless, loved and redeemed by God. While we still battle 
with temptations to self-centeredness and greed, the self-sacrificing 
and humble grace of God offers a new kind of life and love that will 
transform our lives. 

When we look at the Cross, we see the great and complete sacrifice 
done for us and realize that we can add nothing to what it offers us 
in Christ. But this does not mean that we shouldn�t do something in 
response to what we have been given in Christ. On the contrary, we 
must respond, and what better way to respond to the love that has been 
shown us than by showing love to others? 

Read 1 John 3:16, 17. How do these verses so powerfully capture 
what our response to the Cross should be?

_____________________________________________________

_____________________________________________________

(page 83 of Standard Edition)

127



Wednesday

September 4

Our Common Humanity

By His ministry and His teaching, Jesus urged a radical inclusiveness. 
All who sought His attention with honest motives�whether 
women with bad reputations, tax collectors, lepers, Samaritans, Roman 
centurions, religious leaders, or children�He welcomed with genuine 
warmth and care. As the early church was to discover in transformative 
ways, this included the offer of the gift of salvation. 

As the first believers slowly recognized the inclusiveness of the gospel, 
they were not merely adding good works for others onto their faith 
as a �nice� thing to do. It was core to their understanding of the gospel, 
as they had experienced it in the life, ministry, and death of Jesus. As 
they wrestled with the issues and questions that arose, first individually 
for leaders such as Paul and Peter (see, for example, Acts 10:9�20), then 
as a church body at the Jerusalem Council (see Acts 15), they began to 
realize the dramatic shift this good news had brought into their understanding 
of God�s love and inclusiveness and how that should be lived 
out in the lives of those who profess to follow Him.

What do each of the following texts teach us about our common 
humanity? 
How should each idea influence our attitude toward others? 


Mal. 2:10

Acts 17:26

Rom. 3:23

Gal. 3:28

Galatians 3:28 is a theological summary of the practical story Jesus 
told about the good Samaritan. Rather than arguing about whom we 
are obligated to serve, just go and serve, and perhaps even be prepared 
to be served by those we might not expect to serve us. The common 
element of the global human family is realized at a higher level in the 
common family of those who are bound together by the gospel, by 
the saving love of God that calls us to oneness in Him: �For we were 
all baptized by one Spirit so as to form one body�whether Jews or 
Gentiles, slave or free� (1 Cor. 12:13, NIV).

(page 84 of Standard Edition)

128



Thursday

September 5

The Everlasting Gospel 

The transforming invitation and appeal of the gospel �to every 
nation, tribe, language and people� (Rev. 14:6, NIV) has continued 
throughout Christian history. However, Revelation describes a renewed 
proclamation of this message�the good news about Jesus and all that 
entails�at the end of time.

Read Revelation 14:6, 7. How is the common understanding of the 
gospel�most commonly summarized by John 3:16�included in 
the angel�s specific message in verse 7?

_____________________________________________________

_____________________________________________________

Revelation 14:7 brings together three key elements we have already 
noted in this study of God�s concern about evil, poverty, and oppression 
throughout the Bible story:

Judgment. The appeal for judgment�for justice to be done�has 
been a repeated call of those who have been oppressed throughout history. 
Fortunately, the Bible portrays God as One who hears the cries of 
those in distress. As often expressed in the Psalms, for example, those 
who are being treated unfairly regard judgment as good news.

Worship. The writings of the Hebrew prophets often link the subjects 
of worship and good deeds, particularly when comparing the worship 
of those who claimed to be God�s people with the wrongs that they 
committed and continued. In Isaiah 58, for example, God explicitly 
stated that the worship He most desired was acts of kindness and care 
for the poor and needy (see Isa. 58:6, 7).

Creation. As we have seen, one of the foundational elements of God�s 
call for justice is the common family of humanity, that we are all created 
in His image and loved by Him, that we all have value in His sight and 
that no one should be exploited or oppressed for the unjust gain and greed 
of another. It seems clear that this end-time proclamation of the gospel is 
a broad and far-reaching call to accept the rescue, redemption, and restoration 
that God wants for fallen humanity. Hence, even amid the issues 
regarding true and false worship, and persecution (see Rev. 14:8�12), God 
will have a people who will stand for what is right, for the commandments 
of God and the faith of Jesus, even amid the worst of evil. 

How can we find ways of ministering to those in need while at the 
same time sharing with them both the hope and the warning that 
are found in the three angels� messages?

_____________________________________________________

(page 85 of Standard Edition)

129



September 6

Further Thought: Read Ellen G. White, � �God With Us,� � pp. 
19�26, in The Desire of Ages; �Saved to Serve,� pp. 95�107, in The 
Ministry of Healing.

�God claims the whole earth as His vineyard. Though now in the hands 
of the usurper, it belongs to God. By redemption no less than by creation it 
is His. For the world Christ�s sacrifice was made. �God so loved the world, 
that He gave His only begotten Son.� John 3:16. It is through that one gift 
that every other is imparted to men. Daily the whole world receives blessing 
from God. Every drop of rain, every ray of light shed on our unthankful 
race, every leaf and flower and fruit, testifies to God�s long forbearance and 
His great love.��Ellen G. White, Christ�s Object Lessons, pp. 301, 302.

�In Christ there is neither Jew nor Greek, bond nor free. All are brought 
nigh by His precious blood. (Gal. 3:28; Eph. 2:13.) 

�Whatever the difference in religious belief, a call from suffering 
humanity must be heard and answered. . . . 

�All around us are poor, tried souls that need sympathizing words and 
helpful deeds. There are widows who need sympathy and assistance. There 
are orphans whom Christ has bidden His followers receive as a trust from 
God. Too often these are passed by with neglect. They may be ragged, 
uncouth, and seemingly in every way unattractive; yet they are God�s property. 
They have been bought with a price, and they are as precious in His 
sight as we are. They are members of God�s great household, and Christians 
as His stewards are responsible for them.��
Pages 386, 387.

Discussion Questions:

. In seeking to do good works and help others, how can we resist 
the temptation to think that this somehow makes us better and gains 
us merit that God should recognize?

. Is your church a community in which there is �no difference��
but all are one in Christ? How can it become more so? How inclusive 
of others is your church?

. How do we find the right balance in doing good for those in need, 
if for no other reason than that they are in need and we can help 
them, while at the same time reaching out to them with the truths of 
the gospel? How can we learn to do both, and why is it always better 
to do both?

Summary: God�s love as expressed in the plan of salvation and enacted in the life 
and sacrifice of Jesus offers us forgiveness, life, and hope. As recipients 
of this grace, we seek to share this with others, not to earn salvation, but 
because it is what we have been created and re-created to do. As such, the 
gospel transforms relationships and moves us to serve, particularly those 
most in need.

Friday

(page 86 of Standard Edition)

130



Story

inside

Half Loaf of Bread

By Andrew McChesney, Adventist Mission

Maya approached Valentina with a loaf of white bread after the worship 
service. �Valya, please take this,� she said, holding out the loaf.

Valentina, 40, looked at the bread hungrily. She hadn�t eaten a crumb 
of bread in more than six months. It was impossible to find bread on store 
shelves in Sukhumi, capital of Georgia�s breakaway region of Abkhazia. It 
was 1993, and a months-long armed conflict between Georgian and Abkhaz 
forces had resulted in a major food shortage. 

�Take this, please,� Maya, 45, said again, still offering the bread. �This is 
from me to you.�

Valentina slowly shook her head. �I can�t take this from you,� she said. 
�You need it just as badly as we do.�

Maya began to cry. �Please, take this,� she said. �You walked so far to help 
us. This is a gift that I want to give you, but you are refusing to accept it.�

�OK,� Valentina said, finally relenting. �But let�s cut the bread in half. You 
take half, and I�ll take half.�

The women divided the bread with a knife from the kitchen of the house 
church, where about 40 people gathered regularly to pray and read the Bible 
under the leadership of Valentina�s husband, Pavel Dmitrienko, a Seventh-
day Adventist pastor. Moments later, Valentina and Pavel left the house and 
started the nine-mile (15-kilometer) trek back to their home.

Valentina smiled as she thought about the bread in her purse. She looked 
forward to enjoying it with a simple soup of barley and water that evening.

�I will make soup, and we will eat it with real bread,� she said.

Pavel returned her happy smile. He also wanted to eat the bread.

Partway home, the couple met an elderly woman on a bridge. She was thin, 
and her clothing was filthy. She looked at Valentina.

�Daughter,� she said with a wavering voice, �would you happen to have a 
piece of bread?�

Valentina immediately removed the half loaf from her purse and presented 
it to the woman. �Yes, I have, dear Grandmother,� she said. �Please, take this.�

The elderly woman wept as she accepted the bread. �Thank you,� she said, 
tears streaking her dirty, wrinkled cheeks. �I haven�t eaten in three days. 
You�ve saved me from death.�

Valentina and Pavel continued on their way 
home. They were happy that they had been able 
to sacrifice their precious bread. �We gave the 
one thing that we wanted most of all to the grandmother 
and saved her life,� Valentina, now 65 and 
pictured left, said in an interview in her home in 
Belgorod, Russia. �It was a real sacrifice�and it 
made us happier than ever before.�


Provided by the General Conference Office of Adventist Mission, which uses Sabbath School 
mission offerings to spread the gospel worldwide. Read new stories daily at AdventistMission.org.

131



teachers comments

Part I: Overview 

God is proactive in His desire to draw people to Himself. He seeks to 
make disciples who, in turn, become channels of service that exhibit 
His grace to the world. This intention is made clear in Titus 2:11�14: 
�For the grace of God that bringeth salvation hath appeared to all men, 
teaching us that, denying ungodliness and worldly lusts, we should 
live soberly, righteously, and godly, in this present world; looking for 
that blessed hope, and the glorious appearing of the great God and our 
Saviour Jesus Christ; who gave himself for us, that he might redeem us 
from all iniquity, and purify unto himself a peculiar people, zealous of 
good works.�

In this lesson, we ponder the love of God on which the entire plan of 
redemption rests. We sense the compassion of Jesus for the brokenness of 
humanity around Him. Each of us is part of that brokenness. We recognize 
that when we confess our brokenness and repent, we will experience God�s 
grace. How we live our personal lives will then be changed. In response 
to God�s grace to us, we will be motivated, empowered, enabled by God to 
show, without condemnation, His self-sacrificing love and grace to other 
broken people. This love will be demonstrated when we live the everlasting 
gospel in word and deed in our interaction with all humanity, regardless 
of nationality, race, or background. 

Teacher�s Aim:

Explore with your class a deeper understanding of these familiar, though 
life-changing and powerful, truths. 

Part II: Commentary

Scripture: Bring to class a picture or model of the human body. Ask the 
class: What does it mean to be human? 

Read Genesis 1:26. Review what it means to be made in God�s image 
(see lesson 1). 

�When Adam came from the Creator�s hand, he bore, in his physical, 
mental, and spiritual nature, a likeness to his Maker.��Ellen G. White, 
Education, p. 15. The image of God in man became marred by sin. Thus, 
the purpose of redemption is to restore in humanity God�s image. 

Ever since sin started, and up to now, God�s people are called to graciously 
reveal Christ and the gospel by partnering with Him in restoring 
humanity physically, mentally, and spiritually. How is this divine-human 
collaboration for restoration manifested in the Seventh-day Adventist 
Church? Here is one example: the church operates almost 500 hospitals, 

132



sanitariums, clinics, and dispensaries, not including nursing homes, 
orphanages, etc. Over 8,539 Adventist schools, from primary to university 
level, impact communities worldwide.1 Additionally, churches also 
exist to aid in restoration of the whole being. However, �[t]oo many 
times the church has promoted very unbiblical concepts by assigning the 
physical restoration of people merely to health professions, the mental 
part to educators, while the pastors and evangelists are expected to deal 
with the restoration of the spiritual part of a person. This is a very convenient 
arrangement, but unbiblical because a person cannot be divided 
into these parts. A person is a whole human being.�2 

If our churches do not fully proclaim �the gospel of Christ� (Rom. 
15:19) in a wholistic manner�addressing the physical, mental, and 
spiritual (including social) dimensions of humankind�our presentation 
of the gospel will be deficient. Our mission is not about merely saving 
souls through proclamation of the gospel, but saving and serving people 
wholistically.

Draw three columns on a board, if available, entitled: physical, mental, 
spiritual. Ask your class to think of how your church is serving 
your community locally in each of these three areas. List the ideas on 
the board in the appropriate columns. Discuss where your church can 
improve. 

Illustration: Graffiti scrawled in the New York subway says, �God is 
alive�He just doesn�t want to get involved.�3 Sometimes, in the midst of 
our painful experiences we might be tempted to wonder if God is interested 
in our despair and pain. Ask your class: Because God actually wants 
to be involved with each of us individually, and equally loves everyone 
in the whole world (John 3:16), in what ways do you see Him loving you 
and caring for your needs? How is He using you as a channel to show His 
love and care for others?

Think about this admonition for God�s people to get involved: 
�Unless there is practical self-sacrifice for the good of others, in the 
family circle, in the neighborhood, in the church, and wherever we may 
be, then whatever our profession, we are not Christians.��Ellen G. 
White, The Desire of Ages, p. 504.

Scripture: Jesus� disciples believed that Jesus, as Messiah, would free 
Israel from Roman oppression and bring judgment and condemnation to 
their enemies. 

But in John 3:16, Jesus overturns this misguided thinking. He reveals 

teachers comments

1 �Education Statistics,� Education, Adventist.org, December 31, 2017, https://education.adventist.org
/education-statistics/. 

2 Rudi Maier, Working With the Poor: Selected Passages From Ellen G. White on Social Responsibility (Berrien 
Springs, MI: Department of World Mission, Andrews University, 2007) p. 2. 

3 Ferdinand Funk, �John 20:19�31: Scars That Heal�Jubilee� Faithlife, accessed February 27, 2019, https://sermons
.faithlife.com/sermons/113763-john-2019-31-scars-that-heal-jubilee.

 

133



God�s love for this broken world. God gave His unique Son that, whoever 
believes (has faith) in Him will not perish, but inherit eternity. Then, in 
verse 17, Jesus makes it clear that His purpose, during His first coming, 
was not to bring condemnation and judgment, but to bring salvation. 
Jesus had to come as Redeemer before He could come as a Judge. 

Through His life, death, and resurrection, Jesus would pay the penalty 
for our sins so that all humanity would have a choice between perishing 
and having eternal life. Given that choice, all humanity will then face 
Jesus at His second coming. Read 2 Thessalonians 1:6�8. 

How can we by our words and deeds proclaim, in a balanced way, the 
truths connected with both Christ�s first and second comings?

Scripture: In Ephesians 2:1�11, God�s people are reminded that they were 
dead in transgressions and sins. But, because of His great love and grace, 
they are made alive with Christ and reconciled to Him. (See also 2 Cor. 
5:17, 18.)

Notice the same message in Ezekiel 37:1�10. God calls for the dry 
bones of His broken people to take heed of the fact that He will revive 
them. In verse 6, we see that God chooses to do this work by placing 
tendons, flesh, and skin on the bones and breathing upon them to bring 
them to life. What spiritual lessons regarding revival can you draw from 
this revival process? 

God�s grace, which brings new life to His broken people, is given for 
two purposes as described in Ephesians 2:7 and Ephesians 2:10.

Revival and salvation of ourselves is not enough. We are saved �to do 
good works.� Even though we are not saved by such good works (Eph. 
2:9), we are saved for good works (Eph. 2:10). Doing good works should 
not be dismissed as a way of avoiding the risks of legalism. Rather a 
deep understanding of grace prompts us to good works in response and 
in partnership with God. Everything we do must be seen through the 
Cross of Christ. We are not working toward salvation, but from salvation. 

Discuss: How does your salvation change your community? 

Illustration: The gospel is not only an �everlasting gospel� (Rev. 14:6) 
for all time, it is an �all-inclusive� gospel for all humanity (John 3:16). All 
those who accept Jesus are saved (see also John 1:12), and God keeps on 
loving all those who do not accept Him. Ask your class: What additional 
�all� verses can you find? 

Remember the song many of us grew up with in Sabbath School: 
�Jesus loves me, this I know�? In light of our greater understanding 
of the gospel, perhaps we need another verse: �Jesus loves them, this I 
know.� When we see God�s love encompassing others, even people we 
might find difficult to love, we gain a larger insight into the greatness of 

teachers comments

134



the love of God. The love Jesus has for all humanity calls for amending the 
lyrics, as such: �Jesus loves us, this I know.�

This inclusive mind-set also will likely prompt a revision of other songs 
that we might sing and apply to our lives, such as �I�m So Glad Jesus Lifted 
Me.� Invite the class to think of other gospel songs that could use some 
additional inclusivity verses. If appropriate, close the class period by singing 
together one of these �revised� songs. 

Scripture: Read Paul�s words about reconciliation in 2 Corinthians 5:14�21 
together in class:

�For the love of Christ constraineth us; because we thus judge, that if 
one died for all, then were all dead: and that he died for all, that they which 
live should not henceforth live unto themselves, but unto him which died 
for them, and rose again. Wherefore henceforth know we no man after the 
flesh: yea, though we have known Christ after the flesh, yet now henceforth 
know we him no more. Therefore if any man be in Christ, he is a new creature: 
old things are passed away; behold, all things are become new. And 
all things are of God, who hath reconciled us to himself by Jesus Christ, 
and hath given to us the ministry of reconciliation; to wit, that God was in 
Christ, reconciling the world unto himself, not imputing their trespasses 
unto them; and hath committed unto us the word of reconciliation. Now 
then we are ambassadors for Christ, as though God did beseech you by us: 
we pray you in Christ�s stead, be ye reconciled to God. For he hath made 
him to be sin for us, who knew no sin; that we might be made the righteousness 
of God in him.�

The gospel works to break down walls erected by social distinctions. It 
also serves to bring reconciliation, not just between God and humans but 
in human-to-human relationships, as well. How does this teaching apply to 
us? What does Paul mean when he says �that they which live should not 
henceforth live unto themselves�? What does it mean to be �ambassadors� 
for Christ? Why must we become new creatures first? 

 

 

Part III: Life Application

Wholistic biblical justice is central to the gospel and to sharing it. Sharing the 
gospel can be done by enacting it and living out its implications, as truly as it 
is done by proclamation. This intention can be accomplished most effectively 
with all of Christ�s wholistic ministry method, which brings �true success.� 
(See Ellen G. White, The Minsitry of Healing, p. 143.) No matter what step 
we are taking in Christ�s method, we can call it �success.� Being a link in the 

teachers comments

135



chain is as important as being the last link.

Discuss: As we seek to help others, why is it important to provide opportunities 
to them to follow Jesus? Or is social ministry alone enough? Why, 
or why not? As you ponder your answers, consider the following thought: 
When people accept Jesus into their lives, He will impart to them the 
power to make and sustain positive life changes. In fact, the gospel awakens 
a powerful impulse within the converted heart toward social reform. 
But that impulse must flow forth from an authentic relationship with 
Christ Jesus, a relationship in which His love abides in us and we abide 
in Him. United in this way, as the branch is to the Vine (John 15:5�7), 
our efforts to improve the lives of others and lead them to Jesus will bear 
much fruit. �The strongest argument in favor of the gospel is a loving and 
lovable Christian.��Ellen G. White, Counsels on Sabbath School Work, 
p. 100.

Ask class members to share experiences in which they directly introduced 
the people they served to Jesus. How should we treat people who, 
up to now, have not accepted Jesus? Why is it important to serve people 
anyway, just because they are in need?

Notes

teachers comments

136



