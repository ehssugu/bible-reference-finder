
# normalized_eng_book_names_list = [
#     # 2020-10-15 Now in database file
#     'Genesis', 'Exodus', 'Leviticus', 'Numbers', 'Deuteronomy',
#     'Joshua', 'Judges', 'Ruth', '1 Samuel', '2 Samuel',
#     '1 Kings', '2 Kings', '1 Chronicles', '2 Chronicles', 'Ezra',
#     'Nehemiah', 'Esther', 'Job', 'Psalms', 'Proverbs',
#     'Ecclesiastes', 'Song of Solomon', 'Isaiah', 'Jeremiah', 'Lamentations',
#     'Ezekiel', 'Daniel', 'Hosea', 'Joel', 'Amos',
#     'Obadiah', 'Jonah', 'Micah', 'Nahum', 'Habakkuk',
#     'Zephaniah', 'Haggai', 'Zechariah', 'Malachi',
#     'Matthew', 'Mark', 'Luke', 'John', 'Acts',
#     'Romans', '1 Corinthians', '2 Corinthians', 'Galatians', 'Ephesians',
#     'Philippians',  # 50
#     'Colossians', '1 Thessalonians', '2 Thessalonians', '1 Timothy', '2 Timothy',
#     'Titus', 'Philemon', 'Hebrews', 'James', '1 Peter',
#     '2 Peter', '1 Jn', '2 Jn', '3 Jn', 'Jude', 'Revelation', 'Book'
#     # 2019-09-22 x John and John the gospel need to have different names
# ]
#
# t_replace_eng_book_names_list = [
#     # 2020-10-15 Now om database file
#     ['Gen. ', 'Genesis '],
#     ['Gen ', 'Genesis '],
#     ['Ex. ', 'Exodus '],
#     ['Ex ', 'Exodus '],
#     ['Lev. ', 'Leviticus '],
#     ['Lev ', 'Leviticus '],
#     ['Num. ', 'Numbers '],
#     ['Num ', 'Numbers '],
#     ['Deut. ', 'Deuteronomy '],
#     ['Deut ', 'Deuteronomy '],
#     ['Jugd. ', 'Judges '],
#     ['Jugd ', 'Judges '],
#     # ['1 Ki. ', '1 Kings '],
#     # ['1 Ki ', '1 Kings '],
#     # ['2 Ki. ', '2 Kings '],
#     # ['2 Ki ', '2 Kings '],
#     # ['1Ki. ', '1 Kings '],
#     # ['2Ki. ', '2 Kings '],
#     # ['1Ki ', '1 Kings '],
#     # ['2Ki ', '2 Kings '],
#     ['1 Sam. ', '1 Samuel '],
#     ['1 Sam ', '1 Samuel '],
#     ['2 Sam. ', '2 Samuel '],
#     ['2 Sam ', '2 Samuel '],
#     ['1Sm. ', '1 Samuel '],
#     ['2Sm. ', '2 Samuel '],
#     ['1Sm ', '1 Samuel '],
#     ['2Sm ', '2 Samuel '],
#     ['1 Ch. ', '1 Chronicles '],
#     ['2 Ch. ', '2 Chronicles '],
#     ['1Ch. ', '1 Chronicles '],
#     ['2Ch. ', '2 Chronicles '],
#     ['1 Ch ', '1 Chronicles '],
#     ['2 Ch ', '2 Chronicles '],
#     ['1Ch ', '1 Chronicles '],
#     ['2Ch ', '2 Chronicles '],
#     ['Ps. ', 'Psalms '],
#     ['Psalm ', 'Psalms '],
#     ['Psalm. ', 'Psalms '],
#     ['Ps ', 'Psalms '],
#     ['Prov. ', 'Proverbs '],
#     ['Prov ', 'Proverbs '],
#     ['Pr. ', 'Proverbs '],
#     ['Pr ', 'Proverbs '],
#     ['Ecc. ', 'Ecclesiates '],
#     ['Ecc ', 'Ecclesiates '],
#     ['Son. ', 'Songs of Songs '],
#     ['Son ', 'Songs of Songs '],
#     ['Lam. ', 'Lamentations '],
#     ['Lam ', 'Lamentations '],
#     ['Isa ', 'Isaiah '],
#     ['Jer ', 'Jeremiah '],
#     ['Mat ', 'Matthew '],
#     ['Matt. ', 'Matthew '],
#     ['Lk. ', 'Luke '],
#     ['Lk ', 'Luke '],
#     ['Luk. ', 'Luke '],
#     ['Mk. ', 'Mark '],
#     ['Mk ', 'Mark '],
#     ['Jn. ', 'John '],
#     ['Jn ', 'John '],
#     ['Act. ', 'Acts '],
#     ['Act ', 'Acts '],
#     ['Rom. ', 'Romans '],
#     ['Rom ', 'Romans '],
#     ['1 Cor. ', '1 Corinthians '],
#     ['2 Cor. ', '2 Corinthians '],
#     ['1Cor. ', '1 Corinthians '],
#     ['2Cor. ', '2 Corinthians '],
#     ['1 Cor ', '1 Corinthians '],
#     ['2 Cor ', '2 Corinthians '],
#     ['1 Cor ', '1 Corinthians '],
#     ['2 Cor ', '2 Corinthians '],
#     ['Gal. ', 'Galatians '],
#     ['Gal ', 'Galatians '],
#     ['Eph. ', 'Ephesians '],
#     ['Eph ', 'Ephesians '],
#     ['Phil. ', 'Philippians '],
#     ['Phil ', 'Philippians '],
#     ['Col. ', 'Colossians '],
#     ['Col ', 'Colossians '],
#     ['1 Th. ', '1 Thessalonians '],
#     ['2 Th. ', '2 Thessalonians '],
#     ['1Th. ', '1 Thessalonians '],
#     ['2Th. ', '2 Thessalonians '],
#     ['1 Th ', '1 Thessalonians '],
#     ['2 Th ', '2 Thessalonians '],
#     ['1Th ', '1 Thessalonians '],
#     ['2Th ', '2 Thessalonians '],
#     ['Philem. ', 'Philemon '],
#     ['Heb. ', 'Hebrews '],
#     ['Heb ', 'Hebrews '],
#     ['Jas ', 'James '],
#     ['1 Jn. ', '1 Jn '],
#     ['2 Jn. ', '2 Jn '],
#     ['3 Jn. ', '3 Jn '],
#     ['1 John ', '1 Jn '],
#     ['2 John ', '2 Jn '],
#     ['3 John ', '3 Jn '],
#     ['I John ', '1 Jn '],
#     ['II John ', '2 Jn '],
#     ['III John ', '3 Jn '],
#     ['Rev. ', 'Revelation '],
#     ['Rev ', 'Revelation ']
# ]
#
# 2020-10-15
# def normalize_eng_bible_book_names(i_str):
#     i_str = prepare_eng_text_for_replacement(i_str)
#     i_str = remove_eng_reference_prefix(i_str)  # 2019-10-09
#     i_str = remove_eng_reference_suffix(i_str)  # 2019-10-17
#     # print('1010: ' + i_str)
#
#     f_normalized_eng_bible_book_names_list = Path(script_directory) / 'database' / 'normalized_eng_bible_book_names_list.txt'
#     normalized_eng_bible_book_names_list = get_2_tuple_as_list(f_normalized_eng_bible_book_names_list, False)
#
#     for item in normalized_eng_bible_book_names_list:
#         i_str = re.sub(item[0], item[1], i_str)
#     # i_str = normalize_eng_01_genesis(i_str)
#     # i_str = normalize_eng_02_exodus(i_str)
#     # i_str = normalize_eng_03_leviticus(i_str)
#     # i_str = normalize_eng_04_numbers(i_str)
#     # i_str = normalize_eng_05_deuteronomy(i_str)
#     # i_str = normalize_eng_06_joshua(i_str)
#     # i_str = normalize_eng_07_judges(i_str)
#     # i_str = normalize_eng_08_ruth(i_str)
#     # i_str = normalize_eng_10_2samuel(i_str)
#     # i_str = normalize_eng_09_1samuel(i_str)
#     # i_str = normalize_eng_12_2kings(i_str)
#     # i_str = normalize_eng_11_1kings(i_str)
#     # i_str = normalize_eng_14_2chronicles(i_str)
#     # i_str = normalize_eng_13_1chronicles(i_str)
#     # i_str = normalize_eng_15_ezra(i_str)
#     # i_str = normalize_eng_16_nehemiah(i_str)
#     # i_str = normalize_eng_17_esther(i_str)
#     # i_str = normalize_eng_18_job(i_str)
#     # i_str = normalize_eng_19_psalms(i_str)
#     # i_str = normalize_eng_20_proverbs(i_str)
#     # i_str = normalize_eng_21_ecclesiastes(i_str)
#     # i_str = normalize_eng_22_songofsolomon(i_str)
#     # i_str = normalize_eng_23_isaiah(i_str)
#     # i_str = normalize_eng_24_jeremiah(i_str)
#     # i_str = normalize_eng_25_lamentations(i_str)
#     # i_str = normalize_eng_26_ezekiel(i_str)
#     # i_str = normalize_eng_27_daniel(i_str)
#     # i_str = normalize_eng_28_hosea(i_str)
#     # i_str = normalize_eng_29_joel(i_str)
#     # i_str = normalize_eng_30_amos(i_str)
#     # i_str = normalize_eng_31_obadiah(i_str)
#     # i_str = normalize_eng_32_jonah(i_str)
#     # i_str = normalize_eng_33_micah(i_str)
#     # i_str = normalize_eng_34_nahum(i_str)
#     # i_str = normalize_eng_35_habakkuk(i_str)
#     # i_str = normalize_eng_36_zephaniah(i_str)
#     # i_str = normalize_eng_37_haggai(i_str)
#     # i_str = normalize_eng_38_zechariah(i_str)
#     # i_str = normalize_eng_39_malachi(i_str)
#     # i_str = normalize_eng_40_matthew(i_str)
#     # i_str = normalize_eng_41_mark(i_str)
#     # i_str = normalize_eng_42_luke(i_str)
#     # i_str = normalize_eng_43_john(i_str)
#     # i_str = normalize_eng_44_acts(i_str)
#     # i_str = normalize_eng_45_romans(i_str)
#     # i_str = normalize_eng_47_2cor(i_str)
#     # i_str = normalize_eng_46_1cor(i_str)
#     # i_str = normalize_eng_48_galatians(i_str)
#     # i_str = normalize_eng_49_ephesians(i_str)
#     # i_str = normalize_eng_50_philippians(i_str)
#     # i_str = normalize_eng_51_colossians(i_str)
#     # i_str = normalize_eng_53_2thess(i_str)
#     # i_str = normalize_eng_52_1thess(i_str)
#     # i_str = normalize_eng_55_2timothy(i_str)
#     # i_str = normalize_eng_54_1timothy(i_str)
#     # i_str = normalize_eng_56_titus(i_str)
#     # i_str = normalize_eng_57_philemon(i_str)
#     # i_str = normalize_eng_58_hebrews(i_str)
#     # i_str = normalize_eng_59_james(i_str)
#     # i_str = normalize_eng_61_2peter(i_str)
#     # i_str = normalize_eng_60_1peter(i_str)
#     # i_str = normalize_eng_64_3john(i_str)
#     # i_str = normalize_eng_63_2john(i_str)
#     # i_str = normalize_eng_62_1john(i_str)
#     # i_str = normalize_eng_65_jude(i_str)
#     # i_str = normalize_eng_66_revelation(i_str)
#
#     # print('1109 out_string: ' +out_string)
#     return i_str  # rename_eng_books_with_people_name
#
# 2020-10-15
# Procedures for changing English book NAMES
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
#
#
# def normalize_eng_01_genesis(i_str):
#     i_str = re.sub(r'Gen\. ([0-9]+):([0-9]+)', r'Genesis \1:\2', i_str)
#     i_str = re.sub(r'Gn\. ([0-9]+):([0-9]+)', r'Genesis \1:\2', i_str)
#     i_str = re.sub(r'Gn ([0-9]+):([0-9]+)', r'Genesis \1:\2', i_str)
#     i_str = re.sub(r'Gen ([0-9]+):([0-9]+)', r'Genesis \1:\2', i_str)         # 2019-12-04
#     i_str = re.sub(r'GENESIS', r'Genesis', i_str)
#     i_str = re.sub(r'GEN\.', r' Genesis', i_str)
#     i_str = re.sub(r' GEN ', r' Genesis ', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Genesis ([0-9]+)', r'Genesis \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Genesis ([0-9]+)', r'Genesis \2:\1', i_str)
#     i_str = re.sub(r'Genesis chapter ([0-9]+)', r'Genesis \1', i_str)
#     return i_str
#
#
# def normalize_eng_02_exodus(i_str):
#     i_str = re.sub(r'Ex\. ([0-9]+):([0-9]+)', r'Exodus \1:\2', i_str)
#     i_str = re.sub(r'Exod ([0-9]+):([0-9]+)', r'Exodus \1:\2', i_str)
#     i_str = re.sub(r'Ex ([0-9]+):([0-9]+)', r'Exodus \1:\2', i_str)           # 2019-12-04
#     i_str = re.sub(r'EXODUS', r'Exodus', i_str)
#     i_str = re.sub(r'EX\.', r'Exodus', i_str)
#     i_str = re.sub(r' EX ', r'Exodus', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Exodus ([0-9]+)', r'Exodus \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Exodus ([0-9]+)', r'Exodus \2:\1', i_str)
#     i_str = re.sub(r'Exodus chapter ([0-9]+)', r'Exodus \1', i_str)
#     return i_str
#
#
# def normalize_eng_03_leviticus(i_str):
#     i_str = re.sub(r'LEVITICUS', r'Leviticus', i_str)
#     i_str = re.sub(r'LEV\.', r'Leviticus', i_str)
#     i_str = re.sub(r' LEV ', r' Leviticus ', i_str)
#     i_str = re.sub(r'Lev\. ([0-9]+):([0-9]+)', r'Leviticus \1:\2', i_str)
#     i_str = re.sub(r'Lev ([0-9]+):([0-9]+)', r'Leviticus \1:\2', i_str)       # 2019-12-04
#     i_str = re.sub(r'verse ([0-9]+) of Leviticus ([0-9]+)', r'Leviticus \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Leviticus ([0-9]+)', r'Leviticus \2:\1', i_str)
#     i_str = re.sub(r'Leviticus chapter ([0-9]+)', r'Leviticus \1', i_str)
#     return i_str
#
#
# def normalize_eng_04_numbers(i_str):
#     i_str = re.sub(r'NUMBERS', r'Numbers', i_str)
#     i_str = re.sub(r'NUM\.', r'Numbers', i_str)
#     i_str = re.sub(r' NUM ', r' Numbers ', i_str)
#     i_str = re.sub(r'Num\. ([0-9]+):([0-9]+)', r'Numbers \1:\2', i_str)
#     i_str = re.sub(r'Num ([0-9]+):([0-9]+)', r'Numbers \1:\2', i_str)       # 2019-12-04
#     i_str = re.sub(r'verse ([0-9]+) of Numbers ([0-9]+)', r'Numbers \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Numbers ([0-9]+)', r'Numbers \2:\1', i_str)
#     i_str = re.sub(r'Numbers chapter ([0-9]+)', r'Numbers \1', i_str)
#     return i_str
#
#
# def normalize_eng_05_deuteronomy(i_str):
#     i_str = re.sub(r'DEUTERONOMY', r'Deuteronomy', i_str)
#     i_str = re.sub(r'DEU\.', r'Deuteronomy', i_str)
#     i_str = re.sub(r'DEU', r'Deuteronomy', i_str)
#     i_str = re.sub(r' DEU ', r' Deuteronomy ', i_str)
#     i_str = re.sub(r'Dt\. ([0-9]+):([0-9]+)', r'Deuteronomy \1:\2', i_str)
#     i_str = re.sub(r'Dt ([0-9]+):([0-9]+)', r'Deuteronomy \1:\2', i_str)
#     i_str = re.sub(r'Deu ([0-9]+):([0-9]+)', r'Deuteronomy \1:\2', i_str)     # 2019-12-04
#     i_str = re.sub(r'verse ([0-9]+) of Deuteronomy ([0-9]+)', r'Deuteronomy \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Deuteronomy ([0-9]+)', r'Deuteronomy \2:\1', i_str)
#     i_str = re.sub(r'Deuteronomy chapter ([0-9]+)', r'Deuteronomy \1', i_str)
#     return i_str
#
#
# def normalize_eng_06_joshua(i_str):
#     i_str = re.sub(r'JOSHUA', r'Joshua', i_str)
#     i_str = re.sub(r'JOS\.', r'Joshua', i_str)
#     i_str = re.sub(r' JOS ', r' Joshua ', i_str)
#     i_str = re.sub(r'Jos\. ([0-9]+):([0-9]+)', r'Joshua \1:\2', i_str)
#     i_str = re.sub(r'Jos ([0-9]+):([0-9]+)', r'Joshua \1:\2', i_str)
#     i_str = re.sub(r'Josh ([0-9]+):([0-9]+)', r'Joshua \1:\2', i_str)         # 2019-12-05
#     i_str = re.sub(r'verse ([0-9]+) of Joshua ([0-9]+)', r'Joshua \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Joshua ([0-9]+)', r'Joshua \2:\1', i_str)
#     i_str = re.sub(r'Joshua chapter ([0-9]+)', r'Joshua \1', i_str)
#     return i_str
#
#
# def normalize_eng_07_judges(i_str):
#     i_str = re.sub(r'JUDGES ([0-9]+):([0-9]+)', r'Judges \1:\2', i_str)
#     i_str = re.sub(r'JUD\. ([0-9]+):([0-9]+)', r'Judges \1:\2', i_str)
#     i_str = re.sub(r'JUD ([0-9]+):([0-9]+)', r'Judges \1:\2', i_str)
#     i_str = re.sub(r'Judg ([0-9]+):([0-9]+)', r'Judges \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Judges ([0-9]+)', r'Judges \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Judges ([0-9]+)', r'Judges \2:\1', i_str)
#     i_str = re.sub(r'Judges chapter ([0-9]+)', r'Judges \1', i_str)
#     return i_str
#
#
# def normalize_eng_08_ruth(i_str):
#     i_str = re.sub(r'RUTH', r'Ruth', i_str)
#     i_str = re.sub(r'RT\.', r'Ruth', i_str)
#     i_str = re.sub(r' RT ', r' Ruth ', i_str)
#     i_str = re.sub(r'Rt\. ([0-9]+):([0-9]+)', r'Ruth \1:\2', i_str)
#     i_str = re.sub(r'Rt ([0-9]+):([0-9]+)', r'Ruth \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Ruth ([0-9]+)', r'Ruth \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Ruth ([0-9]+)', r'Ruth \2:\1', i_str)
#     i_str = re.sub(r'Ruth chapter ([0-9]+)', r'Ruth \1', i_str)
#     return i_str
#
#
# def normalize_eng_09_1samuel(i_str):
#     i_str = re.sub(r'SAMUEL', r'Samuel', i_str)
#     i_str = re.sub(r'SAM\.', r'Samuel', i_str)
#     i_str = re.sub(r' SAM ', r' Samuel ', i_str)
#     i_str = re.sub(r'1 Sam\. ([0-9]+):([0-9]+)', r'1 Samuel \1:\2', i_str)
#     i_str = re.sub(r'1 Sam ([0-9]+):([0-9]+)', r'1 Samuel \1:\2', i_str)
#     i_str = re.sub(r'1Sa ([0-9]+):([0-9]+)', r'1 Samuel \1:\2', i_str)
#     i_str = re.sub(r'1 Sa ([0-9]+):([0-9]+)', r'1 Samuel \1:\2', i_str)
#     i_str = re.sub(r'I Sam\. ([0-9]+):([0-9]+)', r'1 Samuel \1:\2', i_str)
#     i_str = re.sub(r'I Sam ([0-9]+):([0-9]+)', r'1 Samuel \1:\2', i_str)
#     i_str = re.sub(r'I Samuel ([0-9]+):([0-9]+)', r'1 Samuel \1:\2', i_str)
#     i_str = re.sub(r'1Samuel ([0-9]+):([0-9]+)', r'1 Samuel \1:\2', i_str)
#     i_str = re.sub(r'1st Samuel ([0-9]+):([0-9]+)', r'1 Samuel \1:\2', i_str)
#     i_str = re.sub(r'1st samuel ([0-9]+):([0-9]+)', r'1 Samuel \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of 1 Samuel ([0-9]+)', r'1 Samuel \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) 1 Samuel ([0-9]+)', r'1 Samuel \2:\1', i_str)
#     i_str = re.sub(r'1 Samuel chapter ([0-9]+)', r'1 Samuel \1', i_str)
#     i_str = re.sub(r'1S ([0-9]+):([0-9]+)', r'1 Samuel \1:\2', i_str)
#     return i_str
#
#
# def normalize_eng_10_2samuel(i_str):
#     i_str = re.sub(r'SAMUEL', r'Samuel', i_str)
#     i_str = re.sub(r'SAM\.', r'Samuel', i_str)
#     i_str = re.sub(r' SAM ', r' Samuel ', i_str)
#     i_str = re.sub(r'2 Sam\. ([0-9]+):([0-9]+)', r'2 Samuel \1:\2', i_str)
#     i_str = re.sub(r'2 Sam ([0-9]+):([0-9]+)', r'2 Samuel \1:\2', i_str)
#     i_str = re.sub(r'2Sa ([0-9]+):([0-9]+)', r'2 Samuel \1:\2', i_str)
#     i_str = re.sub(r'2 Sa ([0-9]+):([0-9]+)', r'2 Samuel \1:\2', i_str)
#     i_str = re.sub(r'II Sam\. ([0-9]+):([0-9]+)', r'2 Samuel \1:\2', i_str)
#     i_str = re.sub(r'II Sam ([0-9]+):([0-9]+)', r'2 Samuel \1:\2', i_str)
#     i_str = re.sub(r'II Samuel ([0-9]+):([0-9]+)', r'2 Samuel \1:\2', i_str)
#     i_str = re.sub(r'2Samuel ([0-9]+):([0-9]+)', r'2 Samuel \1:\2', i_str)
#     i_str = re.sub(r'2nd Samuel ([0-9]+):([0-9]+)', r'2 Samuel \1:\2', i_str)
#     i_str = re.sub(r'2nd samuel ([0-9]+):([0-9]+)', r'2 Samuel \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of 2 Samuel ([0-9]+)', r'2 Samuel \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) 2 Samuel ([0-9]+)', r'2 Samuel \2:\1', i_str)
#     i_str = re.sub(r'2 Samuel chapter ([0-9]+)', r'2 Samuel \1', i_str)
#     i_str = re.sub(r'2S ([0-9]+):([0-9]+)', r'2 Samuel \1:\2', i_str)
#     return i_str
#
#
# def normalize_eng_11_1kings(i_str):
#     i_str = re.sub(r'KINGS', r'Kings', i_str)
#     i_str = re.sub(r'KI\.', r'Kings', i_str)
#     i_str = re.sub(r' KI ', r' Kings ', i_str)
#     i_str = re.sub(r'I Kings ([0-9]+):([0-9]+)', r'1 Kings \1:\2', i_str)
#     i_str = re.sub(r'1Ki ([0-9]+):([0-9]+)', r'1 Kings \1:\2', i_str)
#     i_str = re.sub(r'1 Ki ([0-9]+):([0-9]+)', r'1 Kings \1:\2', i_str)
#     i_str = re.sub(r'1K ([0-9]+):([0-9]+)', r'1 Kings \1:\2', i_str)
#     i_str = re.sub(r'1 Ki\. ([0-9]+):([0-9]+)', r'1 Kings \1:\2', i_str)
#     i_str = re.sub(r'1 Ki ([0-9]+):([0-9]+)', r'1 Kings \1:\2', i_str)
#     i_str = re.sub(r'1Ki ([0-9]+):([0-9]+)', r'1 Kings \1:\2', i_str)
#     i_str = re.sub(r'1Kings ([0-9]+):([0-9]+)', r'1 Kings \1:\2', i_str)
#     i_str = re.sub(r'1st Kings ([0-9]+):([0-9]+)', r'1 Kings \1:\2', i_str)
#     i_str = re.sub(r'1st kings ([0-9]+):([0-9]+)', r'1 Kings \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of 1 Kings ([0-9]+)', r'1 Kings \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) 1 Kings ([0-9]+)', r'1 Kings \2:\1', i_str)
#     i_str = re.sub(r'1 Kings chapter ([0-9]+)', r'1 Kings \1', i_str)
#     i_str = re.sub(r'1 Kgs ([0-9]+)', r'1 Kings \1', i_str)
#     return i_str
#
#
# def normalize_eng_12_2kings(i_str):
#     i_str = re.sub(r'KINGS', r'Kings', i_str)
#     i_str = re.sub(r'KI\.', r'Kings', i_str)
#     i_str = re.sub(r' KI ', r' Kings ', i_str)
#     i_str = re.sub(r'II Kings ([0-9]+):([0-9]+)', r'2 Kings \1:\2', i_str)
#     i_str = re.sub(r'2K ([0-9]+):([0-9]+)', r'2 Kings \1:\2', i_str)
#     i_str = re.sub(r'2 Ki\. ([0-9]+):([0-9]+)', r'2 Kings \1:\2', i_str)
#     i_str = re.sub(r'2 Ki ([0-9]+):([0-9]+)', r'2 Kings \1:\2', i_str)
#     i_str = re.sub(r'2Ki ([0-9]+):([0-9]+)', r'2 Kings \1:\2', i_str)
#     i_str = re.sub(r'2 Ki ([0-9]+):([0-9]+)', r'2 Kings \1:\2', i_str)
#     i_str = re.sub(r'2Kings ([0-9]+):([0-9]+)', r'2 Kings \1:\2', i_str)
#     i_str = re.sub(r'2nd Kings ([0-9]+):([0-9]+)', r'2 Kings \1:\2', i_str)
#     i_str = re.sub(r'2nd kings ([0-9]+):([0-9]+)', r'2 Kings \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of 2 Kings ([0-9]+)', r'2 Kings \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) 2 Kings ([0-9]+)', r'2 Kings \2:\1', i_str)
#     i_str = re.sub(r'2 Kings chapter ([0-9]+)', r'2 Kings \1', i_str)
#     i_str = re.sub(r'2 Kgs ([0-9]+)', r'2 Kings \1', i_str)
#     return i_str
#
#
# def normalize_eng_13_1chronicles(i_str):
#     i_str = re.sub(r'CHRONICLES', r'Chronicles', i_str)
#     i_str = re.sub(r'CHR\.', r'Chronicles', i_str)
#     i_str = re.sub(r' CHR ', r' Chronicles ', i_str)
#     i_str = re.sub(r'I Chronicles ([0-9]+):([0-9]+)', r'1 Chronicles \1:\2', i_str)
#     i_str = re.sub(r'1Chronicles ([0-9]+):([0-9]+)', r'1 Chronicles \1:\2', i_str)
#     i_str = re.sub(r'I Chron\. ([0-9]+):([0-9]+)', r'1 Chronicles \1:\2', i_str)
#     i_str = re.sub(r'1 Chron\. ([0-9]+):([0-9]+)', r'1 Chronicles \1:\2', i_str)
#     i_str = re.sub(r'I Chron ([0-9]+):([0-9]+)', r'1 Chronicles \1:\2', i_str)
#     i_str = re.sub(r'1 Chron ([0-9]+):([0-9]+)', r'1 Chronicles \1:\2', i_str)
#     i_str = re.sub(r'1Ch ([0-9]+):([0-9]+)', r'1 Chronicles \1:\2', i_str)
#     i_str = re.sub(r'1 Ch ([0-9]+):([0-9]+)', r'1 Chronicles \1:\2', i_str)
#     i_str = re.sub(r'1st chronicles ([0-9]+):([0-9]+)', r'1 Chronicles \1:\2', i_str)
#     i_str = re.sub(r'1st Chronicles ([0-9]+):([0-9]+)', r'1 Chronicles \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of 1 Chronicles ([0-9]+)', r'1 Chronicles \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) 1 Chronicles ([0-9]+)', r'1 Chronicles \2:\1', i_str)
#     i_str = re.sub(r'1 Chronicles chapter ([0-9]+)', r'1 Chronicles \1', i_str)
#     return i_str
#
#
# def normalize_eng_14_2chronicles(i_str):
#     i_str = re.sub(r'CHRONICLES', r'Chronicles', i_str)
#     i_str = re.sub(r'CHR\.', r'Chronicles', i_str)
#     i_str = re.sub(r' CHR ', r' Chronicles ', i_str)
#     i_str = re.sub(r'II Chronicles ([0-9]+):([0-9]+)', r'2 Chronicles \1:\2', i_str)
#     i_str = re.sub(r'2Chronicles ([0-9]+):([0-9]+)', r'2 Chronicles \1:\2', i_str)
#     i_str = re.sub(r'II Chron\. ([0-9]+):([0-9]+)', r'2 Chronicles \1:\2', i_str)
#     i_str = re.sub(r'2 Chron\. ([0-9]+):([0-9]+)', r'2 Chronicles \1:\2', i_str)
#     i_str = re.sub(r'II Chron ([0-9]+):([0-9]+)', r'2 Chronicles \1:\2', i_str)
#     i_str = re.sub(r'2 Chron ([0-9]+):([0-9]+)', r'2 Chronicles \1:\2', i_str)
#     i_str = re.sub(r'2Ch ([0-9]+):([0-9]+)', r'2 Chronicles \1:\2', i_str)
#     i_str = re.sub(r'2 Ch ([0-9]+):([0-9]+)', r'2 Chronicles \1:\2', i_str)
#     i_str = re.sub(r'2nd chronicles ([0-9]+):([0-9]+)', r'2 Chronicles \1:\2', i_str)
#     i_str = re.sub(r'2nd Chronicles ([0-9]+):([0-9]+)', r'2 Chronicles \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of 2 Chronicles ([0-9]+)', r'2 Chronicles \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) 2 Chronicles ([0-9]+)', r'2 Chronicles \2:\1', i_str)
#     i_str = re.sub(r'2 Chronicles chapter ([0-9]+)', r'2 Chronicles \1', i_str)
#     return i_str
#
#
# def normalize_eng_15_ezra(i_str):
#     i_str = re.sub(r'EZRA', r'Ezra', i_str)
#     i_str = re.sub(r'EZR\.', r'Ezra', i_str)
#     i_str = re.sub(r' EZR ', r' Ezra ', i_str)
#     i_str = re.sub(r'Ez\. ([0-9]+):([0-9]+)', r'Ezra \1:\2', i_str)
#     i_str = re.sub(r'Ezr\. ([0-9]+):([0-9]+)', r'Ezra \1:\2', i_str)
#     i_str = re.sub(r'Ezr ([0-9]+):([0-9]+)', r'Ezra \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Ezra ([0-9]+)', r'Ezra \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Ezra ([0-9]+)', r'Ezra \2:\1', i_str)
#     i_str = re.sub(r'Ezra chapter ([0-9]+)', r'Ezra \1', i_str)
#     return i_str
#
#
# def normalize_eng_16_nehemiah(i_str):
#     i_str = re.sub(r'NEHEMIAH', r'Nehemiah', i_str)
#     i_str = re.sub(r'NEH.', r'Nehemiah', i_str)
#     i_str = re.sub(r' NEH ', r' Nehemiah ', i_str)
#     i_str = re.sub(r'Neh\. ([0-9]+):([0-9]+)', r'Nehemiah \1:\2', i_str)
#     i_str = re.sub(r'Neh ([0-9]+):([0-9]+)', r'Nehemiah \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Nehemiah ([0-9]+)', r'Nehemiah \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Nehemiah ([0-9]+)', r'Nehemiah \2:\1', i_str)
#     i_str = re.sub(r'Nehemiah chapter ([0-9]+)', r'Nehemiah \1', i_str)
#     return i_str
#
#
# def normalize_eng_17_esther(i_str):
#     i_str = re.sub(r'ESTHER', r'Esther', i_str)
#     i_str = re.sub(r'EST.', r'Esther', i_str)
#     i_str = re.sub(r' EST ', r' Esther ', i_str)
#     i_str = re.sub(r'Est\. ([0-9]+):([0-9]+)', r'Esther \1:\2', i_str)
#     i_str = re.sub(r'Est ([0-9]+):([0-9]+)', r'Esther \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Esther ([0-9]+)', r'Esther \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Esther ([0-9]+)', r'Esther \2:\1', i_str)
#     i_str = re.sub(r'Esther chapter ([0-9]+)', r'Esther \1', i_str)
#     return i_str
#
#
# def normalize_eng_18_job(i_str):
#     i_str = re.sub(r'JOB', r'Job', i_str)
#     i_str = re.sub(r'JB.', r'Job', i_str)
#     i_str = re.sub(r' JB ', r' Job ', i_str)
#     i_str = re.sub(r'Jb\. ([0-9]+):([0-9]+)', r'Job \1:\2', i_str)
#     i_str = re.sub(r'Jb ([0-9]+):([0-9]+)', r'Job \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Job ([0-9]+)', r'Job \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Job ([0-9]+)', r'Job \2:\1', i_str)
#     i_str = re.sub(r'Job chapter ([0-9]+)', r'Job \1', i_str)
#     return i_str
#
#
# def normalize_eng_19_psalms(i_str):
#     i_str = re.sub(r'PSALMS', r'Psalms', i_str)
#     i_str = re.sub(r'PS\.', r'Psalms', i_str)
#     i_str = re.sub(r'Ps\.', r'Psalms', i_str)
#     i_str = re.sub(r' PS ', r' Psalms ', i_str)
#     i_str = re.sub(r'Psalm ([0-9]+)', r'Psalms \1', i_str)
#     i_str = re.sub(r'Psa ([0-9]+):([0-9]+)', r'Psalms \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Psalms ([0-9]+)', r'Psalms \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Psalms ([0-9]+)', r'Psalms \2:\1', i_str)
#     i_str = re.sub(r'Psalms ([0-9]+) ([0-9]+)', r'Psalms \1:\2', i_str)
#     i_str = re.sub(r'Psalms chapter ([0-9]+)', r'Psalms \1', i_str)
#     return i_str
#
#
# def normalize_eng_20_proverbs(i_str):
#     i_str = re.sub(r'proverbs', r'Proverbs', i_str)
#     i_str = re.sub(r'PROVERBS', r'Proverbs', i_str)
#     i_str = re.sub(r'PROV\.', r'Proverbs', i_str)
#     i_str = re.sub(r' PROV ', r' Proverbs ', i_str)
#     i_str = re.sub(r'Prov ([0-9]+):([0-9]+)', r'Proverbs \1:\2', i_str)
#     i_str = re.sub(r'PROVERBS', r'Proverbs', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Proverbs ([0-9]+)', r'Proverbs \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Proverbs ([0-9]+)', r'Proverbs \2:\1', i_str)
#     i_str = re.sub(r'Proverbs chapter ([0-9]+)', r'Proverbs \1', i_str)
#     return i_str
#
#
# def normalize_eng_21_ecclesiastes(i_str):
#     i_str = re.sub(r'ECCLESIASTES', r'Ecclesiastes', i_str)
#     i_str = re.sub(r'ECCL\.', r'Ecclesiastes', i_str)
#     i_str = re.sub(r' ECCL ', r' Ecclesiastes ', i_str)
#     i_str = re.sub(r'Eccl\. ([0-9]+):([0-9]+)', r'Ecclesiastes \1:\2', i_str)
#     i_str = re.sub(r'Eccl ([0-9]+):([0-9]+)', r'Ecclesiastes \1:\2', i_str)
#     i_str = re.sub(r'Ecc ([0-9]+):([0-9]+)', r'Ecclesiastes \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Ecclesiastes ([0-9]+)', r'Ecclesiastes \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Ecclesiastes ([0-9]+)', r'Ecclesiastes \2:\1', i_str)
#     i_str = re.sub(r'Ecclesiastes chapter ([0-9]+)', r'Ecclesiastes \1', i_str)
#     return i_str
#
#
# def normalize_eng_22_songofsolomon(i_str):
#     i_str = re.sub(r'SONG OF SOLOMON', r'Song of Solomon', i_str)
#     i_str = re.sub(r'SON\.', r'Song of Solomon', i_str)
#     i_str = re.sub(r' SON ', r' Song of Solomon ', i_str)
#     i_str = re.sub(r'Song of Songs ([0-9]+):([0-9]+)', r'Song of Solomon \1:\2', i_str)
#     i_str = re.sub(r'Son ([0-9]+):([0-9]+)', r'Song of Solomon \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Song of Solomon ([0-9]+)', r'Song of Solomon \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Song of Solomon ([0-9]+)', r'Song of Solomon \2:\1', i_str)
#     i_str = re.sub(r'Song of Solomon chapter ([0-9]+)', r'Song of Solomon \1', i_str)
#     return i_str
#
#
# def normalize_eng_23_isaiah(i_str):
#     i_str = re.sub(r'ISAIAH', r'Isaiah', i_str)
#     i_str = re.sub(r'ISA\.', r'Isaiah', i_str)
#     i_str = re.sub(r' ISA ', r' Isaiah ', i_str)
#     i_str = re.sub(r'Isa\. ([0-9]+):([0-9]+)', r'Isaiah \1:\2', i_str)
#     i_str = re.sub(r'Isa ([0-9]+):([0-9]+)', r'Isaiah \1:\2', i_str)
#     i_str = re.sub(r'Is ([0-9]+):([0-9]+)', r'Isaiah \1:\2', i_str)       # 2019-11-14
#     i_str = re.sub(r'Is\. ([0-9]+):([0-9]+)', r'Isaiah \1:\2', i_str)       # 2019-11-14
#     i_str = re.sub(r'verse ([0-9]+) of Isaiah ([0-9]+)', r'Isaiah \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Isaiah ([0-9]+)', r'Isaiah \2:\1', i_str)
#     i_str = re.sub(r'Isaiah chapter ([0-9]+)', r'Isaiah \1', i_str)
#     return i_str
#
#
# def normalize_eng_24_jeremiah(i_str):
#     i_str = re.sub(r'jeremiah', r'Jeremiah', i_str)
#     i_str = re.sub(r'JEREMIAH', r'Jeremiah', i_str)
#     i_str = re.sub(r'JER\.', r'Jeremiah', i_str)
#     i_str = re.sub(r' JER ', r' Jeremiah ', i_str)
#     i_str = re.sub(r'Jer\. ([0-9]+):([0-9]+)', r'Jeremiah \1:\2', i_str)
#     i_str = re.sub(r'Jer ([0-9]+):([0-9]+)', r'Jeremiah \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Jeremiah ([0-9]+)', r'Jeremiah \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Jeremiah ([0-9]+)', r'Jeremiah \2:\1', i_str)
#     i_str = re.sub(r'Jeremiah chapter ([0-9]+)', r'Jeremiah \1', i_str)
#     return i_str
#
#
# def normalize_eng_25_lamentations(i_str):
#     i_str = re.sub(r'LAMENTATIONS', r'Lamentations', i_str)
#     i_str = re.sub(r'LAM\.', r'Lamentations', i_str)
#     i_str = re.sub(r' LAM ', r'Lamentations', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Lamentations ([0-9]+)', r'Lamentations \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Lamentations ([0-9]+)', r'Lamentations \2:\1', i_str)
#     i_str = re.sub(r'Lamentations chapter ([0-9]+)', r'Lamentations \1', i_str)
#     return i_str
#
#
# def normalize_eng_26_ezekiel(i_str):
#     i_str = re.sub(r'ezekiel', r'Ezekiel', i_str)
#     i_str = re.sub(r'EZEKIEL', r'Ezekiel', i_str)
#     i_str = re.sub(r'EZE\.', r'Ezekiel', i_str)
#     i_str = re.sub(r' EZE ', r'Ezekiel', i_str)
#     i_str = re.sub(r'Eze\. ([0-9]+):([0-9]+)', r'Ezekiel \1:\2', i_str)
#     i_str = re.sub(r'Eze ([0-9]+):([0-9]+)', r'Ezekiel \1:\2', i_str)
#     i_str = re.sub(r'Ezek ([0-9]+):([0-9]+)', r'Ezekiel \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Ezekiel ([0-9]+)', r'Ezekiel \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Ezekiel ([0-9]+)', r'Ezekiel \2:\1', i_str)
#     i_str = re.sub(r'Ezekiel chapter ([0-9]+)', r'Ezekiel \1', i_str)
#     return i_str
#
#
# def normalize_eng_27_daniel(i_str):
#     i_str = re.sub(r'daniel', r'Daniel', i_str)
#     i_str = re.sub(r'DANIEL', r'Daniel', i_str)
#     i_str = re.sub(r'DAN\.', r'Daniel', i_str)
#     i_str = re.sub(r' DAN ', r' Daniel ', i_str)
#     i_str = re.sub(r'Dan\. ([0-9]+):([0-9]+)', r'Daniel \1:\2', i_str)
#     i_str = re.sub(r'Dn\. ([0-9]+):([0-9]+)', r'Daniel \1:\2', i_str)
#     i_str = re.sub(r'Dn ([0-9]+):([0-9]+)', r'Daniel \1:\2', i_str)
#     i_str = re.sub(r'Dan ([0-9]+):([0-9]+)', r'Daniel \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Daniel ([0-9]+)', r'Daniel \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Daniel ([0-9]+)', r'Daniel \2:\1', i_str)
#     i_str = re.sub(r'Daniel chapter ([0-9]+)', r'Daniel \1', i_str)
#     i_str = re.sub(r'([0-9]+), ([0-9]+) at the Daniel ([0-9]+)', r'Daniel \3:\1, \2', i_str)
#     return i_str
#
#
# def normalize_eng_28_hosea(i_str):
#     i_str = re.sub(r'HOSEA', r'Hosea', i_str)
#     i_str = re.sub(r'HOS\.', r'Hosea', i_str)
#     i_str = re.sub(r' HOS ', r'Hosea', i_str)
#     i_str = re.sub(r'Hos\. ([0-9]+):([0-9]+)', r'Hosea \1:\2', i_str)
#     i_str = re.sub(r'Hos ([0-9]+):([0-9]+)', r'Hosea \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Hosea ([0-9]+)', r'Hosea \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Hosea ([0-9]+)', r'Hosea \2:\1', i_str)
#     i_str = re.sub(r'Hosea chapter ([0-9]+)', r'Hosea \1', i_str)
#     return i_str
#
#
# def normalize_eng_29_joel(i_str):
#     i_str = re.sub(r'JOEL', r'Joel', i_str)
#     i_str = re.sub(r'JO\.', r'Joel', i_str)
#     i_str = re.sub(r' JO ', r' Joel ', i_str)
#     i_str = re.sub(r'Joe\. ([0-9]+):([0-9]+)', r'Joel \1:\2', i_str)
#     i_str = re.sub(r'Joe ([0-9]+):([0-9]+)', r'Joel \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Joel ([0-9]+)', r'Joel \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Joel ([0-9]+)', r'Joel \2:\1', i_str)
#     i_str = re.sub(r'Joel chapter ([0-9]+)', r'Joel \1', i_str)
#     return i_str
#
#
# def normalize_eng_30_amos(i_str):
#     i_str = re.sub(r'AMOS', r'Amos', i_str)
#     i_str = re.sub(r'AMO\.', r'Amos', i_str)
#     i_str = re.sub(r' AMO ', r' Amos ', i_str)
#     i_str = re.sub(r'Am\. ([0-9]+):([0-9]+)', r'Amos \1:\2', i_str)
#     i_str = re.sub(r' Am ([0-9]+):([0-9]+)', r' Amos \1:\2', i_str)
#     i_str = re.sub(r'Amo ([0-9]+):([0-9]+)', r'Amos \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Amos ([0-9]+)', r'Amos \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Amos ([0-9]+)', r'Amos \2:\1', i_str)
#     i_str = re.sub(r'Amos chapter ([0-9]+)', r'Amos \1', i_str)
#     return i_str
#
#
# def normalize_eng_31_obadiah(i_str):
#     i_str = re.sub(r'OBADIAH', r'Obadiah', i_str)
#     i_str = re.sub(r'OB\.', r'Obadiah', i_str)
#     i_str = re.sub(r' OB ', r' Obadiah ', i_str)
#     i_str = re.sub(r'Ob\. ([0-9]+):([0-9]+)', r'Obadiah \1:\2', i_str)
#     i_str = re.sub(r'Oba ([0-9]+):([0-9]+)', r'Obadiah \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Obadiah ([0-9]+)', r'Obadiah \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Obadiah ([0-9]+)', r'Obadiah \2:\1', i_str)
#     i_str = re.sub(r'Obadiah chapter ([0-9]+)', r'Obadiah \1', i_str)
#     return i_str
#
#
# def normalize_eng_32_jonah(i_str):
#     i_str = re.sub(r'JONAH', r'Jonah', i_str)
#     i_str = re.sub(r'JON\.', r'Jonah', i_str)
#     i_str = re.sub(r' JON ', r' Jonah ', i_str)
#     i_str = re.sub(r'Jon\. ([0-9]+):([0-9]+)', r'Jonah \1:\2', i_str)
#     i_str = re.sub(r'Jon ([0-9]+):([0-9]+)', r'Jonah \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Jonah ([0-9]+)', r'Jonah \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Jonah ([0-9]+)', r'Jonah \2:\1', i_str)
#     i_str = re.sub(r'Jonah chapter ([0-9]+)', r'Jonah \1', i_str)
#     return i_str
#
#
# def normalize_eng_33_micah(i_str):
#     i_str = re.sub(r'MICAH', r'Micah', i_str)
#     i_str = re.sub(r'MIC\.', r'Micah', i_str)
#     i_str = re.sub(r' MIC ', r' Micah ', i_str)
#     i_str = re.sub(r'Mich\. ([0-9]+):([0-9]+)', r'Micah \1:\2', i_str)
#     i_str = re.sub(r'Mich ([0-9]+):([0-9]+)', r'Micah \1:\2', i_str)
#     i_str = re.sub(r'Mic\. ([0-9]+):([0-9]+)', r'Micah \1:\2', i_str)
#     i_str = re.sub(r'Mic ([0-9]+):([0-9]+)', r'Micah \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Micah ([0-9]+)', r'Micah \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Micah ([0-9]+)', r'Micah \2:\1', i_str)
#     i_str = re.sub(r'Micah chapter ([0-9]+)', r'Micah \1', i_str)
#     return i_str
#
#
# def normalize_eng_34_nahum(i_str):
#     i_str = re.sub(r'NAHUM', r'Nahum', i_str)
#     i_str = re.sub(r'NAH\.', r'Nahum', i_str)
#     i_str = re.sub(r' NAH ', r' Nahum ', i_str)
#     i_str = re.sub(r'Nah\. ([0-9]+):([0-9]+)', r'Nahum \1:\2', i_str)
#     i_str = re.sub(r'Nah ([0-9]+):([0-9]+)', r'Nahum \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Nahum ([0-9]+)', r'Nahum \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Nahum ([0-9]+)', r'Nahum \2:\1', i_str)
#     i_str = re.sub(r'Nahum chapter ([0-9]+)', r'Nahum \1', i_str)
#     return i_str
#
#
# def normalize_eng_35_habakkuk(i_str):
#     i_str = re.sub(r'HABAKKUK', r'Habakkuk', i_str)
#     i_str = re.sub(r'HAB\.', r'Habakkuk', i_str)
#     i_str = re.sub(r' HAB ', r' Habakkuk ', i_str)
#     i_str = re.sub(r'Hab\. ([0-9]+):([0-9]+)', r'Habakkuk \1:\2', i_str)
#     i_str = re.sub(r'Hab ([0-9]+):([0-9]+)', r'Habakkuk \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Habakkuk ([0-9]+)', r'Habakkuk \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Habakkuk ([0-9]+)', r'Habakkuk \2:\1', i_str)
#     i_str = re.sub(r'Habakkuk chapter ([0-9]+)', r'Habakkuk \1', i_str)
#     return i_str
#
#
# def normalize_eng_36_zephaniah(i_str):
#     i_str = re.sub(r'ZEPHANIAH', r'Zephaniah', i_str)
#     i_str = re.sub(r'ZEP\.', r'Zephaniah', i_str)
#     i_str = re.sub(r' ZEP ', r' Zephaniah ', i_str)
#     i_str = re.sub(r'Zep\. ([0-9]+):([0-9]+)', r'Zephaniah \1:\2', i_str)
#     i_str = re.sub(r'Zep ([0-9]+):([0-9]+)', r'Zephaniah \1:\2', i_str)
#     i_str = re.sub(r'CAP', r'low', i_str)
#     i_str = re.sub(r'CAP', r'low', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Zephaniah ([0-9]+)', r'Zephaniah \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Zephaniah ([0-9]+)', r'Zephaniah \2:\1', i_str)
#     i_str = re.sub(r'Zephaniah chapter ([0-9]+)', r'Zephaniah \1', i_str)
#     return i_str
#
#
# def normalize_eng_37_haggai(i_str):
#     i_str = re.sub(r'HAGGAI', r'Haggai', i_str)
#     i_str = re.sub(r'HAG\.', r'Haggai', i_str)
#     i_str = re.sub(r' HAG ', r' Haggai ', i_str)
#     i_str = re.sub(r'Hagg\. ([0-9]+):([0-9]+)', r'Haggai \1:\2', i_str)
#     i_str = re.sub(r'Hag ([0-9]+):([0-9]+)', r'Haggai \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Haggai ([0-9]+)', r'Haggai \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Haggai ([0-9]+)', r'Haggai \2:\1', i_str)
#     i_str = re.sub(r'Haggai chapter ([0-9]+)', r'Haggai \1', i_str)
#     return i_str
#
#
# def normalize_eng_38_zechariah(i_str):
#     i_str = re.sub(r'ZECHARIAH', r'Zechariah', i_str)
#     i_str = re.sub(r'ZEH\.', r'Zechariah', i_str)
#     i_str = re.sub(r' ZEH ', r' Zechariah ', i_str)
#     i_str = re.sub(r'Zech\. ([0-9]+):([0-9]+)', r'Zechariah \1:\2', i_str)
#     i_str = re.sub(r'Zech ([0-9]+):([0-9]+)', r'Zechariah \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Zechariah ([0-9]+)', r'Zechariah \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Zechariah ([0-9]+)', r'Zechariah \2:\1', i_str)
#     i_str = re.sub(r'Zechariah chapter ([0-9]+)', r'Zechariah \1', i_str)
#     return i_str
#
#
# def normalize_eng_39_malachi(i_str):
#     i_str = re.sub(r'MALACHI', r'Malachi', i_str)
#     i_str = re.sub(r'MAL\.', r'Malachi', i_str)
#     i_str = re.sub(r' MAL ', r' Malachi ', i_str)
#     i_str = re.sub(r'Mal\. ([0-9]+):([0-9]+)', r'Malachi \1:\2', i_str)
#     i_str = re.sub(r'Mal ([0-9]+):([0-9]+)', r'Malachi \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Malachi ([0-9]+)', r'Malachi \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Malachi ([0-9]+)', r'Malachi \2:\1', i_str)
#     i_str = re.sub(r'Malachi chapter ([0-9]+)', r'Malachi \1', i_str)
#     return i_str
#
#
# def normalize_eng_40_matthew(i_str):
#
#     i_str = re.sub(r'MATTHEW', r'Matthew', i_str)
#     i_str = re.sub(r'MATT\.', r'Matthew', i_str)
#     i_str = re.sub(r' MATT ', r' Matthew ', i_str)
#     i_str = re.sub(r'Matt\. ([0-9]+):([0-9]+)', r'Matthew \1:\2', i_str)
#     i_str = re.sub(r'Matt ([0-9]+):([0-9]+)', r'Matthew \1:\2', i_str)
#     i_str = re.sub(r'Mat ([0-9]+):([0-9]+)', r'Matthew \1:\2', i_str)
#     i_str = re.sub(r'Mt ([0-9]+):([0-9]+)', r'Matthew \1:\2', i_str)
#     i_str = re.sub(r'MATTHEW ([0-9]+):([0-9]+)', r'Matthew \1:\2', i_str)
#     i_str = re.sub(r'Matthews ([0-9]+):([0-9]+)', r'Matthew \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Matthew ([0-9]+)', r'Matthew \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Matthew ([0-9]+)', r'Matthew \2:\1', i_str)
#     i_str = re.sub(r'Matthew chapter ([0-9]+)', r'Matthew \1', i_str)
#     return i_str
#
#
# def normalize_eng_41_mark(i_str):
#     i_str = re.sub(r'MARK', r'Mark', i_str)
#     i_str = re.sub(r'MK\.', r'Mark', i_str)
#     i_str = re.sub(r' MK ', r' Mark ', i_str)
#     i_str = re.sub(r'Mark\. ([0-9]+):([0-9]+)', r'Mark \1:\2', i_str)
#     i_str = re.sub(r'Mar ([0-9]+):([0-9]+)', r'Mark \1:\2', i_str)
#     i_str = re.sub(r'MARK ([0-9]+):([0-9]+)', r'Mark \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Mark ([0-9]+)', r'Mark \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Mark ([0-9]+)', r'Mark \2:\1', i_str)
#     i_str = re.sub(r'Mark chapter ([0-9]+)', r'Mark \1', i_str)
#     return i_str
#
#
# def normalize_eng_42_luke(i_str):
#     i_str = re.sub(r'Luke s', r'Luke', i_str)
#     i_str = re.sub(r'LUKE', r'Luke', i_str)
#     i_str = re.sub(r'LK\.', r'Luke', i_str)
#     i_str = re.sub(r' LK ', r' Luke ', i_str)
#     i_str = re.sub(r'Luk\. ([0-9]+):([0-9]+)', r'Luke \1:\2', i_str)
#     i_str = re.sub(r'Lk ([0-9]+):([0-9]+)', r'Luke \1:\2', i_str)
#     i_str = re.sub(r'Luk ([0-9]+):([0-9]+)', r'Luke \1:\2', i_str)
#     i_str = re.sub(r'LUKE ([0-9]+):([0-9]+)', r'Luke \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Luke ([0-9]+)', r'Luke \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Luke ([0-9]+)', r'Luke \2:\1', i_str)
#     i_str = re.sub(r'Luke chapter ([0-9]+)', r'Luke \1', i_str)
#     return i_str
#
#
# def normalize_eng_43_john(i_str):
#     i_str = re.sub(r'JOHN', r'John', i_str)
#     i_str = re.sub(r'JH\.', r'John', i_str)
#     i_str = re.sub(r' JH ', r' John ', i_str)
#     i_str = re.sub(r'Jn\. ([0-9]+):([0-9]+)', r'John \1:\2', i_str)
#     i_str = re.sub(r'Joh ([0-9]+):([0-9]+)', r'John \1:\2', i_str)
#     i_str = re.sub(r'JOHN ([0-9]+):([0-9]+)', r'John \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of John ([0-9]+)', r'John \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) John ([0-9]+)', r'John \2:\1', i_str)
#     i_str = re.sub(r'John chapter ([0-9]+)', r'John \1', i_str)
#     return i_str
#
#
# def normalize_eng_44_acts(i_str):
#     i_str = re.sub(r'ACTS', r'Acts', i_str)
#     i_str = re.sub(r'ACT\.', r'Acts', i_str)
#     i_str = re.sub(r' ACT ', r' Acts ', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Acts ([0-9]+)', r'Acts \2:\1', i_str)
#     i_str = re.sub(r'Acts chapter ([0-9]+)', r'Acts \1', i_str)
#     return i_str
#
#
# def normalize_eng_45_romans(i_str):
#     i_str = re.sub(r'ROMANS', r'Romans', i_str)
#     i_str = re.sub(r'ROM\.', r'Romans', i_str)
#     i_str = re.sub(r' ROM ', r' Romans ', i_str)
#     i_str = re.sub(r'Rom\. ([0-9]+):([0-9]+)', r'Romans \1:\2', i_str)
#     i_str = re.sub(r'Rom ([0-9]+):([0-9]+)', r'Romans \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Romans ([0-9]+)', r'Romans \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Romans ([0-9]+)', r'Romans \2:\1', i_str)
#     i_str = re.sub(r'Romans chapter ([0-9]+)', r'Romans \1', i_str)
#     i_str = re.sub(r'Ro ([0-9]+):([0-9]+)', r'Romans \1:\2', i_str)
#     return i_str
#
#
# def normalize_eng_46_1cor(i_str):
#     i_str = re.sub(r'CORINTHIANS', r'Corinthians', i_str)
#     i_str = re.sub(r'COR\.', r'Corinthians', i_str)
#     i_str = re.sub(r' COR ', r' Corinthians ', i_str)
#     i_str = re.sub(r'I Corinthians ([0-9]+):([0-9]+)', r'1 Corinthians \1:\2', i_str)
#     i_str = re.sub(r'ICorinthians ([0-9]+):([0-9]+)', r'1 Corinthians \1:\2', i_str)
#     i_str = re.sub(r'1Corinthians ([0-9]+):([0-9]+)', r'1 Corinthians \1:\2', i_str)
#     i_str = re.sub(r'1Cor ([0-9]+):([0-9]+)', r'1 Corinthians \1:\2', i_str)
#     i_str = re.sub(r'1 Cor ([0-9]+):([0-9]+)', r'1 Corinthians \1:\2', i_str)
#     i_str = re.sub(r'1Co ([0-9]+):([0-9]+)', r'1 Corinthians \1:\2', i_str)
#     i_str = re.sub(r'1 Co ([0-9]+):([0-9]+)', r'1 Corinthians \1:\2', i_str)
#     i_str = re.sub(r'1st Corinthians ([0-9]+):([0-9]+)', r'1 Corinthians \1:\2', i_str)
#     i_str = re.sub(r'1st corinthians ([0-9]+):([0-9]+)', r'1 Corinthians \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of 1 Corinthians ([0-9]+)', r'1 Corinthians \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) 1 Corinthians ([0-9]+)', r'1 Corinthians \2:\1', i_str)
#     i_str = re.sub(r'1 Corinthians chapter ([0-9]+)', r'1 Corinthians \1', i_str)
#     i_str = re.sub(r'First Corinthians ([0-9]+):([0-9]+)', r'1 Corinthians \1:\2', i_str)
#     return i_str
#
#
# def normalize_eng_47_2cor(i_str):
#     i_str = re.sub(r'CORINTHIANS', r'Corinthians', i_str)
#     i_str = re.sub(r'COR\.', r'Corinthians', i_str)
#     i_str = re.sub(r' COR ', r' Corinthians ', i_str)
#     i_str = re.sub(r'II Corinthians ([0-9]+):([0-9]+)', r'2 Corinthians \1:\2', i_str)
#     i_str = re.sub(r'IICorinthians ([0-9]+):([0-9]+)', r'2 Corinthians \1:\2', i_str)
#     i_str = re.sub(r'2Corinthians ([0-9]+):([0-9]+)', r'2 Corinthians \1:\2', i_str)
#     i_str = re.sub(r'2Cor ([0-9]+):([0-9]+)', r'2 Corinthians \1:\2', i_str)
#     i_str = re.sub(r'2 Cor ([0-9]+):([0-9]+)', r'2 Corinthians \1:\2', i_str)
#     i_str = re.sub(r'2Co ([0-9]+):([0-9]+)', r'2 Corinthians \1:\2', i_str)
#     i_str = re.sub(r'2 Co ([0-9]+):([0-9]+)', r'2 Corinthians \1:\2', i_str)
#     i_str = re.sub(r'2nd Corinthians ([0-9]+):([0-9]+)', r'2 Corinthians \1:\2', i_str)
#     i_str = re.sub(r'2nd corinthians ([0-9]+):([0-9]+)', r'2 Corinthians \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of 2 Corinthians ([0-9]+)', r'2 Corinthians \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) 2 Corinthians ([0-9]+)', r'2 Corinthians \2:\1', i_str)
#     i_str = re.sub(r'2 Corinthians chapter ([0-9]+)', r'2 Corinthians \1', i_str)
#     i_str = re.sub(r'Second Corinthians ([0-9]+):([0-9]+)', r'2 Corinthians \1:\2', i_str)
#     return i_str
#
#
# def normalize_eng_48_galatians(i_str):
#     i_str = re.sub(r'GALATIANS', r'Galatians', i_str)
#     i_str = re.sub(r'GAL\.', r'Galatians', i_str)
#     i_str = re.sub(r' GAL ', r' Galatians ', i_str)
#     i_str = re.sub(r'Gal\. ([0-9]+):([0-9]+)', r'Galatians \1:\2', i_str)
#     i_str = re.sub(r'Gal ([0-9]+):([0-9]+)', r'Galatians \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Galatians ([0-9]+)', r'Galatians \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Galatians ([0-9]+)', r'Galatians \2:\1', i_str)
#     i_str = re.sub(r'Galatians chapter ([0-9]+)', r'Galatians \1', i_str)
#     return i_str
#
#
# def normalize_eng_49_ephesians(i_str):
#     i_str = re.sub(r'EPHESIANS', r'Ephesians', i_str)
#     i_str = re.sub(r'EPH\.', r'Ephesians', i_str)
#     i_str = re.sub(r' EPH ', r' Ephesians ', i_str)
#     i_str = re.sub(r'Eph\. ([0-9]+):([0-9]+)', r'Ephesians \1:\2', i_str)
#     i_str = re.sub(r'Eph ([0-9]+):([0-9]+)', r'Ephesians \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Ephesians ([0-9]+)', r'Ephesians \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Ephesians ([0-9]+)', r'Ephesians \2:\1', i_str)
#     i_str = re.sub(r'Ephesians chapter ([0-9]+)', r'Ephesians \1', i_str)
#     return i_str
#
#
# def normalize_eng_50_philippians(i_str):
#     i_str = re.sub(r'PHILIPPIANS', r'Philippians', i_str)
#     i_str = re.sub(r'PHIL\.', r'Philippians', i_str)
#     i_str = re.sub(r' PHIL ', r' Philippians ', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Philippians ([0-9]+)', r'Philippians \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Philippians ([0-9]+)', r'Philippians \2:\1', i_str)
#     i_str = re.sub(r'Philippians chapter ([0-9]+)', r'Philippians \1', i_str)
#     i_str = re.sub(r'Philip ([0-9]+):([0-9]+)', r'Philippians \1:\2', i_str)
#     return i_str
#
#
# def normalize_eng_51_colossians(i_str):
#     i_str = re.sub(r'COLOSSIANS', r'Colossians', i_str)
#     i_str = re.sub(r'COL ([0-9]+):', r'Colossians \1:', i_str)
#     # i_str = re.sub(r' COL ', r' Colossians ', i_str)  # Will be confused with EGW Book COL
#     i_str = re.sub(r'verse ([0-9]+) of Colossians ([0-9]+)', r'Colossians \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Colossians ([0-9]+)', r'Colossians \2:\1', i_str)
#     i_str = re.sub(r'Colossians chapter ([0-9]+)', r'Colossians \1', i_str)
#     return i_str
#
#
# def normalize_eng_52_1thess(i_str):
#     i_str = re.sub(r'THESSALONIANS', r'Thessalonians', i_str)
#     i_str = re.sub(r'THESS\.', r'Thessalonians', i_str)
#     i_str = re.sub(r'THESS', r'Thessalonians', i_str)
#     i_str = re.sub(r' THESS ', r' Thessalonians ', i_str)
#     i_str = re.sub(r'I Thessalonians ([0-9]+):([0-9]+)', r'1 Thessalonians \1:\2', i_str)
#     i_str = re.sub(r'1Th ([0-9]+):([0-9]+)', r'1 Thessalonians \1:\2', i_str)
#     i_str = re.sub(r'1 Th ([0-9]+):([0-9]+)', r'1 Thessalonians \1:\2', i_str)
#     i_str = re.sub(r'1 Thess\. ([0-9]+):([0-9]+)', r'1 Thessalonians \1:\2', i_str)
#     i_str = re.sub(r'1 Thess ([0-9]+):([0-9]+)', r'1 Thessalonians \1:\2', i_str)
#     i_str = re.sub(r'1Thessalonians ([0-9]+):([0-9]+)', r'1 Thessalonians \1:\2', i_str)
#     i_str = re.sub(r'1st Thessalonians ([0-9]+):([0-9]+)', r'1 Thessalonians \1:\2', i_str)
#     i_str = re.sub(r'1st thessalonians ([0-9]+):([0-9]+)', r'1 Thessalonians \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of 1 Thessalonians ([0-9]+)', r'1 Thessalonians \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) 1 Thessalonians ([0-9]+)', r'1 Thessalonians \2:\1', i_str)
#     i_str = re.sub(r'1 Thessalonians chapter ([0-9]+)', r'1 Thessalonians \1', i_str)
#     return i_str
#
#
# def normalize_eng_53_2thess(i_str):
#     i_str = re.sub(r'THESSALONIANS', r'Thessalonians', i_str)
#     i_str = re.sub(r'THESS\.', r'Thessalonians', i_str)
#     i_str = re.sub(r'THESS', r'Thessalonians', i_str)
#     i_str = re.sub(r' THESS ', r' Thessalonians ', i_str)
#     i_str = re.sub(r'II Thessalonians ([0-9]+):([0-9]+)', r'2 Thessalonians \1:\2', i_str)
#     i_str = re.sub(r'2Th ([0-9]+):([0-9]+)', r'2 Thessalonians \1:\2', i_str)
#     i_str = re.sub(r'2 Th ([0-9]+):([0-9]+)', r'2 Thessalonians \1:\2', i_str)
#     i_str = re.sub(r'2 Thess\. ([0-9]+):([0-9]+)', r'2 Thessalonians \1:\2', i_str)
#     i_str = re.sub(r'2 Thess ([0-9]+):([0-9]+)', r'2 Thessalonians \1:\2', i_str)
#     i_str = re.sub(r'2Thessalonians ([0-9]+):([0-9]+)', r'2 Thessalonians \1:\2', i_str)
#     i_str = re.sub(r'2nd Thessalonians ([0-9]+):([0-9]+)', r'2 Thessalonians \1:\2', i_str)
#     i_str = re.sub(r'2nd thessalonians ([0-9]+):([0-9]+)', r'2 Thessalonians \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of 2 Thessalonians ([0-9]+)', r'2 Thessalonians \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) 2 Thessalonians ([0-9]+)', r'2 Thessalonians \2:\1', i_str)
#     i_str = re.sub(r'2 Thessalonians chapter ([0-9]+)', r'2 Thessalonians \1', i_str)
#     return i_str
#
#
# def normalize_eng_54_1timothy(i_str):
#     i_str = re.sub(r'TIMOTHY', r'Timothy', i_str)
#     i_str = re.sub(r'TIM\.', r'Timothy', i_str)
#     i_str = re.sub(r'TIM', r'Timothy', i_str)
#     i_str = re.sub(r' TIM ', r' Timothy ', i_str)
#     i_str = re.sub(r'1 Tim\. ([0-9]+):([0-9]+)', r'1 Timothy \1:\2', i_str)
#     i_str = re.sub(r'1 Tim ([0-9]+):([0-9]+)', r'1 Timothy \1:\2', i_str)
#     i_str = re.sub(r'1Ti ([0-9]+):([0-9]+)', r'1 Timothy \1:\2', i_str)
#     i_str = re.sub(r'1 Ti ([0-9]+):([0-9]+)', r'1 Timothy \1:\2', i_str)
#     i_str = re.sub(r'I Tim ([0-9]+):([0-9]+)', r'1 Timothy \1:\2', i_str)
#     i_str = re.sub(r'I Timothy ([0-9]+):([0-9]+)', r'1 Timothy \1:\2', i_str)
#     i_str = re.sub(r'1Timothy ([0-9]+):([0-9]+)', r'1 Timothy \1:\2', i_str)
#     i_str = re.sub(r'1st Timothy ([0-9]+):([0-9]+)', r'1 Timothy \1:\2', i_str)
#     i_str = re.sub(r'1st timothy ([0-9]+):([0-9]+)', r'1 Timothy \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of 1 Timothy ([0-9]+)', r'1 Timothy \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) 1 Timothy ([0-9]+)', r'1 Timothy \2:\1', i_str)
#     i_str = re.sub(r'1 Timothy chapter ([0-9]+)', r'1 Timothy \1', i_str)
#     return i_str
#
#
# def normalize_eng_55_2timothy(i_str):
#     i_str = re.sub(r'TIMOTHY', r'Timothy', i_str)
#     i_str = re.sub(r'TIM\.', r'Timothy', i_str)
#     i_str = re.sub(r' TIM ', r' Timothy ', i_str)
#     i_str = re.sub(r'2 Tim\. ([0-9]+):([0-9]+)', r'2 Timothy \1:\2', i_str)
#     i_str = re.sub(r'2 Tim ([0-9]+):([0-9]+)', r'2 Timothy \1:\2', i_str)
#     i_str = re.sub(r'2Ti ([0-9]+):([0-9]+)', r'2 Timothy \1:\2', i_str)
#     i_str = re.sub(r'2 Ti ([0-9]+):([0-9]+)', r'2 Timothy \1:\2', i_str)
#     i_str = re.sub(r'II Tim ([0-9]+):([0-9]+)', r'2 Timothy \1:\2', i_str)
#     i_str = re.sub(r'II Timothy ([0-9]+):([0-9]+)', r'2 Timothy \1:\2', i_str)
#     i_str = re.sub(r'2Timothy ([0-9]+):([0-9]+)', r'2 Timothy \1:\2', i_str)
#     i_str = re.sub(r'2nd Timothy ([0-9]+):([0-9]+)', r'2 Timothy \1:\2', i_str)
#     i_str = re.sub(r'2nd timothy ([0-9]+):([0-9]+)', r'2 Timothy \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of 2 Timothy ([0-9]+)', r'2 Timothy \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) 2 Timothy ([0-9]+)', r'2 Timothy \2:\1', i_str)
#     i_str = re.sub(r'2 Timothy chapter ([0-9]+)', r'2 Timothy \1', i_str)
#     return i_str
#
#
# def normalize_eng_56_titus(i_str):
#     i_str = re.sub(r'TITUS', r'Titus', i_str)
#     i_str = re.sub(r'TIT\.', r'Titus', i_str)
#     i_str = re.sub(r' TIT ', r' Titus ', i_str)
#     i_str = re.sub(r'Tit\. ([0-9]+):([0-9]+)', r'Titus \1:\2', i_str)
#     i_str = re.sub(r'Tit ([0-9]+):([0-9]+)', r'Titus \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Titus ([0-9]+)', r'Titus \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Titus ([0-9]+)', r'Titus \2:\1', i_str)
#     i_str = re.sub(r'Titus chapter ([0-9]+)', r'Titus \1', i_str)
#     return i_str
#
#
# def normalize_eng_57_philemon(i_str):
#     i_str = re.sub(r'PHILEMON', r'Philemon', i_str)
#     i_str = re.sub(r'PHM.', r'Philemon', i_str)
#     i_str = re.sub(r' PHM ', r' Philemon ', i_str)
#     i_str = re.sub(r'Philem\. ([0-9]+)', r'Philemon 1:\1', i_str)
#     i_str = re.sub(r'Philem ([0-9]+)', r'Philemon 1:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Philemon', r'Philemon 1:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Philemon', r'Philemon 1:\1', i_str)
#     i_str = re.sub(r'Philemon chapter ([0-9]+)', r'Philemon 1:\1', i_str)
#     i_str = re.sub(r'Phlm ([0-9]+)', r'Philemon 1:\1', i_str)
#     i_str = re.sub(r'Phlm\. ([0-9]+)', r'Philemon 1:\1', i_str)
#     return i_str
#
#
# def normalize_eng_58_hebrews(i_str):
#     i_str = re.sub(r'HEBREWS', r'Hebrews', i_str)
#     i_str = re.sub(r'HEB\.', r'Hebrews', i_str)
#     i_str = re.sub(r' HEB ', r' Hebrews ', i_str)
#     i_str = re.sub(r'Hebrew ([0-9]+):([0-9]+)', r'Hebrews \1:\2', i_str)
#     i_str = re.sub(r'Hebrews. ([0-9]+):([0-9]+)', r'Hebrews \1:\2', i_str)
#     i_str = re.sub(r'Hebr ([0-9]+):([0-9]+)', r'Hebrews \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Hebrews ([0-9]+)', r'Hebrews \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Hebrews ([0-9]+)', r'Hebrews \2:\1', i_str)
#     i_str = re.sub(r'Hebrews chapter ([0-9]+)', r'Hebrews \1', i_str)
#     return i_str
#
#
# def normalize_eng_59_james(i_str):
#     i_str = re.sub(r'JAMES', r'James', i_str)
#     i_str = re.sub(r'JAS\.', r'James', i_str)
#     i_str = re.sub(r' JAS ', r' James ', i_str)
#     i_str = re.sub(r'Jam\. ([0-9]+):([0-9]+)', r'James \1:\2', i_str)
#     i_str = re.sub(r'Jam ([0-9]+):([0-9]+)', r'James \1:\2', i_str)
#     i_str = re.sub(r'Jas ([0-9]+):([0-9]+)', r'James \1:\2', i_str)           # 2019-12-04
#     i_str = re.sub(r'verse ([0-9]+) of James ([0-9]+)', r'James \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) James ([0-9]+)', r'James \2:\1', i_str)
#     i_str = re.sub(r'James chapter ([0-9]+)', r'James \1', i_str)
#     return i_str
#
#
# def normalize_eng_60_1peter(i_str):
#     i_str = re.sub(r'PETER', r'Peter', i_str)
#     i_str = re.sub(r'PT\.', r'Peter', i_str)
#     i_str = re.sub(r' PT ', r' Peter', i_str)
#     i_str = re.sub(r'1 Pet\. ([0-9]+):([0-9]+)', r'1 Peter \1:\2', i_str)
#     i_str = re.sub(r'1 Pet ([0-9]+):([0-9]+)', r'1 Peter \1:\2', i_str)
#     i_str = re.sub(r'1Pe ([0-9]+):([0-9]+)', r'1 Peter \1:\2', i_str)
#     i_str = re.sub(r'1 Pe ([0-9]+):([0-9]+)', r'1 Peter \1:\2', i_str)
#     i_str = re.sub(r'I Peter ([0-9]+):([0-9]+)', r'1 Peter \1:\2', i_str)
#     i_str = re.sub(r'1Peter ([0-9]+):([0-9]+)', r'1 Peter \1:\2', i_str)
#     i_str = re.sub(r'1st Peter ([0-9]+):([0-9]+)', r'1 Peter \1:\2', i_str)
#     i_str = re.sub(r'1st peter ([0-9]+):([0-9]+)', r'1 Peter \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of 1 Peter ([0-9]+)', r'1 Peter \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) 1 Peter ([0-9]+)', r'1 Peter \2:\1', i_str)
#     i_str = re.sub(r'1 Peter chapter ([0-9]+)', r'1 Peter \1', i_str)
#     i_str = re.sub(r'1 Ped ([0-9]+):([0-9]+)', r'1 Peter \1:\2', i_str)
#     i_str = re.sub(r'1P\. ([0-9]+):([0-9]+)', r'1 Peter \1:\2', i_str)
#     i_str = re.sub(r'1P ([0-9]+):([0-9]+)', r'1 Peter \1:\2', i_str)
#     return i_str
#
#
# def normalize_eng_61_2peter(i_str):
#     i_str = re.sub(r'PETER', r'Peter', i_str)
#     i_str = re.sub(r'PT\.', r'Peter', i_str)
#     i_str = re.sub(r' PT ', r' Peter', i_str)
#     i_str = re.sub(r'2 Pet\. ([0-9]+):([0-9]+)', r'2 Peter \1:\2', i_str)
#     i_str = re.sub(r'2 Pet ([0-9]+):([0-9]+)', r'2 Peter \1:\2', i_str)       # 2019-11-04
#     i_str = re.sub(r'2Pe ([0-9]+):([0-9]+)', r'2 Peter \1:\2', i_str)         # 2019-12-04
#     i_str = re.sub(r'2 Pe ([0-9]+):([0-9]+)', r'2 Peter \1:\2', i_str)         # 2019-12-04
#     i_str = re.sub(r'II Peter ([0-9]+):([0-9]+)', r'2 Peter \1:\2', i_str)    # I Peter 2:5
#     i_str = re.sub(r'2Peter ([0-9]+):([0-9]+)', r'2 Peter \1:\2', i_str)
#     i_str = re.sub(r'2nd Peter ([0-9]+):([0-9]+)', r'2 Peter \1:\2', i_str)
#     i_str = re.sub(r'2nd peter ([0-9]+):([0-9]+)', r'2 Peter \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of 2 Peter ([0-9]+)', r'2 Peter \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) 2 Peter ([0-9]+)', r'2 Peter \2:\1', i_str)
#     i_str = re.sub(r'2 Peter chapter ([0-9]+)', r'2 Peter \1', i_str)
#     i_str = re.sub(r'2 Ped ([0-9]+):([0-9]+)', r'2 Peter \1:\2', i_str)
#     i_str = re.sub(r'2P\. ([0-9]+):([0-9]+)', r'2 Peter \1:\2', i_str)
#     i_str = re.sub(r'2P ([0-9]+):([0-9]+)', r'2 Peter \1:\2', i_str)
#     return i_str
#
#
# def normalize_eng_62_1john(i_str):
#     i_str = re.sub(r'JOHN', r'John', i_str)
#     i_str = re.sub(r'JH\.', r'John', i_str)
#     i_str = re.sub(r' JH ', r' John ', i_str)
#     i_str = re.sub(r'1 Jn\. ([0-9]+):([0-9]+)', r'1 Jn \1:\2', i_str)
#     i_str = re.sub(r'1 Jn ([0-9]+):([0-9]+)', r'1 Jn \1:\2', i_str)
#     i_str = re.sub(r'1Jn ([0-9]+):([0-9]+)', r'1 Jn \1:\2', i_str)             # 2019-12-04
#     i_str = re.sub(r'I John ([0-9]+):([0-9]+)', r'1 Jn \1:\2', i_str)
#     i_str = re.sub(r'1John ([0-9]+):([0-9]+)', r'1 Jn \1:\2', i_str)
#     i_str = re.sub(r'I Jn\. ([0-9]+):([0-9]+)', r'1 Jn \1:\2', i_str)
#     i_str = re.sub(r'I Jn ([0-9]+):([0-9]+)', r'1 Jn \1:\2', i_str)
#     i_str = re.sub(r'CAP', r'low', i_str)
#     i_str = re.sub(r'CAP', r'low', i_str)
#     i_str = re.sub(r'1st John ([0-9]+):([0-9]+)', r'1 Jn \1:\2', i_str)
#     i_str = re.sub(r'1st john ([0-9]+):([0-9]+)', r'1 Jn \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of 1 Jn ([0-9]+)', r'1 Jn \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) 1 Jn ([0-9]+)', r'1 Jn \2:\1', i_str)
#     i_str = re.sub(r'1 Jn chapter ([0-9]+)', r'1 Jn \1', i_str)
#     return i_str
#
#
# def normalize_eng_63_2john(i_str):
#     i_str = re.sub(r'JOHN', r'John', i_str)
#     i_str = re.sub(r'JH\.', r'John', i_str)
#     i_str = re.sub(r' JH ', r' John ', i_str)
#     i_str = re.sub(r'2 Jn\. ([0-9]+):([0-9]+)', r'2 Jn \1:\2', i_str)
#     i_str = re.sub(r'2 Jn ([0-9]+):([0-9]+)', r'2 Jn \1:\2', i_str)
#     i_str = re.sub(r'2Jn ([0-9]+):([0-9]+)', r'2 Jn \1:\2', i_str)
#     i_str = re.sub(r'2John ([0-9]+):([0-9]+)', r'2 Jn \1:\2', i_str)
#     i_str = re.sub(r'II John ([0-9]+):([0-9]+)', r'2 Jn \1:\2', i_str)
#     i_str = re.sub(r'II Jn\. ([0-9]+):([0-9]+)', r'2 Jn \1:\2', i_str)
#     i_str = re.sub(r'II Jn ([0-9]+):([0-9]+)', r'2 Jn \1:\2', i_str)
#     i_str = re.sub(r'2nd John ([0-9]+):([0-9]+)', r'2 Jn \1:\2', i_str)
#     i_str = re.sub(r'2nd John ([0-9]+):([0-9]+)', r'2 Jn \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of 2 Jn ([0-9]+)', r'2 Jn \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) 2 Jn ([0-9]+)', r'2 Jn \2:\1', i_str)
#     i_str = re.sub(r'2 Jn chapter ([0-9]+)', r'2 Jn \1', i_str)
#     return i_str
#
#
# def normalize_eng_64_3john(i_str):
#     i_str = re.sub(r'JOHN', r'John', i_str)
#     i_str = re.sub(r'JH\.', r'John', i_str)
#     i_str = re.sub(r' JH ', r' John ', i_str)
#     i_str = re.sub(r'3 Jn\. ([0-9]+):([0-9]+)', r'3 Jn \1:\2', i_str)
#     i_str = re.sub(r'3 Jn ([0-9]+):([0-9]+)', r'3 Jn \1:\2', i_str)
#     i_str = re.sub(r'3Jn ([0-9]+):([0-9]+)', r'3 Jn \1:\2', i_str)
#     i_str = re.sub(r'3John ([0-9]+):([0-9]+)', r'3 Jn \1:\2', i_str)
#     i_str = re.sub(r'III John ([0-9]+):([0-9]+)', r'3 Jn \1:\2', i_str)
#     i_str = re.sub(r'III Jn\. ([0-9]+):([0-9]+)', r'3 Jn \1:\2', i_str)
#     i_str = re.sub(r'III Jn ([0-9]+):([0-9]+)', r'3 Jn \1:\2', i_str)
#     i_str = re.sub(r'3rd John ([0-9]+):([0-9]+)', r'3 Jn \1:\2', i_str)
#     i_str = re.sub(r'3rd john ([0-9]+):([0-9]+)', r'3 Jn \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of 3 Jn ([0-9]+)', r'3 Jn \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) 3 Jn ([0-9]+)', r'3 Jn \2:\1', i_str)
#     i_str = re.sub(r'3 Jn chapter ([0-9]+)', r'3 Jn \1', i_str)
#     return i_str
#
#
# def normalize_eng_65_jude(i_str):
#     # Add chapter '1' if missing
#     # Jude 24, 25   =>      Jude 1:24\nJude 1:25
#     # Jude 24   => Jude 1:24
#     i_str = re.sub(r'JUDE', r'Jude', i_str)
#     i_str = re.sub(r'JD\.', r'Jude', i_str)
#     i_str = re.sub(r' JD ', r' Jude ', i_str)
#     i_str = re.sub(r'Jude ([0-9]+), ([0-9]+)', r'Jude 1:\1\nJude 1:\2', i_str)    # 2019-12-22
#     i_str = re.sub(r'Jd\. 1:([0-9]+)', r'Jude 1:\1', i_str)
#     i_str = re.sub(r'Jude ([0-9]+)', r'Jude 1:\1', i_str)
#     i_str = re.sub(r'Jud ([0-9]+)', r'Jude 1:\1', i_str)
#     i_str = re.sub(r'Jude 1:1:', r'Jude 1:', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Jude ([0-9]+)', r'Jude \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Jude ([0-9]+)', r'Jude \2:\1', i_str)
#     i_str = re.sub(r'Jude chapter ([0-9]+)', r'Jude \1', i_str)
#     return i_str
#
#
# def normalize_eng_66_revelation(i_str):
#     i_str = re.sub(r'REVELATION', r'Revelation', i_str)
#     i_str = re.sub(r'REV\.', r'Revelation', i_str)
#     i_str = re.sub(r' REV ', r' Revelation ', i_str)
#     i_str = re.sub(r'Rev\. ([0-9]+):([0-9]+)', r'Revelation \1:\2', i_str)
#     i_str = re.sub(r'Rev ([0-9]+):([0-9]+)', r'Revelation \1:\2', i_str)
#     i_str = re.sub(r'verse ([0-9]+) of Revelation ([0-9]+)', r'Revelation \2:\1', i_str)
#     i_str = re.sub(r'verse ([0-9]+) Revelation ([0-9]+)', r'Revelation \2:\1', i_str)
#     i_str = re.sub(r'Revelation chapter ([0-9]+)', r'Revelation \1', i_str)
#     return i_str
#
# WINDOWS_NEWLINE = '\r\n'
#
# def replace_swe_from_list(i_replace_lists, i_str):
#     for t_replace_list in i_replace_lists:
#         i_str = i_str.replace(t_replace_list[0], t_replace_list[1])
#         i_str = change_swe_book_name_johannes(i_str)
#     return i_str
#
#
# def change_swe_book_name_johannes(i_str):  # Johannes 12:15 => Joh. 12:15 2019-09-05
#     import re
#     out_string = re.sub(r'Johannes ([0-9]+):([0-9]+)', r'Joh. \1:\2', i_str)
#     return out_string
#
#
# def change_swe_book_name_josua(i_str):  # Josua 12:15 => Jos. 12:15  2019-09-05
#     import re
#     out_string = re.sub(r'Josua ([0-9]+):([0-9]+)', r'Jos. \1:\2', i_str)
#     return out_string
#
#
#
# def get_bible_book_number_from_swedish_names(i_swe_book_name):
#     # print("1030 i_swe_book_name: " + i_swe_book_name)
#
#     if sweGenesis(i_swe_book_name):
#         # print("1034: " +i_swe_book_name)
#         t_bible_book_number = 1
#
#     elif sweExodus(i_swe_book_name):
#         t_bible_book_number = 2
#
#     elif sweLeviticus(i_swe_book_name):
#         t_bible_book_number = 3
#
#     elif sweNumbers(i_swe_book_name):
#         t_bible_book_number = 4
#
#     elif sweDeuteronomy(i_swe_book_name):
#         t_bible_book_number = 5
#
#     elif sweJoshua(i_swe_book_name):
#         t_bible_book_number = 6
#
#     elif sweJudges(i_swe_book_name):
#         t_bible_book_number = 7
#
#     elif sweRuth(i_swe_book_name):
#         t_bible_book_number = 8
#
#     elif swe1Samuel(i_swe_book_name):
#         t_bible_book_number = 9
#
#     elif swe2Samuel(i_swe_book_name):
#         t_bible_book_number = 10
#
#     elif swe1Kings(i_swe_book_name):
#         t_bible_book_number = 11
#
#     elif swe2Kings(i_swe_book_name):
#         t_bible_book_number = 12
#
#     elif swe1Chronicles(i_swe_book_name):
#         t_bible_book_number = 13
#
#     elif swe2Chronicles(i_swe_book_name):
#         t_bible_book_number = 14
#
#     elif sweEzra(i_swe_book_name):
#         t_bible_book_number = 15
#
#     elif sweNehemiah(i_swe_book_name):
#         t_bible_book_number = 16
#
#     elif sweEsther(i_swe_book_name):
#         t_bible_book_number = 17
#
#     elif sweJob(i_swe_book_name):
#         t_bible_book_number = 18
#
#     elif swePsalms(i_swe_book_name):
#         t_bible_book_number = 19
#
#     elif sweProverbs(i_swe_book_name):
#         t_bible_book_number = 20
#
#     elif sweEcclesiastes(i_swe_book_name):
#         t_bible_book_number = 21
#
#     elif sweSongofSongs(i_swe_book_name):
#         t_bible_book_number = 22
#
#     elif sweIsaiah(i_swe_book_name):
#         t_bible_book_number = 23
#
#     elif sweJeremiah(i_swe_book_name):
#         t_bible_book_number = 24
#
#     elif sweSongofSongs(i_swe_book_name):
#         t_bible_book_number = 23
#
#     elif sweJeremiah(i_swe_book_name):
#         t_bible_book_number = 24
#
#     elif sweLamentations(i_swe_book_name):
#         t_bible_book_number = 25
#
#     elif sweEzekiel(i_swe_book_name):
#         t_bible_book_number = 26
#
#     elif sweDaniel(i_swe_book_name):
#         t_bible_book_number = 27
#
#     elif sweHosea(i_swe_book_name):
#         t_bible_book_number = 28
#
#     elif sweJoel(i_swe_book_name):
#         t_bible_book_number = 29
#
#     elif sweAmos(i_swe_book_name):
#         t_bible_book_number = 30
#
#     elif sweObadiah(i_swe_book_name):
#         t_bible_book_number = 31
#
#     elif sweJonah(i_swe_book_name):
#         t_bible_book_number = 32
#
#     elif sweMicah(i_swe_book_name):
#         t_bible_book_number = 33
#
#     elif sweNahum(i_swe_book_name):
#         t_bible_book_number = 34
#
#     elif sweHabakkuk(i_swe_book_name):
#         t_bible_book_number = 35
#
#     elif sweZephaniah(i_swe_book_name):
#         t_bible_book_number = 36
#
#     elif sweHaggai(i_swe_book_name):
#         t_bible_book_number = 37
#
#     elif sweZechariah(i_swe_book_name):
#         t_bible_book_number = 38
#
#     elif sweMalachi(i_swe_book_name):
#         t_bible_book_number = 39
#
#     elif sweMatthew(i_swe_book_name):
#         t_bible_book_number = 40
#
#     elif sweMark(i_swe_book_name):
#         t_bible_book_number = 41
#
#     elif sweLuke(i_swe_book_name):
#         t_bible_book_number = 42
#
#     elif sweJohn(i_swe_book_name):
#         t_bible_book_number = 43
#
#     elif sweActs(i_swe_book_name):
#         t_bible_book_number = 44
#
#     elif sweRomans(i_swe_book_name):
#         t_bible_book_number = 45
#
#     elif swe1Corinthians(i_swe_book_name):
#         t_bible_book_number = 46
#
#     elif swe2Corinthians(i_swe_book_name):
#         t_bible_book_number = 47
#
#     elif sweGalatians(i_swe_book_name):
#         t_bible_book_number = 48
#
#     elif sweEphesians(i_swe_book_name):
#         t_bible_book_number = 49
#
#     elif swePhilippians(i_swe_book_name):
#         t_bible_book_number = 50
#
#     elif sweColossians(i_swe_book_name):
#         t_bible_book_number = 51
#
#     elif swe1Thessalonians(i_swe_book_name):
#         t_bible_book_number = 52
#
#     elif swe2Thessalonians(i_swe_book_name):
#         t_bible_book_number = 53
#
#     elif swe1Timothy(i_swe_book_name):
#         t_bible_book_number = 54
#
#     elif swe2Timothy(i_swe_book_name):
#         t_bible_book_number = 55
#
#     elif sweTitus(i_swe_book_name):
#         t_bible_book_number = 56
#
#     elif swePhilemon(i_swe_book_name):
#         t_bible_book_number = 57
#
#     elif sweHebrews(i_swe_book_name):
#         t_bible_book_number = 58
#
#     elif sweJames(i_swe_book_name):
#         t_bible_book_number = 59
#
#     elif swe1Peter(i_swe_book_name):
#         t_bible_book_number = 60
#
#     elif swe2Peter(i_swe_book_name):
#         t_bible_book_number = 61
#
#     elif swe1John(i_swe_book_name):
#         t_bible_book_number = 62
#
#     elif swe2John(i_swe_book_name):
#         t_bible_book_number = 63
#
#     elif swe3John(i_swe_book_name):
#         t_bible_book_number = 64
#
#     elif sweJude(i_swe_book_name):
#         t_bible_book_number = 65
#
#     elif sweRevelation(i_swe_book_name):
#         t_bible_book_number = 66
#     else:
#         # print ("Didn't find book")
#         t_bible_book_number = 67
#     # print("End")
#     return t_bible_book_number
#
# def get_genesis_swedish_bible_book_number(i_line):
#     if i_line.find("1 Mos") > -1:
#         t_bible_book_number = 1
#     elif i_line.find("1. Mos.") > -1:
#         t_bible_book_number = 1
#     elif i_line.find("FÃ¶rsta Moseboken") > -1:
#         t_bible_book_number = 1
#     elif i_line.find("2 Mos") > -1:
#         t_bible_book_number = 2
#     elif i_line.find("3 Mos") > -1:
#         t_bible_book_number = 3
#     elif i_line.find("4 Mos") > -1:
#         t_bible_book_number = 4
#     elif i_line.find("5 Mos") > -1:
#         t_bible_book_number = 5
#     elif i_line.find("1Mos") > -1:
#         t_bible_book_number = 1
#     elif i_line.find("2Mos") > -1:
#         t_bible_book_number = 2
#     elif i_line.find("2Mos") > -1:
#         t_bible_book_number = 2
#     elif i_line.find("3Mos") > -1:
#         t_bible_book_number = 3
#     elif i_line.find("4Mos") > -1:
#         t_bible_book_number = 4
#     elif i_line.find("5Mos") > -1:
#         t_bible_book_number = 5
#     else:
#         # print("1568")
#         t_bible_book_number = 1
#     return t_bible_book_number
#
#
# def get_samuel_swedish_bible_book_number(i_line):
#     if i_line.find("1 Sam") > -1:
#         t_bible_book_number = 9
#     elif i_line.find("1 Sam.") > -1:
#         t_bible_book_number = 9
#     elif i_line.find("1 Samuelsboken") > -1:
#         t_bible_book_number = 9
#     elif i_line.find("Första Samuelsboken") > -1:
#         t_bible_book_number = 9
#     elif i_line.find("Första Sam") > -1:
#         t_bible_book_number = 9
#     if i_line.find("2 Sam") > -1:
#         t_bible_book_number = 10
#     elif i_line.find("2 Sam.") > -1:
#         t_bible_book_number = 10
#     elif i_line.find("2 Samuelsboken") > -1:
#         t_bible_book_number = 10
#     elif i_line.find("Andra Samuelsboken") > -1:
#         t_bible_book_number = 10
#     elif i_line.find("Andra Sam") > -1:
#         t_bible_book_number = 10
#     return t_bible_book_number
#
#
# def get_kings_swedish_bible_book_number(i_line):
#     t_bible_book_number = 11
#     if i_line.find("1 Kung") > -1:
#         t_bible_book_number = 11
#     elif i_line.find("1 Kung.") > -1:
#         t_bible_book_number = 11
#     elif i_line.find("1 Kungaboken") > -1:
#         t_bible_book_number = 11
#     elif i_line.find("Första Kungaboken") > -1:
#         t_bible_book_number = 11
#     elif i_line.find("Första Kung") > -1:
#         t_bible_book_number = 11
#     if i_line.find("2 Kung") > -1:
#         t_bible_book_number = 12
#     elif i_line.find("2 Kung.") > -1:
#         t_bible_book_number = 12
#     elif i_line.find("2 Kungabokan") > -1:
#         t_bible_book_number = 12
#     elif i_line.find("Andra Kungabokan") > -1:
#         t_bible_book_number = 12
#     elif i_line.find("Andra Kung") > -1:
#         t_bible_book_number = 12
#     return t_bible_book_number
#
#
# def get_chronicles_swedish_bible_book_number(i_line):
#     t_bible_book_number = 13
#     if i_line.find("1 Krön") > -1:
#         t_bible_book_number = 13
#     elif i_line.find("1 Krön.") > -1:
#         t_bible_book_number = 13
#     elif i_line.find("1 Krönikerboken") > -1:
#         t_bible_book_number = 13
#     elif i_line.find("Första Krönikerboken") > -1:
#         t_bible_book_number = 13
#     elif i_line.find("Första Krön") > -1:
#         t_bible_book_number = 13
#     if i_line.find("2 Krön") > -1:
#         t_bible_book_number = 14
#     elif i_line.find("2 Krön.") > -1:
#         t_bible_book_number = 14
#     elif i_line.find("2 Krön") > -1:
#         t_bible_book_number = 14
#     elif i_line.find("Andra Krönikerboken") > -1:
#         t_bible_book_number = 14
#     elif i_line.find("Andra Krön") > -1:
#         t_bible_book_number = 14
#     return t_bible_book_number
#
#
# def get_corinthians_swedish_bible_book_number(i_line):
#     t_bible_book_number = 46
#     if i_line.find("1 Kor") > -1:
#         t_bible_book_number = 46
#     elif i_line.find("1 Kor.") > -1:
#         t_bible_book_number = 46
#     elif i_line.find("1 Korintierbrevet") > -1:
#         t_bible_book_number = 46
#     elif i_line.find("Första Korintierbrevet") > -1:
#         t_bible_book_number = 46
#     elif i_line.find("Första Kor") > -1:
#         t_bible_book_number = 46
#     if i_line.find("2 Kor") > -1:
#         t_bible_book_number = 47
#     elif i_line.find("2 Kor.") > -1:
#         t_bible_book_number = 47
#     elif i_line.find("2 Korintierbrevet") > -1:
#         t_bible_book_number = 47
#     elif i_line.find("Andra Korintierbrevet") > -1:
#         t_bible_book_number = 47
#     elif i_line.find("Andra Kor") > -1:
#         t_bible_book_number = 47
#     return t_bible_book_number
#
#
# def get_thessalonians_swedish_bible_book_number(i_line):
#     t_bible_book_number = 52
#     if i_line.find("1 Thess") > -1:
#         t_bible_book_number = 52
#     elif i_line.find("1 Thess.") > -1:
#         t_bible_book_number = 52
#     elif i_line.find("1 Thessalonikebrevet") > -1:
#         t_bible_book_number = 52
#     elif i_line.find("Första Thessalonikebrevet") > -1:
#         t_bible_book_number = 52
#     elif i_line.find("Första Thess") > -1:
#         t_bible_book_number = 52
#     if i_line.find("2 Thess") > -1:
#         t_bible_book_number = 53
#     elif i_line.find("2 Thess.") > -1:
#         t_bible_book_number = 53
#     elif i_line.find("2 Thessalonikebrevet") > -1:
#         t_bible_book_number = 53
#     elif i_line.find("Andra Thessalonikebrevet") > -1:
#         t_bible_book_number = 53
#     elif i_line.find("Andra Thess") > -1:
#         t_bible_book_number = 53
#     return t_bible_book_number
#
#
# def get_timothy_swedish_bible_book_number(i_line):
#     t_bible_book_number = 54
#     if i_line.find("1 Tim") > -1:
#         t_bible_book_number = 54
#     elif i_line.find("1 Tim.") > -1:
#         t_bible_book_number = 54
#     elif i_line.find("1 Timoteusbrevet") > -1:
#         t_bible_book_number = 54
#     elif i_line.find("Första Timoteusbrevet") > -1:
#         t_bible_book_number = 54
#     elif i_line.find("Första Tim") > -1:
#         t_bible_book_number = 54
#     if i_line.find("2 Tim") > -1:
#         t_bible_book_number = 55
#     elif i_line.find("2 Tim.") > -1:
#         t_bible_book_number = 55
#     elif i_line.find("2 Timoteusbrevet") > -1:
#         t_bible_book_number = 55
#     elif i_line.find("Andra Timoteusbrevet") > -1:
#         t_bible_book_number = 55
#     elif i_line.find("Andra Tim") > -1:
#         t_bible_book_number = 55
#     return t_bible_book_number
#
#
# def get_peter_swedish_bible_book_number(i_line):
#     t_bible_book_number = 60
#     if i_line.find("1 Petr") > -1:
#         t_bible_book_number = 60
#     elif i_line.find("1 Petr.") > -1:
#         t_bible_book_number = 60
#     elif i_line.find("1 Petrusbrevet") > -1:
#         t_bible_book_number = 60
#     elif i_line.find("Första Petrusbrevet") > -1:
#         t_bible_book_number = 60
#     elif i_line.find("Första Petr") > -1:
#         t_bible_book_number = 60
#     if i_line.find("2 Petr") > -1:
#         t_bible_book_number = 61
#     elif i_line.find("2 Petr.") > -1:
#         t_bible_book_number = 61
#     elif i_line.find("2 Petrusbrevet") > -1:
#         t_bible_book_number = 61
#     elif i_line.find("Andra Petrusbrevet") > -1:
#         t_bible_book_number = 61
#     elif i_line.find("Andra Petr") > -1:
#         t_bible_book_number = 61
#     return t_bible_book_number
#
#
# def get_john_swedish_bible_book_number(i_line):
#     t_bible_book_number = 43
#     print("Line: " + i_line)
#     # if i_line.find("Joh") > -1:
#     #     t_bible_book_number = 43
#     if i_line.find("1 Joh") > -1:
#         t_bible_book_number = 62
#     elif i_line.find("1 Joh.") > -1:
#         t_bible_book_number = 62
#     elif i_line.find("1 Johannesbrevet") > -1:
#         t_bible_book_number = 62
#     elif i_line.find("Första Johannesbrevet") > -1:
#         t_bible_book_number = 62
#     elif i_line.find("Första Joh") > -1:
#         t_bible_book_number = 62
#     if i_line.find("2 Joh") > -1:
#         t_bible_book_number = 63
#     elif i_line.find("2 Joh.") > -1:
#         t_bible_book_number = 63
#     elif i_line.find("2 Johannesbrevet") > -1:
#         t_bible_book_number = 63
#     elif i_line.find("Andra Johannesbrevet") > -1:
#         t_bible_book_number = 63
#     elif i_line.find("Andra Joh") > -1:
#         t_bible_book_number = 63
#     elif i_line.find("3 Joh") > -1:
#         t_bible_book_number = 64
#     elif i_line.find("3 Joh.") > -1:
#         t_bible_book_number = 64
#     elif i_line.find("3 Johannesbrevet") > -1:
#         t_bible_book_number = 64
#     elif i_line.find("Tredje Johannesbrevet") > -1:
#         t_bible_book_number = 64
#     elif i_line.find("Tredje Joh") > -1:
#         t_bible_book_number = 64
#     # elif i_line.find("Joh") > -1:
#     #     t_bible_book_number = 43
#     return t_bible_book_number
#
#
# swe_book_names_no_prefix_list = ["Mos", "Mos.", "1Mos.", "1Mos", "1.Mos.", "FÃ¶rsta Moseboken", "FörstaMoseboken",
#                                  "Första Mos.", "Moseboken",
#                                  "2 Mos", "2Mos.", "2Mos.", "2Mos", "2.Mos.", "AndraMoseboken", "Andra Mos.",
#                                  "2Moseboken",
#                                  "3 Mos", "3Mos.", "3Mos.", "3.Mos.", "3Mos.", "TredjeMoseboken", "Tredje Mos.",
#                                  "3Moseboken",
#                                  "4 Mos", "4 Mos.", "4Mos.", "4.Mos.", "4Mos.", "Fjärde Moseboken", "Fjärde Mos.",
#                                  "4Moseboken",
#                                  "5Mos", "5 Mos.", "5Mos.", "5.Mos.", "5Mos.", "Femte Moseboken", "FemteMos.",
#                                  "5Moseboken",
#                                  "Jos", "Josua", "Jos.", "Dom", "Domarboken", "Dom.",
#                                  "Rut", "Ruts bok", "Rut.",
#                                  "Sam", "FörstaSamuelsboken", "Sam.", "FörstaSamuelsboken", "Sam",
#                                  "AndraSamuelsboken", "2Sam.", "2Samuelsboken",
#                                  "Kung", "FörstaKungaboken", "Kung.",
#                                  "2 Kung", "AndraKungaboken", "2 Kung.",
#                                  "Krön", "FörstaKrönikeboken", "Krön.",
#                                  "2 Krön", "AndraKrönikeboken", "2Krön.", "Esra", "Esr", "Esr.",
#                                  "Neh", "Nehemja", "Neh.",
#                                  "Est", "Esters bok", "Est.", "Job", "Jobs bok", "Job.", "Ps",
#                                  "Psaltaren", "Psalm", "Psa.", "Psa", "Ords", "Ordspråksboken", "Ords.", "Ordspr.",
#                                  "Pred", "Predikaren", "Pred.", "Höga V", "Höga Visan", "Höga V.", "Höga v.",
#                                  "Jes", "Jesaja", "Jes.", "Jer", "Jeremia", "Jer.", "Klag", "Klagovisorna", "Klag.",
#                                  "Hes", "Hesekiel", "Hes.", "Dan", "Daniel", "Dan.", "Hos", "Hosea", "Hos.", "Joel",
#                                  "Joe", "Joe.", "Amos", "Amo", "Amo.", "Ob", "Obadja", "Ob.", "Oba.", "Jon", "Jona",
#                                  "Jon.", "Mik", "Mika", "Mik.", "Nah", "Nahum", "Nah.", "Hab", "Habakkuk", "Hab.",
#                                  "Sef", "Sefanja", "Sef.", "Hagg", "Haggai", "Hag.", "Sak", "Sakarja,", "Sak.",
#                                  "Mal",
#                                  "Malaki", "Mal.", "Matt", "Matteus", "Matt.", "Mat.", "Matteusevangeliet",
#                                  "Mark", "Markus", "Mar.", "Markusevangeliet", "Luk", "Lukas", "Luk.",
#                                  "Lukasevangeliet", "Joh", "Johannes", "Joh.", "Johannesevangeliet", "Apg",
#                                  "Apostlagärningarna", "Apg.", "Rom", "Romarbrevet", "Rom.", "Romarna",
#                                  "Kor", "Korintierbrevet", "1Kor", "1Kor.", "FörsteKorintierbrevet",
#                                  "FörstaKorintierbrevet", "Första Kor", "FörstaKor.", "2Kor",
#                                  "2Korintierbrevet", "2Kor", "2Kor.", "AndraKorintierbrevet",
#                                  "AndreKorintierbrevet",
#                                  "AndraKor", "AndraKor.", "Gal", "Galaterbrevet", "Gal.",
#                                  "Ef", "Efesierbrevet", "Ef.", "Efésierbrevet", "Fil", "Filipperbrevet", "Fil.",
#                                  "Kol", "Kolosserbrevet", "Kol.",
#                                  "Thess", "Thessalonikerbrevet", "Thess.", "FörstaThessalonikerbrevet",
#                                  "FörstaThess", "FörstaThess.",
#                                  "2Thess", "2Thessalonikerbrevet", "2Thess.", "AndraThessalonikerbrevet",
#                                  "AndraThess", "AndraThess.",
#                                  "Tim", "Timoteusbrevet", "Tim.", "FörstaTimoteusbrevet", "FörstaTim.", "FörstaTim",
#                                  "2 Tim", "2 Timoteusbrevet", "2 Tim.", "Andra Timoteusbrevet", "Andra Tim.",
#                                  "AndraTim",
#                                  "Tit", "Titusbrevet", "Tit.", "Titus", "Filem", "Filemon", "Filem.",
#                                  "BrevettillFilemon",
#                                  "Hebr", "Hebreerbrevet", "Hebr.", "Jak", "Jakobsbrevet", "Jak.", "Jakob",
#                                  "Petr", "Petrusbrevet", "FörstaPetrusbrevet", "FörstaPetr", "FörstaPetr.",
#                                  "1Petr.", "Petr.",
#                                  "2 Petr", "2 Petrusbrevet", "Andra Petrusbrevet", "Andra Petr", "Andra Petr.",
#                                  "2Petr.", "2 Petr.",
#                                  "Joh", "Johannesbrevet", "Joh.", "Första Johannesbrevet,", "Första Joh.",
#                                  "Första Joh",
#                                  "2Joh", "2 Johannesbrevet", "2Joh.", "Andra Johannesbrevet,", "AndraJoh.",
#                                  "AndraJoh",
#                                  "3Joh", "3Johannesbrevet", "3Joh.", "TredjeJohannesbrevet,", "TredjeJoh.",
#                                  "TredjeJoh",
#                                  "Jud", "Judasbrevet", "Jud.", "Upp", "Uppenbarelseboken", "Upp."]
#
#
# swe_book_numbers_with_prefix_list = [1, 2, 3, 4, 5, 9, 10, 11, 12, 13, 14, 46, 47, 52, 53, 54, 55, 60, 61, 62, 63,
#                                      64]
#
# # Procedures for CHECKING for Swedish book NAMES
# # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
#
#
# def sweGenesis(i_swe_book_name):
#     # print("535: ", i_swe_book_name)
#     swe_book_name = ["Mos", "1 Mos", "1 Mos.", "1Mos.", "1Mos", "1. Mos.", "Första Moseboken", "Första Mos.",
#                      "1 Moseboken"]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweExodus(i_swe_book_name):
#     swe_book_name = ["Mos", "2 Mos", "2 Mos.", "2Mos.", "2. Mos.", "Andra Moseboken", "Andra Mos.", "2 Moseboken"]
#     # print("i_swe_book_name in Exodus: " +i_swe_book_name)
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweLeviticus(i_swe_book_name):
#     swe_book_name = ["Mos", "3 Mos", "3 Mos.", "3Mos.", "3. Mos.", "Tredje Moseboken", "Tredje Mos.", "3 Moseboken"]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweNumbers(i_swe_book_name):
#     swe_book_name = ["Mos", "4 Mos", "4 Mos.", "4Mos.", ". Mos.", "Fjärde Moseboken", "Fjärde Mos.", "4 Moseboken"]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweDeuteronomy(i_swe_book_name):
#     swe_book_name = ["Mos", "5 Mos", "5 Mos.", "5Mos.", "5. Mos.", "Femte Moseboken", "Femte Mos.", "5 Moseboken"]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweJoshua(i_swe_book_name):
#     swe_book_name = ["Jos", "Josua", "Jos."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweJudges(i_swe_book_name):
#     swe_book_name = ["Dom", "Domarboken", "Dom."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweRuth(i_swe_book_name):
#     swe_book_name = ["Rut", "Ruts bok", "Rut."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def swe1Samuel(i_swe_book_name):
#     swe_book_name = ["Sam", "1 Sam", "Första Samuelsboken", "1 Sam.", "1 Första Samuelsboken"]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def swe2Samuel(i_swe_book_name):
#     swe_book_name = ["Sam", "2 Sam", "Andra Samuelsboken", "2 Sam.", "2 Samuelsboken"]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def swe1Kings(i_swe_book_name):
#     swe_book_name = ["Kung", "1 Kung", "Första Kungaboken", "1 Kung."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def swe2Kings(i_swe_book_name):
#     swe_book_name = ["Kung", "2 Kung", "Andra Kungaboken", "2 Kung."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def swe1Chronicles(i_swe_book_name):
#     swe_book_name = ["Krön", "1 Krön", "Första Krönikeboken", "1 Krön."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def swe2Chronicles(i_swe_book_name):
#     swe_book_name = ["Krön", "2 Krön", "Andra Krönikeboken", "2 Krön."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweEzra(i_swe_book_name):
#     swe_book_name = ["Esra", "Esr", "Esr."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweNehemiah(i_swe_book_name):
#     swe_book_name = ["Neh", "Nehemja", "Neh."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweEsther(i_swe_book_name):
#     swe_book_name = ["Est", "Esters bok", "Est."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweJob(i_swe_book_name):
#     swe_book_name = ["Job", "Jobs bok", "Job."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def swePsalms(i_swe_book_name):
#     swe_book_name = ["Ps", "Psaltaren", "Psalm", "Psa.", "Psa"]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweProverbs(i_swe_book_name):
#     swe_book_name = ["Ords", "Ordspråksboken", "Ords.", "Ordspr."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweEcclesiastes(i_swe_book_name):
#     swe_book_name = ["Pred", "Predikaren", "Pred."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweSongofSongs(i_swe_book_name):
#     swe_book_name = ["Höga V", "Höga Visan", "Höga V.", "Höga v."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweIsaiah(i_swe_book_name):
#     swe_book_name = ["Jes", "Jesaja", "Jes."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweJeremiah(i_swe_book_name):
#     swe_book_name = ["Jer", "Jeremia", "Jer."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweLamentations(i_swe_book_name):
#     swe_book_name = ["Klag", "Klagovisorna", "Klag."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweEzekiel(i_swe_book_name):
#     swe_book_name = ["Hes", "Hesekiel", "Hes."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweDaniel(i_swe_book_name):
#     swe_book_name = ["Dan", "Daniel", "Dan."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweHosea(i_swe_book_name):
#     swe_book_name = ["Hos", "Hosea", "Hos."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweJoel(i_swe_book_name):
#     swe_book_name = ["Joel", "Joe", "Joe."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweAmos(i_swe_book_name):
#     swe_book_name = ["Amos", "Amo", "Amo."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweObadiah(i_swe_book_name):
#     swe_book_name = ["Ob", "Obadja", "Ob.", "Oba."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweJonah(i_swe_book_name):
#     swe_book_name = ["Jon", "Jona", "Jon."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweMicah(i_swe_book_name):
#     swe_book_name = ["Mik", "Mika", "Mik."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweNahum(i_swe_book_name):
#     swe_book_name = ["Nah", "Nahum", "Nah."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweHabakkuk(i_swe_book_name):
#     swe_book_name = ["Hab", "Habakkuk", "Hab."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweZephaniah(i_swe_book_name):
#     swe_book_name = ["Sef", "Sefanja", "Sef."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweHaggai(i_swe_book_name):
#     swe_book_name = ["Hagg", "Haggai", "Hag."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweZechariah(i_swe_book_name):
#     swe_book_name = ["Sak", "Sakarja,", "Sak."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweMalachi(i_swe_book_name):
#     swe_book_name = ["Mal", "Malaki", "Mal."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweMatthew(i_swe_book_name):
#     swe_book_name = ["Matt", "Matteus", "Matt.", "Mat.", "Matteusevangeliet"]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweMark(i_swe_book_name):
#     swe_book_name = ["Mark", "Markus", "Mar.", "Markusevangeliet"]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweLuke(i_swe_book_name):
#     swe_book_name = ["Luk", "Lukas", "Luk.", "Lukasevangeliet"]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweJohn(i_swe_book_name):
#     swe_book_name = ["Joh", "Johannes", "Joh.", "Johannesevangeliet"]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweActs(i_swe_book_name):
#     swe_book_name = ["Apg", "Apostlagärningarna", "Apg."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweRomans(i_swe_book_name):
#     swe_book_name = ["Rom", "Romarbrevet", "Rom.", "Romarna"]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def swe1Corinthians(i_swe_book_name):
#     swe_book_name = ["Kor", "1 Kor", "1 Korintierbrevet", "1Kor", "1Kor.", "Förste Korintierbrevet",
#                      "Första Korintierbrevet", "Första Kor", "Första Kor."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def swe2Corinthians(i_swe_book_name):
#     swe_book_name = ["Kor", "2 Kor", "2 Korintierbrevet", "2Kor", "2Kor.", "Andra Korintierbrevet",
#                      "Andre Korintierbrevet", "Andra Kor", "Andra Kor."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweGalatians(i_swe_book_name):
#     swe_book_name = ["Gal", "Galaterbrevet", "Gal."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweEphesians(i_swe_book_name):
#     swe_book_name = ["Ef", "Efesierbrevet", "Ef.", "Efésierbrevet"]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def swePhilippians(i_swe_book_name):
#     swe_book_name = ["Fil", "Filipperbrevet", "Fil."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweColossians(i_swe_book_name):
#     swe_book_name = ["Kol", "Kolosserbrevet", "Kol."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def swe1Thessalonians(i_swe_book_name):
#     swe_book_name = ["Thess", "1 Thess", "1 Thessalonikerbrevet", "1 Thess.", "Första Thessalonikerbrevet",
#                      "Första Thess", "Första Thess."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def swe2Thessalonians(i_swe_book_name):
#     swe_book_name = ["Thess", "2 Thess", "2 Thessalonikerbrevet", "2 Thess.", "Andra Thessalonikerbrevet",
#                      "Andra Thess", "Andra Thess."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def swe1Timothy(i_swe_book_name):
#     swe_book_name = ["Tim", "1 Tim", "1 Timoteusbrevet", "1 Tim.", "Första Timoteusbrevet", "Första Tim.", "Första Tim"]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def swe2Timothy(i_swe_book_name):
#     swe_book_name = ["Tim", "2 Tim", "2 Timoteusbrevet", "2 Tim.", "Andra Timoteusbrevet", "Andra Tim.", "Andra Tim"]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweTitus(i_swe_book_name):
#     swe_book_name = ["Tit", "Titusbrevet", "Tit.", "Titus"]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def swePhilemon(i_swe_book_name):
#     swe_book_name = ["Filem", "Filemon", "Filem.", "Brevet till Filemon"]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweHebrews(i_swe_book_name):
#     swe_book_name = ["Hebr", "Hebreerbrevet", "Hebr."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweJames(i_swe_book_name):
#     swe_book_name = ["Jak", "Jakobsbrevet", "Jak.", "Jakob"]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def swe1Peter(i_swe_book_name):
#     swe_book_name = ["Petr", "1 Petr", "1 Petrusbrevet", "Första Petrusbrevet", "Första Petr", "Första Petr.", "1Petr.",
#                      "1 Petr."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def swe2Peter(i_swe_book_name):
#     swe_book_name = ["Petr", "2 Petr", "2 Petrusbrevet", "Andra Petrusbrevet", "Andra Petr", "Andra Petr.", "2Petr.",
#                      "2 Petr."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def swe1John(i_swe_book_name):
#     swe_book_name = ["Joh", "1 Joh", "1 Johannesbrevet", "1 Joh.", "Första Johannesbrevet,", "Första Joh.",
#                      "Första Joh"]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def swe2John(i_swe_book_name):
#     swe_book_name = ["Joh", "2 Joh", "2 Johannesbrevet", "2 Joh.", "Andra Johannesbrevet,", "Andra Joh.", "Andra Joh"]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def swe3John(i_swe_book_name):
#     swe_book_name = ["Joh", "3 Joh", "3 Johannesbrevet", "3 Joh.", "Tredje Johannesbrevet,", "Tredje Joh.",
#                      "Tredje Joh"]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweJude(i_swe_book_name):
#     swe_book_name = ["Jud", "Judasbrevet", "Jud."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
#
# def sweRevelation(i_swe_book_name):
#     swe_book_name = ["Upp", "Uppenbarelseboken", "Upp."]
#     if swe_book_name.__contains__(i_swe_book_name):
#         return True
#     else:
#         return False
#
# # #############################################
# #
# # def get_references_from_english_text(input_file_and_folder_name, output_folder_and_file_name):        # 2019-09-19 Not used in updated project code
# #     # encoding "Latin-1" "utf-8" "ISO-8859-1"
# #     with open(input_file_and_folder_name, encoding="latin-1") as f:
# #         t_all_lines = f.readlines()
# #         print("##############################################")
# #         print("### Get Bible references from English text ###")
# #         print("##############################################" + '\n')
# #         # print("148 t_all_lines: " + t_all_lines.__str__())
# #         for t_line in t_all_lines:
# #             try:
# #                 word_list = []
# #                 word_list = t_line.split(" ")
# #                 # print("Wordstring###: " + word_list.__str__())
# #
# #                 for word in word_list:
# #                     for char in word:
# #                         if char in "ï¿½?.!/;()":
# #                             word = word.replace(char, '')
# #                     # print("1528 Word¤¤: " +word)
# #
# #                     # word.replace("(", '')
# #                     # print("162 word: " +word)
# #
# #                     if eng_book_names_no_prefix_list.__contains__(word):
# #
# #                         # print("166 Reference: " + word + " Line: " + t_line)
# #                         bible_book_number = get_book_number_from_english_book_names(word)
# #                         # print("168 bible_book_number: " +bible_book_number.__str__())
# #
# #                         if bible_book_number < 68:
# #
# #                             if bible_book_number == 9:  # OK
# #                                 bible_book_number = get_samuel_english_bible_book_number(t_line)
# #
# #                             elif bible_book_number == 11:  # OK
# #                                 bible_book_number = get_kings_english_bible_book_number(t_line)
# #
# #                             elif bible_book_number == 13:  # OK
# #                                 bible_book_number = get_kings_english_bible_book_number(t_line)
# #
# #                             elif bible_book_number == 16:  # OK
# #                                 bible_book_number = get_chronicles_english_bible_book_number(t_line)
# #
# #                             elif bible_book_number == 46:  # OK
# #                                 bible_book_number = get_corinthians_english_bible_book_number(t_line)
# #
# #                             elif bible_book_number == 52:  # OK
# #                                 bible_book_number = get_thessalonians_english_bible_book_number(t_line)
# #
# #                             elif bible_book_number == 54:  # OK
# #                                 bible_book_number = get_timothy_english_bible_book_number(t_line)
# #
# #                             elif bible_book_number == 60:  # OK
# #                                 bible_book_number = get_peter_english_bible_book_number(t_line)
# #
# #                             elif bible_book_number == 62:  # OK
# #                                 bible_book_number = get_john_english_bible_book_number(t_line)
# #
# #                             # print("bibleBookNumber: " + bible_book_number.__str__()+" " + t_one_line)
# #                             t_chapter_start_index = t_line.index(word)
# #
# #                             if bible_book_number in eng_book_numbers_with_prefix_list:
# #                                 t_chapter_start_index = t_chapter_start_index - 2
# #
# #                             # print("t_chapter_start-index: " +t_chapter_start_index.__str__())
# #                             t_book = t_line[t_chapter_start_index:]
# #                             # print("2276: " +bible_book_number.__str__())
# #
# #                             t_reference_end = t_line[t_chapter_start_index + 1:]
# #
# #                             if t_book.find(":") > -1:
# #
# #                                 # Remove any characters after last digit
# #                                 # last_digit = max([int(s) for s in t_book.split() if s.isdigit()])
# #                                 # print("line & lastdigit: " +t_line + " " +last_digit.__str__())
# #
# #                                 ## Remove unneeded characters
# #                                 #  �  ï¿½: €   â  € 
# #                                 # print("ASCII number for : "+ ord('').__str__())      # ASCII number for : 147
# #                                 # print("ASCII number for €: "+ ord('€').__str__())       # ASCII number for €: 8364
# #                                 # print("ASCII number for : "+ ord('').__str__())      # ASCII number for €: 128
# #                                 # print("ASCII number for �: "+ ord('�').__str__())     # ASCII number for �: 65533
# #                                 # print("ASCII number for ï: "+ ord('ï').__str__())       # ASCII number for �: 239
# #                                 # print("ASCII number for ½: "+ ord('½').__str__())       # ASCII number for �: 189
# #                                 # print("ASCII number for : " + ord(':').__str__())       # ASCII number for �: 58
# #                                 # print("ASCII number for â " + ord('â').__str__())       # ASCII number for �: 226
# #
# #                                 # print("2214 Before removal of char: t_book: " +t_book)
# #                                 for char in t_book:
# #                                     # if char in "ï":
# #                                     #     t_book = t_book.replace(char, '-')
# #                                     #   print("ASCII number for : "+ ord('').__str__())   # ASCII number for : 147
# #                                     # w = w.replace('\xa0', ' ')
# #                                     t_book = t_book.replace('\u0128', '')
# #                                     t_book = t_book.replace('\u0147', '')
# #                                     t_book = t_book.replace('\u8364', '')
# #                                     if char in "â":
# #                                         t_book = t_book.replace(char, '-')
# #                                     if char in "?!/()[];ââ":
# #                                         t_book = t_book.replace(char, '')
# #                                         if t_book[0] == " ":
# #                                             t_book = t_book[1:]
# #
# #                                 # print("t_book: " + t_book.__str__())
# #                                 index = get_index_of_last_digit_in_string(t_book)
# #                                 # print("246 Index: " +index.__str__())
# #
# #                                 t_book = t_book[0:index] + "\n"
# #
# #                                 if not t_book.endswith(": \n"):
# #                                     t_verses.append(t_book)
# #                                     # print(t_book)
# #
# #                         else:
# #                             t_standard_book_name = get_english_standard_book_name(bible_book_number)
# #                             # print("2269: " + t_standard_book_name)
# #
# #                             # print("2273 Bible book number: " + bible_book_number.__str__())
# #
# #                             # t_book_and_ref_out = getSwedishBibleBookName(getBibleBookNumber(t_book)) + " " +t_reference_end
# #                             t_verses.append(t_book_and_ref_out)
# #                             # print("Book: ", t_book, "Swedish biblebook: ", getSwedishBibleBookName(getBibleBookNumber(t_book)), t_reference_end)
# #                             # print(t_book_and_ref_out)
# #                     # else:
# #                     #     #print("1865 word: " +word)
# #                     #     print("")
# #
# #             except ValueError:
# #                 if t_line is not " ":
# #                     print("ValueError" + t_line)
# #
# #         # print("t_verses: " + t_verses.__str__())
# #         write_list_to_file(get_input_file_folder_and_name(output_folder_and_file_name), t_verses)
# #
# #         input_file_path_and_name, output_file_path_and_name = get_input_and_output_backup_file_and_folder_names(
# #             output_folder_and_file_name, sub_folder_for_logfiles)
# #         # print("input_file_path_and_name: " + input_file_path_and_name)
# #         # print("output_file_path_and_name: " + output_file_path_and_name)
# #         backup_file(i_input_file_path_and_name=input_file_path_and_name,
# #                     i_output_file_path_and_name=output_file_path_and_name)
# #
# #     return ()
# #
#
#
# # def replace_common_from_list(i_replace_lists, out_string):
# #     # print("112 out_string: " +out_string)
# #     for t_replace_list in i_replace_lists:
# #         out_string = out_string.replace(t_replace_list[0], t_replace_list[1])
# #         # print("872 out_string: " +out_string)
# #         out_string = remove_space_in_bible_verse1(out_string)
# #         out_string = remove_space_in_bible_verse2(out_string)
# #         out_string = remove_space_in_bible_verse3(out_string)
# #         out_string = remove_space_in_bible_verse4(out_string)
# #         out_string = remove_space_in_bible_verse5(out_string)
# #         # print("121 out_string: " +out_string)
# #         out_string = add_space_in_bible_verse1(out_string)
# #         out_string = add_space_in_bible_verse2(out_string)
# #         out_string = remove_dot_in_bible_verse1(out_string)
# #         # out_string = remove_dot_in_bible_verse2(out_string)  # 2019-09-08 Does not work OK
# #         out_string = remove_dot_in_bible_verse3(out_string)
# #         out_string = remove_dot_in_bible_verse4(out_string)
# #         out_string = change_colon_to_semicolon1(out_string)
# #         out_string = change_colon_to_semicolon2(out_string)
# #         out_string = change_colon_to_semicolon3(out_string)
# #         # print("130 out_string: " +out_string)
# #     return out_string
#
#
#
# #
# # def get_references_from_swedish_text(input_file, output_file):
# #     # encoding "Latin-1" "utf-8" "ISO-8859-1"
# #     with open(input_file, encoding=ENCODING) as f:
# #         t_all_lines = f.readlines()
# #
# #         print("####################################################")
# #         print("### Result from get_references_from_swedish_text ###")
# #         print("####################################################" + '\n')
# #         for t_line in t_all_lines:
# #             try:
# #                 word_list = []
# #                 word_list = t_line.split(" ")
# #                 # print("Wordstring###: " + word_list.__str__())
# #
# #                 for word in word_list:
# #                     for char in word:
# #                         if char in "ï¿½?.!/;()":
# #                             word = word.replace(char, '')
# #                     # print("1528 Word¤¤: " +word)
# #
# #                     # word.replace("(", '')
# #                     # print("1528: " +word)
# #
# #                     if swe_book_names_no_prefix_list.__contains__(word):
# #
# #                         # print("Reference: " + word + " Line: " + t_line)
# #                         bible_book_number = get_bible_book_number_from_swedish_names(word)
# #                         # print("2051 bible_book_number: " +bible_book_number.__str__())
# #
# #                         if bible_book_number < 68:
# #
# #                             if bible_book_number == 1:  # OK
# #                                 bible_book_number = get_genesis_swedish_bible_book_number(t_line)
# #
# #                             elif bible_book_number == 9:  # OK
# #                                 bible_book_number = get_samuel_swedish_bible_book_number(t_line)
# #
# #                             elif bible_book_number == 11:  # OK
# #                                 bible_book_number = get_kings_swedish_bible_book_number(t_line)
# #
# #                             elif bible_book_number == 13:  # OK
# #                                 bible_book_number = get_kings_swedish_bible_book_number(t_line)
# #
# #                             elif bible_book_number == 46:  # OK
# #                                 bible_book_number = get_corinthians_swedish_bible_book_number(t_line)
# #
# #                             elif bible_book_number == 52:  # OK
# #                                 bible_book_number = get_thessalonians_swedish_bible_book_number(t_line)
# #
# #                             elif bible_book_number == 54:  # OK
# #                                 bible_book_number = get_timothy_swedish_bible_book_number(t_line)
# #
# #                             elif bible_book_number == 60:  # OK
# #                                 bible_book_number = get_peter_swedish_bible_book_number(t_line)
# #
# #                             elif bible_book_number == 43:  # OK
# #                                 bible_book_number = get_john_swedish_bible_book_number(t_line)
# #
# #                             # print("bibleBookNumber: " + bible_book_number.__str__()+" " + t_one_line)
# #                             t_chapter_start_index = t_line.index(word)
# #
# #                             if bible_book_number in swe_book_numbers_with_prefix_list:
# #                                 t_chapter_start_index = t_chapter_start_index - 2
# #
# #                             # print("t_chapter_start-index: " +t_chapter_start_index.__str__())
# #                             t_book = t_line[t_chapter_start_index:]
# #                             t_reference_end = t_line[t_chapter_start_index + 1:]
# #
# #                             # print("1621 Swedish book name: " +get_swedish_bible_book_name_from_bible_book_number(bible_book_number) +" " +word)
# #                             # print("1834Line with bible book: " +t_book)
# #
# #                             # print("2092 Before removal of char: t_book: " +t_book)
# #                             if t_book.find(":") > -1:
# #
# #                                 # Remove any characters after last digit
# #                                 # last_digit = max([int(s) for s in t_book.split() if s.isdigit()])
# #                                 # print("line & lastdigit: " +t_line + " " +last_digit.__str__())
# #
# #                                 ## Remove unneeded characters
# #                                 #  �  "ï¿½": 
# #
# #                                 for char in t_book:
# #                                     # if char in "ï":
# #                                     #     t_book = t_book.replace(char, '-')
# #                                     if char in "":
# #                                         t_book = t_book.replace(char, '-')
# #                                     if char in "?.!/()[];ââ":
# #                                         t_book = t_book.replace(char, '')
# #                                 if t_book[0] == " ":
# #                                     t_book = t_book[1:]
# #
# #                                 # print("t_book: " + t_book.__str__())
# #                                 index = get_index_of_last_digit_in_string(t_book)
# #                                 # print("Index: " +index.__str__())
# #
# #                                 t_book = t_book[0:index] + "\n"
# #
# #                                 if not t_book.endswith(": \n"):
# #                                     t_verses.append(t_book)
# #                                     # print(t_book)
# #
# #                     # else:
# #                     #     #print("1865 word: " +word)
# #                     #     print("")
# #
# #             except ValueError:
# #                 if t_line is not " ":
# #                     print("ValueError" + t_line)
# #         # print(t_verses)
# #
# #         write_list_to_file(output_file, t_verses)
# #
# #     return ()  # get_references_from_swedish_text
#
# #
# # def normalize_book_name_chapter_and_verse(i_replace_lists,
# #                                           i_input_file_name,
# #                                           i_norm_chapter_and_verses_file_name):
# #     with open(i_input_file_name, 'r', encoding=ENCODING) as myfile:
# #         t_text = myfile.read().replace(")", " ")
# #         # print("287 t_text: " + t_text.__str__())
# #
# #         f = open(i_norm_chapter_and_verses_file_name, "w+", encoding=ENCODING)
# #
# #         f.write(normalize_bible_book_names(i_replace_lists=i_replace_lists, out_string=t_text))
# #     f.close()
# #
# #     with open(i_norm_chapter_and_verses_file_name, 'r', encoding=ENCODING) as myfile:
# #         t_text = myfile.read()
# #         myfile.close()
# #
# #         # print("295 t_text: " + t_text.__str__())
# #
# #         out_string = replace_eng_from_list(i_replace_lists=i_replace_lists, out_string=t_text)
# #         # print("296 out_string: " +out_string)
# #         # print("286 i_output_file2: " + i_output_file2)
# #         write_list_to_file(i_norm_book_chapter_verse_file_name, out_string)
# #
# #     f.close()
# #
#
# eng_book_namesList2 = ["Genesis", "Gen", "Gen.",
#                        "Exodus", "Exo", "Exo.", "Ex.",
#                        "Leviticus", "Lev", "Lev",
#                        "Numbers", "Num", "Num",
#                        "Deuteronomy", "Deu", "Deu",
#                        "Joshua", "Jos", "Jos",
#                        "Judges", "Judg", "Judg.", "Jdg", "Jdg",
#                        "Ruth", "Ruth.", "Rth", "Rth",
#                        "1 Samuel", "1 Sam", "1 Sam.", "1Sa", "1Sa.", "I Samuel",
#                        "2 Samuel", "2 Sam", "2 Sam.", "2Sa", "2Sa.", "II Samuel",
#                        "1 Kings", "1 King", "1 King.", "1Ki", "1Ki.", "I Kings",
#                        "2 Kings", "2 King", "2 King.", "2Ki", "2Ki.", "II Kings",
#                        "1 Chronicles", "1 Chron", "1 Chron.", "1Ch", "1Ch.", "I Chronicles",
#                        "2 Chronicles", "2 Chron", "1 Chron.", "2Ch", "2Ch.", "II Chronicles",
#                        "Ezra", "Ezr", "Ezr.",
#                        "Nehemiah", "Neh", "Neh.",
#                        "Esther", "Est", "Est.",
#                        "Job", "Job.", "Psalm", "Psa", "Psa.",
#                        "Proverbs", "proverbs", "Pro", "Pro.",
#                        "Ecclesiastes", "Ecc", "Ecc.",
#                        "Song of Songs", "Son", "Son.",
#                        "Isaiah", "Isa", "Isa.",
#                        "Jeremiah", "Jer", "Jer.",
#                        "Lamentations", "Lam", "Lam.",
#                        "Ezekiel", "Eze", "Eze.",
#                        "Daniel", "Dan", "Dan.",
#                        "Hosea", "Hos", "Hos.",
#                        "Joel", "Joe", "Joe.",
#                        "Amos", "Amo", "Amo.",
#                        "Obadiah", "Oba", "Oba.",
#                        "Jonah", "Jon", "Jon.",
#                        "Micah", "Mic", "Mic.",
#                        "Nahum", "Nah", "Nah.",
#                        "Habakkuk", "Hab", "Hab.",
#                        "Zephaniah", "Zep", "Zep.",
#                        "Haggai", "Hag", "Hag.",
#                        "Zechariah", "Zec, Zec.",
#                        "Malachi", "Mal", "Mal.",
#                        "Matthew", "Mat", "Mat.",
#                        "Mark", "Mar", "Mar.",
#                        "Luke", "Luk", "Luk.",
#                        "John", "Joh", "Joh.",
#                        "Acts", "Act", "Act.",
#                        "Romans", "romans", "Rom", "Rom.",
#                        "1 Corinthians", "1st Corinthians", "first Corinthians", "1Co", "1Co.",
#                        "2 Corinthians", "2nd Corinthians", "second Corinthians 2Co", "2Co.",
#                        "Galatians", "galatians", "Gal", "Gal.",
#                        "Ephesians", "Eph", "Eph.",
#                        "Philippians", "Php", "Php.", "Philip", "Philip.",
#                        "Colossians", "Col", "Col.",
#                        "1 Thessalonians", "1Th", "1Th.",
#                        "2 Thessalonians", "2Th", "2Th.",
#                        "1 Timothy", "1st Timothy", "first Timothy", "1Ti", "1Ti.",
#                        "2 Timothy", "2nd Timothy", "second Timothy", "2Ti", "2Ti.",
#                        "Titus", "Tit", "Tit.",
#                        "Philemon", "Phm", "Phm.",
#                        "Hebrews", "hebrews", "Heb", "Heb.",
#                        "James", "Jas", "Jas.",
#                        "1 Peter", "1Pe", "1Pe.", "I Peter",
#                        "2 Peter", "2Pe", "2Pe.", "II Peter",
#                        "1 John", "1Jn", "1Jn.", "I John",
#                        "2 John", "2Jn", "2Jn.", "II John",
#                        "3 John", "3Jn", "3Jn.", "III John",
#                        "Jude", "Jud", "Jud.",
#                        "Revelation", "revelation", "Rev", "Rev."]   # 2019-10-01
#
#
# eng_book_names_no_prefix_list = [
#     "Genesis", "Gen", "Gen.",
#     "Exodus", "Exo", "Exo.", "Ex.",
#     "Leviticus", "Lev", "Lev",
#     "Numbers", "Num", "Num",
#     "Deuteronomy", "Deu", "Deu",
#     "Joshua", "Jos", "Jos",
#     "Judges", "Judg", "Judg.", "Jdg", "Jdg",
#     "Ruth", "Ruth.", "Rth", "Rth",
#     "Samuel", "Kings", "Chronicles",
#     "Ezra", "Ezr", "Ezr.",
#     "Nehemiah", "Neh", "Neh.",
#     "Esther", "Est", "Est.",
#     "Job", "Job.", "Psalms", "Psa", "Psa.", "Psalm",
#     "Proverbs", "proverbs", "Pro", "Pro.",
#     "Ecclesiastes", "Ecc", "Ecc.",
#     "Song of Songs", "Son", "Son.",
#     "Isaiah", "Isa", "Isa.",
#     "Jeremiah", "Jer", "Jer.",
#     "Lamentations", "Lam", "Lam.",
#     "Ezekiel", "Eze", "Eze.",
#     "Daniel", "Dan", "Dan.",
#     "Hosea", "Hos", "Hos.",
#     "Joel", "Joe", "Joe.",
#     "Amos", "Amo", "Amo.",
#     "Obadiah", "Oba", "Oba.",
#     "Jonah", "Jon", "Jon.",
#     "Micah", "Mic", "Mic.",
#     "Nahum", "Nah", "Nah.",
#     "Habakkuk", "Hab", "Hab.",
#     "Zephaniah", "Zep", "Zep.",
#     "Haggai", "Hag", "Hag.",
#     "Zechariah", "Zec, Zec.",
#     "Malachi", "Mal", "Mal.",
#     "Matthew", "Mat", "Mat.",
#     "Mark", "Mar", "Mar.",
#     "Luke", "Luk", "Luk.",
#     "John", "Joh", "Joh.",
#     "Acts", "Act", "Act.",
#     "Romans", "romans", "Rom", "Rom.",
#     "Corinthians",
#     "Galatians", "galatians", "Gal", "Gal.",
#     "Ephesians", "Eph", "Eph.",
#     "Philippians", "Php", "Php.", "Philip", "Philip.",
#     "Colossians", "Col", "Col.", "Thessalonians", "Timothy",
#     "Titus", "Tit", "Tit.",
#     "Philemon", "Phm", "Phm.",
#     "Hebrews", "hebrews", "Heb", "Heb.",
#     "James", "Jas", "Jas.",
#     "Peter", "John",
#     "Jude", "Jud", "Jud.",
#     "Revelation", "revelation", "Rev", "Rev."
# ]   # 2019-10-01
#
#
# eng_book_numbers_with_prefix_list = [1, 2, 3, 4, 5, 9, 10, 11, 12, 13, 14, 46, 47, 52, 53, 54, 55, 60, 61, 62, 63,
#                                      64]    # 2019-10-01
#
#
#
# def get_book_number_from_english_book_names(i_eng_bookname):
#     if engGenesis(i_eng_bookname):
#         t_bible_book_number = 1
#
#     elif engExodus(i_eng_bookname):
#         t_bible_book_number = 2
#
#     elif engLeviticus(i_eng_bookname):
#         t_bible_book_number = 3
#
#     elif engNumbers(i_eng_bookname):
#         t_bible_book_number = 4
#
#     elif engDeuteronomy(i_eng_bookname):
#         t_bible_book_number = 5
#
#     elif engLeviticus(i_eng_bookname):
#         t_bible_book_number = 6
#
#     elif engNumbers(i_eng_bookname):
#         t_bible_book_number = 7
#
#     elif engDeuteronomy(i_eng_bookname):
#         t_bible_book_number = 8
#
#     elif engJoshua(i_eng_bookname):
#         t_bible_book_number = 9
#
#     elif engJudges(i_eng_bookname):
#         t_bible_book_number = 10
#
#     elif engRuth(i_eng_bookname):
#         t_bible_book_number = 11
#
#     elif eng1Samuel(i_eng_bookname):
#         t_bible_book_number = 12
#
#     elif eng2Samuel(i_eng_bookname):
#         t_bible_book_number = 13
#
#     elif eng1Kings(i_eng_bookname):
#         t_bible_book_number = 14
#
#     elif eng2Kings(i_eng_bookname):
#         t_bible_book_number = 15
#
#     elif eng1Chronicles(i_eng_bookname):
#         t_bible_book_number = 16
#
#     elif eng2Chronicles(i_eng_bookname):
#         t_bible_book_number = 17
#
#     elif engEzra(i_eng_bookname):
#         t_bible_book_number = 18
#
#     elif engNehemiah(i_eng_bookname):
#         t_bible_book_number = 19
#
#     elif engEsther(i_eng_bookname):
#         t_bible_book_number = 20
#
#     elif engJob(i_eng_bookname):
#         t_bible_book_number = 21
#
#     elif engPsalms(i_eng_bookname):
#         t_bible_book_number = 22
#
#     elif engProverbs(i_eng_bookname):
#         t_bible_book_number = 23
#
#     elif engEcclesiastes(i_eng_bookname):
#         t_bible_book_number = 22
#
#     elif engSongofSongs(i_eng_bookname):
#         t_bible_book_number = 23
#
#     elif engIsaiah(i_eng_bookname):
#         t_bible_book_number = 24
#
#     elif engJeremiah(i_eng_bookname):
#         t_bible_book_number = 25
#
#     elif engLamentations(i_eng_bookname):
#         t_bible_book_number = 26
#
#     elif engEzekiel(i_eng_bookname):
#         t_bible_book_number = 27
#
#     elif engDaniel(i_eng_bookname):
#         t_bible_book_number = 28
#
#     elif engHosea(i_eng_bookname):
#         t_bible_book_number = 29
#
#     elif engJoel(i_eng_bookname):
#         t_bible_book_number = 30
#
#     elif engAmos(i_eng_bookname):
#         t_bible_book_number = 31
#
#     elif engObadiah(i_eng_bookname):
#         t_bible_book_number = 31
#
#     elif engJonah(i_eng_bookname):
#         t_bible_book_number = 32
#
#     elif engMicah(i_eng_bookname):
#         t_bible_book_number = 33
#
#     elif engNahum(i_eng_bookname):
#         t_bible_book_number = 34
#
#     elif engHabakkuk(i_eng_bookname):
#         t_bible_book_number = 35
#
#     elif engZephaniah(i_eng_bookname):
#         t_bible_book_number = 36
#
#     elif engHaggai(i_eng_bookname):
#         t_bible_book_number = 37
#
#     elif engZechariah(i_eng_bookname):
#         t_bible_book_number = 38
#
#     elif engMalachi(i_eng_bookname):
#         t_bible_book_number = 39
#
#     elif engMatthew(i_eng_bookname):
#         t_bible_book_number = 40
#
#     elif engMark(i_eng_bookname):
#         t_bible_book_number = 41
#
#     elif engLuke(i_eng_bookname):
#         t_bible_book_number = 42
#
#     elif engJohn(i_eng_bookname):
#         t_bible_book_number = 43
#
#     elif engActs(i_eng_bookname):
#         t_bible_book_number = 44
#
#     elif engRomans(i_eng_bookname):
#         t_bible_book_number = 45
#
#     elif eng1Corinthians(i_eng_bookname):
#         t_bible_book_number = 46
#
#     elif eng2Corinthians(i_eng_bookname):
#         t_bible_book_number = 47
#
#     elif engGalatians(i_eng_bookname):
#         t_bible_book_number = 48
#
#     elif engEphesians(i_eng_bookname):
#         t_bible_book_number = 49
#
#     elif engPhilippians(i_eng_bookname):
#         t_bible_book_number = 50
#
#     elif engColossians(i_eng_bookname):
#         t_bible_book_number = 51
#
#     elif eng1Thessalonians(i_eng_bookname):
#         t_bible_book_number = 52
#
#     elif eng2Thessalonians(i_eng_bookname):
#         t_bible_book_number = 53
#
#     elif eng1Timothy(i_eng_bookname):
#         t_bible_book_number = 54
#
#     elif eng2Timothy(i_eng_bookname):
#         t_bible_book_number = 55
#
#     elif engTitus(i_eng_bookname):
#         t_bible_book_number = 56
#
#     elif engPhilemon(i_eng_bookname):
#         t_bible_book_number = 57
#
#     elif engHebrews(i_eng_bookname):
#         t_bible_book_number = 58
#
#     elif engJames(i_eng_bookname):
#         t_bible_book_number = 59
#
#     elif eng1Peter(i_eng_bookname):
#         t_bible_book_number = 60
#
#     elif eng2Peter(i_eng_bookname):
#         t_bible_book_number = 61
#
#     elif eng1John(i_eng_bookname):
#         t_bible_book_number = 62
#
#     elif eng2John(i_eng_bookname):
#         t_bible_book_number = 63
#
#     elif eng3John(i_eng_bookname):
#         t_bible_book_number = 64
#
#     elif engJude(i_eng_bookname):
#         t_bible_book_number = 65
#
#     elif engRevelation(i_eng_bookname):
#         t_bible_book_number = 66
#     else:
#         # print ("Didn't find book")
#         t_bible_book_number = 67
#
#     # print ("Bible book number: ", t_bible_book_number)
#     return t_bible_book_number  # 2019-10-01
#
#
#
# # Procedures for CHECKING for English book NAMES
# # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
#
# def engGenesis(i_Eng_Bookname):
#     eng_book_name = ["Genesis", "Gen", "Gen."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engExodus(i_Eng_Bookname):
#     eng_book_name = ["Exodus", "Exo", "Exo.", "Ex."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engLeviticus(i_Eng_Bookname):
#     eng_book_name = ["Leviticus", "Lev", "Lev."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engNumbers(i_Eng_Bookname):
#     eng_book_name = ["Numbers", "Num", "Num."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engDeuteronomy(i_Eng_Bookname):
#     eng_book_name = ["Deuteronomy", "Deu", "Deu."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engJoshua(i_Eng_Bookname):
#     eng_book_name = ["Joshua", "Jos", "Jos"]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engJudges(i_Eng_Bookname):
#     eng_book_name = ["Judges", "Judg", "Judg.", "Jdg", "Jdg."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engRuth(i_Eng_Bookname):
#     eng_book_name = ["Ruth", "Ruth.", "Rth", "Rth."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def eng1Samuel(i_Eng_Bookname):
#     eng_book_name = ["1 Samuel", "1 Sam", "1 Sam.", "1Sa", "1Sa.", "I Samuel", "Samuel"]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def eng2Samuel(i_Eng_Bookname):
#     eng_book_name = ["2 Samuel", "2 Sam", "2 Sam.", "2Sa", "2Sa.", "II Samuel"]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def eng1Kings(i_Eng_Bookname):
#     eng_book_name = ["1 Kings", "1 King", "1 King.", "1Ki", "1Ki.", "I Kings", "Kings"]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def eng2Kings(i_Eng_Bookname):
#     eng_book_name = ["2 Kings", "2 King", "2 King.", "2Ki", "2Ki.", "II Kings"]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def eng1Chronicles(i_Eng_Bookname):
#     eng_book_name = ["1 Chronicles", "1 Chron", "1 Chron.", "1Ch", "1Ch.", "I Chronicles", "Chronicles"]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def eng2Chronicles(i_Eng_Bookname):
#     eng_book_name = ["2 Chronicles", "2 Chron", "1 Chron.", "2Ch", "2Ch.", "II Chronicles"]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engEzra(i_Eng_Bookname):
#     eng_book_name = ["Ezra", "Ezr", "Ezr."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engNehemiah(i_Eng_Bookname):
#     eng_book_name = ["Nehemiah", "Neh", "Neh."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engEsther(i_Eng_Bookname):
#     eng_book_name = ["Esther", "Est", "Est."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engJob(i_Eng_Bookname):
#     eng_book_name = ["Job", "Job."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engPsalms(i_Eng_Bookname):
#     eng_book_name = ["Psalm", "Psa", "Psa."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engProverbs(i_Eng_Bookname):
#     eng_book_name = ["Proverbs", "proverbs", "Pro", "Pro.", "Pr", "Pr."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engEcclesiastes(i_Eng_Bookname):
#     eng_book_name = ["Ecclesiastes", "Ecc", "Ecc."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engSongofSongs(i_Eng_Bookname):
#     eng_book_name = ["Song of Songs", "Son", "Son."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engIsaiah(i_Eng_Bookname):
#     eng_book_name = ["Isaiah", "Isa", "Isa."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engJeremiah(i_Eng_Bookname):
#     eng_book_name = ["Jeremiah", "Jer", "Jer."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engLamentations(i_Eng_Bookname):
#     eng_book_name = ["Lamentations", "Lam", "Lam."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engEzekiel(i_Eng_Bookname):
#     eng_book_name = ["Ezekiel", "Eze", "Eze."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engDaniel(i_Eng_Bookname):
#     eng_book_name = ["Daniel", "Dan", "Dan."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engHosea(i_Eng_Bookname):
#     eng_book_name = ["Hosea", "Hos", "Hos."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engJoel(i_Eng_Bookname):
#     eng_book_name = ["Joel", "Joe", "Joe."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engAmos(i_Eng_Bookname):
#     eng_book_name = ["Amos", "Amo", "Amo."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engObadiah(i_Eng_Bookname):
#     eng_book_name = ["Obadiah", "Oba", "Oba."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engJonah(i_Eng_Bookname):
#     eng_book_name = ["Jonah", "Jon", "Jon."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engMicah(i_Eng_Bookname):
#     eng_book_name = ["Micah", "Mic", "Mic."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engNahum(i_Eng_Bookname):
#     eng_book_name = ["Nahum", "Nah", "Nah."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engHabakkuk(i_Eng_Bookname):
#     eng_book_name = ["Habakkuk", "Hab", "Hab."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engZephaniah(i_Eng_Bookname):
#     eng_book_name = ["Zephaniah", "Zep", "Zep."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engHaggai(i_Eng_Bookname):
#     eng_book_name = ["Haggai", "Hag", "Hag."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engZechariah(i_Eng_Bookname):
#     eng_book_name = ["Zechariah", "Zec, Zec."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engMalachi(i_Eng_Bookname):
#     eng_book_name = ["Malachi", "Mal", "Mal."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engMatthew(i_Eng_Bookname):
#     eng_book_name = ["Matthew", "Mat", "Mat."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engMark(i_Eng_Bookname):
#     eng_book_name = ["Mark", "Mar", "Mar."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engLuke(i_Eng_Bookname):
#     eng_book_name = ["Luke", "Luk", "Luk."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engJohn(i_Eng_Bookname):
#     eng_book_name = ["John", "Joh", "Joh."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engActs(i_Eng_Bookname):
#     eng_book_name = ["Acts", "Act", "Act."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engRomans(i_Eng_Bookname):
#     eng_book_name = ["Romans", "romans", "Rom", "Rom."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def eng1Corinthians(i_Eng_Bookname):
#     eng_book_name = ["1 Corinthians", "1st Corinthians", "first Corinthians", "1Co", "1Co.", "Corinthians"]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def eng2Corinthians(i_Eng_Bookname):
#     eng_book_name = ["2 Corinthians", "2nd Corinthians", "second Corinthians 2Co", "2Co."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engGalatians(i_Eng_Bookname):
#     eng_book_name = ["Galatians", "galatians", "Gal", "Gal."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engEphesians(i_Eng_Bookname):
#     eng_book_name = ["Ephesians", "Eph", "Eph."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engPhilippians(i_Eng_Bookname):
#     eng_book_name = ["Philippians", "Php", "Php.", "Philip", "Philip."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engColossians(i_Eng_Bookname):
#     eng_book_name = ["Colossians", "Col", "Col."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def eng1Thessalonians(i_Eng_Bookname):
#     eng_book_name = ["1 Thessalonians", "1Th", "1Th.", "Thessalonians"]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def eng2Thessalonians(i_Eng_Bookname):
#     eng_book_name = ["2 Thessalonians", "2Th", "2Th."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def eng1Timothy(i_Eng_Bookname):
#     eng_book_name = ["1 Timothy", "1st Timothy", "first Timothy", "1Ti", "1Ti.", "Timothy"]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def eng2Timothy(i_Eng_Bookname):
#     eng_book_name = ["2 Timothy", "2nd Timothy", "second Timothy", "2Ti", "2Ti."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engTitus(i_Eng_Bookname):
#     eng_book_name = ["Titus", "Tit", "Tit."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engPhilemon(i_Eng_Bookname):
#     eng_book_name = ["Philemon", "Phm", "Phm."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engHebrews(i_Eng_Bookname):
#     eng_book_name = ["Hebrews", "hebrews", "Heb", "Heb."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engJames(i_Eng_Bookname):
#     eng_book_name = ["James", "Jas", "Jas."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def eng1Peter(i_Eng_Bookname):
#     eng_book_name = ["1 Peter", "1Pe", "1Pe.", "I Peter", "Peter"]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def eng2Peter(i_Eng_Bookname):
#     eng_book_name = ["2 Peter", "2Pe", "2Pe.", "II Peter"]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def eng1John(i_Eng_Bookname):
#     eng_book_name = ["1 John", "1Jn", "1 Jn", "1Jn.", "I John", "John"]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def eng2John(i_Eng_Bookname):
#     eng_book_name = ["2 John", "2Jn", "2 Jn", "2Jn.", "II John"]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def eng3John(i_Eng_Bookname):
#     eng_book_name = ["3 John", "3Jn", "3 Jn", "3Jn.", "III John"]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engJude(i_Eng_Bookname):
#     eng_book_name = ["Jude", "Jud", "Jud."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
# def engRevelation(i_Eng_Bookname):
#     eng_book_name = ["Revelation", "revelation", "Rev", "Rev."]
#     if eng_book_name.__contains__(i_Eng_Bookname):
#         return True
#     else:
#         return False
#
#
#
#
#
#
# def get_swedish_bible_book_name_from_bible_book_number(i_bible_book_number):  # 2019-09-19
#     t_bible_book_name = swe_bible_book_name_string[i_bible_book_number - 1]
#
#     return t_bible_book_name
#
#
# # <editor-fold desc="Description">
# def get_execution_mode(i_file):
#     with open(i_file, encoding=ENCODING) as f:
#         t_lines = f.readlines()
#
#         for t_line in t_lines:
#             # print("Config line: " + t_line)
#             try:
#                 # print("Config line: " + t_line + "t_line(0): " + t_line[0])
#                 if t_line[0] != "#":
#                     ##find_references_in_english_text
#                     # find_references_in_swedish_text
#                     if t_line.find("find_references_in_english_text") > -1:
#                         execution_mode = "english_text"
#                     elif t_line.find("find_references_in_swedish_text") > -1:
#                         execution_mode = "swedish_text"
#                     else:
#                         execution_mode = "none"
#             except ValueError:
#                 if t_line is not " ":
#                     print("ValueError" + t_line)
#         f.close
#     # print("Execution mode: " +execution_mode)
#     return execution_mode       # get_execution_mode
#
#
# # </editor-fold>
#
#
# def split_bible_reference_by_comma(i_str):          # 2019-10-08 07:37:17
#     # Test cases: "Hebr 8:13, 9:15", "Matt 24:9,11,24"
#     out_list = []
#     t_str = i_str.replace("[", "").replace("]", "").replace("\n", "").strip().lstrip()
#     #print("split_bible_reference_by_comma: " + t_str)
#     num_comma = t_str.count(",")                # Genesis 3:7, 21       "1 Krön 11, 12:23-40 25"
#     # Should tolerate "Psalms 39:6  COL, p"
#     if num_comma ==  0:
#         out_list.append(t_str +"\n")
#     else:
#         if t_str.__contains__(":"):
#             ix_comma = t_str.index(",")
#             ix_colon = t_str.index(":")
#             if ix_comma < ix_colon:
#                 out_list.append(i_str + "\n")
#                 return out_list
#             str_up_to_colon = t_str[0:ix_colon]
#             t_verses = t_str[ix_colon+1:]
#
#             if not t_verses.__contains__(","):
#                 out_list.append(i_str + "\n")
#                 return out_list
#             else:
#                 ix_comma = t_verses.index(",")          # "Psalms 39:6  COL, p"
#                 if t_verses.__contains__(" "):
#                     ix_space = t_verses.index(" ")
#                     if ix_comma > ix_space:
#                         out_list.append(i_str + "\n")
#                         return out_list
#                 v_now = t_verses[0:ix_comma]       # 1 Samuel 17:11, 24, 25
#                 i = 0
#                 while i < num_comma:
#                     if not hasNumbers(v_now):
#                         return out_list
#                     out_list.append(str_up_to_colon + ":" + v_now +"\n")
#                     if t_verses.__contains__(","):
#                         ix_comma = t_verses.index(",")
#                         t_verses = t_verses[ix_comma+1:].strip()
#                         if t_verses.__contains__(" "):
#                             v_now = t_verses[0:t_verses.index(" ")-1]
#                         else:
#                             if t_verses.__contains__(":"):      # 2019-10-10 'Hebr 8:13, 9:15'
#                                 out_list.append(t_verses + "\n")
#                                 return out_list
#                             v_now = t_verses
#                     i = i+1
#
#                 out_list.append(str_up_to_colon + ":" +v_now +"\n")
#
#     #print("85: " +out_list.__str__())
#     return out_list # split_bible_reference_by_comma
#
#
# def modify_2_consecutive_verses_to_range(i_str=str):           # "Luk 4:18, 19" => "Luk 4:18-19"
#     # Should also tolerate " Nehemiah 1:6, 7 .    4"
#     # Should also tolerate: "Psalms 39:6  COL, p"
#     # Skip Nehemiah 1:6-7
#     # Skip Genesis 10:32, 11:1
#
#     out_string = i_str
#     out_string = out_string.replace("\\n\'", " ").replace(")", " ").replace("\'", "").replace("\\x0", " ").rstrip()
#     print("216: " +out_string)
#     if (i_str.__contains__(":")) and \
#             not (i_str.__contains__("-")) and \
#             not (i_str.__contains__(";")) and \
#             not (i_str.__contains__('\n')
#             ):
#         ix_colon = i_str.index(":")
#         ref_up_to_colon = i_str[0:ix_colon+1]
#         if i_str.__contains__(","):
#             ix_comma = i_str.index(",")
#             verses = i_str[ix_colon:]
#             if not ix_colon < ix_comma:
#                 return out_string
#             if verses.__contains__(" "):
#                 ix_space = verses.index(" ")    # "Psalms 39:6  COL, p"
#                 if not hasNumbers(verses[ix_space:]):
#                     return out_string
#             tmp = i_str[ix_colon+1:ix_comma]         # "–"
#             if tmp.__contains__("-") or tmp.__contains__("–"):
#                 return out_string
#             else:
#                 tmp2 = i_str[ix_colon+1:ix_comma]
#                 v1 = int(i_str[ix_colon+1:ix_comma])
#
#             # Verify that there only is one comma in the reference
#             tmp3 = i_str[ix_comma+1:]
#             if not (tmp3.__contains__(",")) and \
#                     not (tmp3.__contains__(":")):
#                 v_list = verses.split(" ")
#                 if v_list.__len__() == 2:
#
#                     tmp = verses[verses.index(" "):]
#                     if tmp.__contains__("-") or tmp.__contains__("–"):  # 2019-10-10
#                         return out_string
#                     v2 = int(verses[verses.index(" "):])
#                     if v2-v1 == 1:  # Consecutive verses
#                         out_string = ref_up_to_colon + v1.__str__() +"-" +v2.__str__()
#                         # print("259 Merged two verses: " + out_string)
#
#     return out_string  # modify_2verses_to_range
#
#
# def get_samuel_english_bible_book_number(i_line):
#     t_bible_book_number = 9
#     if i_line.find("1 Sam") > -1:
#         t_bible_book_number = 9
#     elif i_line.find("1 Sam.") > -1:
#         t_bible_book_number = 9
#     elif i_line.find("1 Samuel") > -1:
#         t_bible_book_number = 9
#     elif i_line.find("First Samuel") > -1:
#         t_bible_book_number = 9
#     elif i_line.find("First Sam") > -1:
#         t_bible_book_number = 9
#     if i_line.find("2 Sam") > -1:
#         t_bible_book_number = 10
#     elif i_line.find("2 Sam.") > -1:
#         t_bible_book_number = 10
#     elif i_line.find("2 Samuel") > -1:
#         t_bible_book_number = 10
#     elif i_line.find("Second Samuel") > -1:
#         t_bible_book_number = 10
#     elif i_line.find("Second Sam") > -1:
#         t_bible_book_number = 10
#     return t_bible_book_number
#
#
# def get_kings_english_bible_book_number(i_line):
#     t_bible_book_number = 11
#     if i_line.find("1 King") > -1:
#         t_bible_book_number = 11
#     elif i_line.find("1 King.") > -1:
#         t_bible_book_number = 11
#     elif i_line.find("1 Kings") > -1:
#         t_bible_book_number = 11
#     elif i_line.find("First Kings") > -1:
#         t_bible_book_number = 11
#     elif i_line.find("First King") > -1:
#         t_bible_book_number = 11
#     if i_line.find("2 King") > -1:
#         t_bible_book_number = 12
#     elif i_line.find("2 King.") > -1:
#         t_bible_book_number = 12
#     elif i_line.find("2 Kings") > -1:
#         t_bible_book_number = 12
#     elif i_line.find("Second Kings") > -1:
#         t_bible_book_number = 12
#     elif i_line.find("Second Kung") > -1:
#         t_bible_book_number = 12
#     return t_bible_book_number
#
#
# def get_chronicles_english_bible_book_number(i_line):
#     t_bible_book_number = 13
#     if i_line.find("1 Chron") > -1:
#         t_bible_book_number = 13
#     elif i_line.find("1 Chron.") > -1:
#         t_bible_book_number = 13
#     elif i_line.find("1 Chronicles") > -1:
#         t_bible_book_number = 13
#     elif i_line.find("First Chronicles") > -1:
#         t_bible_book_number = 13
#     elif i_line.find("First Chron") > -1:
#         t_bible_book_number = 13
#     if i_line.find("2 Chron") > -1:
#         t_bible_book_number = 14
#     elif i_line.find("2 Chron.") > -1:
#         t_bible_book_number = 14
#     elif i_line.find("2 Chronicles") > -1:
#         t_bible_book_number = 14
#     elif i_line.find("Second Chronicles") > -1:
#         t_bible_book_number = 14
#     elif i_line.find("Second Krön") > -1:
#         t_bible_book_number = 14
#     return t_bible_book_number
#
#
# def get_corinthians_english_bible_book_number(i_line):
#     t_bible_book_number = 46
#     if i_line.find("1 Cor") > -1:
#         t_bible_book_number = 46
#     elif i_line.find("1 Cor.") > -1:
#         t_bible_book_number = 46
#     elif i_line.find("1 Corinthians") > -1:
#         t_bible_book_number = 46
#     elif i_line.find("Första Corinthians") > -1:
#         t_bible_book_number = 46
#     elif i_line.find("First Cor") > -1:
#         t_bible_book_number = 46
#     if i_line.find("2 Cor") > -1:
#         t_bible_book_number = 47
#     elif i_line.find("2 Cor.") > -1:
#         t_bible_book_number = 47
#     elif i_line.find("2 Corinthians") > -1:
#         t_bible_book_number = 47
#     elif i_line.find("Second Corinthians") > -1:
#         t_bible_book_number = 47
#     elif i_line.find("Second Cor") > -1:
#         t_bible_book_number = 47
#     return t_bible_book_number
#
#
# def get_thessalonians_english_bible_book_number(i_line):
#     t_bible_book_number = 52
#     if i_line.find("1 Thess") > -1:
#         t_bible_book_number = 52
#     elif i_line.find("1 Thess.") > -1:
#         t_bible_book_number = 52
#     elif i_line.find("1 Thessalonians") > -1:
#         t_bible_book_number = 52
#     elif i_line.find("First Thessalonians") > -1:
#         t_bible_book_number = 52
#     elif i_line.find("First Thess") > -1:
#         t_bible_book_number = 52
#     if i_line.find("2 Thess") > -1:
#         t_bible_book_number = 53
#     elif i_line.find("2 Thess.") > -1:
#         t_bible_book_number = 53
#     elif i_line.find("2 Thessalonians") > -1:
#         t_bible_book_number = 53
#     elif i_line.find("Second Thessalonians") > -1:
#         t_bible_book_number = 53
#     elif i_line.find("Second Thess") > -1:
#         t_bible_book_number = 53
#     return t_bible_book_number
#
#
# def get_timothy_english_bible_book_number(i_line):
#     t_bible_book_number = 54
#     if i_line.find("1 Tim") > -1:
#         t_bible_book_number = 54
#     elif i_line.find("1 Tim.") > -1:
#         t_bible_book_number = 54
#     elif i_line.find("1 Timothy") > -1:
#         t_bible_book_number = 54
#     elif i_line.find("First Timothy") > -1:
#         t_bible_book_number = 54
#     elif i_line.find("First Tim") > -1:
#         t_bible_book_number = 54
#     if i_line.find("2 Tim") > -1:
#         t_bible_book_number = 55
#     elif i_line.find("2 Tim.") > -1:
#         t_bible_book_number = 55
#     elif i_line.find("2 Timothy") > -1:
#         t_bible_book_number = 55
#     elif i_line.find("Second Timothy") > -1:
#         t_bible_book_number = 55
#     elif i_line.find("Second Tim") > -1:
#         t_bible_book_number = 55
#     return t_bible_book_number
#
#
# def get_peter_english_bible_book_number(i_line):
#     t_bible_book_number = 60
#     if i_line.find("1 Pet") > -1:
#         t_bible_book_number = 60
#     elif i_line.find("1 Pet.") > -1:
#         t_bible_book_number = 60
#     elif i_line.find("1 Peter") > -1:
#         t_bible_book_number = 60
#     elif i_line.find("First Peter") > -1:
#         t_bible_book_number = 60
#     elif i_line.find("First Petr") > -1:
#         t_bible_book_number = 60
#     if i_line.find("2 Petr") > -1:
#         t_bible_book_number = 61
#     elif i_line.find("2 Petr.") > -1:
#         t_bible_book_number = 61
#     elif i_line.find("2 Peter") > -1:
#         t_bible_book_number = 61
#     elif i_line.find("Second Peter") > -1:
#         t_bible_book_number = 61
#     elif i_line.find("Second Petr") > -1:
#         t_bible_book_number = 61
#     return t_bible_book_number
#
#
# def get_john_english_bible_book_number(i_line):
#     t_bible_book_number = 43
#     # print("Line: " + i_line)
#     # if i_line.find("Joh") > -1:
#     #     t_bible_book_number = 43
#     if i_line.find("1 Joh") > -1:
#         t_bible_book_number = 62
#     elif i_line.find("1 Joh.") > -1:
#         t_bible_book_number = 62
#     elif i_line.find("1 John") > -1:
#         t_bible_book_number = 62
#     elif i_line.find("First John") > -1:
#         t_bible_book_number = 62
#     elif i_line.find("First Joh") > -1:
#         t_bible_book_number = 62
#     if i_line.find("2 Joh") > -1:
#         t_bible_book_number = 63
#     elif i_line.find("2 Joh.") > -1:
#         t_bible_book_number = 63
#     elif i_line.find("2 John") > -1:
#         t_bible_book_number = 63
#     elif i_line.find("Second John") > -1:
#         t_bible_book_number = 63
#     elif i_line.find("Second Joh") > -1:
#         t_bible_book_number = 63
#     elif i_line.find("3 Joh") > -1:
#         t_bible_book_number = 64
#     elif i_line.find("3 Joh.") > -1:
#         t_bible_book_number = 64
#     elif i_line.find("3 John") > -1:
#         t_bible_book_number = 64
#     elif i_line.find("Third John") > -1:
#         t_bible_book_number = 64
#     elif i_line.find("Third Joh") > -1:
#         t_bible_book_number = 64
#
#     return t_bible_book_number
#
#     def test_get_genesis_swedish_bible_book_number1(self):
#         self.assertEqual(
#             1,
#             get_genesis_swedish_bible_book_number("1 Mos"))
#
#     def test_get_genesis_swedish_bible_book_number2(self):
#         self.assertEqual(
#             2,
#             get_genesis_swedish_bible_book_number("2 Mos"))
#
#     def test_get_genesis_swedish_bible_book_number3(self):
#         self.assertEqual(
#             3,
#             get_genesis_swedish_bible_book_number("3 Mos"))
#
#     def test_get_genesis_swedish_bible_book_number4(self):
#         self.assertEqual(
#             4,
#             get_genesis_swedish_bible_book_number("4 Mos"))
#
#     def test_get_genesis_swedish_bible_book_number5(self):
#         self.assertEqual(
#             5,
#             get_genesis_swedish_bible_book_number("5 Mos"))
#
#     def test_get_samuel_swedish_bible_book_number1(self):
#         self.assertEqual(
#             9,
#             get_samuel_swedish_bible_book_number("1 Sam."))
#
#     def test_get_samuel_swedish_bible_book_number2(self):
#         self.assertEqual(
#             10,
#             get_samuel_swedish_bible_book_number("2 Sam."))
#
#     def test_get_kings_swedish_bible_book_number1(self):
#         self.assertEqual(
#             11
#             , get_kings_swedish_bible_book_number("1 Kung"))
#
#     def test_get_kings_swedish_bible_book_number2(self):
#         self.assertEqual(
#             12
#             , get_kings_swedish_bible_book_number("2 Kung"))
#
#     def test_get_chronicles_swedish_bible_book_number1(self):
#         self.assertEqual(
#             13
#             , get_chronicles_swedish_bible_book_number("1 Krön"))
#
#     def test_get_chronicles_swedish_bible_book_number2(self):
#         self.assertEqual(
#             14
#             , get_chronicles_swedish_bible_book_number("2 Krön"))
#
#
#     def test_get_john_english_bible_book_number1(self):
#         self.assertEqual(
#             62,
#             get_john_english_bible_book_number("1 John"))
#
#     def test_get_john_english_bible_book_number2(self):
#         self.assertEqual(
#             63,
#             get_john_english_bible_book_number("2 John"))
#
#     def test_get_john_english_bible_book_number3(self):
#         self.assertEqual(
#             64,
#             get_john_english_bible_book_number("3 John"))
#
#     def test_get_john_english_bible_book_number4(self):
#         self.assertEqual(
#             43,
#             get_john_english_bible_book_number("John"))
#
#     def test_get_peter_english_bible_book_number1(self):
#         self.assertEqual(
#             60,
#             get_peter_english_bible_book_number("1 Peter"))
#
#     def test_get_peter_english_bible_book_number2(self):
#         self.assertEqual(
#             61
#             , get_peter_english_bible_book_number("2 Peter"))
#
#     def test_get_timothy_english_bible_book_number1(self):
#         self.assertEqual(
#             54
#             , get_timothy_english_bible_book_number("1 Timothy"))
#
#     def test_get_timothy_english_bible_book_number2(self):
#         self.assertEqual(
#             55
#             , get_timothy_english_bible_book_number("2 Timothy"))
#
#     def test_get_thessalonians_english_bible_book_number1(self):
#         self.assertEqual(
#             52
#             , get_thessalonians_english_bible_book_number("1 Thessalonians"))
#
#     def test_get_thessalonians_english_bible_book_number2(self):
#         self.assertEqual(
#             53
#             , get_thessalonians_english_bible_book_number("2 Thessalonians"))
#
#     def test_get_corinthians_english_bible_book_number1(self):
#         self.assertEqual(
#             46
#             , get_corinthians_english_bible_book_number("1 Corinthians"))
#
#     def test_get_corinthians_english_bible_book_number2(self):
#         self.assertEqual(
#             47
#             , get_corinthians_english_bible_book_number("2 Corinthians"))
#
#    def test_get_samuel_english_bible_book_number1(self):
#         self.assertEqual(
#             9
#             , get_samuel_english_bible_book_number("1 Samuel"))
#
#     def test_get_samuel_english_bible_book_number2(self):
#         self.assertEqual(
#             10
#             , get_samuel_english_bible_book_number("2 Samuel"))
#
#     def test_get_kings_english_bible_book_number1(self):
#         self.assertEqual(
#             11
#             , get_kings_english_bible_book_number("1 Kings"))
#
#     def test_get_kings_english_bible_book_number2(self):
#         self.assertEqual(
#             12
#             , get_kings_english_bible_book_number("2 Kings"))
#
#     def test_get_chronicles_english_bible_book_number1(self):
#         self.assertEqual(
#             13
#             , get_chronicles_english_bible_book_number("1 Chronicles"))
#
#     def test_get_chronicles_english_bible_book_number2(self):
#         self.assertEqual(
#             14
#             , get_chronicles_english_bible_book_number("2 Chronicles"))


def get_samuel_english_bible_book_number(i_line):
    if i_line.find("1 Sam") > -1:
        t_bible_book_number = 9
    elif i_line.find("1 Sam.") > -1:
        t_bible_book_number = 9
    elif i_line.find("1 Samuel") > -1:
        t_bible_book_number = 9
    elif i_line.find("First Samuel") > -1:
        t_bible_book_number = 9
    elif i_line.find("First Sam") > -1:
        t_bible_book_number = 9
    if i_line.find("2 Sam") > -1:
        t_bible_book_number = 10
    elif i_line.find("2 Sam.") > -1:
        t_bible_book_number = 10
    elif i_line.find("2 Samuel") > -1:
        t_bible_book_number = 10
    elif i_line.find("Second Samuel") > -1:
        t_bible_book_number = 10
    elif i_line.find("Second Sam") > -1:
        t_bible_book_number = 10
    return t_bible_book_number


def get_kings_english_bible_book_number(i_line):
    t_bible_book_number = 11
    if i_line.find("1 King") > -1:
        t_bible_book_number = 11
    elif i_line.find("1 King.") > -1:
        t_bible_book_number = 11
    elif i_line.find("1 Kings") > -1:
        t_bible_book_number = 11
    elif i_line.find("First Kings") > -1:
        t_bible_book_number = 11
    elif i_line.find("First King") > -1:
        t_bible_book_number = 11
    if i_line.find("2 King") > -1:
        t_bible_book_number = 12
    elif i_line.find("2 King.") > -1:
        t_bible_book_number = 12
    elif i_line.find("2 Kings") > -1:
        t_bible_book_number = 12
    elif i_line.find("Second Kings") > -1:
        t_bible_book_number = 12
    elif i_line.find("Second Kung") > -1:
        t_bible_book_number = 12
    return t_bible_book_number


def get_chronicles_english_bible_book_number(i_line):
    t_bible_book_number = 13
    if i_line.find("1 Chron") > -1:
        t_bible_book_number = 13
    elif i_line.find("1 Chron.") > -1:
        t_bible_book_number = 13
    elif i_line.find("1 Chronicles") > -1:
        t_bible_book_number = 13
    elif i_line.find("First Chronicles") > -1:
        t_bible_book_number = 13
    elif i_line.find("First Chron") > -1:
        t_bible_book_number = 13
    if i_line.find("2 Chron") > -1:
        t_bible_book_number = 14
    elif i_line.find("2 Chron.") > -1:
        t_bible_book_number = 14
    elif i_line.find("2 Chronicles") > -1:
        t_bible_book_number = 14
    elif i_line.find("Second Chronicles") > -1:
        t_bible_book_number = 14
    elif i_line.find("Second Krön") > -1:
        t_bible_book_number = 14
    return t_bible_book_number


def get_corinthians_english_bible_book_number(i_line):
    t_bible_book_number = 46
    if i_line.find("1 Cor") > -1:
        t_bible_book_number = 46
    elif i_line.find("1 Cor.") > -1:
        t_bible_book_number = 46
    elif i_line.find("1 Corinthians") > -1:
        t_bible_book_number = 46
    elif i_line.find("Första Corinthians") > -1:
        t_bible_book_number = 46
    elif i_line.find("First Cor") > -1:
        t_bible_book_number = 46
    if i_line.find("2 Cor") > -1:
        t_bible_book_number = 47
    elif i_line.find("2 Cor.") > -1:
        t_bible_book_number = 47
    elif i_line.find("2 Corinthians") > -1:
        t_bible_book_number = 47
    elif i_line.find("Second Corinthians") > -1:
        t_bible_book_number = 47
    elif i_line.find("Second Cor") > -1:
        t_bible_book_number = 47
    return t_bible_book_number


def get_thessalonians_english_bible_book_number(i_line):
    t_bible_book_number = 52
    if i_line.find("1 Thess") > -1:
        t_bible_book_number = 52
    elif i_line.find("1 Thess.") > -1:
        t_bible_book_number = 52
    elif i_line.find("1 Thessalonians") > -1:
        t_bible_book_number = 52
    elif i_line.find("First Thessalonians") > -1:
        t_bible_book_number = 52
    elif i_line.find("First Thess") > -1:
        t_bible_book_number = 52
    if i_line.find("2 Thess") > -1:
        t_bible_book_number = 53
    elif i_line.find("2 Thess.") > -1:
        t_bible_book_number = 53
    elif i_line.find("2 Thessalonians") > -1:
        t_bible_book_number = 53
    elif i_line.find("Second Thessalonians") > -1:
        t_bible_book_number = 53
    elif i_line.find("Second Thess") > -1:
        t_bible_book_number = 53
    return t_bible_book_number


def get_timothy_english_bible_book_number(i_line):
    t_bible_book_number = 54
    if i_line.find("1 Tim") > -1:
        t_bible_book_number = 54
    elif i_line.find("1 Tim.") > -1:
        t_bible_book_number = 54
    elif i_line.find("1 Timothy") > -1:
        t_bible_book_number = 54
    elif i_line.find("First Timothy") > -1:
        t_bible_book_number = 54
    elif i_line.find("First Tim") > -1:
        t_bible_book_number = 54
    if i_line.find("2 Tim") > -1:
        t_bible_book_number = 55
    elif i_line.find("2 Tim.") > -1:
        t_bible_book_number = 55
    elif i_line.find("2 Timothy") > -1:
        t_bible_book_number = 55
    elif i_line.find("Second Timothy") > -1:
        t_bible_book_number = 55
    elif i_line.find("Second Tim") > -1:
        t_bible_book_number = 55
    return t_bible_book_number


def get_peter_english_bible_book_number(i_line):
    t_bible_book_number = 60
    if i_line.find("1 Pet") > -1:
        t_bible_book_number = 60
    elif i_line.find("1 Pet.") > -1:
        t_bible_book_number = 60
    elif i_line.find("1 Peter") > -1:
        t_bible_book_number = 60
    elif i_line.find("First Peter") > -1:
        t_bible_book_number = 60
    elif i_line.find("First Petr") > -1:
        t_bible_book_number = 60
    if i_line.find("2 Petr") > -1:
        t_bible_book_number = 61
    elif i_line.find("2 Petr.") > -1:
        t_bible_book_number = 61
    elif i_line.find("2 Peter") > -1:
        t_bible_book_number = 61
    elif i_line.find("Second Peter") > -1:
        t_bible_book_number = 61
    elif i_line.find("Second Petr") > -1:
        t_bible_book_number = 61
    return t_bible_book_number


def get_john_english_bible_book_number(i_line):
    t_bible_book_number = 43
    # print("Line: " + i_line)
    # if i_line.find("Joh") > -1:
    #     t_bible_book_number = 43
    if i_line.find("1 Joh") > -1:
        t_bible_book_number = 62
    elif i_line.find("1 Joh.") > -1:
        t_bible_book_number = 62
    elif i_line.find("1 John") > -1:
        t_bible_book_number = 62
    elif i_line.find("First John") > -1:
        t_bible_book_number = 62
    elif i_line.find("First Joh") > -1:
        t_bible_book_number = 62
    if i_line.find("2 Joh") > -1:
        t_bible_book_number = 63
    elif i_line.find("2 Joh.") > -1:
        t_bible_book_number = 63
    elif i_line.find("2 John") > -1:
        t_bible_book_number = 63
    elif i_line.find("Second John") > -1:
        t_bible_book_number = 63
    elif i_line.find("Second Joh") > -1:
        t_bible_book_number = 63
    elif i_line.find("3 Joh") > -1:
        t_bible_book_number = 64
    elif i_line.find("3 Joh.") > -1:
        t_bible_book_number = 64
    elif i_line.find("3 John") > -1:
        t_bible_book_number = 64
    elif i_line.find("Third John") > -1:
        t_bible_book_number = 64
    elif i_line.find("Third Joh") > -1:
        t_bible_book_number = 64
    # elif i_line.find("Joh") > -1:
    #     t_bible_book_number = 43
    return t_bible_book_number


############### SWEDISH ##########################
def get_genesis_swedish_bible_book_number(i_line):
    if i_line.find("1 Mos") > -1:
        t_bible_book_number = 1
    elif i_line.find("1. Mos.") > -1:
        t_bible_book_number = 1
    elif i_line.find("FÃ¶rsta Moseboken") > -1:
        t_bible_book_number = 1
    elif i_line.find("2 Mos") > -1:
        t_bible_book_number = 2
    elif i_line.find("3 Mos") > -1:
        t_bible_book_number = 3
    elif i_line.find("4 Mos") > -1:
        t_bible_book_number = 4
    elif i_line.find("5 Mos") > -1:
        t_bible_book_number = 5
    elif i_line.find("1Mos") > -1:
        t_bible_book_number = 1
    elif i_line.find("2Mos") > -1:
        t_bible_book_number = 2
    elif i_line.find("2Mos") > -1:
        t_bible_book_number = 2
    elif i_line.find("3Mos") > -1:
        t_bible_book_number = 3
    elif i_line.find("4Mos") > -1:
        t_bible_book_number = 4
    elif i_line.find("5Mos") > -1:
        t_bible_book_number = 5
    else:
        # print("1568")
        t_bible_book_number = 1
    return t_bible_book_number


def get_samuel_swedish_bible_book_number(i_line):
    if i_line.find("1 Sam") > -1:
        t_bible_book_number = 9
    elif i_line.find("1 Sam.") > -1:
        t_bible_book_number = 9
    elif i_line.find("1 Samuelsboken") > -1:
        t_bible_book_number = 9
    elif i_line.find("Första Samuelsboken") > -1:
        t_bible_book_number = 9
    elif i_line.find("Första Sam") > -1:
        t_bible_book_number = 9
    if i_line.find("2 Sam") > -1:
        t_bible_book_number = 10
    elif i_line.find("2 Sam.") > -1:
        t_bible_book_number = 10
    elif i_line.find("2 Samuelsboken") > -1:
        t_bible_book_number = 10
    elif i_line.find("Andra Samuelsboken") > -1:
        t_bible_book_number = 10
    elif i_line.find("Andra Sam") > -1:
        t_bible_book_number = 10
    return t_bible_book_number


def get_kings_swedish_bible_book_number(i_line):
    t_bible_book_number = 11
    if i_line.find("1 Kung") > -1:
        t_bible_book_number = 11
    elif i_line.find("1 Kung.") > -1:
        t_bible_book_number = 11
    elif i_line.find("1 Kungaboken") > -1:
        t_bible_book_number = 11
    elif i_line.find("Första Kungaboken") > -1:
        t_bible_book_number = 11
    elif i_line.find("Första Kung") > -1:
        t_bible_book_number = 11
    if i_line.find("2 Kung") > -1:
        t_bible_book_number = 12
    elif i_line.find("2 Kung.") > -1:
        t_bible_book_number = 12
    elif i_line.find("2 Kungabokan") > -1:
        t_bible_book_number = 12
    elif i_line.find("Andra Kungabokan") > -1:
        t_bible_book_number = 12
    elif i_line.find("Andra Kung") > -1:
        t_bible_book_number = 12
    return t_bible_book_number


def get_chronicles_swedish_bible_book_number(i_line):
    t_bible_book_number = 13
    if i_line.find("1 Krön") > -1:
        t_bible_book_number = 13
    elif i_line.find("1 Krön.") > -1:
        t_bible_book_number = 13
    elif i_line.find("1 Krönikerboken") > -1:
        t_bible_book_number = 13
    elif i_line.find("Första Krönikerboken") > -1:
        t_bible_book_number = 13
    elif i_line.find("Första Krön") > -1:
        t_bible_book_number = 13
    if i_line.find("2 Krön") > -1:
        t_bible_book_number = 14
    elif i_line.find("2 Krön.") > -1:
        t_bible_book_number = 14
    elif i_line.find("2 Krön") > -1:
        t_bible_book_number = 14
    elif i_line.find("Andra Krönikerboken") > -1:
        t_bible_book_number = 14
    elif i_line.find("Andra Krön") > -1:
        t_bible_book_number = 14
    return t_bible_book_number


def get_corinthians_swedish_bible_book_number(i_line):
    t_bible_book_number = 46
    if i_line.find("1 Kor") > -1:
        t_bible_book_number = 46
    elif i_line.find("1 Kor.") > -1:
        t_bible_book_number = 46
    elif i_line.find("1 Korintierbrevet") > -1:
        t_bible_book_number = 46
    elif i_line.find("Första Korintierbrevet") > -1:
        t_bible_book_number = 46
    elif i_line.find("Första Kor") > -1:
        t_bible_book_number = 46
    if i_line.find("2 Kor") > -1:
        t_bible_book_number = 47
    elif i_line.find("2 Kor.") > -1:
        t_bible_book_number = 47
    elif i_line.find("2 Korintierbrevet") > -1:
        t_bible_book_number = 47
    elif i_line.find("Andra Korintierbrevet") > -1:
        t_bible_book_number = 47
    elif i_line.find("Andra Kor") > -1:
        t_bible_book_number = 47
    return t_bible_book_number


def get_thessalonians_swedish_bible_book_number(i_line):
    t_bible_book_number = 52
    if i_line.find("1 Thess") > -1:
        t_bible_book_number = 52
    elif i_line.find("1 Thess.") > -1:
        t_bible_book_number = 52
    elif i_line.find("1 Thessalonikebrevet") > -1:
        t_bible_book_number = 52
    elif i_line.find("Första Thessalonikebrevet") > -1:
        t_bible_book_number = 52
    elif i_line.find("Första Thess") > -1:
        t_bible_book_number = 52
    if i_line.find("2 Thess") > -1:
        t_bible_book_number = 53
    elif i_line.find("2 Thess.") > -1:
        t_bible_book_number = 53
    elif i_line.find("2 Thessalonikebrevet") > -1:
        t_bible_book_number = 53
    elif i_line.find("Andra Thessalonikebrevet") > -1:
        t_bible_book_number = 53
    elif i_line.find("Andra Thess") > -1:
        t_bible_book_number = 53
    return t_bible_book_number


def get_timothy_swedish_bible_book_number(i_line):
    t_bible_book_number = 54
    if i_line.find("1 Tim") > -1:
        t_bible_book_number = 54
    elif i_line.find("1 Tim.") > -1:
        t_bible_book_number = 54
    elif i_line.find("1 Timoteusbrevet") > -1:
        t_bible_book_number = 54
    elif i_line.find("Första Timoteusbrevet") > -1:
        t_bible_book_number = 54
    elif i_line.find("Första Tim") > -1:
        t_bible_book_number = 54
    if i_line.find("2 Tim") > -1:
        t_bible_book_number = 55
    elif i_line.find("2 Tim.") > -1:
        t_bible_book_number = 55
    elif i_line.find("2 Timoteusbrevet") > -1:
        t_bible_book_number = 55
    elif i_line.find("Andra Timoteusbrevet") > -1:
        t_bible_book_number = 55
    elif i_line.find("Andra Tim") > -1:
        t_bible_book_number = 55
    return t_bible_book_number


def get_peter_swedish_bible_book_number(i_line):
    t_bible_book_number = 60
    if i_line.find("1 Petr") > -1:
        t_bible_book_number = 60
    elif i_line.find("1 Petr.") > -1:
        t_bible_book_number = 60
    elif i_line.find("1 Petrusbrevet") > -1:
        t_bible_book_number = 60
    elif i_line.find("Första Petrusbrevet") > -1:
        t_bible_book_number = 60
    elif i_line.find("Första Petr") > -1:
        t_bible_book_number = 60
    if i_line.find("2 Petr") > -1:
        t_bible_book_number = 61
    elif i_line.find("2 Petr.") > -1:
        t_bible_book_number = 61
    elif i_line.find("2 Petrusbrevet") > -1:
        t_bible_book_number = 61
    elif i_line.find("Andra Petrusbrevet") > -1:
        t_bible_book_number = 61
    elif i_line.find("Andra Petr") > -1:
        t_bible_book_number = 61
    return t_bible_book_number


def get_john_swedish_bible_book_number(i_line):
    t_bible_book_number = 43
    # print("Line: " + i_line)
    # if i_line.find("Joh") > -1:
    #     t_bible_book_number = 43
    if i_line.find("1 Joh") > -1:
        t_bible_book_number = 62
    elif i_line.find("1 Joh.") > -1:
        t_bible_book_number = 62
    elif i_line.find("1 Johannesbrevet") > -1:
        t_bible_book_number = 62
    elif i_line.find("Första Johannesbrevet") > -1:
        t_bible_book_number = 62
    elif i_line.find("Första Joh") > -1:
        t_bible_book_number = 62
    if i_line.find("2 Joh") > -1:
        t_bible_book_number = 63
    elif i_line.find("2 Joh.") > -1:
        t_bible_book_number = 63
    elif i_line.find("2 Johannesbrevet") > -1:
        t_bible_book_number = 63
    elif i_line.find("Andra Johannesbrevet") > -1:
        t_bible_book_number = 63
    elif i_line.find("Andra Joh") > -1:
        t_bible_book_number = 63
    elif i_line.find("3 Joh") > -1:
        t_bible_book_number = 64
    elif i_line.find("3 Joh.") > -1:
        t_bible_book_number = 64
    elif i_line.find("3 Johannesbrevet") > -1:
        t_bible_book_number = 64
    elif i_line.find("Tredje Johannesbrevet") > -1:
        t_bible_book_number = 64
    elif i_line.find("Tredje Joh") > -1:
        t_bible_book_number = 64
    # elif i_line.find("Joh") > -1:
    #     t_bible_book_number = 43

    return t_bible_book_number



def cleanup_unwanted_characters_in_string(i_string):
    # print("out_string: " + out_string)
    for char in i_string:
        #  �  ï¿½: €   â  € 
        # print("ASCII number for : "+ ord('').__str__())      # ASCII number for : 147
        # print("ASCII number for €: "+ ord('€').__str__())       # ASCII number for €: 8364
        # print("ASCII number for : "+ ord('').__str__())      # ASCII number for €: 128
        # print("ASCII number for �: "+ ord('�').__str__())     # ASCII number for �: 65533
        # print("ASCII number for ï: "+ ord('ï').__str__())       # ASCII number for �: 239
        # print("ASCII number for ½: "+ ord('½').__str__())       # ASCII number for �: 189
        # print("ASCII number for : " + ord(':').__str__())       # ASCII number for �: 58
        # print("ASCII number for â " + ord('â').__str__())       # ASCII number for �: 226

        t_string = i_string.replace('\u0128', '')
        t_string = t_string.replace('\u0147', '')
        t_string = t_string.replace('\u8364', '')
        if char in "â":
            t_string = t_string.replace(char, '-')
        if char in "?.!/()[];ââ":
            t_string = t_string.replace(char, '')
    return t_string
