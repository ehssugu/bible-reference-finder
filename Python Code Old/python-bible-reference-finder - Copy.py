__author__ = "Sune Gustafsson"
__date__ = "2019-08-14 05:28:05"

import unittest
import os
import sys


def engGenesis(i_Eng_Bookname):
    engBookName = ["Genesis", "Gen", "Gen."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engExodus(i_Eng_Bookname):
    engBookName = ["Exodus", "Exo", "Exo.", "Ex."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engLeviticus(i_Eng_Bookname):
    engBookName = ["Leviticus", "Lev", "Lev."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engNumbers(i_Eng_Bookname):
    engBookName = ["Numbers", "Num", "Num."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engDeuteronomy(i_Eng_Bookname):
    engBookName = ["Deuteronomy", "Deu", "Deu."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engJoshua(i_Eng_Bookname):
    engBookName = ["Joshua", "Jos", "Jos"]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engJudges(i_Eng_Bookname):
    engBookName = ["Judges", "Judg", "Judg.", "Jdg", "Jdg."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engRuth(i_Eng_Bookname):
    engBookName = ["Ruth", "Ruth.", "Rth", "Rth."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng1Samuel(i_Eng_Bookname):
    engBookName = ["1 Samuel", "1 Sam", "1 Sam.", "1Sa", "1Sa.", "I Samuel", "Samuel"]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng2Samuel(i_Eng_Bookname):
    engBookName = ["2 Samuel", "2 Sam", "2 Sam.", "2Sa", "2Sa.", "II Samuel"]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng1Kings(i_Eng_Bookname):
    engBookName = ["1 Kings", "1 King", "1 King.", "1Ki", "1Ki.", "I Kings", "Kings"]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng2Kings(i_Eng_Bookname):
    engBookName = ["2 Kings", "2 King", "2 King.", "2Ki", "2Ki.", "II Kings"]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng1Chronicles(i_Eng_Bookname):
    engBookName = ["1 Chronicles", "1 Chron", "1 Chron.", "1Ch", "1Ch.", "I Chronicles", "Chronicles"]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng2Chronicles(i_Eng_Bookname):
    engBookName = ["2 Chronicles", "2 Chron", "1 Chron.", "2Ch", "2Ch.", "II Chronicles"]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engEzra(i_Eng_Bookname):
    engBookName = ["Ezra", "Ezr", "Ezr."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engNehemiah(i_Eng_Bookname):
    engBookName = ["Nehemiah", "Neh", "Neh."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engEsther(i_Eng_Bookname):
    engBookName = ["Esther", "Est", "Est."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engJob(i_Eng_Bookname):
    engBookName = ["Job", "Job."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engPsalm(i_Eng_Bookname):
    engBookName = ["Psalm", "Psa", "Psa."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engProverbs(i_Eng_Bookname):
    engBookName = ["Proverbs", "proverbs", "Pro", "Pro."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engEcclesiastes(i_Eng_Bookname):
    engBookName = ["Ecclesiastes", "Ecc", "Ecc."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engSongofSongs(i_Eng_Bookname):
    engBookName = ["Song of Songs", "Son", "Son."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engIsaiah(i_Eng_Bookname):
    engBookName = ["Isaiah", "Isa", "Isa."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engJeremiah(i_Eng_Bookname):
    engBookName = ["Jeremiah", "Jer", "Jer."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engLamentations(i_Eng_Bookname):
    engBookName = ["Lamentations", "Lam", "Lam."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engEzekiel(i_Eng_Bookname):
    engBookName = ["Ezekiel", "Eze", "Eze."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engDaniel(i_Eng_Bookname):
    engBookName = ["Daniel", "Dan", "Dan."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engHosea(i_Eng_Bookname):
    engBookName = ["Hosea", "Hos", "Hos."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engJoel(i_Eng_Bookname):
    engBookName = ["Joel", "Joe", "Joe."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engAmos(i_Eng_Bookname):
    engBookName = ["Amos", "Amo", "Amo."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engObadiah(i_Eng_Bookname):
    engBookName = ["Obadiah", "Oba", "Oba."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engJonah(i_Eng_Bookname):
    engBookName = ["Jonah", "Jon", "Jon."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engMicah(i_Eng_Bookname):
    engBookName = ["Micah", "Mic", "Mic."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engNahum(i_Eng_Bookname):
    engBookName = ["Nahum", "Nah", "Nah."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engHabakkuk(i_Eng_Bookname):
    engBookName = ["Habakkuk", "Hab", "Hab."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engZephaniah(i_Eng_Bookname):
    engBookName = ["Zephaniah", "Zep", "Zep."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engHaggai(i_Eng_Bookname):
    engBookName = ["Haggai", "Hag", "Hag."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engZechariah(i_Eng_Bookname):
    engBookName = ["Zechariah", "Zec, Zec."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engMalachi(i_Eng_Bookname):
    engBookName = ["Malachi", "Mal", "Mal."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engMatthew(i_Eng_Bookname):
    engBookName = ["Matthew", "Mat", "Mat."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engMark(i_Eng_Bookname):
    engBookName = ["Mark", "Mar", "Mar."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engLuke(i_Eng_Bookname):
    engBookName = ["Luke", "Luk", "Luk."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engJohn(i_Eng_Bookname):
    engBookName = ["John", "Joh", "Joh."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engActs(i_Eng_Bookname):
    engBookName = ["Acts", "Act", "Act."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engRomans(i_Eng_Bookname):
    engBookName = ["Romans", "romans", "Rom", "Rom."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng1Corinthians(i_Eng_Bookname):
    engBookName = ["1 Corinthians", "1st Corinthians", "first Corinthians", "1Co", "1Co.", "Corinthians"]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng2Corinthians(i_Eng_Bookname):
    engBookName = ["2 Corinthians", "2nd Corinthians", "second Corinthians 2Co", "2Co."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engGalatians(i_Eng_Bookname):
    engBookName = ["Galatians", "galatians", "Gal", "Gal."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engEphesians(i_Eng_Bookname):
    engBookName = ["Ephesians", "Eph", "Eph."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engPhilippians(i_Eng_Bookname):
    engBookName = ["Philippians", "Php", "Php.", "Philip", "Philip."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engColossians(i_Eng_Bookname):
    engBookName = ["Colossians", "Col", "Col."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng1Thessalonians(i_Eng_Bookname):
    engBookName = ["1 Thessalonians", "1Th", "1Th.", "Thessalonians"]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng2Thessalonians(i_Eng_Bookname):
    engBookName = ["2 Thessalonians", "2Th", "2Th."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng1Timothy(i_Eng_Bookname):
    engBookName = ["1 Timothy", "1st Timothy", "first Timothy", "1Ti", "1Ti.", "Timothy"]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng2Timothy(i_Eng_Bookname):
    engBookName = ["2 Timothy", "2nd Timothy", "second Timothy", "2Ti", "2Ti."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engTitus(i_Eng_Bookname):
    engBookName = ["Titus", "Tit", "Tit."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engPhilemon(i_Eng_Bookname):
    engBookName = ["Philemon", "Phm", "Phm."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engHebrews(i_Eng_Bookname):
    engBookName = ["Hebrews", "hebrews", "Heb", "Heb."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engJames(i_Eng_Bookname):
    engBookName = ["James", "Jas", "Jas."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng1Peter(i_Eng_Bookname):
    engBookName = ["1 Peter", "1Pe", "1Pe.", "I Peter", "Peter"]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng2Peter(i_Eng_Bookname):
    engBookName = ["2 Peter", "2Pe", "2Pe.", "II Peter"]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng1John(i_Eng_Bookname):
    engBookName = ["1 John", "1Jn", "1Jn.", "I John", "John"]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng2John(i_Eng_Bookname):
    engBookName = ["2 John", "2Jn", "2Jn.", "II John"]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng3John(i_Eng_Bookname):
    engBookName = ["3 John", "3Jn", "3Jn.", "III John"]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engJude(i_Eng_Bookname):
    engBookName = ["Jude", "Jud", "Jud."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engRevelation(i_Eng_Bookname):
    engBookName = ["Revelation", "revelation", "Rev", "Rev."]
    if engBookName.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def getBibleBookNumber(i_Eng_Bookname):
    if engGenesis(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 1

    elif engExodus(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 2

    elif engLeviticus(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 3

    elif engNumbers(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 4

    elif engDeuteronomy(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 5

    elif engLeviticus(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 6

    elif engNumbers(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 7

    elif engDeuteronomy(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 8

    elif engJoshua(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 9

    elif engJudges(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 10

    elif engRuth(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 11

    elif eng1Samuel(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 12

    elif eng2Samuel(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 13

    elif eng1Kings(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 14

    elif eng2Kings(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 15

    elif eng1Chronicles(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 16

    elif eng2Chronicles(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 17

    elif engEzra(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 18

    elif engNehemiah(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 19

    elif engEsther(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 20

    elif engJob(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 21

    elif engPsalm(i_Eng_Bookname):
        # print(test_Book_Name)

        t_Bible_Book_Number = 22

    elif engProverbs(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 23

    elif engEcclesiastes(i_Eng_Bookname):

        t_Bible_Book_Number = 22

    elif engSongofSongs(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 23

    elif engIsaiah(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 24

    elif engJeremiah(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 25

    elif engLamentations(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 26

    elif engEzekiel(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 27

    elif engDaniel(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 28

    elif engHosea(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 29

    elif engJoel(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 30

    elif engAmos(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 31

    elif engObadiah(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 31

    elif engJonah(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 32

    elif engMicah(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 33

    elif engNahum(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 34

    elif engHabakkuk(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 35

    elif engZephaniah(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 36

    elif engHaggai(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 37

    elif engZechariah(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 38

    elif engMalachi(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 39

    elif engMatthew(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 40

    elif engMark(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 41

    elif engLuke(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 42

    elif engJohn(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 43

    elif engActs(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 44

    elif engRomans(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 45

    elif eng1Corinthians(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 46

    elif eng2Corinthians(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 47

    elif engGalatians(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 48

    elif engEphesians(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 49

    elif engPhilippians(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 50

    elif engColossians(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 51

    elif eng1Thessalonians(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 52

    elif eng2Thessalonians(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 53

    elif eng1Timothy(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 54

    elif eng2Timothy(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 55

    elif engTitus(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 56

    elif engPhilemon(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 57

    elif engHebrews(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 58

    elif engJames(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 59

    elif eng1Peter(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 60

    elif eng2Peter(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 61

    elif eng1John(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 62

    elif eng2John(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 63

    elif eng3John(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 64

    elif engJude(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 65

    elif engRevelation(i_Eng_Bookname):
        # print(test_Book_Name)
        t_Bible_Book_Number = 66
    else:
        # print ("Didn't find book")
        t_Bible_Book_Number = 67
    # print("End")

    # print ("Bible book number: ", t_Bible_Book_Number)
    return t_Bible_Book_Number


def getSwedishBibleBookName(i_bibleBookNumber):
    swedishBibleBookNameString = ["1 Mos", "2 Mos", "3 Mos", "4 Mos", "5 Mos",
                                  "Jos", "Dom", "Rut", "1 Sam", "2 Sam",
                                  "1 Kung", "2 Kung", "1 Krön", "2 Krön", "Esra",
                                  "Neh", "Est", "Job", "Ps", "Ords",
                                  "Pred", "Höga V", "Jes", "Jer", "Klag",
                                  "Hes", "Dan", "Hos", "Joel", "Amos",
                                  "Ob", "Jona", "Mika", "Nah", "Hab",
                                  "Sef", "Hagg", "Sak", "Mal",
                                  "Matt", "Mark", "Luk", "Joh", "Apg",
                                  "Rom", "1 Kor", "2 Kor", "Gal", "Ef",
                                  "Fil", "Kol", "1 Thess", "2 Thess", "1 Tim",
                                  "2 Tim", "Tit", "Filem", "Hebr", "Jak",
                                  "1 Pet", "2 Pet", "1 Joh", "2 Joh", "3 Joh", "Jud", "Upp", "Unresolved"]

    t_bibleBookName = swedishBibleBookNameString[i_bibleBookNumber - 1]

    return t_bibleBookName


if __name__ == '__main__':

    # i_Eng_Bookname = "Matthew"

    # print("Bible book number: ", getBibleBookNumber(i_Eng_Bookname))

    # print("Bible book name:   ", getSwedishBibleBookName(getBibleBookNumber (i_Eng_Bookname)))

    t_verses = []

    engBookNamesList1 = ["Genesis", "Gen", "Gen.",
                         "Exodus", "Exo", "Exo.", "Ex.",
                         "Leviticus", "Lev", "Lev",
                         "Numbers", "Num", "Num",
                         "Deuteronomy", "Deu", "Deu",
                         "Joshua", "Jos", "Jos",
                         "Judges", "Judg", "Judg.", "Jdg", "Jdg",
                         "Ruth", "Ruth.", "Rth", "Rth",
                         "Samuel", "Kings", "Chronicles",
                         "Ezra", "Ezr", "Ezr.",
                         "Nehemiah", "Neh", "Neh.",
                         "Esther", "Est", "Est.",
                         "Job", "Job.", "Psalm", "Psa", "Psa.",
                         "Proverbs", "proverbs", "Pro", "Pro.",
                         "Ecclesiastes", "Ecc", "Ecc.",
                         "Song of Songs", "Son", "Son.",
                         "Isaiah", "Isa", "Isa.",
                         "Jeremiah", "Jer", "Jer.",
                         "Lamentations", "Lam", "Lam.",
                         "Ezekiel", "Eze", "Eze.",
                         "Daniel", "Dan", "Dan.",
                         "Hosea", "Hos", "Hos.",
                         "Joel", "Joe", "Joe.",
                         "Amos", "Amo", "Amo.",
                         "Obadiah", "Oba", "Oba.",
                         "Jonah", "Jon", "Jon.",
                         "Micah", "Mic", "Mic.",
                         "Nahum", "Nah", "Nah.",
                         "Habakkuk", "Hab", "Hab.",
                         "Zephaniah", "Zep", "Zep.",
                         "Haggai", "Hag", "Hag.",
                         "Zechariah", "Zec, Zec.",
                         "Malachi", "Mal", "Mal.",
                         "Matthew", "Mat", "Mat.",
                         "Mark", "Mar", "Mar.",
                         "Luke", "Luk", "Luk.",
                         "John", "Joh", "Joh.",
                         "Acts", "Act", "Act.",
                         "Romans", "romans", "Rom", "Rom.",
                         "Corinthians",
                         "Galatians", "galatians", "Gal", "Gal.",
                         "Ephesians", "Eph", "Eph.",
                         "Philippians", "Php", "Php.", "Philip", "Philip.",
                         "Colossians", "Col", "Col.", "Thessalonians", "Timothy",
                         "Titus", "Tit", "Tit.",
                         "Philemon", "Phm", "Phm.",
                         "Hebrews", "hebrews", "Heb", "Heb.",
                         "James", "Jas", "Jas.",
                         "Peter", "John",
                         "Jude", "Jud", "Jud.",
                         "Revelation", "revelation", "Rev", "Rev."]

    engBookNamesList2 = ["Genesis", "Gen", "Gen.",
                         "Exodus", "Exo", "Exo.", "Ex.",
                         "Leviticus", "Lev", "Lev",
                         "Numbers", "Num", "Num",
                         "Deuteronomy", "Deu", "Deu",
                         "Joshua", "Jos", "Jos",
                         "Judges", "Judg", "Judg.", "Jdg", "Jdg",
                         "Ruth", "Ruth.", "Rth", "Rth",
                         "1 Samuel", "1 Sam", "1 Sam.", "1Sa", "1Sa.", "I Samuel",
                         "2 Samuel", "2 Sam", "2 Sam.", "2Sa", "2Sa.", "II Samuel",
                         "1 Kings", "1 King", "1 King.", "1Ki", "1Ki.", "I Kings",
                         "2 Kings", "2 King", "2 King.", "2Ki", "2Ki.", "II Kings",
                         "1 Chronicles", "1 Chron", "1 Chron.", "1Ch", "1Ch.", "I Chronicles",
                         "2 Chronicles", "2 Chron", "1 Chron.", "2Ch", "2Ch.", "II Chronicles",
                         "Ezra", "Ezr", "Ezr.",
                         "Nehemiah", "Neh", "Neh.",
                         "Esther", "Est", "Est.",
                         "Job", "Job.", "Psalm", "Psa", "Psa.",
                         "Proverbs", "proverbs", "Pro", "Pro.",
                         "Ecclesiastes", "Ecc", "Ecc.",
                         "Song of Songs", "Son", "Son.",
                         "Isaiah", "Isa", "Isa.",
                         "Jeremiah", "Jer", "Jer.",
                         "Lamentations", "Lam", "Lam.",
                         "Ezekiel", "Eze", "Eze.",
                         "Daniel", "Dan", "Dan.",
                         "Hosea", "Hos", "Hos.",
                         "Joel", "Joe", "Joe.",
                         "Amos", "Amo", "Amo.",
                         "Obadiah", "Oba", "Oba.",
                         "Jonah", "Jon", "Jon.",
                         "Micah", "Mic", "Mic.",
                         "Nahum", "Nah", "Nah.",
                         "Habakkuk", "Hab", "Hab.",
                         "Zephaniah", "Zep", "Zep.",
                         "Haggai", "Hag", "Hag.",
                         "Zechariah", "Zec, Zec.",
                         "Malachi", "Mal", "Mal.",
                         "Matthew", "Mat", "Mat.",
                         "Mark", "Mar", "Mar.",
                         "Luke", "Luk", "Luk.",
                         "John", "Joh", "Joh.",
                         "Acts", "Act", "Act.",
                         "Romans", "romans", "Rom", "Rom.",
                         "1 Corinthians", "1st Corinthians", "first Corinthians", "1Co", "1Co.",
                         "2 Corinthians", "2nd Corinthians", "second Corinthians 2Co", "2Co.",
                         "Galatians", "galatians", "Gal", "Gal.",
                         "Ephesians", "Eph", "Eph.",
                         "Philippians", "Php", "Php.", "Philip", "Philip.",
                         "Colossians", "Col", "Col.",
                         "1 Thessalonians", "1Th", "1Th.",
                         "2 Thessalonians", "2Th", "2Th.",
                         "1 Timothy", "1st Timothy", "first Timothy", "1Ti", "1Ti.",
                         "2 Timothy", "2nd Timothy", "second Timothy", "2Ti", "2Ti.",
                         "Titus", "Tit", "Tit.",
                         "Philemon", "Phm", "Phm.",
                         "Hebrews", "hebrews", "Heb", "Heb.",
                         "James", "Jas", "Jas.",
                         "1 Peter", "1Pe", "1Pe.", "I Peter",
                         "2 Peter", "2Pe", "2Pe.", "II Peter",
                         "1 John", "1Jn", "1Jn.", "I John",
                         "2 John", "2Jn", "2Jn.", "II John",
                         "3 John", "3Jn", "3Jn.", "III John",
                         "Jude", "Jud", "Jud.",
                         "Revelation", "revelation", "Rev", "Rev."]

    with open("input.txt", encoding="latin-1") as f:
        t_Lines = f.readlines()
        bookNumbersWithPrefixList = [9, 10, 11, 12, 13, 14, 46, 47, 52, 53, 54, 55, 60, 61, 62, 63, 64]

        for t_Line in t_Lines:
            try:
                wordList = []
                wordList = t_Line.split(" ")
                # print("Wordstring###: " + wordList.__str__())

                for word in wordList:

                    for char in word:
                        if char in "?.!/;:()":
                            word = word.replace(char, '')
                    # print("Word¤¤: " +word)

                    # word.replace("(", '')
                    # print(word)
                    if engBookNamesList1.__contains__(word):

                        # print("Refrence: " + word +" Line: " + t_Line)
                        bibleBookNumber = getBibleBookNumber(word)
                        # print("BiblebookNumber###IN###: " +bibleBookNumber.__str__() +" " +t_Line)
                        if bibleBookNumber < 67:

                            if bibleBookNumber == 9:  # OK
                                if t_Line.find("2 Samuel") > -1:
                                    bibleBookNumber = 10

                            if bibleBookNumber == 11:  # OK
                                if t_Line.find("2 Chronicles") > -1:
                                    bibleBookNumber = 13

                            if bibleBookNumber == 13:  # OK
                                if t_Line.find("2 Kings") > -1:
                                    bibleBookNumber = 14

                            if bibleBookNumber == 46:  # OK
                                if t_Line.find("2 Corinthians") > -1:
                                    bibleBookNumber = 47

                            if bibleBookNumber == 52:  # OK
                                if t_Line.find("2 Thessalonians") > -1:
                                    bibleBookNumber = 53

                            if bibleBookNumber == 54:  # OK
                                if (t_Line.find("2 Timothy") > -1):
                                    # print("55 Timothy: * " +t_Line)
                                    bibleBookNumber = 55

                            if bibleBookNumber == 60:
                                if (t_Line.find("2 Peter") > -1):
                                    bibleBookNumber = 61

                            if bibleBookNumber == 43:  # OK. This may be John(the Gospel), 1 John, 2 John, 3 John
                                # print("¤¤¤: " +t_Line)
                                if t_Line.find("1 John") > -1:
                                    bibleBookNumber = 62
                                elif t_Line.find("2 John") > -1:
                                    bibleBookNumber = 63
                                elif t_Line.find("3 John") > -1:
                                    bibleBookNumber = 64

                            # print("bibleBookNumber: " + bibleBookNumber.__str__()+" " +t_Line)
                            t_chapter_start_index = t_Line.index(word)

                            if bibleBookNumber in bookNumbersWithPrefixList:
                                t_chapter_start_index = t_chapter_start_index - 2

                            # print("t_chapter_start-index: " +t_chapter_start_index.__str__())
                            t_book = t_Line[t_chapter_start_index:]
                            t_reference_end = t_Line[t_chapter_start_index + 1:]

                            # print("Swedish book name: " +getSwedishBibleBookName(bibleBookNumber) +" " +t_reference_end)
                            # print("Line with bible book: " +t_book)

                            # Keep only lines with ":", i.e. references
                            if t_book.find(":") > -1:
                                print(t_book)

                            # t_book_and_ref_out = getSwedishBibleBookName(getBibleBookNumber(t_book)) + " " +t_reference_end
                            # t_verses.append(t_book_and_ref_out)
                            # print("Book: ", t_book, "Swedish biblebook: ", getSwedishBibleBookName(getBibleBookNumber(t_book)), t_reference_end)
                            # print(t_book_and_ref_out)

            except ValueError:
                if t_Line is not " ":
                    print("ValueError" + t_Line)
        # print(t_verses)

    with open('output.txt', mode='wt', encoding='latin-1') as myfile:
        for t_Line in t_verses:
            myfile.write(t_Line)
