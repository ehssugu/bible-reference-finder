__author__ = "Sune Gustafsson"
__date__ = "2019-08-14 05:28:05"

import unittest
import os
import sys


def engGenesis(i_Eng_Bookname):
    eng_book_name = ["Genesis", "Gen", "Gen."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engExodus(i_Eng_Bookname):
    eng_book_name = ["Exodus", "Exo", "Exo.", "Ex."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engLeviticus(i_Eng_Bookname):
    eng_book_name = ["Leviticus", "Lev", "Lev."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engNumbers(i_Eng_Bookname):
    eng_book_name = ["Numbers", "Num", "Num."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engDeuteronomy(i_Eng_Bookname):
    eng_book_name = ["Deuteronomy", "Deu", "Deu."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engJoshua(i_Eng_Bookname):
    eng_book_name = ["Joshua", "Jos", "Jos"]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engJudges(i_Eng_Bookname):
    eng_book_name = ["Judges", "Judg", "Judg.", "Jdg", "Jdg."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engRuth(i_Eng_Bookname):
    eng_book_name = ["Ruth", "Ruth.", "Rth", "Rth."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng1Samuel(i_Eng_Bookname):
    eng_book_name = ["1 Samuel", "1 Sam", "1 Sam.", "1Sa", "1Sa.", "I Samuel", "Samuel"]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng2Samuel(i_Eng_Bookname):
    eng_book_name = ["2 Samuel", "2 Sam", "2 Sam.", "2Sa", "2Sa.", "II Samuel"]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng1Kings(i_Eng_Bookname):
    eng_book_name = ["1 Kings", "1 King", "1 King.", "1Ki", "1Ki.", "I Kings", "Kings"]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng2Kings(i_Eng_Bookname):
    eng_book_name = ["2 Kings", "2 King", "2 King.", "2Ki", "2Ki.", "II Kings"]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng1Chronicles(i_Eng_Bookname):
    eng_book_name = ["1 Chronicles", "1 Chron", "1 Chron.", "1Ch", "1Ch.", "I Chronicles", "Chronicles"]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng2Chronicles(i_Eng_Bookname):
    eng_book_name = ["2 Chronicles", "2 Chron", "1 Chron.", "2Ch", "2Ch.", "II Chronicles"]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engEzra(i_Eng_Bookname):
    eng_book_name = ["Ezra", "Ezr", "Ezr."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engNehemiah(i_Eng_Bookname):
    eng_book_name = ["Nehemiah", "Neh", "Neh."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engEsther(i_Eng_Bookname):
    eng_book_name = ["Esther", "Est", "Est."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engJob(i_Eng_Bookname):
    eng_book_name = ["Job", "Job."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engPsalm(i_Eng_Bookname):
    eng_book_name = ["Psalm", "Psa", "Psa."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engProverbs(i_Eng_Bookname):
    eng_book_name = ["Proverbs", "proverbs", "Pro", "Pro."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engEcclesiastes(i_Eng_Bookname):
    eng_book_name = ["Ecclesiastes", "Ecc", "Ecc."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engSongofSongs(i_Eng_Bookname):
    eng_book_name = ["Song of Songs", "Son", "Son."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engIsaiah(i_Eng_Bookname):
    eng_book_name = ["Isaiah", "Isa", "Isa."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engJeremiah(i_Eng_Bookname):
    eng_book_name = ["Jeremiah", "Jer", "Jer."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engLamentations(i_Eng_Bookname):
    eng_book_name = ["Lamentations", "Lam", "Lam."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engEzekiel(i_Eng_Bookname):
    eng_book_name = ["Ezekiel", "Eze", "Eze."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engDaniel(i_Eng_Bookname):
    eng_book_name = ["Daniel", "Dan", "Dan."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engHosea(i_Eng_Bookname):
    eng_book_name = ["Hosea", "Hos", "Hos."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engJoel(i_Eng_Bookname):
    eng_book_name = ["Joel", "Joe", "Joe."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engAmos(i_Eng_Bookname):
    eng_book_name = ["Amos", "Amo", "Amo."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engObadiah(i_Eng_Bookname):
    eng_book_name = ["Obadiah", "Oba", "Oba."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engJonah(i_Eng_Bookname):
    eng_book_name = ["Jonah", "Jon", "Jon."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engMicah(i_Eng_Bookname):
    eng_book_name = ["Micah", "Mic", "Mic."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engNahum(i_Eng_Bookname):
    eng_book_name = ["Nahum", "Nah", "Nah."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engHabakkuk(i_Eng_Bookname):
    eng_book_name = ["Habakkuk", "Hab", "Hab."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engZephaniah(i_Eng_Bookname):
    eng_book_name = ["Zephaniah", "Zep", "Zep."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engHaggai(i_Eng_Bookname):
    eng_book_name = ["Haggai", "Hag", "Hag."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engZechariah(i_Eng_Bookname):
    eng_book_name = ["Zechariah", "Zec, Zec."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engMalachi(i_Eng_Bookname):
    eng_book_name = ["Malachi", "Mal", "Mal."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engMatthew(i_Eng_Bookname):
    eng_book_name = ["Matthew", "Mat", "Mat."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engMark(i_Eng_Bookname):
    eng_book_name = ["Mark", "Mar", "Mar."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engLuke(i_Eng_Bookname):
    eng_book_name = ["Luke", "Luk", "Luk."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engJohn(i_Eng_Bookname):
    eng_book_name = ["John", "Joh", "Joh."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engActs(i_Eng_Bookname):
    eng_book_name = ["Acts", "Act", "Act."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engRomans(i_Eng_Bookname):
    eng_book_name = ["Romans", "romans", "Rom", "Rom."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng1Corinthians(i_Eng_Bookname):
    eng_book_name = ["1 Corinthians", "1st Corinthians", "first Corinthians", "1Co", "1Co.", "Corinthians"]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng2Corinthians(i_Eng_Bookname):
    eng_book_name = ["2 Corinthians", "2nd Corinthians", "second Corinthians 2Co", "2Co."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engGalatians(i_Eng_Bookname):
    eng_book_name = ["Galatians", "galatians", "Gal", "Gal."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engEphesians(i_Eng_Bookname):
    eng_book_name = ["Ephesians", "Eph", "Eph."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engPhilippians(i_Eng_Bookname):
    eng_book_name = ["Philippians", "Php", "Php.", "Philip", "Philip."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engColossians(i_Eng_Bookname):
    eng_book_name = ["Colossians", "Col", "Col."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng1Thessalonians(i_Eng_Bookname):
    eng_book_name = ["1 Thessalonians", "1Th", "1Th.", "Thessalonians"]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng2Thessalonians(i_Eng_Bookname):
    eng_book_name = ["2 Thessalonians", "2Th", "2Th."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng1Timothy(i_Eng_Bookname):
    eng_book_name = ["1 Timothy", "1st Timothy", "first Timothy", "1Ti", "1Ti.", "Timothy"]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng2Timothy(i_Eng_Bookname):
    eng_book_name = ["2 Timothy", "2nd Timothy", "second Timothy", "2Ti", "2Ti."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engTitus(i_Eng_Bookname):
    eng_book_name = ["Titus", "Tit", "Tit."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engPhilemon(i_Eng_Bookname):
    eng_book_name = ["Philemon", "Phm", "Phm."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engHebrews(i_Eng_Bookname):
    eng_book_name = ["Hebrews", "hebrews", "Heb", "Heb."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engJames(i_Eng_Bookname):
    eng_book_name = ["James", "Jas", "Jas."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng1Peter(i_Eng_Bookname):
    eng_book_name = ["1 Peter", "1Pe", "1Pe.", "I Peter", "Peter"]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng2Peter(i_Eng_Bookname):
    eng_book_name = ["2 Peter", "2Pe", "2Pe.", "II Peter"]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng1John(i_Eng_Bookname):
    eng_book_name = ["1 John", "1Jn", "1Jn.", "I John", "John"]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng2John(i_Eng_Bookname):
    eng_book_name = ["2 John", "2Jn", "2Jn.", "II John"]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def eng3John(i_Eng_Bookname):
    eng_book_name = ["3 John", "3Jn", "3Jn.", "III John"]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engJude(i_Eng_Bookname):
    eng_book_name = ["Jude", "Jud", "Jud."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def engRevelation(i_Eng_Bookname):
    eng_book_name = ["Revelation", "revelation", "Rev", "Rev."]
    if eng_book_name.__contains__(i_Eng_Bookname):
        return True
    else:
        return False


def sweGenesis(i_swe_book_name):
    # print("535: ", i_swe_book_name)
    swe_book_name = ["Mos", "1 Mos", "1 Mos.", "1Mos.", "1Mos", "1. Mos.", "Första Moseboken", "Första Mos.",
                     "1 Moseboken"]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweExodus(i_swe_book_name):
    swe_book_name = ["Mos", "2 Mos", "2 Mos.", "2Mos.", "2. Mos.", "Andra Moseboken", "Andra Mos.", "2 Moseboken"]
    # print("i_swe_book_name in Exodus: " +i_swe_book_name)
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweLeviticus(i_swe_book_name):
    swe_book_name = ["Mos", "3 Mos", "3 Mos.", "3Mos.", "3. Mos.", "Tredje Moseboken", "Tredje Mos.", "3 Moseboken"]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweNumbers(i_swe_book_name):
    swe_book_name = ["Mos", "4 Mos", "4 Mos.", "4Mos.", ". Mos.", "Fjärde Moseboken", "Fjärde Mos.", "4 Moseboken"]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweDeuteronomy(i_swe_book_name):
    swe_book_name = ["Mos", "5 Mos", "5 Mos.", "5Mos.", "5. Mos.", "Femte Moseboken", "Femte Mos.", "5 Moseboken"]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweJoshua(i_swe_book_name):
    swe_book_name = ["Jos", "Josua", "Jos."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweJudges(i_swe_book_name):
    swe_book_name = ["Dom", "Domarboken", "Dom."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweRuth(i_swe_book_name):
    swe_book_name = ["Rut", "Ruts bok", "Rut."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def swe1Samuel(i_swe_book_name):
    swe_book_name = ["Sam", "1 Sam", "Första Samuelsboken", "1 Sam.", "1 Första Samuelsboken"]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def swe2Samuel(i_swe_book_name):
    swe_book_name = ["Sam", "2 Sam", "Andra Samuelsboken", "2 Sam.", "2 Samuelsboken"]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def swe1Kings(i_swe_book_name):
    swe_book_name = ["Kung", "1 Kung", "Första Kungaboken", "1 Kung."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def swe2Kings(i_swe_book_name):
    swe_book_name = ["Kung", "2 Kung", "Andra Kungaboken", "2 Kung."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def swe1Chronicles(i_swe_book_name):
    swe_book_name = ["Krön", "1 Krön", "Första Krönikeboken", "1 Krön."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def swe2Chronicles(i_swe_book_name):
    swe_book_name = ["Krön", "2 Krön", "Andra Krönikeboken", "2 Krön."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweEzra(i_swe_book_name):
    swe_book_name = ["Esra", "Esr", "Esr."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweNehemiah(i_swe_book_name):
    swe_book_name = ["Neh", "Nehemja", "Neh."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweEsther(i_swe_book_name):
    swe_book_name = ["Est", "Esters bok", "Est."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweJob(i_swe_book_name):
    swe_book_name = ["Job", "Jobs bok", "Job."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def swePsalms(i_swe_book_name):
    swe_book_name = ["Ps", "Psaltaren", "Psalm", "Psa.", "Psa"]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweProverbs(i_swe_book_name):
    swe_book_name = ["Ords", "Ordspråksboken", "Ords.", "Ordspr."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweEcclesiastes(i_swe_book_name):
    swe_book_name = ["Pred", "Predikaren", "Pred."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweSongofSongs(i_swe_book_name):
    swe_book_name = ["Höga V", "Höga Visan", "Höga V.", "Höga v."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweIsaiah(i_swe_book_name):
    swe_book_name = ["Jes", "Jesaja", "Jes."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweJeremiah(i_swe_book_name):
    swe_book_name = ["Jer", "Jeremia", "Jer."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweLamentations(i_swe_book_name):
    swe_book_name = ["Klag", "Klagovisorna", "Klag."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweEzekiel(i_swe_book_name):
    swe_book_name = ["Hes", "Hesekiel", "Hes."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweDaniel(i_swe_book_name):
    swe_book_name = ["Dan", "Daniel", "Dan."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweHosea(i_swe_book_name):
    swe_book_name = ["Hos", "Hosea", "Hos."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweJoel(i_swe_book_name):
    swe_book_name = ["Joel", "Joe", "Joe."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweAmos(i_swe_book_name):
    swe_book_name = ["Amos", "Amo", "Amo."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweObadiah(i_swe_book_name):
    swe_book_name = ["Ob", "Obadja", "Ob.", "Oba."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweJonah(i_swe_book_name):
    swe_book_name = ["Jon", "Jona", "Jon."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweMicah(i_swe_book_name):
    swe_book_name = ["Mik", "Mika", "Mik."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweNahum(i_swe_book_name):
    swe_book_name = ["Nah", "Nahum", "Nah."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweHabakkuk(i_swe_book_name):
    swe_book_name = ["Hab", "Habakkuk", "Hab."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweZephaniah(i_swe_book_name):
    swe_book_name = ["Sef", "Sefanja", "Sef."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweHaggai(i_swe_book_name):
    swe_book_name = ["Hagg", "Haggai", "Hag."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweZechariah(i_swe_book_name):
    swe_book_name = ["Sak", "Sakarja,", "Sak."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweMalachi(i_swe_book_name):
    swe_book_name = ["Mal", "Malaki", "Mal."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweMatthew(i_swe_book_name):
    swe_book_name = ["Matt", "Matteus", "Matt.", "Mat.", "Matteusevangeliet"]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweMark(i_swe_book_name):
    swe_book_name = ["Mark", "Markus", "Mar.", "Markusevangeliet"]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweLuke(i_swe_book_name):
    swe_book_name = ["Luk", "Lukas", "Luk.", "Lukasevangeliet"]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweJohn(i_swe_book_name):
    swe_book_name = ["Joh", "Johannes", "Joh.", "Johannesevangeliet"]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweActs(i_swe_book_name):
    swe_book_name = ["Apg", "Apostlagärningarna", "Apg."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweRomans(i_swe_book_name):
    swe_book_name = ["Rom", "Romarbrevet", "Rom.", "Romarna"]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def swe1Corinthians(i_swe_book_name):
    swe_book_name = ["Kor", "1 Kor", "1 Korintierbrevet", "1Kor", "1Kor.", "Förste Korintierbrevet",
                     "Första Korintierbrevet", "Första Kor", "Första Kor."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def swe2Corinthians(i_swe_book_name):
    swe_book_name = ["Kor", "2 Kor", "2 Korintierbrevet", "2Kor", "2Kor.", "Andra Korintierbrevet",
                     "Andre Korintierbrevet", "Andra Kor", "Andra Kor."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweGalatians(i_swe_book_name):
    swe_book_name = ["Gal", "Galaterbrevet", "Gal."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweEphesians(i_swe_book_name):
    swe_book_name = ["Ef", "Efesierbrevet", "Ef.", "Efésierbrevet"]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def swePhilippians(i_swe_book_name):
    swe_book_name = ["Fil", "Filipperbrevet", "Fil."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweColossians(i_swe_book_name):
    swe_book_name = ["Kol", "Kolosserbrevet", "Kol."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def swe1Thessalonians(i_swe_book_name):
    swe_book_name = ["Thess", "1 Thess", "1 Thessalonikerbrevet", "1 Thess.", "Första Thessalonikerbrevet",
                     "Första Thess", "Första Thess."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def swe2Thessalonians(i_swe_book_name):
    swe_book_name = ["Thess", "2 Thess", "2 Thessalonikerbrevet", "2 Thess.", "Andra Thessalonikerbrevet",
                     "Andra Thess", "Andra Thess."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def swe1Timothy(i_swe_book_name):
    swe_book_name = ["Tim", "1 Tim", "1 Timoteusbrevet", "1 Tim.", "Första Timoteusbrevet", "Första Tim.", "Första Tim"]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def swe2Timothy(i_swe_book_name):
    swe_book_name = ["Tim", "2 Tim", "2 Timoteusbrevet", "2 Tim.", "Andra Timoteusbrevet", "Andra Tim.", "Andra Tim"]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweTitus(i_swe_book_name):
    swe_book_name = ["Tit", "Titusbrevet", "Tit.", "Titus"]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def swePhilemon(i_swe_book_name):
    swe_book_name = ["Filem", "Filemon", "Filem.", "Brevet till Filemon"]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweHebrews(i_swe_book_name):
    swe_book_name = ["Hebr", "Hebreerbrevet", "Hebr."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweJames(i_swe_book_name):
    swe_book_name = ["Jak", "Jakobsbrevet", "Jak.", "Jakob"]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def swe1Peter(i_swe_book_name):
    swe_book_name = ["Petr", "1 Petr", "1 Petrusbrevet", "Första Petrusbrevet", "Första Petr", "Första Petr.", "1Petr.",
                     "1 Petr."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def swe2Peter(i_swe_book_name):
    swe_book_name = ["Petr", "2 Petr", "2 Petrusbrevet", "Andra Petrusbrevet", "Andra Petr", "Andra Petr.", "2Petr.",
                     "2 Petr."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def swe1John(i_swe_book_name):
    swe_book_name = ["Joh", "1 Joh", "1 Johannesbrevet", "1 Joh.", "Första Johannesbrevet,", "Första Joh.",
                     "Första Joh"]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def swe2John(i_swe_book_name):
    swe_book_name = ["Joh", "2 Joh", "2 Johannesbrevet", "2 Joh.", "Andra Johannesbrevet,", "Andra Joh.", "Andra Joh"]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def swe3John(i_swe_book_name):
    swe_book_name = ["Joh", "3 Joh", "3 Johannesbrevet", "3 Joh.", "Tredje Johannesbrevet,", "Tredje Joh.",
                     "Tredje Joh"]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweJude(i_swe_book_name):
    swe_book_name = ["Jud", "Judasbrevet", "Jud."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def sweRevelation(i_swe_book_name):
    swe_book_name = ["Upp", "Uppenbarelseboken", "Upp."]
    if swe_book_name.__contains__(i_swe_book_name):
        return True
    else:
        return False


def get_bible_book_number_from_swedish_names(i_swe_book_name):
    # print("1030 i_swe_book_name: " + i_swe_book_name)

    if sweGenesis(i_swe_book_name):
        # print("1034: " +i_swe_book_name)
        t_bible_book_number = 1

    elif sweExodus(i_swe_book_name):
        t_bible_book_number = 2

    elif sweLeviticus(i_swe_book_name):
        t_bible_book_number = 3

    elif sweNumbers(i_swe_book_name):
        t_bible_book_number = 4

    elif sweDeuteronomy(i_swe_book_name):
        t_bible_book_number = 5

    elif sweJoshua(i_swe_book_name):
        t_bible_book_number = 6

    elif sweJudges(i_swe_book_name):
        t_bible_book_number = 7

    elif sweRuth(i_swe_book_name):
        t_bible_book_number = 8

    elif swe1Samuel(i_swe_book_name):
        t_bible_book_number = 9

    elif swe2Samuel(i_swe_book_name):
        t_bible_book_number = 10

    elif swe1Kings(i_swe_book_name):
        t_bible_book_number = 11

    elif swe2Kings(i_swe_book_name):
        t_bible_book_number = 12

    elif swe1Chronicles(i_swe_book_name):
        t_bible_book_number = 13

    elif swe2Chronicles(i_swe_book_name):
        t_bible_book_number = 14

    elif sweEzra(i_swe_book_name):
        t_bible_book_number = 15

    elif sweNehemiah(i_swe_book_name):
        t_bible_book_number = 16

    elif sweEsther(i_swe_book_name):
        t_bible_book_number = 17

    elif sweJob(i_swe_book_name):
        t_bible_book_number = 18

    elif swePsalms(i_swe_book_name):
        t_bible_book_number = 19

    elif sweProverbs(i_swe_book_name):
        t_bible_book_number = 20

    elif sweEcclesiastes(i_swe_book_name):
        t_bible_book_number = 21

    elif sweSongofSongs(i_swe_book_name):
        t_bible_book_number = 22

    elif sweIsaiah(i_swe_book_name):
        t_bible_book_number = 23

    elif sweJeremiah(i_swe_book_name):
        t_bible_book_number = 24

    elif sweSongofSongs(i_swe_book_name):
        t_bible_book_number = 23

    elif sweJeremiah(i_swe_book_name):
        t_bible_book_number = 24

    elif sweLamentations(i_swe_book_name):
        t_bible_book_number = 25

    elif sweEzekiel(i_swe_book_name):
        t_bible_book_number = 26

    elif sweDaniel(i_swe_book_name):
        t_bible_book_number = 27

    elif sweHosea(i_swe_book_name):
        t_bible_book_number = 28

    elif sweJoel(i_swe_book_name):
        t_bible_book_number = 29

    elif sweAmos(i_swe_book_name):
        t_bible_book_number = 30

    elif sweObadiah(i_swe_book_name):
        t_bible_book_number = 31

    elif sweJonah(i_swe_book_name):
        t_bible_book_number = 32

    elif sweMicah(i_swe_book_name):
        t_bible_book_number = 33

    elif sweNahum(i_swe_book_name):
        t_bible_book_number = 34

    elif sweHabakkuk(i_swe_book_name):
        t_bible_book_number = 35

    elif sweZephaniah(i_swe_book_name):
        t_bible_book_number = 36

    elif sweHaggai(i_swe_book_name):
        t_bible_book_number = 37

    elif sweZechariah(i_swe_book_name):
        t_bible_book_number = 38

    elif sweMalachi(i_swe_book_name):
        t_bible_book_number = 39

    elif sweMatthew(i_swe_book_name):
        t_bible_book_number = 40

    elif sweMark(i_swe_book_name):
        t_bible_book_number = 41

    elif sweLuke(i_swe_book_name):
        t_bible_book_number = 42

    elif sweJohn(i_swe_book_name):
        t_bible_book_number = 43

    elif sweActs(i_swe_book_name):
        t_bible_book_number = 44

    elif sweRomans(i_swe_book_name):
        t_bible_book_number = 45

    elif swe1Corinthians(i_swe_book_name):
        t_bible_book_number = 46

    elif swe2Corinthians(i_swe_book_name):
        t_bible_book_number = 47

    elif sweGalatians(i_swe_book_name):
        t_bible_book_number = 48

    elif sweEphesians(i_swe_book_name):
        t_bible_book_number = 49

    elif swePhilippians(i_swe_book_name):
        t_bible_book_number = 50

    elif sweColossians(i_swe_book_name):
        t_bible_book_number = 51

    elif swe1Thessalonians(i_swe_book_name):
        t_bible_book_number = 52

    elif swe2Thessalonians(i_swe_book_name):
        t_bible_book_number = 53

    elif swe1Timothy(i_swe_book_name):
        t_bible_book_number = 54

    elif swe2Timothy(i_swe_book_name):
        t_bible_book_number = 55

    elif sweTitus(i_swe_book_name):
        t_bible_book_number = 56

    elif swePhilemon(i_swe_book_name):
        t_bible_book_number = 57

    elif sweHebrews(i_swe_book_name):
        t_bible_book_number = 58

    elif sweJames(i_swe_book_name):
        t_bible_book_number = 59

    elif swe1Peter(i_swe_book_name):
        t_bible_book_number = 60

    elif swe2Peter(i_swe_book_name):
        t_bible_book_number = 61

    elif swe1John(i_swe_book_name):
        t_bible_book_number = 62

    elif swe2John(i_swe_book_name):
        t_bible_book_number = 63

    elif swe3John(i_swe_book_name):
        t_bible_book_number = 64

    elif sweJude(i_swe_book_name):
        t_bible_book_number = 65

    elif sweRevelation(i_swe_book_name):
        t_bible_book_number = 66
    else:
        # print ("Didn't find book")
        t_bible_book_number = 67
    # print("End")
    return t_bible_book_number


def get_book_number_from_english_book_names(i_eng_bookname):
    if engGenesis(i_eng_bookname):
        t_bible_book_number = 1

    elif engExodus(i_eng_bookname):
        t_bible_book_number = 2

    elif engLeviticus(i_eng_bookname):
        t_bible_book_number = 3

    elif engNumbers(i_eng_bookname):
        t_bible_book_number = 4

    elif engDeuteronomy(i_eng_bookname):
        t_bible_book_number = 5

    elif engLeviticus(i_eng_bookname):
        t_bible_book_number = 6

    elif engNumbers(i_eng_bookname):
        t_bible_book_number = 7

    elif engDeuteronomy(i_eng_bookname):
        t_bible_book_number = 8

    elif engJoshua(i_eng_bookname):
        t_bible_book_number = 9

    elif engJudges(i_eng_bookname):
        t_bible_book_number = 10

    elif engRuth(i_eng_bookname):
        t_bible_book_number = 11

    elif eng1Samuel(i_eng_bookname):
        t_bible_book_number = 12

    elif eng2Samuel(i_eng_bookname):
        t_bible_book_number = 13

    elif eng1Kings(i_eng_bookname):
        t_bible_book_number = 14

    elif eng2Kings(i_eng_bookname):
        t_bible_book_number = 15

    elif eng1Chronicles(i_eng_bookname):
        t_bible_book_number = 16

    elif eng2Chronicles(i_eng_bookname):
        t_bible_book_number = 17

    elif engEzra(i_eng_bookname):
        t_bible_book_number = 18

    elif engNehemiah(i_eng_bookname):
        t_bible_book_number = 19

    elif engEsther(i_eng_bookname):
        t_bible_book_number = 20

    elif engJob(i_eng_bookname):
        t_bible_book_number = 21

    elif engPsalm(i_eng_bookname):
        t_bible_book_number = 22

    elif engProverbs(i_eng_bookname):
        t_bible_book_number = 23

    elif engEcclesiastes(i_eng_bookname):
        t_bible_book_number = 22

    elif engSongofSongs(i_eng_bookname):
        t_bible_book_number = 23

    elif engIsaiah(i_eng_bookname):
        t_bible_book_number = 24

    elif engJeremiah(i_eng_bookname):
        t_bible_book_number = 25

    elif engLamentations(i_eng_bookname):
        t_bible_book_number = 26

    elif engEzekiel(i_eng_bookname):
        t_bible_book_number = 27

    elif engDaniel(i_eng_bookname):
        t_bible_book_number = 28

    elif engHosea(i_eng_bookname):
        t_bible_book_number = 29

    elif engJoel(i_eng_bookname):
        t_bible_book_number = 30

    elif engAmos(i_eng_bookname):
        t_bible_book_number = 31

    elif engObadiah(i_eng_bookname):
        t_bible_book_number = 31

    elif engJonah(i_eng_bookname):
        t_bible_book_number = 32

    elif engMicah(i_eng_bookname):
        t_bible_book_number = 33

    elif engNahum(i_eng_bookname):
        t_bible_book_number = 34

    elif engHabakkuk(i_eng_bookname):
        t_bible_book_number = 35

    elif engZephaniah(i_eng_bookname):
        t_bible_book_number = 36

    elif engHaggai(i_eng_bookname):
        t_bible_book_number = 37

    elif engZechariah(i_eng_bookname):
        t_bible_book_number = 38

    elif engMalachi(i_eng_bookname):
        t_bible_book_number = 39

    elif engMatthew(i_eng_bookname):
        t_bible_book_number = 40

    elif engMark(i_eng_bookname):
        t_bible_book_number = 41

    elif engLuke(i_eng_bookname):
        t_bible_book_number = 42

    elif engJohn(i_eng_bookname):
        t_bible_book_number = 43

    elif engActs(i_eng_bookname):
        t_bible_book_number = 44

    elif engRomans(i_eng_bookname):
        t_bible_book_number = 45

    elif eng1Corinthians(i_eng_bookname):
        t_bible_book_number = 46

    elif eng2Corinthians(i_eng_bookname):
        t_bible_book_number = 47

    elif engGalatians(i_eng_bookname):
        t_bible_book_number = 48

    elif engEphesians(i_eng_bookname):
        t_bible_book_number = 49

    elif engPhilippians(i_eng_bookname):
        t_bible_book_number = 50

    elif engColossians(i_eng_bookname):
        t_bible_book_number = 51

    elif eng1Thessalonians(i_eng_bookname):
        t_bible_book_number = 52

    elif eng2Thessalonians(i_eng_bookname):
        t_bible_book_number = 53

    elif eng1Timothy(i_eng_bookname):
        t_bible_book_number = 54

    elif eng2Timothy(i_eng_bookname):
        t_bible_book_number = 55

    elif engTitus(i_eng_bookname):
        t_bible_book_number = 56

    elif engPhilemon(i_eng_bookname):
        t_bible_book_number = 57

    elif engHebrews(i_eng_bookname):
        t_bible_book_number = 58

    elif engJames(i_eng_bookname):
        t_bible_book_number = 59

    elif eng1Peter(i_eng_bookname):
        t_bible_book_number = 60

    elif eng2Peter(i_eng_bookname):
        t_bible_book_number = 61

    elif eng1John(i_eng_bookname):
        t_bible_book_number = 62

    elif eng2John(i_eng_bookname):
        t_bible_book_number = 63

    elif eng3John(i_eng_bookname):
        t_bible_book_number = 64

    elif engJude(i_eng_bookname):
        t_bible_book_number = 65

    elif engRevelation(i_eng_bookname):
        t_bible_book_number = 66
    else:
        # print ("Didn't find book")
        t_bible_book_number = 67
    # print("End")

    # print ("Bible book number: ", t_bible_book_number)
    return t_bible_book_number


def get_swedish_bible_book_name_from_bible_book_number(i_bibleBookNumber):
    swedishBibleBookNameString = ["1 Mos", "2 Mos", "3 Mos", "4 Mos", "5 Mos",
                                  "Jos", "Dom", "Rut", "1 Sam", "Sam",
                                  "1 Kung", "Kung", "1 Krön", "Krön", "Esra",
                                  "Neh", "Est", "Job", "Ps", "Ords",
                                  "Pred", "Höga V", "Jes", "Jer", "Klag",
                                  "Hes", "Dan", "Hos", "Joel", "Amos",
                                  "Ob", "Jona", "Mika", "Nah", "Hab",
                                  "Sef", "Hagg", "Sak", "Mal",
                                  "Matt", "Mark", "Luk", "Joh", "Apg",
                                  "Rom", "1 Kor", "Kor", "Gal", "Ef",
                                  "Fil", "Kol", "1 Thess", "Thess", "1 Tim",
                                  "Tim", "Tit", "Filem", "Hebr", "Jak",
                                  "1 Pet", "Pet", "1 Joh", "Joh", " Joh", "Jud", "Upp", "Unresolved"]

    t_bible_book_name = swedishBibleBookNameString[i_bibleBookNumber - 1]

    return t_bible_book_name


def write_to_file(output_file, t_verses):
    # utf-8
    with open(output_file, mode='wt', encoding='latin-1') as myfile:
        for t_line in t_verses:
            myfile.write(t_line)

# <editor-fold desc="Description">
def get_execution_mode(i_file):
    with open(i_file, encoding="latin-1") as f:
        t_lines = f.readlines()

        for t_line in t_lines:
            # print("Config line: " + t_line)
            try:
                # print("Config line: " + t_line + "t_line(0): " + t_line[0])
                if t_line[0] != "#":
                    ##find_references_in_english_text
                    # find_references_in_swedish_text
                    if t_line.find("find_references_in_english_text") > -1:
                        execution_mode = "english_text"
                    elif t_line.find("find_references_in_swedish_text") > -1:
                        execution_mode = "swedish_text"
                    else:
                        execution_mode = "none"
            except ValueError:
                if t_line is not " ":
                    print("ValueError" + t_line)

        # print("Execution mode: " +execution_mode)
    return execution_mode
# </editor-fold>

#################### ENGLLISH #####################

def get_samuel_english_bible_book_number(i_line):
    if i_line.find("1 Sam") > -1:
        t_bible_book_number = 9
    elif i_line.find("1 Sam.") > -1:
        t_bible_book_number = 9
    elif i_line.find("1 Samuel") > -1:
        t_bible_book_number = 9
    elif i_line.find("First Samuel") > -1:
        t_bible_book_number = 9
    elif i_line.find("First Sam") > -1:
        t_bible_book_number = 9
    if i_line.find("2 Sam") > -1:
        t_bible_book_number = 10
    elif i_line.find("2 Sam.") > -1:
        t_bible_book_number = 10
    elif i_line.find("2 Samuel") > -1:
        t_bible_book_number = 10
    elif i_line.find("Second Samuel") > -1:
        t_bible_book_number = 10
    elif i_line.find("Second Sam") > -1:
        t_bible_book_number = 10
    return t_bible_book_number


def get_kings_english_bible_book_number(i_line):
    t_bible_book_number = 11
    if i_line.find("1 King") > -1:
        t_bible_book_number = 11
    elif i_line.find("1 King.") > -1:
        t_bible_book_number = 11
    elif i_line.find("1 Kings") > -1:
        t_bible_book_number = 11
    elif i_line.find("First Kings") > -1:
        t_bible_book_number = 11
    elif i_line.find("First King") > -1:
        t_bible_book_number = 11
    if i_line.find("2 King") > -1:
        t_bible_book_number = 12
    elif i_line.find("2 King.") > -1:
        t_bible_book_number = 12
    elif i_line.find("2 Kings") > -1:
        t_bible_book_number = 12
    elif i_line.find("Second Kings") > -1:
        t_bible_book_number = 12
    elif i_line.find("Second Kung") > -1:
        t_bible_book_number = 12
    return t_bible_book_number


def get_chronicles_english_bible_book_number(i_line):
    t_bible_book_number = 13
    if i_line.find("1 Chron") > -1:
        t_bible_book_number = 13
    elif i_line.find("1 Chron.") > -1:
        t_bible_book_number = 13
    elif i_line.find("1 Chronicles") > -1:
        t_bible_book_number = 13
    elif i_line.find("First Chronicles") > -1:
        t_bible_book_number = 13
    elif i_line.find("First Chron") > -1:
        t_bible_book_number = 13
    if i_line.find("2 Chron") > -1:
        t_bible_book_number = 14
    elif i_line.find("2 Chron.") > -1:
        t_bible_book_number = 14
    elif i_line.find("2 Chronicles") > -1:
        t_bible_book_number = 14
    elif i_line.find("Second Chronicles") > -1:
        t_bible_book_number = 14
    elif i_line.find("Second Krön") > -1:
        t_bible_book_number = 14
    return t_bible_book_number


def get_corinthians_english_bible_book_number(i_line):
    t_bible_book_number = 46
    if i_line.find("1 Cor") > -1:
        t_bible_book_number = 46
    elif i_line.find("1 Cor.") > -1:
        t_bible_book_number = 46
    elif i_line.find("1 Corinthians") > -1:
        t_bible_book_number = 46
    elif i_line.find("Första Corinthians") > -1:
        t_bible_book_number = 46
    elif i_line.find("First Cor") > -1:
        t_bible_book_number = 46
    if i_line.find("2 Cor") > -1:
        t_bible_book_number = 47
    elif i_line.find("2 Cor.") > -1:
        t_bible_book_number = 47
    elif i_line.find("2 Corinthians") > -1:
        t_bible_book_number = 47
    elif i_line.find("Second Corinthians") > -1:
        t_bible_book_number = 47
    elif i_line.find("Second Cor") > -1:
        t_bible_book_number = 47
    return t_bible_book_number


def get_thessalonians_english_bible_book_number(i_line):
    t_bible_book_number = 52
    if i_line.find("1 Thess") > -1:
        t_bible_book_number = 52
    elif i_line.find("1 Thess.") > -1:
        t_bible_book_number = 52
    elif i_line.find("1 Thessalonians") > -1:
        t_bible_book_number = 52
    elif i_line.find("First Thessalonians") > -1:
        t_bible_book_number = 52
    elif i_line.find("First Thess") > -1:
        t_bible_book_number = 52
    if i_line.find("2 Thess") > -1:
        t_bible_book_number = 53
    elif i_line.find("2 Thess.") > -1:
        t_bible_book_number = 53
    elif i_line.find("2 Thessalonians") > -1:
        t_bible_book_number = 53
    elif i_line.find("Second Thessalonians") > -1:
        t_bible_book_number = 53
    elif i_line.find("Second Thess") > -1:
        t_bible_book_number = 53
    return t_bible_book_number


def get_timothy_english_bible_book_number(i_line):
    t_bible_book_number = 54
    if i_line.find("1 Tim") > -1:
        t_bible_book_number = 54
    elif i_line.find("1 Tim.") > -1:
        t_bible_book_number = 54
    elif i_line.find("1 Timothy") > -1:
        t_bible_book_number = 54
    elif i_line.find("First Timothy") > -1:
        t_bible_book_number = 54
    elif i_line.find("First Tim") > -1:
        t_bible_book_number = 54
    if i_line.find("2 Tim") > -1:
        t_bible_book_number = 55
    elif i_line.find("2 Tim.") > -1:
        t_bible_book_number = 55
    elif i_line.find("2 Timothy") > -1:
        t_bible_book_number = 55
    elif i_line.find("Second Timothy") > -1:
        t_bible_book_number = 55
    elif i_line.find("Second Tim") > -1:
        t_bible_book_number = 55
    return t_bible_book_number


def get_peter_english_bible_book_number(i_line):
    t_bible_book_number = 60
    if i_line.find("1 Pet") > -1:
        t_bible_book_number = 60
    elif i_line.find("1 Pet.") > -1:
        t_bible_book_number = 60
    elif i_line.find("1 Peter") > -1:
        t_bible_book_number = 60
    elif i_line.find("First Peter") > -1:
        t_bible_book_number = 60
    elif i_line.find("First Petr") > -1:
        t_bible_book_number = 60
    if i_line.find("2 Petr") > -1:
        t_bible_book_number = 61
    elif i_line.find("2 Petr.") > -1:
        t_bible_book_number = 61
    elif i_line.find("2 Peter") > -1:
        t_bible_book_number = 61
    elif i_line.find("Second Peter") > -1:
        t_bible_book_number = 61
    elif i_line.find("Second Petr") > -1:
        t_bible_book_number = 61
    return t_bible_book_number


def get_john_english_bible_book_number(i_line):
    t_bible_book_number = 43
    print("Line: " + i_line)
    # if i_line.find("Joh") > -1:
    #     t_bible_book_number = 43
    if i_line.find("1 Joh") > -1:
        t_bible_book_number = 62
    elif i_line.find("1 Joh.") > -1:
        t_bible_book_number = 62
    elif i_line.find("1 John") > -1:
        t_bible_book_number = 62
    elif i_line.find("First John") > -1:
        t_bible_book_number = 62
    elif i_line.find("First Joh") > -1:
        t_bible_book_number = 62
    if i_line.find("2 Joh") > -1:
        t_bible_book_number = 63
    elif i_line.find("2 Joh.") > -1:
        t_bible_book_number = 63
    elif i_line.find("2 John") > -1:
        t_bible_book_number = 63
    elif i_line.find("Second John") > -1:
        t_bible_book_number = 63
    elif i_line.find("Second Joh") > -1:
        t_bible_book_number = 63
    elif i_line.find("3 Joh") > -1:
        t_bible_book_number = 64
    elif i_line.find("3 Joh.") > -1:
        t_bible_book_number = 64
    elif i_line.find("3 John") > -1:
        t_bible_book_number = 64
    elif i_line.find("Third John") > -1:
        t_bible_book_number = 64
    elif i_line.find("Third Joh") > -1:
        t_bible_book_number = 64
    # elif i_line.find("Joh") > -1:
    #     t_bible_book_number = 43
    return t_bible_book_number

################# SWEDISH START #####################################################

def get_genesis_swedish_bible_book_number(i_line):
    if i_line.find("1 Mos") > -1:
        t_bible_book_number = 1
    elif i_line.find("1. Mos.") > -1:
        t_bible_book_number = 1
    elif i_line.find("FÃ¶rsta Moseboken") > -1:
        t_bible_book_number = 1
    elif i_line.find("2 Mos") > -1:
        t_bible_book_number = 2
    elif i_line.find("3 Mos") > -1:
        t_bible_book_number = 3
    elif i_line.find("4 Mos") > -1:
        t_bible_book_number = 4
    elif i_line.find("5 Mos") > -1:
        t_bible_book_number = 5
    elif i_line.find("1Mos") > -1:
        t_bible_book_number = 1
    elif i_line.find("2Mos") > -1:
        t_bible_book_number = 2
    elif i_line.find("2Mos") > -1:
        t_bible_book_number = 2
    elif i_line.find("3Mos") > -1:
        t_bible_book_number = 3
    elif i_line.find("4Mos") > -1:
        t_bible_book_number = 4
    elif i_line.find("5Mos") > -1:
        t_bible_book_number = 5
    else:
        # print("1568")
        t_bible_book_number = 1
    return t_bible_book_number


def get_samuel_swedish_bible_book_number(i_line):
    if i_line.find("1 Sam") > -1:
        t_bible_book_number = 9
    elif i_line.find("1 Sam.") > -1:
        t_bible_book_number = 9
    elif i_line.find("1 Samuelsboken") > -1:
        t_bible_book_number = 9
    elif i_line.find("Första Samuelsboken") > -1:
        t_bible_book_number = 9
    elif i_line.find("Första Sam") > -1:
        t_bible_book_number = 9
    if i_line.find("2 Sam") > -1:
        t_bible_book_number = 10
    elif i_line.find("2 Sam.") > -1:
        t_bible_book_number = 10
    elif i_line.find("2 Samuelsboken") > -1:
        t_bible_book_number = 10
    elif i_line.find("Andra Samuelsboken") > -1:
        t_bible_book_number = 10
    elif i_line.find("Andra Sam") > -1:
        t_bible_book_number = 10
    return t_bible_book_number


def get_kings_swedish_bible_book_number(i_line):
    t_bible_book_number = 11
    if i_line.find("1 Kung") > -1:
        t_bible_book_number = 11
    elif i_line.find("1 Kung.") > -1:
        t_bible_book_number = 11
    elif i_line.find("1 Kungaboken") > -1:
        t_bible_book_number = 11
    elif i_line.find("Första Kungaboken") > -1:
        t_bible_book_number = 11
    elif i_line.find("Första Kung") > -1:
        t_bible_book_number = 11
    if i_line.find("2 Kung") > -1:
        t_bible_book_number = 12
    elif i_line.find("2 Kung.") > -1:
        t_bible_book_number = 12
    elif i_line.find("2 Kungabokan") > -1:
        t_bible_book_number = 12
    elif i_line.find("Andra Kungabokan") > -1:
        t_bible_book_number = 12
    elif i_line.find("Andra Kung") > -1:
        t_bible_book_number = 12
    return t_bible_book_number


def get_chronicles_swedish_bible_book_number(i_line):
    t_bible_book_number = 13
    if i_line.find("1 Krön") > -1:
        t_bible_book_number = 13
    elif i_line.find("1 Krön.") > -1:
        t_bible_book_number = 13
    elif i_line.find("1 Krönikerboken") > -1:
        t_bible_book_number = 13
    elif i_line.find("Första Krönikerboken") > -1:
        t_bible_book_number = 13
    elif i_line.find("Första Krön") > -1:
        t_bible_book_number = 13
    if i_line.find("2 Krön") > -1:
        t_bible_book_number = 14
    elif i_line.find("2 Krön.") > -1:
        t_bible_book_number = 14
    elif i_line.find("2 Krön") > -1:
        t_bible_book_number = 14
    elif i_line.find("Andra Krönikerboken") > -1:
        t_bible_book_number = 14
    elif i_line.find("Andra Krön") > -1:
        t_bible_book_number = 14
    return t_bible_book_number


def get_corinthians_swedish_bible_book_number(i_line):
    t_bible_book_number = 46
    if i_line.find("1 Kor") > -1:
        t_bible_book_number = 46
    elif i_line.find("1 Kor.") > -1:
        t_bible_book_number = 46
    elif i_line.find("1 Korintierbrevet") > -1:
        t_bible_book_number = 46
    elif i_line.find("Första Korintierbrevet") > -1:
        t_bible_book_number = 46
    elif i_line.find("Första Kor") > -1:
        t_bible_book_number = 46
    if i_line.find("2 Kor") > -1:
        t_bible_book_number = 47
    elif i_line.find("2 Kor.") > -1:
        t_bible_book_number = 47
    elif i_line.find("2 Korintierbrevet") > -1:
        t_bible_book_number = 47
    elif i_line.find("Andra Korintierbrevet") > -1:
        t_bible_book_number = 47
    elif i_line.find("Andra Kor") > -1:
        t_bible_book_number = 47
    return t_bible_book_number


def get_thessalonians_swedish_bible_book_number(i_line):
    t_bible_book_number = 52
    if i_line.find("1 Thess") > -1:
        t_bible_book_number = 52
    elif i_line.find("1 Thess.") > -1:
        t_bible_book_number = 52
    elif i_line.find("1 Thessalonikebrevet") > -1:
        t_bible_book_number = 52
    elif i_line.find("Första Thessalonikebrevet") > -1:
        t_bible_book_number = 52
    elif i_line.find("Första Thess") > -1:
        t_bible_book_number = 52
    if i_line.find("2 Thess") > -1:
        t_bible_book_number = 53
    elif i_line.find("2 Thess.") > -1:
        t_bible_book_number = 53
    elif i_line.find("2 Thessalonikebrevet") > -1:
        t_bible_book_number = 53
    elif i_line.find("Andra Thessalonikebrevet") > -1:
        t_bible_book_number = 53
    elif i_line.find("Andra Thess") > -1:
        t_bible_book_number = 53
    return t_bible_book_number


def get_timothy_swedish_bible_book_number(i_line):
    t_bible_book_number = 54
    if i_line.find("1 Tim") > -1:
        t_bible_book_number = 54
    elif i_line.find("1 Tim.") > -1:
        t_bible_book_number = 54
    elif i_line.find("1 Timoteusbrevet") > -1:
        t_bible_book_number = 54
    elif i_line.find("Första Timoteusbrevet") > -1:
        t_bible_book_number = 54
    elif i_line.find("Första Tim") > -1:
        t_bible_book_number = 54
    if i_line.find("2 Tim") > -1:
        t_bible_book_number = 55
    elif i_line.find("2 Tim.") > -1:
        t_bible_book_number = 55
    elif i_line.find("2 Timoteusbrevet") > -1:
        t_bible_book_number = 55
    elif i_line.find("Andra Timoteusbrevet") > -1:
        t_bible_book_number = 55
    elif i_line.find("Andra Tim") > -1:
        t_bible_book_number = 55
    return t_bible_book_number


def get_peter_swedish_bible_book_number(i_line):
    t_bible_book_number = 60
    if i_line.find("1 Petr") > -1:
        t_bible_book_number = 60
    elif i_line.find("1 Petr.") > -1:
        t_bible_book_number = 60
    elif i_line.find("1 Petrusbrevet") > -1:
        t_bible_book_number = 60
    elif i_line.find("Första Petrusbrevet") > -1:
        t_bible_book_number = 60
    elif i_line.find("Första Petr") > -1:
        t_bible_book_number = 60
    if i_line.find("2 Petr") > -1:
        t_bible_book_number = 61
    elif i_line.find("2 Petr.") > -1:
        t_bible_book_number = 61
    elif i_line.find("2 Petrusbrevet") > -1:
        t_bible_book_number = 61
    elif i_line.find("Andra Petrusbrevet") > -1:
        t_bible_book_number = 61
    elif i_line.find("Andra Petr") > -1:
        t_bible_book_number = 61
    return t_bible_book_number


def get_john_swedish_bible_book_number(i_line):
    t_bible_book_number = 43
    print("Line: " + i_line)
    # if i_line.find("Joh") > -1:
    #     t_bible_book_number = 43
    if i_line.find("1 Joh") > -1:
        t_bible_book_number = 62
    elif i_line.find("1 Joh.") > -1:
        t_bible_book_number = 62
    elif i_line.find("1 Johannesbrevet") > -1:
        t_bible_book_number = 62
    elif i_line.find("Första Johannesbrevet") > -1:
        t_bible_book_number = 62
    elif i_line.find("Första Joh") > -1:
        t_bible_book_number = 62
    if i_line.find("2 Joh") > -1:
        t_bible_book_number = 63
    elif i_line.find("2 Joh.") > -1:
        t_bible_book_number = 63
    elif i_line.find("2 Johannesbrevet") > -1:
        t_bible_book_number = 63
    elif i_line.find("Andra Johannesbrevet") > -1:
        t_bible_book_number = 63
    elif i_line.find("Andra Joh") > -1:
        t_bible_book_number = 63
    elif i_line.find("3 Joh") > -1:
        t_bible_book_number = 64
    elif i_line.find("3 Joh.") > -1:
        t_bible_book_number = 64
    elif i_line.find("3 Johannesbrevet") > -1:
        t_bible_book_number = 64
    elif i_line.find("Tredje Johannesbrevet") > -1:
        t_bible_book_number = 64
    elif i_line.find("Tredje Joh") > -1:
        t_bible_book_number = 64
    # elif i_line.find("Joh") > -1:
    #     t_bible_book_number = 43
    return t_bible_book_number


def get_references_from_swedish_text(input_file, output_file):
    # encoding "Latin-1" "utf-8" "ISO-8859-1"
    with open(input_file, encoding="latin-1") as f:
        t_all_lines = f.readlines()
        book_numbers_with_prefix_list = [1, 2, 3, 4, 5, 9, 10, 11, 12, 13, 14, 46, 47, 52, 53, 54, 55, 60, 61, 62, 63,
                                         64]
        print("Result from get_references_from_swedish_text" + "\n")
        for t_line in t_all_lines:
            try:
                word_list = []
                word_list = t_line.split(" ")
                # print("Wordstring###: " + word_list.__str__())

                for word in word_list:
                    for char in word:
                        if char in "ï¿½?.!/;()":
                            word = word.replace(char, '')
                    # print("1528 Word¤¤: " +word)

                    # word.replace("(", '')
                    # print("1528: " +word)

                    if swe_book_names_no_prefix_list.__contains__(word):

                        print("Reference: " + word + " Line: " + t_line)
                        bible_book_number = get_bible_book_number_from_swedish_names(word)
                        # print("1791 bible_book_number: " +bible_book_number.__str__())

                        if bible_book_number < 68:

                            if bible_book_number == 1:  # OK
                                bible_book_number = get_genesis_swedish_bible_book_number(t_line)

                            elif bible_book_number == 9:  # OK
                                bible_book_number = get_samuel_swedish_bible_book_number(t_line)

                            elif bible_book_number == 11:  # OK
                                bible_book_number = get_kings_swedish_bible_book_number(t_line)

                            elif bible_book_number == 13:  # OK
                                bible_book_number = get_kings_swedish_bible_book_number(t_line)

                            elif bible_book_number == 46:  # OK
                                bible_book_number = get_corinthians_swedish_bible_book_number(t_line)

                            elif bible_book_number == 52:  # OK
                                bible_book_number = get_thessalonians_swedish_bible_book_number(t_line)

                            elif bible_book_number == 54:  # OK
                                bible_book_number = get_timothy_swedish_bible_book_number(t_line)

                            elif bible_book_number == 60:  # OK
                                bible_book_number = get_peter_swedish_bible_book_number(t_line)

                            elif bible_book_number == 62:  # OK
                                bible_book_number = get_john_swedish_bible_book_number(t_line)

                            # print("bibleBookNumber: " + bible_book_number.__str__()+" " + t_one_line)
                            t_chapter_start_index = t_line.index(word)

                            if bible_book_number in book_numbers_with_prefix_list:
                                t_chapter_start_index = t_chapter_start_index - 2

                            # print("t_chapter_start-index: " +t_chapter_start_index.__str__())
                            t_book = t_line[t_chapter_start_index:]
                            t_reference_end = t_line[t_chapter_start_index + 1:]

                            # print("1621 Swedish book name: " +get_swedish_bible_book_name_from_bible_book_number(bible_book_number) +" " +word)
                            # print("1834Line with bible book: " +t_book)

                            if t_book.find(":") > -1:

                                # Remove any characters after last digit
                                # last_digit = max([int(s) for s in t_book.split() if s.isdigit()])
                                # print("line & lastdigit: " +t_line + " " +last_digit.__str__())

                                ## Remove unneeded characters
                                #  �  "ï¿½":
                                for char in t_book:
                                    # if char in "ï":
                                    #     t_book = t_book.replace(char, '-')
                                    # if char in '€':
                                    #     t_book = t_book.replace(char, '-')
                                    if char in "?.!/()[];ââ":
                                        t_book = t_book.replace(char, '')
                                if t_book[0] == " ":
                                    t_book = t_book[1:]

                                if not t_book.endswith(": \n"):
                                    t_verses.append(t_book)
                                    print(t_book)
                        else:
                            print("1637 Bible book number: " + bible_book_number.__str__())

                            # t_book_and_ref_out = getSwedishBibleBookName(getBibleBookNumber(t_book)) + " " +t_reference_end
                            # t_verses.append(t_book_and_ref_out)
                            # print("Book: ", t_book, "Swedish biblebook: ", getSwedishBibleBookName(getBibleBookNumber(t_book)), t_reference_end)
                            # print(t_book_and_ref_out)
                    # else:
                    #     #print("1865 word: " +word)
                    #     print("")

            except ValueError:
                if t_line is not " ":
                    print("ValueError" + t_line)
        # print(t_verses)

        write_to_file(output_file, t_verses)

    return ()


def get_references_from_english_text(input_file, output_file):
    # print("get_references_from_english_text NOT IMPLEMENTED")

    with open(input_file, encoding="latin-1") as f:
        t_Lines = f.readlines()
        bookNumbersWithPrefixList = [9, 10, 11, 12, 13, 14, 46, 47, 52, 53, 54, 55, 60, 61, 62, 63, 64]

        for t_line in t_Lines:
            try:
                wordList = []
                wordList = t_line.split(" ")
                # print("Wordstring###: " + wordList.__str__())

                for word in wordList:

                    for char in word:
                        if char in "?.!/;:()":
                            word = word.replace(char, '')
                    # print("Word¤¤: " +word)

                    # word.replace("(", '')
                    # print(word)
                    if eng_book_names_no_prefix_list.__contains__(word):

                        # print("Reference: " + word +" Line: " + t_Line)
                        bible_book_number = get_book_number_from_english_book_names(word)
                        # print("BiblebookNumber###IN###: " +bibleBookNumber.__str__() +" " +t_Line)
                        if bible_book_number < 67:

                            if bible_book_number == 9:  # OK
                                if t_line.find("2 Samuel") > -1:
                                    bible_book_number = 10

                            if bible_book_number == 11:  # OK
                                if t_line.find("2 Chronicles") > -1:
                                    bible_book_number = 13

                            if bible_book_number == 13:  # OK
                                if t_line.find("2 Kings") > -1:
                                    bible_book_number = 14

                            if bible_book_number == 46:  # OK
                                if t_line.find("2 Corinthians") > -1:
                                    bible_book_number = 47

                            if bible_book_number == 52:  # OK
                                if t_line.find("2 Thessalonians") > -1:
                                    bible_book_number = 53

                            if bible_book_number == 54:  # OK
                                if (t_line.find("2 Timothy") > -1):
                                    # print("55 Timothy: * " +t_Line)
                                    bible_book_number = 55

                            if bible_book_number == 60:
                                if (t_line.find("2 Peter") > -1):
                                    bible_book_number = 61

                            if bible_book_number == 43:  # OK. This may be John(the Gospel), 1 John, 2 John, 3 John
                                # print("¤¤¤: " +t_Line)
                                if t_line.find("1 John") > -1:
                                    bible_book_number = 62
                                elif t_line.find("2 John") > -1:
                                    bible_book_number = 63
                                elif t_line.find("3 John") > -1:
                                    bible_book_number = 64

                            # print("bibleBookNumber: " + bibleBookNumber.__str__()+" " +t_Line)
                            t_chapter_start_index = t_line.index(word)

                            if bible_book_number in bookNumbersWithPrefixList:
                                t_chapter_start_index = t_chapter_start_index - 2

                            # print("t_chapter_start-index: " +t_chapter_start_index.__str__())
                            t_book = t_line[t_chapter_start_index:]
                            t_reference_end = t_line[t_chapter_start_index + 1:]

                            # print("Swedish book name: " +getSwedishBibleBookName(bibleBookNumber) +" " +t_reference_end)
                            # print("Line with bible book: " +t_book)

                            if t_book.find(":") > -1:

                                # Remove any characters after last digit
                                # last_digit = max([int(s) for s in t_book.split() if s.isdigit()])
                                # print("line & lastdigit: " +t_line + " " +last_digit.__str__())

                                ## Remove unneeded characters
                                for char in t_book:
                                    if char in "":
                                        t_book = t_book.replace(char, '-')
                                    if char in "?.!/()[];":
                                        t_book = t_book.replace(char, '')
                                if t_book[0] == " ":
                                    t_book = t_book[1:]

                                if not t_book.endswith(": \n"):
                                    t_verses.append(t_book)
                                    print(t_book)

                            # t_book_and_ref_out = getSwedishBibleBookName(getBibleBookNumber(t_book)) + " " +t_reference_end
                            # t_verses.append(t_book_and_ref_out)
                            # print("Book: ", t_book, "Swedish biblebook: ", getSwedishBibleBookName(getBibleBookNumber(t_book)), t_reference_end)
                            # print(t_book_and_ref_out)

            except ValueError:
                if t_line is not " ":
                    print("ValueError" + t_line)
        # print(t_verses)

        write_to_file(output_file, t_verses)

    return ()

    # print("get_references_from_english_text NOT IMPLEMENTED")

if __name__ == '__main__':

    # i_Eng_Bookname = "Matthew"
    # print("Bible book number: ", getBibleBookNumber(i_Eng_Bookname))
    # print("Bible book name:   ", getSwedishBibleBookName(getBibleBookNumber (i_Eng_Bookname)))

    t_verses = []

    eng_book_names_no_prefix_list = ["Genesis", "Gen", "Gen.",
                                     "Exodus", "Exo", "Exo.", "Ex.",
                                     "Leviticus", "Lev", "Lev",
                                     "Numbers", "Num", "Num",
                                     "Deuteronomy", "Deu", "Deu",
                                     "Joshua", "Jos", "Jos",
                                     "Judges", "Judg", "Judg.", "Jdg", "Jdg",
                                     "Ruth", "Ruth.", "Rth", "Rth",
                                     "Samuel", "Kings", "Chronicles",
                                     "Ezra", "Ezr", "Ezr.",
                                     "Nehemiah", "Neh", "Neh.",
                                     "Esther", "Est", "Est.",
                                     "Job", "Job.", "Psalm", "Psa", "Psa.",
                                     "Proverbs", "proverbs", "Pro", "Pro.",
                                     "Ecclesiastes", "Ecc", "Ecc.",
                                     "Song of Songs", "Son", "Son.",
                                     "Isaiah", "Isa", "Isa.",
                                     "Jeremiah", "Jer", "Jer.",
                                     "Lamentations", "Lam", "Lam.",
                                     "Ezekiel", "Eze", "Eze.",
                                     "Daniel", "Dan", "Dan.",
                                     "Hosea", "Hos", "Hos.",
                                     "Joel", "Joe", "Joe.",
                                     "Amos", "Amo", "Amo.",
                                     "Obadiah", "Oba", "Oba.",
                                     "Jonah", "Jon", "Jon.",
                                     "Micah", "Mic", "Mic.",
                                     "Nahum", "Nah", "Nah.",
                                     "Habakkuk", "Hab", "Hab.",
                                     "Zephaniah", "Zep", "Zep.",
                                     "Haggai", "Hag", "Hag.",
                                     "Zechariah", "Zec, Zec.",
                                     "Malachi", "Mal", "Mal.",
                                     "Matthew", "Mat", "Mat.",
                                     "Mark", "Mar", "Mar.",
                                     "Luke", "Luk", "Luk.",
                                     "John", "Joh", "Joh.",
                                     "Acts", "Act", "Act.",
                                     "Romans", "romans", "Rom", "Rom.",
                                     "Corinthians",
                                     "Galatians", "galatians", "Gal", "Gal.",
                                     "Ephesians", "Eph", "Eph.",
                                     "Philippians", "Php", "Php.", "Philip", "Philip.",
                                     "Colossians", "Col", "Col.", "Thessalonians", "Timothy",
                                     "Titus", "Tit", "Tit.",
                                     "Philemon", "Phm", "Phm.",
                                     "Hebrews", "hebrews", "Heb", "Heb.",
                                     "James", "Jas", "Jas.",
                                     "Peter", "John",
                                     "Jude", "Jud", "Jud.",
                                     "Revelation", "revelation", "Rev", "Rev."]

    swe_book_names_no_prefix_list = ["Mos", "Mos.", "1Mos.", "1Mos", "1.Mos.", "FÃ¶rsta Moseboken", "FörstaMoseboken",
                                     "Första Mos.", "Moseboken",
                                     "2 Mos", "2Mos.", "2Mos.", "2Mos", "2.Mos.", "AndraMoseboken", "Andra Mos.",
                                     "2Moseboken",
                                     "3 Mos", "3Mos.", "3Mos.", "3.Mos.", "3Mos.", "TredjeMoseboken", "Tredje Mos.",
                                     "3Moseboken",
                                     "4 Mos", "4 Mos.", "4Mos.", "4.Mos.", "4Mos.", "Fjärde Moseboken", "Fjärde Mos.",
                                     "4Moseboken",
                                     "5Mos", "5 Mos.", "5Mos.", "5.Mos.", "5Mos.", "Femte Moseboken", "FemteMos.",
                                     "5Moseboken",
                                     "Jos", "Josua", "Jos.", "Dom", "Domarboken", "Dom.",
                                     "Rut", "Ruts bok", "Rut.",
                                     "Sam", "FörstaSamuelsboken", "Sam.", "FörstaSamuelsboken", "Sam",
                                     "AndraSamuelsboken", "2Sam.", "2Samuelsboken",
                                     "Kung", "FörstaKungaboken", "Kung.",
                                     "2 Kung", "AndraKungaboken", "2 Kung.",
                                     "Krön", "FörstaKrönikeboken", "Krön.",
                                     "2 Krön", "AndraKrönikeboken", "2Krön.", "Esra", "Esr", "Esr.",
                                     "Neh", "Nehemja", "Neh.",
                                     "Est", "Esters bok", "Est.", "Job", "Jobs bok", "Job.", "Ps",
                                     "Psaltaren", "Psalm", "Psa.", "Psa", "Ords", "Ordspråksboken", "Ords.", "Ordspr.",
                                     "Pred", "Predikaren", "Pred.", "Höga V", "Höga Visan", "Höga V.", "Höga v.",
                                     "Jes", "Jesaja", "Jes.", "Jer", "Jeremia", "Jer.", "Klag", "Klagovisorna", "Klag.",
                                     "Hes", "Hesekiel", "Hes.", "Dan", "Daniel", "Dan.", "Hos", "Hosea", "Hos.", "Joel",
                                     "Joe", "Joe.", "Amos", "Amo", "Amo.", "Ob", "Obadja", "Ob.", "Oba.", "Jon", "Jona",
                                     "Jon.", "Mik", "Mika", "Mik.", "Nah", "Nahum", "Nah.", "Hab", "Habakkuk", "Hab.",
                                     "Sef", "Sefanja", "Sef.", "Hagg", "Haggai", "Hag.", "Sak", "Sakarja,", "Sak.",
                                     "Mal",
                                     "Malaki", "Mal.", "Matt", "Matteus", "Matt.", "Mat.", "Matteusevangeliet",
                                     "Mark", "Markus", "Mar.", "Markusevangeliet", "Luk", "Lukas", "Luk.",
                                     "Lukasevangeliet", "Joh", "Johannes", "Joh.", "Johannesevangeliet", "Apg",
                                     "Apostlagärningarna", "Apg.", "Rom", "Romarbrevet", "Rom.", "Romarna",
                                     "Kor", "Korintierbrevet", "1Kor", "1Kor.", "FörsteKorintierbrevet",
                                     "FörstaKorintierbrevet", "Första Kor", "FörstaKor.", "2Kor",
                                     "2Korintierbrevet", "2Kor", "2Kor.", "AndraKorintierbrevet",
                                     "AndreKorintierbrevet",
                                     "AndraKor", "AndraKor.", "Gal", "Galaterbrevet", "Gal.",
                                     "Ef", "Efesierbrevet", "Ef.", "Efésierbrevet", "Fil", "Filipperbrevet", "Fil.",
                                     "Kol", "Kolosserbrevet", "Kol.",
                                     "Thess", "Thessalonikerbrevet", "Thess.", "FörstaThessalonikerbrevet",
                                     "FörstaThess", "FörstaThess.",
                                     "2Thess", "2Thessalonikerbrevet", "2Thess.", "AndraThessalonikerbrevet",
                                     "AndraThess", "AndraThess.",
                                     "Tim", "Timoteusbrevet", "Tim.", "FörstaTimoteusbrevet", "FörstaTim.", "FörstaTim",
                                     "2 Tim", "2 Timoteusbrevet", "2 Tim.", "Andra Timoteusbrevet", "Andra Tim.",
                                     "AndraTim",
                                     "Tit", "Titusbrevet", "Tit.", "Titus", "Filem", "Filemon", "Filem.",
                                     "BrevettillFilemon",
                                     "Hebr", "Hebreerbrevet", "Hebr.", "Jak", "Jakobsbrevet", "Jak.", "Jakob",
                                     "Petr", "Petrusbrevet", "FörstaPetrusbrevet", "FörstaPetr", "FörstaPetr.",
                                     "1Petr.", "Petr.",
                                     "2 Petr", "2 Petrusbrevet", "Andra Petrusbrevet", "Andra Petr", "Andra Petr.",
                                     "2Petr.", "2 Petr.",
                                     "Joh", "Johannesbrevet", "Joh.", "Första Johannesbrevet,", "Första Joh.",
                                     "Första Joh",
                                     "2Joh", "2 Johannesbrevet", "2Joh.", "Andra Johannesbrevet,", "AndraJoh.",
                                     "AndraJoh",
                                     "3Joh", "3Johannesbrevet", "3Joh.", "TredjeJohannesbrevet,", "TredjeJoh.",
                                     "TredjeJoh",
                                     "Jud", "Judasbrevet", "Jud.", "Upp", "Uppenbarelseboken", "Upp."]

    eng_book_namesList2 = ["Genesis", "Gen", "Gen.",
                           "Exodus", "Exo", "Exo.", "Ex.",
                           "Leviticus", "Lev", "Lev",
                           "Numbers", "Num", "Num",
                           "Deuteronomy", "Deu", "Deu",
                           "Joshua", "Jos", "Jos",
                           "Judges", "Judg", "Judg.", "Jdg", "Jdg",
                           "Ruth", "Ruth.", "Rth", "Rth",
                           "1 Samuel", "1 Sam", "1 Sam.", "1Sa", "1Sa.", "I Samuel",
                           "2 Samuel", "2 Sam", "2 Sam.", "2Sa", "2Sa.", "II Samuel",
                           "1 Kings", "1 King", "1 King.", "1Ki", "1Ki.", "I Kings",
                           "2 Kings", "2 King", "2 King.", "2Ki", "2Ki.", "II Kings",
                           "1 Chronicles", "1 Chron", "1 Chron.", "1Ch", "1Ch.", "I Chronicles",
                           "2 Chronicles", "2 Chron", "1 Chron.", "2Ch", "2Ch.", "II Chronicles",
                           "Ezra", "Ezr", "Ezr.",
                           "Nehemiah", "Neh", "Neh.",
                           "Esther", "Est", "Est.",
                           "Job", "Job.", "Psalm", "Psa", "Psa.",
                           "Proverbs", "proverbs", "Pro", "Pro.",
                           "Ecclesiastes", "Ecc", "Ecc.",
                           "Song of Songs", "Son", "Son.",
                           "Isaiah", "Isa", "Isa.",
                           "Jeremiah", "Jer", "Jer.",
                           "Lamentations", "Lam", "Lam.",
                           "Ezekiel", "Eze", "Eze.",
                           "Daniel", "Dan", "Dan.",
                           "Hosea", "Hos", "Hos.",
                           "Joel", "Joe", "Joe.",
                           "Amos", "Amo", "Amo.",
                           "Obadiah", "Oba", "Oba.",
                           "Jonah", "Jon", "Jon.",
                           "Micah", "Mic", "Mic.",
                           "Nahum", "Nah", "Nah.",
                           "Habakkuk", "Hab", "Hab.",
                           "Zephaniah", "Zep", "Zep.",
                           "Haggai", "Hag", "Hag.",
                           "Zechariah", "Zec, Zec.",
                           "Malachi", "Mal", "Mal.",
                           "Matthew", "Mat", "Mat.",
                           "Mark", "Mar", "Mar.",
                           "Luke", "Luk", "Luk.",
                           "John", "Joh", "Joh.",
                           "Acts", "Act", "Act.",
                           "Romans", "romans", "Rom", "Rom.",
                           "1 Corinthians", "1st Corinthians", "first Corinthians", "1Co", "1Co.",
                           "2 Corinthians", "2nd Corinthians", "second Corinthians 2Co", "2Co.",
                           "Galatians", "galatians", "Gal", "Gal.",
                           "Ephesians", "Eph", "Eph.",
                           "Philippians", "Php", "Php.", "Philip", "Philip.",
                           "Colossians", "Col", "Col.",
                           "1 Thessalonians", "1Th", "1Th.",
                           "2 Thessalonians", "2Th", "2Th.",
                           "1 Timothy", "1st Timothy", "first Timothy", "1Ti", "1Ti.",
                           "2 Timothy", "2nd Timothy", "second Timothy", "2Ti", "2Ti.",
                           "Titus", "Tit", "Tit.",
                           "Philemon", "Phm", "Phm.",
                           "Hebrews", "hebrews", "Heb", "Heb.",
                           "James", "Jas", "Jas.",
                           "1 Peter", "1Pe", "1Pe.", "I Peter",
                           "2 Peter", "2Pe", "2Pe.", "II Peter",
                           "1 John", "1Jn", "1Jn.", "I John",
                           "2 John", "2Jn", "2Jn.", "II John",
                           "3 John", "3Jn", "3Jn.", "III John",
                           "Jude", "Jud", "Jud.",
                           "Revelation", "revelation", "Rev", "Rev."]

    input_file = "input.txt"
    output_file = "output.txt"
    config_file = "config.txt"

    execution_mode = get_execution_mode(config_file)

    if execution_mode == "english_text":
        get_references_from_english_text(input_file, output_file)
    elif execution_mode == "swedish_text":
        get_references_from_swedish_text(input_file, output_file)
    else:
        print("Execution mode NONE")
