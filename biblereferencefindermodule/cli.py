#!/usr/bin/env python3
import click
import os
from pathlib import Path

import biblereferencefindermodule.biblereferencefinder

###########################################################
def get_text(ctx, param, value):
    if not value and not click.get_text_stream('stdin').isatty():
        return click.get_text_stream('stdin').read().strip()
    else:
        return value
#############################################################

script_directory = os.path.dirname(os.path.realpath(__file__))

@click.command()
@click.argument('references_str', callback=get_text, required=False)
@click.option('--log_folder_name', '-o', show_default=True, default='Logfiles')
@click.option('--execution_mode', '-l', show_default=True, default='english_text')
@click.option('--infile_encoding', '-e', show_default=True, default='iso-8859-1')
@click.option('--input_from_speech', '-s', show_default=True, default=False)
def cli(references_str, log_folder_name, execution_mode, infile_encoding, input_from_speech):
    """Using the references in TEXT"""
    f_in = Path(script_directory) / '..' / 'input' / 'biblereferencefinder_input.txt'

    f_out_final_std = Path(script_directory) / '..' / 'output' / 'biblereferencefinder_output03_Final.txt'  # Contains the FINAL output from this program. One Bible Reference per line
    f_out_final_std_split = Path(script_directory) / '..' / 'output' / 'biblereferencefinder_output04_Final_Split.txt'


    biblereferencefindermodule.biblereferencefinder.get_bible_references_single_file(f_in, log_folder_name, execution_mode, infile_encoding, input_from_speech, f_out_final_std, f_out_final_std_split)


if __name__ == '__main__':
    cli()
