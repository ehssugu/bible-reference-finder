#!/usr/bin/env python
__author__ = 'Sune Gustafsson'
__copyright__ = 'Copyright 2019, Sune Gustafsson'
__credits__ = ['Sune Gustafsson']
__license__ = 'GPL'
__version__ = '1.0.1'
__date__ = '2019-09-13 08:47:13'
__maintainer__ = 'Sune Gustafsson'
__email__ = 'ehssugu@gmail.com'
__status__ = 'Test'

import datetime
import os
import re
import time
from pathlib import Path

import PyPDF2
import docx
from pptx import Presentation

from biblereferencefindereng import normalize_eng_bible_book_names, get_kjv_number_of_verses_per_chapter_list
from biblereferencefinderswe import normalize_swe_bible_book_names, get_FB98_SRB16_number_of_verses_per_chapter_list  # , normalized_swe_book_names_list

no_sub_folder = ''
sub_folder_for_logfiles = 'Logfiles'
# ENCODING = 'iso-8859-1'  # 2019-10-04 Does not work with PDF as input
# ENCODING = 'latin-1'  # 2019-10-04 Does not work with PDF as input
ENCODING = 'utf-8'  # 2019-10-04 Works with PDF as input
script_directory = os.path.dirname(os.path.realpath(__file__))

EXECUTION_FOLDER = Path(script_directory)
MAX_BIBLE_REFERENCE_LENGTH = 35  # Defines a maximum length of a Bible Text Reference (B+C+VxN) # 2024-01-27 Changed this from 30 t0 35
MAX_BIBLE_REFERENCE_LENGTH = 40  # Defines a maximum length of a Bible Text Reference (B+C+VxN) # 2024-01-27 Changed this from 35 to 40

DOT = '.'
COMMA = ','
COLON = ':'
SEMICOLON = ';'
DASH = '-'
NOSPACE = ''
ONESPACE = ' '
TWOSPACE = '  '
THREESPACE = '   '
NEWLINE = '\n'
TAB = '\t'
start_string = 'Bible Reference Finder - Start'
end_string = 'Bible Reference Finder - End'
LINESEPARATOR = '|============================================================'
# start_string =  '|                  Bible Reference Finder Results                      |'
VBAR = '| '


def get_file_content_as_str(f_in, infile_encoding):  # 2019-09-19
    # print('54 get_file_content_as_str With encoding: ' + infile_encoding)
    infile_encoding = 'latin1' # 2023-01-11 test
    with open(f_in, encoding=infile_encoding) as myfile:
        t_text = myfile.readlines().__str__()
    myfile.close()
    t_text = remove_char_in_txt_file(t_text)

    return t_text  # get_file_content_as_str


def prepare_input_text_file_from_txt_input(f_in_txt, f_out_txt, infile_encoding, outfile_encoding):
    # 2022-01-27
    # Used to clean-up characters from former PDF files

    txt_str = get_file_content_as_str(f_in_txt, infile_encoding)

    # Joel 2:1-8 \\uf0
    # Psalms 24:3-6 \\uf0b7
    # \uf0b7
    txt_str = re.sub(r' \\uf0b7', r'', txt_str)
    txt_str = re.sub(r' \\uf0', r'', txt_str)
    txt_str = re.sub(r' \\n', r'', txt_str)
    txt_str = re.sub(r'”', r'', txt_str)

    txt_str = txt_str.replace('\\x86', 'å')
    txt_str = txt_str.replace('\\x84', 'ä')
    # txt_str = txt_str.replace('\\x84', 'ö')

    txt_str = re.sub(r'Ã¥', r'å', txt_str)
    txt_str = re.sub(r'Ã', r'Å', txt_str)

    txt_str = re.sub(r'Ã¤', r'ä', txt_str)
    txt_str = re.sub(r'Ã„', r'Ä', txt_str)

    txt_str = re.sub(r'Å¶', r'ö', txt_str)
    txt_str = re.sub(r'Ã-', r'Ö', txt_str)

    if txt_str.__contains__('\\x83'):
        print(85)

    txt_str = txt_str.replace('\\', '') # 2023-11-02
    outfile_encoding = 'latin1'  # 2023-01-11 test
    write_list_to_file_with_line_breaks(f_out_txt,
                                        txt_str,
                                        outfile_encoding)

    return ()  # prepare_input_text_file_from_txt_input


def remove_non_digits_at_end_of_reference(single_reference: str):
    # Example "Revelation 6:14-17 in GC" => "Revelation 6:14-17"
    # Acts 1:9-11; DA
    # print('84 ' + single_reference)

    # Avoid cases with zero length and without any digits
    if single_reference.__len__() > 0 and re.findall('[0-9]', single_reference):
        # Greedy algorithm to fins last digit in string
        single_reference = re.sub(r'(.*\d).*', r'\1', single_reference)

    return single_reference  # remove_non_digits_at_end_of_reference


def post_process_difficult_references(single_reference: str):
    # 2021-02-27 Difficult references
    # Matthew 6:21 ISBN 0-7684-2960
    # Luke 24:45 SECTION 1
    # John 2:
    # John 2:
    # Numbers 14:34 Thus the 490
    # Revelation 14:1416
    # Matthew 12:1620
    # Romans 4:1821
    # Luke 21:2931
    # Numbers 14:4145
    # Revelation 14:5 about the 144
    # Revelation 2:4-5 2:3
    # Daniel 13:2
    # DONE Matthew 24:37-39 with verses 4
    # Matthew 24:37-39 38

    # Luke 24:45 SECTION 1

    single_reference = single_reference.replace(TWOSPACE, ONESPACE)

    single_reference = remove_non_digits_at_end_of_reference(single_reference)  # 2022-01-27

    # Daniel 8:14 If there is a 2000
    single_reference = re.sub(
        r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+) ([A-Z]+)([a-z]+) ([a-z]+) ([a-z]+) ([a-z]+) ([0-9]+)', r'\1\2 \3:\4',
        single_reference)

    # Revelation 12:14 as 1260
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+) ([a-z]+) ([0-9]+)', r'\1\2 \3:\4', single_reference)

    # Matthew 24:37-39 38
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+)-([0-9]+) ([0-9]+)', r'\1\2 \3:\4-\5',
                              single_reference)

    # Matthew 24:37-39 with verses 4
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+)-([0-9]+) ([a-z]+) ([a-z]+) ([0-9]+)',
                              r'\1\2 \3:\4-\5', single_reference)

    # Isaiah 12:1-6 Isaiah 12
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+)-([0-9]+) ([a-z]+)([a-z]+) ([0-9]+)',
                              r'\1\2 \3:\4-\5',
                              single_reference)

    # Matthew 6:21 ISBN 0-7684-2960
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+) ([A-Z]+) ([0-9]+)-([0-9]+)-([0-9]+)', r'\1\2 \3:\4',
                              single_reference)

    # Numbers 14:34 Thus the 490
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+) ([A-Z]+)([a-z]+) ([a-z]+) ([0-9]+)', r'\1\2 \3:\4',
                              single_reference)

    # Revelation 14:5 about the 144
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+) ([a-z]+) ([a-z]+) ([0-9]+)', r'\1\2 \3:\4',
                              single_reference)

    # Luke 24:45 SECTION 1
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+) ([A-Z]+) ([0-9]+)', r'\1\2 \3:\4', single_reference)

    # Luke 24:45 SECTION
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+) ([A-Z]+)', r'\1\2 \3:\4', single_reference)
    # print('84 ' + single_reference)

    # 1 Peter 2:11 fleshy lusts
    single_reference = re.sub(r'([a-z]+) ([0-9]+):([0-9]+) ([a-z]+) ([a-z]+)', r'\1 \2:\3', single_reference)

    # Mark 7:34 5 6
    # Mark 7:34 5 6
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+) ([0-9]+) ([0-9]+)',
                              r'\1\2 \3:\4', single_reference)

    # Proverbs 23:31 5
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+) ([0-9]+)',
                              r'\1\2 \3:\4', single_reference)

    # Mark 7:34phphata 5 6
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+)([a-z]+)',
                              r'\1\2 \3:\4', single_reference)

    # # Revelation 14:5ar 241
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+)([a-z]+) ([0-9]+)', r'\1\2 \3:\4', single_reference)

    # Revelation 14:7 and Rev 10
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+) ([a-z]+) ([A-Z]+) ([0-9]+)', r'\1\2 \3:\4',
                              single_reference)

    # Revelation 12:16-17 "1
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+)-([0-9]+) \"([0-9]+)',
                              r'\1\2 \3:\4-\5', single_reference)

    # Matthew 11:28-30 The 144
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+)-([0-9]+) ([A-Z]+)([a-z]+) ([0-9]+)',
                              r'\1\2 \3:\4-\5', single_reference)

    # Genesis 28:12-16 SOP PP 19
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+)-([0-9]+) ([A-Z]+) ([A-Z]+) ([0-9]+)',
                              r'\1\2 \3:\4-\5', single_reference)

    # Micah 1:10-16 See Dan
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+)-([0-9]+) ([A-Z]+)([a-z]+) ([A-Z]+)([a-z]+)',
                              r'\1\2 \3:\4-\5', single_reference)

    # Job 1:7 Septuagint
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+) ([A-Z]+)([a-z]+)',
                              r'\1\2 \3:\4', single_reference)

    # Jeremiah 18:18 Taanith 2
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+) ([A-Z]+)([a-z]+) ([0-9]+)',
                              r'\1\2 \3:\4', single_reference)

    # Genesis 34:3; S
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+); ([A-Z])',
                              r'\1\2 \3:\4', single_reference)

    # Revelation 17:3; cf
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+); ([a-z])',
                              r'\1\2 \3:\4', single_reference)

    # Revelation 22:1-5; Eze
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+)-([0-9]+); ([A-Z])([a-z]+)',
                              r'\1\2 \3:\4-\5', single_reference)

    # Daniel 12:4erses 9
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+)([a-z]+) ([0-9]+)',
                              r'\1\2 \3:\4', single_reference)

    # John 12:13 "57
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+) "([0-9]+)',
                              r'\1\2 \3:\4', single_reference)

    # Revelation 10:1f
    # Deuteronomy 6:8x
    # Exodus 40:33n
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+)([a-z])',
                              r'\1\2 \3:\4', single_reference)

    # Exodus 40:33 ; Gen
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+) ; ([A-Z])([a-z])',
                              r'\1\2 \3:\4', single_reference)

    # Isaiah 41:2erse 25
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+)([a-z]+) ([0-9]+)',
                              r'\1\2 \3:\4', single_reference)

    # Leviticus 23:9 -21 13
    single_reference = re.sub(r'([A-Z]+)([a-z]+) ([0-9]+):([0-9]+) -([0-9]+) ([0-9]+)',
                              r'\1\2 \3:\4-\5', single_reference)

    # Revelation "1:4
    single_reference = re.sub(r'([A-Z]+)([a-z]+) \"([0-9]+):([0-9]+)',
                              r'\1\2 \3:\4', single_reference)

    return single_reference  # post_process_difficult_references


def split_b_ref_by_semicolon2(i_str, bible_book_hit_list=[]):
    # Created 2021-02-13
    out_list = []
    # print('259: bible_book_hit_list ' + bible_book_hit_list.__str__())

    # f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
    # normalized_eng_book_names_list = get_file_rows_as_list(f)

    # Remove '\n' at end of reference
    i_str = i_str[:-1]

    split_list = i_str.split(';')
    # print('24 test_str: ' + split_list.__str__())

    if split_list.__len__() > 1:
        # Possible variants to sort out
        # 1. 'bk ch:vs'
        # vs may be 'vs' or 'vs1-vs2'
        # 2. 'ch:vs'
        # 3. 'ch1:vs1 ch2:vs2 ch3:vs3' ...
        # 4. 'vs'

        # Possible nice scenarios between each ";"
        # book1 ch:vs1
        # book1 ch:vs1-vs2
        # book1 ch:vs1, vs2, ... vsn
        #
        # Possible difficult scenarios
        # No book name => Return input string
        # book1 ch;
        # book1 ch; xx => Return []
        # book1 ch1:v1 ch2:1
        # book1 ch1:v1 book2 ch2:ch1 => i.e. no semicolon. => Return Instring
        # Instring "book1 ch1:v1; book2 ch2:v2" => ['book1 ch1:v1', 'book2 ch2:v2']

        found_chapter = False
        found_book = False
        book_name = 'book'
        for item in split_list:
            a_item = item.strip()

            if not found_book:
                # Check if book name is in two parts e.g. '1 Peter 2:5'
                for eng_book_name in bible_book_hit_list:
                    if a_item.startswith(eng_book_name):
                        book_name = eng_book_name
                        found_book = True

                # split a_item on space
                if book_name == 'book':
                    b_item_list = a_item.split(ONESPACE)
                else:
                    a = a_item[book_name.__len__() + 1:]
                    b_item_list = a.split(ONESPACE)
            else:
                b_item_list = a_item.split(ONESPACE)
                # # Avoid to add reference without ":"    2024-04-14
                # if a_item.__contains__(":"):
                #     b_item_list = a_item.split(ONESPACE)
                # else:
                #     b_item_list = []

            for b_item in b_item_list:
                # print(b_item)
                # # Handle 'Luke 12:35-37; COL 307-324' This quick fix didn't work 2024-01-09
                # if bible_book_hit_list.__contains__(b_item):
                if b_item.isalpha():
                    book_name = b_item

                else:

                    c_item_list = b_item.split(':')
                    if c_item_list.__len__() > 1:
                        chapter = c_item_list[0]
                        found_chapter = True
                        verse = c_item_list[1].replace(',', '')
                        out_list.append(book_name + ONESPACE + chapter + COLON + verse + NEWLINE)
                        # print(out_list.__str__())
                    else:
                        if found_chapter:
                            # We have a verse
                            verse = b_item.replace(',', '')
                            out_list.append(book_name + ONESPACE + chapter + COLON + verse + NEWLINE)
                        else:
                            # We have a chapter ony case
                            chapter = b_item
                            found_chapter = True

    else:
        # There is no SEMICOLON in instring
        out_list.append(i_str.strip() + NEWLINE)

    return out_list


def get_last_verse_number_in_bible_reference(i_str: str, i_execution_mode: str):

    kjv_number_of_verses_per_chapter_list = get_kjv_number_of_verses_per_chapter_list()
    fb98_srb16_number_of_verses_per_chapter_list = get_FB98_SRB16_number_of_verses_per_chapter_list()
    # 2021-02-13
    last_verse = '999'
    # Input format example: 'Isaiah 36'

    if i_execution_mode == 'english_text':

        for item in kjv_number_of_verses_per_chapter_list:
            book_and_chap = item[0:item.index(':')]
            if book_and_chap == i_str:
                last_verse = item[item.index(':') + 1:]
                return last_verse

    if i_execution_mode == 'swedish_text':
        for item in fb98_srb16_number_of_verses_per_chapter_list:
            book_and_chap = item[0:item.index(':')]
            if book_and_chap == i_str:
                last_verse = item[item.index(':') + 1:]
                return last_verse

    return last_verse  # get_last_verse_number_in_bible_reference


def split_reference_crossing_chapter_border(i_str: str, i_execution_mode: str):
    # Isaiah 36:21-37:20  ==>
    out_list = []
    out_list2 = []
    found = False

    # Check if this reference is across chapter border
    pattern = '([0-9]+):([0-9]+)-([0-9]+):([0-9]+)'
    # a = re.match(r'([0-9]+):([0-9]+)-([0-9]+):([0-9]+)', i_str)
    # a = re.match(r'([0-9]+):([0-9]+)-([0-9]+):([0-9]+)', i_str)
    for match in re.finditer(r'([0-9]+):([0-9]+)-([0-9]+):([0-9]+)', i_str):
        # print('153 found reference crossing chapter border ' + i_str)
        start_ix = match.start()
        end_ix = match.end()
        book = i_str[:start_ix - 1]
        ch_vs = i_str[len(book) + 1:].strip()
        a_list = ch_vs.split('-')
        ref1 = a_list[0]
        ch1 = ref1[0:ref1.index(':')]
        vs1 = ref1[ref1.index(':') + 1:]

        ref2 = a_list[1]
        ch2 = ref2[0:ref2.index(':')]
        vs2 = ref2[ref2.index(':') + 1:]

        last_verse = get_last_verse_number_in_bible_reference(book + ONESPACE + ch1, i_execution_mode)
        out_list.append(book + ONESPACE + ch1 + COLON + vs1 + DASH + last_verse + NEWLINE)

        if vs2 == "1":
            out_list.append(book + ONESPACE + ch2 + COLON + '1' + NEWLINE)
        else:
            out_list.append(book + ONESPACE + ch2 + COLON + '1' + DASH + vs2 + NEWLINE)
        found = True

        # Cleanup difficult endings for the single references
        for item in out_list:
            out_list2.append(post_process_difficult_references(item))  # 2021-04-24

    if not found:
        out_list2.append(i_str + NEWLINE)

    return out_list2  # split_reference_crossing_chapter_border


def split_b_ref_by_comma3(list_after_split_by_semicolon):
    # Created 2021-02-13
    out_list = []

    for outer_item in list_after_split_by_semicolon:

        # Remove '\n' at the end of string
        i_str = outer_item[:-1]

        split_list = i_str.split(',')
        # print('185 test_str: ' + split_list.__str__())
        # 185 test_str: ['Job 1', ' 2', ' 42:7-17\n']

        book = ''
        chapter = ''
        next_str = str
        for item in split_list:
            start_str = item.strip()
            if start_str.__contains__(':'):

                # We have a chapter and verse
                # Hebr 8:13, 9:15
                ix_end_book_name = start_str.find(ONESPACE)
                ix_end_chapter = start_str.rfind(COLON)
                if ix_end_book_name > 0:
                    book = start_str[0:ix_end_book_name]
                    chapter = start_str[ix_end_book_name + 1:ix_end_chapter]
                    verse = start_str[ix_end_chapter + 1:]
                    out_list.append(book + ONESPACE + chapter + COLON + verse + '\n')
                else:
                    chapter = start_str[0:start_str.find(':')]
                    verse = start_str[start_str.find(':') + 1:]

                    # print('book: ' + book + ' chapter: ' + chapter + ' verse: ' + verse)
                    out_list.append(book + ONESPACE + chapter + COLON + verse + '\n')
            else:
                # We don't have Book or chapter, i.e. re.use from last found
                next_str = start_str
                if next_str.isnumeric():
                    # We have a verse ONLY
                    out_list.append(book + ONESPACE + chapter + COLON + next_str + '\n')
                    # print('book: ' + book + ' chapter: ' + chapter + ' verse: ' + verse)
                else:
                    if next_str.__contains__(DASH):
                        out_list.append(book + ONESPACE + chapter + COLON + next_str + '\n')
                    else:
                        out_list.append(next_str + '\n')
                        if book == '':
                            if next_str.__contains__(' '):
                                book = next_str.split(' ')[0]

    return out_list


def get_2_tuple_as_list(f_in, new_line: bool):
    # 2020-10-03
    # This function assumes that the file data is organized in the following way
    # # <Text> Data that should be ignored
    # <Text-A><\t><Text-B><\n>
    #
    # Output is returned in a list with following format
    # [
    # [Text-A]['\n'Text-B]
    # ...
    # [Text-A]['\n'Text-B]
    # ]
    # OR
    # [
    # [Text-A][Text-B]
    # ...
    # [Text-A][Text-B]
    # ]

    out_list = []
    line_list = []

    if new_line:
        b_prefix = '\n'
    else:
        b_prefix = ''

    with open(f_in, 'r', encoding=ENCODING) as myfile:
        file_lines = myfile.readlines()

        for line in file_lines:
            if not line.startswith('#'):
                if line.__contains__('\t'):
                    a = line[0:line.index('\t')]
                    b = b_prefix + line[line.index('\t') + 1:line.index('\n')]
                    line_list.append(a)
                    line_list.append(b)
                    out_list.append(line_list)
                else:
                    print(VBAR + 'Error in file: ' + f_in + ' line: ' + line)
                line_list = []

    myfile.close()
    # print('186 t_text: ' +t_text.__str__())

    return out_list  # get_2_tuple_as_list


def print_start_program(start_str):
    print(NEWLINE +
          LINESEPARATOR + NEWLINE +
          VBAR + start_str + NEWLINE +
          LINESEPARATOR + NEWLINE + VBAR)
    return ()


def print_end_program(end_str):
    print(LINESEPARATOR + NEWLINE +
          VBAR + end_str + NEWLINE +
          LINESEPARATOR)
    return ()


def get_file_encoding(i_execution_mode: str):
    if i_execution_mode == 'swedish_text':
        out_encoding = 'iso-8859-1'
    else:
        out_encoding = ENCODING
    return out_encoding  # get_file_encoding


def prepare_incomplete_references_from_text(i_str):
    # This procedure prepares the input text to later add on the real BibleBook name. Therefore is "BibleBook" part of the "hitlist" when searching for references.
    # "BibleBook" is replaced in procedure "replace_fake_bible_book_names_with_prevoius"


    # then verses 5-8
    # i_str = re.sub(r'then verses ([0-9]+)-([0-9]+)\)', r'then verses BibleBook x:\1-\2', i_str)
    # # So verses 7-10
    # i_str = re.sub(r'So verses ([0-9]+)-([0-9]+)\)', r' So verses BibleBook x:\1-\2', i_str)
    # i_str = re.sub(r'so verses ([0-9]+)-([0-9]+)\)', r'so verses BibleBook x:\1-\2', i_str)
    # # than verse 3 and verse 5
    # i_str = re.sub(r'than verse ([0-9]+) and verse ([0-9]+)\)', r'than BibleBook x:\1 and BibleBook x:\2', i_str)
    # Verses 10-12
    i_str = re.sub(r'Verses ([0-9]+)-([0-9]+)', r'BibleBook x:\1-\2', i_str)
    i_str = re.sub(r'verses ([0-9]+)-([0-9]+)', r'BibleBook x:\1-\2', i_str)
    # verses 16, 17
    i_str = re.sub(r'Verses ([0-9]+), ([0-9]+)', r'BibleBook x:\1, \2', i_str)
    i_str = re.sub(r'verses ([0-9]+), ([0-9]+)', r'BibleBook x:\1, \2', i_str)
    #  in verse 6
    i_str = re.sub(r'Verse ([0-9]+)', r'BibleBook x:\1', i_str)
    i_str = re.sub(r'verse ([0-9]+)', r'BibleBook x:\1', i_str)

    # verse one-twenty
    i_str = re.sub(r' verse one', r' BibleBook x:1', i_str)
    i_str = re.sub(r' verse two', r' BibleBook x:2', i_str)
    i_str = re.sub(r' verse three', r' BibleBook x:3', i_str)
    i_str = re.sub(r' verse four', r' BibleBook x:4', i_str)
    i_str = re.sub(r' verse five', r' BibleBook x:5', i_str)
    i_str = re.sub(r' verse six', r' BibleBook x:6', i_str)
    i_str = re.sub(r' verse seven', r' BibleBook x:7', i_str)
    i_str = re.sub(r' verse eight', r' BibleBook x:8', i_str)
    i_str = re.sub(r' verse nine', r' BibleBook x:9', i_str)
    i_str = re.sub(r' verse ten', r' BibleBook x:10', i_str)
    i_str = re.sub(r' verse eleven', r' BibleBook x:11', i_str)
    i_str = re.sub(r' verse twelve', r' BibleBook x:12', i_str)
    i_str = re.sub(r' verse thirteen', r' BibleBook x:13', i_str)
    i_str = re.sub(r' verse fourteen', r' BibleBook x:14', i_str)
    i_str = re.sub(r' verse fifteen', r' BibleBook x:15', i_str)
    i_str = re.sub(r' verse sixteen', r' BibleBook x:16', i_str)
    i_str = re.sub(r' verse seventeen', r' BibleBook x:17', i_str)
    i_str = re.sub(r' verse eightteen', r' BibleBook x:18', i_str)
    i_str = re.sub(r' verse nineteen', r' BibleBook x:19', i_str)
    i_str = re.sub(r' verse twenty', r' BibleBook x:20', i_str)
    i_str = re.sub(r' verses one', r' BibleBook x:1', i_str)
    i_str = re.sub(r' verses two', r' BibleBook x:2', i_str)
    i_str = re.sub(r' verses three', r' BibleBook x:3', i_str)
    i_str = re.sub(r' verses four', r' BibleBook x:4', i_str)
    i_str = re.sub(r' verses five', r' BibleBook x:5', i_str)
    i_str = re.sub(r' verses six', r' BibleBook x:6', i_str)
    i_str = re.sub(r' verses seven', r' BibleBook x:7', i_str)
    i_str = re.sub(r' verses eight', r' BibleBook x:8', i_str)
    i_str = re.sub(r' verses nine', r' BibleBook x:9', i_str)
    i_str = re.sub(r' verses ten', r' BibleBook x:10', i_str)
    i_str = re.sub(r' verses eleven', r' BibleBook x:11', i_str)
    i_str = re.sub(r' verses twelve', r' BibleBook x:12', i_str)
    i_str = re.sub(r' verses thirteen', r' BibleBook x:13', i_str)
    i_str = re.sub(r' verses fourteen', r' BibleBook x:14', i_str)
    i_str = re.sub(r' verses fifteen', r' BibleBook x:15', i_str)
    i_str = re.sub(r' verses sixteen', r' BibleBook x:16', i_str)
    i_str = re.sub(r' verses seventeen', r' BibleBook x:17', i_str)
    i_str = re.sub(r' verses eightteen', r' BibleBook x:18', i_str)
    i_str = re.sub(r' verses nineteen', r' BibleBook x:19', i_str)
    i_str = re.sub(r' verses twenty', r' BibleBook x:20', i_str)
    # and one-twenty
    # i_str = re.sub(r' and one', r' and BibleBook x:1', i_str)   # 2024-03-17 how to separate "and one of the most" from "
    # i_str = re.sub(r' and two', r' and BibleBook x:2', i_str)
    # i_str = re.sub(r' and three', r' and BibleBook x:3', i_str)
    # i_str = re.sub(r' and four', r' and BibleBook x:4', i_str)
    # i_str = re.sub(r' and five', r' and BibleBook x:5', i_str)
    # i_str = re.sub(r' and six', r' and BibleBook x:6', i_str)
    # i_str = re.sub(r' and seven', r' and BibleBook x:7', i_str)
    # i_str = re.sub(r' and eight', r' and BibleBook x:8', i_str)
    # i_str = re.sub(r' and nine', r' and BibleBook x:9', i_str)
    # i_str = re.sub(r' and ten', r' and BibleBook x:10', i_str)
    # i_str = re.sub(r' and eleven', r' and BibleBook x:11', i_str)
    # i_str = re.sub(r' and twelve', r' and BibleBook x:12', i_str)
    # i_str = re.sub(r' and thirteen', r' and BibleBook x:13', i_str)
    # i_str = re.sub(r' and fourteen', r' and BibleBook x:14', i_str)
    # i_str = re.sub(r' and fifteen', r' and BibleBook x:15', i_str)
    # i_str = re.sub(r' and sixteen', r' and BibleBook x:16', i_str)
    # i_str = re.sub(r' and seventeen', r' and BibleBook x:17', i_str)
    # i_str = re.sub(r' and eightteen', r' and BibleBook x:18', i_str)
    # i_str = re.sub(r' and nineteen', r' and BibleBook x:19', i_str)
    # i_str = re.sub(r' and twenty', r' and BibleBook x:20', i_str)
    # Verse 13:
    # i_str = re.sub(r'Verse ([0-9]+):', r'Verse BibleBook x:\1:', i_str)
    # i_str = re.sub(r'Verse ([0-9]+);', r'Verse BibleBook x:\1;', i_str)
    #
    # # (verse 12)
    # i_str = re.sub(r'\(verse ([0-9]+)\)', r'(BibleBook x:\1)', i_str)
    # i_str = re.sub(r'\(Verse ([0-9]+)\)', r'(BibleBook x:\1)', i_str)

    # TODO 2024-01-27 Experimental
    # Replace "vs. yy" with "BibleBook x:yy", and similar expressions
    i_str = re.sub(r'\(vs. ([0-9]+)', r'BibleBook x:\1', i_str)
    i_str = re.sub(r'\(vss. ([0-9]+), ([0-9]+)', r'BibleBook x:\1, \2', i_str)
    i_str = re.sub(r'vs. ([0-9]+)', r'BibleBook x:\1', i_str)
    i_str = re.sub(r'vss. ([0-9]+), ([0-9]+)', r'BibleBook x:\1, \2', i_str)


    # No bible book just a chapter and verse "(44:31)"
    i_str = re.sub(r'\(([0-9]+):([0-9]+)\)', r'BibleBook \1:\2', i_str)
    i_str = re.sub(r'\(([0-9]+):([0-9]+)-([0-9]+)\)', r'BibleBook \1:\2-\3', i_str)
    i_str = re.sub(r'with ([0-9]+):([0-9]+)', r'with BibleBook \1:\2', i_str)
    i_str = re.sub(r'\(cf ([0-9]+):([0-9]+); ([0-9]+):([0-9]+)\)', r'BibleBook \1:\2; BibleBook \3:\4', i_str)

    return i_str  # prepare_incomplete_references_from_text


def prepare_incomplete_references_from_speech(i_str):
    # This procedure prepares the input text to later add on the real BibleBook name. Therefore is "BibleBook" part of the "hitlist" when searching for references.
    # "BibleBook" is replaced in procedure "replace_fake_bible_book_names_with_prevoius"

    i_str = re.sub(r' is verse ([0-9]+)', r'BibleBook x:\1', i_str)
    # so verse 4
    i_str = re.sub(r' so verse ([0-9]+)', r'BibleBook x:\1', i_str)
    # (v 2)		=> BibleBook x:2
    i_str = re.sub(r' \(v ([0-9]+)\)', r'BibleBook x:\1', i_str)
    i_str = re.sub(r' \(v([0-9]+)\)', r'BibleBook x:\1', i_str)
    # (v 7f)
    i_str = re.sub(r' \(v ([0-9]+)f\)', r'BibleBook x:\1', i_str)
    # (v 7-9)		=> BibleBook x:7-9
    i_str = re.sub(r' \(v ([0-9]+)-([0-9]+)\)', r'BibleBook x:\1-\2', i_str)
    # (v 28, 30)	=> BibleBook x:28, 30
    i_str = re.sub(r' \(v ([0-9]+), ([0-9]+)\)', r'BibleBook x:\1, \2', i_str)
    # (v 7-8, 11)	=> BibleBook x:7-8, 11
    i_str = re.sub(r' \(v ([0-9]+)-([0-9]+), ([0-9]+)\)', r'BibleBook x:\1-\2, \3', i_str)
    # (v 5-6, 9-10)	=> BibleBook x:5-6, 9-10
    i_str = re.sub(r' \(v ([0-9]+)-([0-9]+), ([0-9]+)-([0-9]+)\)', r'BibleBook x:\1-\2, \3-\4', i_str)
    # (v 29.)		=> BibleBook x:29
    i_str = re.sub(r' \(v ([0-9]+).\)', r'BibleBook x:\1', i_str)
    # I v 3		=> BibleBook x:3
    i_str = re.sub(r'I v ([0-9]+)', r'BibleBook x:\1', i_str)
    i_str = re.sub(r'i v ([0-9]+)', r'BibleBook x:\1', i_str)
    # I v 4-14	=> BibleBook x:4-14
    i_str = re.sub(r' I v ([0-9]+)-([0-9]+) ', r'BibleBook x:\1-\2 ', i_str)
    # 2023-08-03
    # # i 13:6-9
    # i_str = re.sub(r'i ([0-9]+):([0-9]+)-([0-9]+)', r'BibleBook \1:\2-\3', i_str)
    # # i 13:6
    # i_str = re.sub(r'i ([0-9]+):([0-9]+)', r'BibleBook x:\1:\2', i_str)
    # Verserna 29-51
    i_str = re.sub(r'Verserna ([0-9]+)-([0-9]+) ', r'BibleBook x:\1-\2 ', i_str)
    i_str = re.sub(r'verserna ([0-9]+)-([0-9]+) ', r'BibleBook x:\1-\2 ', i_str)
    #  v 7-25
    i_str = re.sub(r' v ([0-9]+)-([0-9]+) ', r'BibleBook x:\1-\2 ', i_str)
    # se 4:14	=> BibleBook 4:14
    # se 13:15, 14:9, 11; 16:2; 19:20 och 20:4
    i_str = re.sub(r'se ([0-9]+):([0-9]+)', r'BibleBook \1:\2', i_str)
    # (4:27)		=> BibleBook 4:27
    i_str = re.sub(r' \(([0-9]+):([0-9]+)\)', r'BibleBook \1:\2', i_str)
    # (4:27-29)	=> BibleBook 4:27-29
    i_str = re.sub(r' \(([0-9]+):([0-9]+)-([0-9]+)\)', r'BibleBook \1:\2-\3', i_str)
    # (4:27, 30)	=> BibleBook 4:27, 30
    i_str = re.sub(r' \(([0-9]+):([0-9]+), ([0-9]+)\)', r'BibleBook \1:\2, \3', i_str)
    # (4:27-29, 31)	=> BibleBook 4:27-29, 31
    i_str = re.sub(r' \(([0-9]+):([0-9]+)-([0-9]+), ([0-9]+)\)', r'BibleBook \1:\2-\3, \4', i_str)
    # (4:27-29, 31-33)=> BibleBook 4:27-29, 31-33
    i_str = re.sub(r' \(([0-9]+):([0-9]+)-([0-9]+), ([0-9]+)-([0-9]+)\)', r'BibleBook \1:\2-\3, \4-\5', i_str)
    # (4:27, 5:1)	=> BibleBook 4:27; BibleBook 5:1
    i_str = re.sub(r' \(([0-9]+):([0-9]+), ([0-9]+):([0-9]+)\)', r'BibleBook \1:\2; BibleBook \3:\4', i_str)
    i_str = re.sub(r' \(([0-9]+):([0-9]+); ([0-9]+):([0-9]+)\)', r'BibleBook \1:\2; BibleBook \3:\4', i_str)

    return i_str  # prepare_incomplete_references


def change_book_names_to_capital_start(i_str):
    i_str = re.sub(r'genesis', r'Genesis', i_str)
    i_str = re.sub(r'exodus', r'Exodus', i_str)
    i_str = re.sub(r'leviticus', r'Leviticus', i_str)
    i_str = re.sub(r'numbers', r'Numbers', i_str)
    i_str = re.sub(r'deuteronomy', r'Deuteronomy', i_str)
    i_str = re.sub(r'joshua', r'Joshua', i_str)
    i_str = re.sub(r'judges', r'Judges', i_str)
    i_str = re.sub(r'ruth', r'Ruth', i_str)
    i_str = re.sub(r'1 samuel', r'1 Samuel', i_str)
    i_str = re.sub(r'2 samuel', r'2 Samuel', i_str)
    i_str = re.sub(r'1 kings', r'1 Kings', i_str)
    i_str = re.sub(r'2 kings', r'2 Kings', i_str)
    i_str = re.sub(r'1 chronicles', r'1 Chronicles', i_str)
    i_str = re.sub(r'2 chronicles', r'2 Chronicles', i_str)
    i_str = re.sub(r'ezra', r'Ezra', i_str)
    i_str = re.sub(r'nehemiah', r'Nehemiah', i_str)
    i_str = re.sub(r'esther', r'Esther', i_str)
    i_str = re.sub(r'job', r'Job', i_str)
    i_str = re.sub(r'ssalms', r'Psalms', i_str)
    i_str = re.sub(r'proverbs', r'Proverbs', i_str)
    i_str = re.sub(r'ecclesiastes', r'Ecclesiastes', i_str)
    i_str = re.sub(r'song of songs', r'Song of Songs', i_str)
    i_str = re.sub(r'isaiah', r'Isaiah', i_str)
    i_str = re.sub(r'jeremiah', r'Jeremiah', i_str)
    i_str = re.sub(r'lamentations', r'Lamentations', i_str)
    i_str = re.sub(r'ezekiel', r'Ezekiel', i_str)
    i_str = re.sub(r'daniel', r'Daniel', i_str)
    i_str = re.sub(r'hosea', r'Hosea', i_str)
    i_str = re.sub(r'joel', r'Joel', i_str)
    i_str = re.sub(r'amos', r'Amos', i_str)
    i_str = re.sub(r'obadiah', r'Obadiah', i_str)
    i_str = re.sub(r'jonah', r'Jonah', i_str)
    i_str = re.sub(r'micah', r'Micah', i_str)
    i_str = re.sub(r'nahum', r'Nahum', i_str)
    i_str = re.sub(r'habakkuk', r'Habakkuk', i_str)
    i_str = re.sub(r'zephaniah', r'Zephaniah', i_str)
    i_str = re.sub(r'haggai', r'Haggai', i_str)
    i_str = re.sub(r'zechariah', r'Zechariah', i_str)
    i_str = re.sub(r'malachi', r'Malachi', i_str)
    i_str = re.sub(r'matthew', r'Matthew', i_str)
    i_str = re.sub(r'mark', r'Mark', i_str)
    i_str = re.sub(r'luke', r'Luke', i_str)
    i_str = re.sub(r'john', r'John', i_str)
    i_str = re.sub(r'acts ', r'Acts ', i_str)
    i_str = re.sub(r'romans', r'Romans', i_str)
    i_str = re.sub(r'1 corinthians', r'1 Corinthians', i_str)
    i_str = re.sub(r'2 corinthians', r'2 Corinthians', i_str)
    i_str = re.sub(r'galatians', r'Galatians', i_str)
    i_str = re.sub(r'ephesians', r'Ephesians', i_str)
    i_str = re.sub(r'philippians', r'Philippians', i_str)
    i_str = re.sub(r'colossians', r'Colossians', i_str)
    i_str = re.sub(r'1 thessalonians', r'1 Thessalonians', i_str)
    i_str = re.sub(r'2 thessalonians', r'2 Thessalonians', i_str)
    i_str = re.sub(r'1 timothy', r'1 Timothy', i_str)
    i_str = re.sub(r'2 timothy', r'2 Timothy', i_str)
    i_str = re.sub(r'Tttus', r'Titus', i_str)
    i_str = re.sub(r'philemon', r'Philemon', i_str)
    i_str = re.sub(r'hebrews', r'Hebrews', i_str)
    i_str = re.sub(r'james', r'James', i_str)
    i_str = re.sub(r'1 peter', r'1 Peter', i_str)
    i_str = re.sub(r'2 peter', r'2 Peter', i_str)
    i_str = re.sub(r'1 jn', r'1 Jn', i_str)
    i_str = re.sub(r'2 jn', r'2 Jn', i_str)
    i_str = re.sub(r'3 jn', r'3 Jn', i_str)
    i_str = re.sub(r'jude', r'Jude', i_str)
    i_str = re.sub(r'revelation', r'Revelation', i_str)

    return i_str  # change_book_names_to_capital_start


def prepare_input_text_file_from_speech_input(f_in, f_in_txt, f_in_encoding,
                                              i_execution_mode, outfile_encoding):
    # 2020-01-26
    t_str = str

    t_str = get_file_content_as_str(f_in, f_in_encoding)
    # print('71 t_str: ' + t_str)
    # John chapter 14 verse 29
    # Amos chapter three verse seven
    # Daniel 12 verse 4
    # Daniel chapter 1 it says we're going to look at verse 19 20 and 21
    # Daniel 1 verse 2 and 3
    # second Corinthians three verse eighteen
    # Isaiah chapter 38 verses 1 2 & 5 as

    # Remove new line characters
    t_str = re.sub(r'\\n', r'  ', t_str)
    t_str = re.sub(r'\.', r' ', t_str)  # 2020-04-19 18:08:04

    # Misspelled words
    t_str = re.sub(r'Amber', r'number', t_str)
    t_str = re.sub(r'shaft', r'chapter', t_str)
    t_str = re.sub(r'chapter Tim', r'chapter 10', t_str)
    t_str = re.sub(r'verses verse', r'verse', t_str)
    # t_str = re.sub(r'xxx', r'xxx', t_str)
    # t_str = re.sub(r'xxx', r'xxx', t_str)
    # t_str = re.sub(r'xxx', r'xxx', t_str)
    # t_str = re.sub(r'xxx', r'xxx', t_str)
    # t_str = re.sub(r'xxx', r'xxx', t_str)

    # Misspelled bible book names
    t_str = change_book_names_to_capital_start(t_str)

    t_str = re.sub(r'March', r'Mark', t_str)
    t_str = re.sub(r'revelations', r'Revelation', t_str)
    t_str = re.sub(r'relation', r'Revelation', t_str)
    t_str = re.sub(r'first enemy', r'1 Timothy', t_str)
    t_str = re.sub(r'second enemy', r'2 Timothy', t_str)
    t_str = re.sub(r' act', r' Acts', t_str)
    t_str = re.sub(r'chapter to', r'chapter 2', t_str)
    t_str = re.sub(r'Roberts', r'Proverbs', t_str)
    t_str = re.sub(r'Daniels', r'Daniel', t_str)
    t_str = re.sub(r'Roman ', r'Romans ', t_str)
    t_str = re.sub(r'xxxx ', r'xxxx ', t_str)
    t_str = re.sub(r'xxxx ', r'xxxx ', t_str)
    t_str = re.sub(r'xxxx ', r'xxxx ', t_str)
    t_str = re.sub(r'xxxx ', r'xxxx ', t_str)

    # Strip from unnecessary words
    t_str = re.sub(r'it says', r'', t_str)
    t_str = re.sub(r'so we re gonna start', r'', t_str)
    t_str = re.sub(r' And we re gonna read verse ', r' verse ', t_str)
    t_str = re.sub(r' And we re gonna read verse ', r' verse ', t_str)

    t_str = re.sub(r'  ', r' ', t_str)
    t_str = re.sub(r' , with', r'', t_str)
    t_str = re.sub(r' , ', r'', t_str)
    # '[ I m gonna read the closing verses of ,  Daniel chapter 4  ,  with verse 34 and read through verse 37 ]'

    t_str = re.sub(r'\\n ,', r' ', t_str)
    t_str = re.sub(r'\\', r'', t_str)
    t_str = re.sub(r'Chapter', r'chapter', t_str)
    t_str = re.sub(r'Chapters', r'chapter', t_str)
    t_str = re.sub(r'chapters', r'chapter', t_str)
    t_str = re.sub(r'Verse', r'verse', t_str)
    t_str = re.sub(r'versus', r'verse', t_str)
    t_str = re.sub(r'verse by', r'verse', t_str)
    t_str = re.sub(r'verse of', r'verse', t_str)

    t_str = re.sub(r'after ', r'', t_str)
    t_str = re.sub(r'adverse ', r'and verse', t_str)

    # Exclude known dates
    # October twenty two eighteen forty four
    t_str = re.sub(r'October twenty two eighteen forty four ', r'October 22, 1844 ', t_str)
    t_str = re.sub(r'of eighteen forty fourth', r'1844', t_str)
    t_str = re.sub(r'of eighteen forty four', r'1844', t_str)
    t_str = re.sub(r'twelve hundred and sixty ', r'1260 ', t_str)
    t_str = re.sub(r'seventeen ninety eight ', r'1798 ', t_str)
    t_str = re.sub(r'four one thousand two hundred and sixty', r'1260', t_str)
    t_str = re.sub(r'one thousand two hundred and sixty ', r'for 1260 ', t_str)

    # read the first five verses
    t_str = re.sub(r'read the first five verses', r'verse 1-5', t_str)
    t_str = re.sub(r'the first two verses', r'verse 1-2', t_str)
    t_str = re.sub(r'in verse T', r'in verse 2', t_str)

    t_str = re.sub(r'the first chapter', 'chapter 1', t_str)
    t_str = re.sub(r'the second chapter', 'chapter 2', t_str)
    t_str = re.sub(r'the third chapter', 'chapter 3', t_str)
    t_str = re.sub(r'the fourth chapter', 'chapter 4', t_str)
    t_str = re.sub(r'the fifth chapter', 'chapter 5', t_str)
    t_str = re.sub(r'the sixth chapter', 'chapter 6', t_str)
    t_str = re.sub(r'the seventh chapter', 'chapter 7', t_str)
    t_str = re.sub(r'the eighth chapter', 'chapter 8', t_str)
    t_str = re.sub(r'the ninth chapter', 'chapter 9', t_str)
    t_str = re.sub(r'the tenth chapter', 'chapter 10', t_str)
    t_str = re.sub(r'the eleventh chapter', 'chapter 11', t_str)
    # t_str = re.sub(r'xx', 'yy', t_str)
    # t_str = re.sub(r'xx', 'yy', t_str)
    # t_str = re.sub(r'xx', 'yy', t_str)
    # t_str = re.sub(r'xx', 'yy', t_str)
    # t_str = re.sub(r'xx', 'yy', t_str)
    # t_str = re.sub(r'xx', 'yy', t_str)
    # t_str = re.sub(r'xx', 'yy', t_str)
    # t_str = re.sub(r'xx', 'yy', t_str)
    # t_str = re.sub(r'xx', 'yy', t_str)
    # t_str = re.sub(r'xx', 'yy', t_str)
    # t_str = re.sub(r'xx', 'yy', t_str)
    # t_str = re.sub(r'xx', 'yy', t_str)
    # t_str = re.sub(r'xx', 'yy', t_str)
    # t_str = re.sub(r'xx', 'yy', t_str)
    # t_str = re.sub(r'xx', 'yy', t_str)
    # t_str = re.sub(r'xx', 'yy', t_str)

    t_str = re.sub(r' eleven', r' 11', t_str)
    t_str = re.sub(r' twelve', r' 12', t_str)
    t_str = re.sub(r' thirteen', r' 13', t_str)
    t_str = re.sub(r' fourteen', r' 14', t_str)
    t_str = re.sub(r' Fort seen', r' 14', t_str)
    t_str = re.sub(r' fifteen', r' 15', t_str)
    t_str = re.sub(r' sixteen', r' 16', t_str)
    t_str = re.sub(r' seventeen', r' 17', t_str)
    t_str = re.sub(r' eighteen', r' 18', t_str)
    t_str = re.sub(r' nineteen', r' 19', t_str)
    t_str = re.sub(r' twenty one', r' 21', t_str)
    t_str = re.sub(r' twenty two', r' 22', t_str)
    t_str = re.sub(r' twenty three', r' 23', t_str)
    t_str = re.sub(r' twenty four', r' 24', t_str)
    t_str = re.sub(r' twenty five', r' 25', t_str)
    t_str = re.sub(r' twenty six', r' 26', t_str)
    t_str = re.sub(r' twenty seven', r' 27', t_str)
    t_str = re.sub(r' twenty eight', r' 28', t_str)
    t_str = re.sub(r' twenty nine', r' 29', t_str)
    t_str = re.sub(r' twenty ', r' 20', t_str)
    t_str = re.sub(r' twentieth ', r' 20', t_str)

    t_str = re.sub(r' thirty one', r' 31', t_str)
    t_str = re.sub(r' thirty two', r' 32', t_str)
    t_str = re.sub(r' thirty three', r' 33', t_str)
    t_str = re.sub(r' thirty four', r' 34', t_str)
    t_str = re.sub(r' thirty five', r' 35', t_str)
    t_str = re.sub(r' thirty six', r' 36', t_str)
    t_str = re.sub(r' thirty seven', r' 37', t_str)
    t_str = re.sub(r' thirty eight', r' 38', t_str)
    t_str = re.sub(r' thirty nine', r' 39', t_str)
    t_str = re.sub(r' thirty', r' 30', t_str)

    t_str = re.sub(r' forty one', r' 41', t_str)
    t_str = re.sub(r' forty two', r' 42', t_str)
    t_str = re.sub(r' forty three', r' 43', t_str)
    t_str = re.sub(r' forty four', r' 44', t_str)
    t_str = re.sub(r' forty five', r' 45', t_str)
    t_str = re.sub(r' forty six', r' 46', t_str)
    t_str = re.sub(r' forty seven', r' 47', t_str)
    t_str = re.sub(r' forty eight', r' 48', t_str)
    t_str = re.sub(r' forty nine', r' 49', t_str)
    t_str = re.sub(r' forty', r' 40', t_str)

    t_str = re.sub(r' fifty one', r' 51', t_str)
    t_str = re.sub(r' fifty two', r' 52', t_str)
    t_str = re.sub(r' fifty three', r' 53', t_str)
    t_str = re.sub(r' fifty four', r' 54', t_str)
    t_str = re.sub(r' fifty five', r' 55', t_str)
    t_str = re.sub(r' fifty six', r' 56', t_str)
    t_str = re.sub(r' fifty seven', r' 57', t_str)
    t_str = re.sub(r' fifty eight', r' 58', t_str)
    t_str = re.sub(r' fifty nine', r' 59', t_str)
    t_str = re.sub(r' fifty', r' 50', t_str)

    t_str = re.sub(r' sixty one', r' 61', t_str)
    t_str = re.sub(r' sixty two', r' 62', t_str)
    t_str = re.sub(r' sixty three', r' 63', t_str)
    t_str = re.sub(r' sixty four', r' 64', t_str)
    t_str = re.sub(r' sixty five', r' 65', t_str)
    t_str = re.sub(r' sixty six', r' 66', t_str)
    t_str = re.sub(r' sixty seven', r' 67', t_str)
    t_str = re.sub(r' sixty eight', r' 68', t_str)
    t_str = re.sub(r' sixty nine', r' 69', t_str)
    t_str = re.sub(r' sixty', r' 60', t_str)

    t_str = re.sub(r' seventy one', r' 71', t_str)
    t_str = re.sub(r' seventy two', r' 72', t_str)
    t_str = re.sub(r' seventy three', r' 73', t_str)
    t_str = re.sub(r' seventy four', r' 74', t_str)
    t_str = re.sub(r' seventy five', r' 75', t_str)
    t_str = re.sub(r' seventy six', r' 76', t_str)
    t_str = re.sub(r' seventy seven', r' 77', t_str)
    t_str = re.sub(r' seventy eight', r' 78', t_str)
    t_str = re.sub(r' seventy nine', r' 79', t_str)
    t_str = re.sub(r' seventy', r' 70', t_str)

    t_str = re.sub(r' eighty one', r' 81', t_str)
    t_str = re.sub(r' eighty two', r' 82', t_str)
    t_str = re.sub(r' eighty three', r' 83', t_str)
    t_str = re.sub(r' eighty four', r' 84', t_str)
    t_str = re.sub(r' eighty five', r' 85', t_str)
    t_str = re.sub(r' eighty six', r' 86', t_str)
    t_str = re.sub(r' eighty seven', r' 87', t_str)
    t_str = re.sub(r' eighty eight', r' 88', t_str)
    t_str = re.sub(r' eighty nine', r' 89', t_str)
    t_str = re.sub(r' eighty', r' 80', t_str)

    t_str = re.sub(r' ninety one', r' 91', t_str)
    t_str = re.sub(r' ninety two', r' 92', t_str)
    t_str = re.sub(r' ninety three', r' 93', t_str)
    t_str = re.sub(r' ninety four', r' 94', t_str)
    t_str = re.sub(r' ninety five', r' 95', t_str)
    t_str = re.sub(r' ninety six', r' 96', t_str)
    t_str = re.sub(r' ninety seven', r' 97', t_str)
    t_str = re.sub(r' ninety eight', r' 98', t_str)
    t_str = re.sub(r' ninety nine', r' 99', t_str)
    t_str = re.sub(r' one hundred', r' 100', t_str)

    t_str = re.sub(r' chapter one', r' chapter 1', t_str)
    t_str = re.sub(r' chapter two', r' chapter 2', t_str)
    t_str = re.sub(r' chapter three', r' chapter 3', t_str)
    t_str = re.sub(r' chapter four', r' chapter 4', t_str)
    t_str = re.sub(r' chapter five', r' chapter 5', t_str)
    t_str = re.sub(r' chapter six', r' chapter 6', t_str)
    t_str = re.sub(r' chapter seven', r' chapter 7', t_str)
    t_str = re.sub(r' chapter eight', r' chapter 8', t_str)
    t_str = re.sub(r' chapter nine', r' chapter 9', t_str)
    t_str = re.sub(r' chapter ten', r' chapter 10', t_str)
    t_str = re.sub(r' chapter eleven', r' chapter 11', t_str)
    t_str = re.sub(r' chapter twelve', r' chapter 12', t_str)
    t_str = re.sub(r' chapter thirteen', r' chapter 13', t_str)
    t_str = re.sub(r' chapter fourteen', r' chapter 14', t_str)
    t_str = re.sub(r' chapter fifteen', r' chapter 15', t_str)
    t_str = re.sub(r' chapter sixteen', r' chapter 16', t_str)
    t_str = re.sub(r' chapter seventeen', r' chapter 17', t_str)
    t_str = re.sub(r' chapter eighteen', r' chapter 18', t_str)
    t_str = re.sub(r' chapter nineteen', r' chapter 19', t_str)
    t_str = re.sub(r' chapter twenty', r' chapter 20', t_str)

    t_str = re.sub(r' chapter 1', r' chapter 1', t_str)
    t_str = re.sub(r' chapter 2', r' chapter 2', t_str)
    t_str = re.sub(r' chapter 3', r' chapter 3', t_str)
    t_str = re.sub(r' chapter 4', r' chapter 4', t_str)
    t_str = re.sub(r' chapter 5', r' chapter 5', t_str)
    t_str = re.sub(r' chapter 6', r' chapter 6', t_str)
    t_str = re.sub(r' chapter 7', r' chapter 7', t_str)
    t_str = re.sub(r' chapter 8', r' chapter 8', t_str)
    t_str = re.sub(r' chapter 9', r' chapter 9', t_str)
    t_str = re.sub(r' chapter 10', r' chapter 10', t_str)
    t_str = re.sub(r' chapter 11', r' chapter 11', t_str)
    t_str = re.sub(r' chapter 12', r' chapter 12', t_str)
    t_str = re.sub(r' chapter 13', r' chapter 13', t_str)
    t_str = re.sub(r' chapter 14', r' chapter 14', t_str)
    t_str = re.sub(r' chapter 15', r' chapter 15', t_str)
    t_str = re.sub(r' chapter 16', r' chapter 16', t_str)
    t_str = re.sub(r' chapter 17', r' chapter 17', t_str)
    t_str = re.sub(r' chapter 18', r' chapter 18', t_str)
    t_str = re.sub(r' chapter 19', r' chapter 19', t_str)
    t_str = re.sub(r' chapter 20', r' chapter 20', t_str)

    t_str = re.sub(r' one ', r' 1 ', t_str)
    t_str = re.sub(r' two ', r' 2 ', t_str)
    t_str = re.sub(r' three ', r' 3 ', t_str)
    t_str = re.sub(r' four ', r' 4 ', t_str)
    t_str = re.sub(r' five ', r' 5 ', t_str)
    t_str = re.sub(r' six ', r' 6 ', t_str)
    t_str = re.sub(r' seven ', r' 7 ', t_str)
    t_str = re.sub(r' eight ', r' 8 ', t_str)
    t_str = re.sub(r' nine ', r' 9 ', t_str)
    t_str = re.sub(r' ten ', r' 10 ', t_str)

    t_str = re.sub(r' one, ', r' 1 ', t_str)
    t_str = re.sub(r' two, ', r' 2 ', t_str)
    t_str = re.sub(r' three, ', r' 3 ', t_str)
    t_str = re.sub(r' four, ', r' 4 ', t_str)
    t_str = re.sub(r' five, ', r' 5 ', t_str)
    t_str = re.sub(r' six, ', r' 6 ', t_str)
    t_str = re.sub(r' seven, ', r' 7 ', t_str)
    t_str = re.sub(r' eight, ', r' 8 ', t_str)
    t_str = re.sub(r' nine, ', r' 9 ', t_str)
    t_str = re.sub(r' ten, ', r' 10 ', t_str)

    t_str = re.sub(r' it says in ', r' ', t_str)
    t_str = re.sub(r' second ', r' 2 ', t_str)
    t_str = re.sub(r' first ', r' 1 ', t_str)
    t_str = re.sub(r'proverbs', r'Proverbs', t_str)
    t_str = re.sub(r' third ', r' ', t_str)

    # 1312 => 13:12
    # t_str = re.sub(r'([0-9])([0-9])([0-9])([0-9])', r'\1\2:\3\4', t_str)
    # 614 => 6:14
    # t_str = re.sub(r'([0-9])([0-9])([0-9])', r'\1:\2\3', t_str)

    t_str = re.sub(r'in verse', r'verse', t_str)

    t_str = re.sub(r'& ', r'', t_str)
    t_str = re.sub(r'   ', r' ', t_str)
    t_str = re.sub(r'([0-9]+) verse ([0-9]+) and ([0-9]+)', r'\1:\2, \3', t_str)
    t_str = re.sub(r'chapter ([0-9]+) verse ([0-9]+)', r'\1:\2', t_str)
    t_str = re.sub(r' ([0-9]+) verse ([0-9]+)', r'\1:\2', t_str)
    t_str = re.sub(r'verse([0-9]+)', r'verse \1', t_str)
    # Daniel chapter 4 verses 1 through five
    t_str = re.sub(r' chapter ([0-9]+) verses ([0-9]+) through ([0-9]+)', r' \1:\2-\3', t_str)
    t_str = re.sub(r'([0-9]+) and read through verse ([0-9]+)', r'\1-\2', t_str)
    # Daniel 6 we read verse 4      2020-03-01
    t_str = re.sub(r'([0-9]+) we read verse ([0-9]+)', r'\1:\2', t_str)
    t_str = re.sub(r' chapter ([0-9]+) verses ([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+)', r' \1:\2, \3, \4, \5', t_str)
    t_str = re.sub(r' chapter ([0-9]+) verses ([0-9]+) ([0-9]+) ([0-9]+)', r' \1:\2, \3, \4', t_str)
    t_str = re.sub(r' chapter ([0-9]+) verses ([0-9]+) ([0-9]+)', r' \1:\2, \3', t_str)
    # 9 and verses 22 to 24
    t_str = re.sub(r'from verse ([0-9]+) and verse ([0-9]+)', r'BibleBook x:\1, \2', t_str)
    t_str = re.sub(r' ([0-9]+) and verses ([0-9]+) to ([0-9]+)', r' \1:\2-\3', t_str)
    t_str = re.sub(r' ([0-9]+) and verses ([0-9]+) and ([0-9]+)', r' \1:\2, \3', t_str)
    t_str = re.sub(r' chapter ([0-9]+) and verse ([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r' ([0-9]+) and verse ([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r' ([0-9]+) verses ([0-9]+) and ([0-9]+)', r' \1:\2, \3', t_str)
    # verses 20 and 21 at the Daniel chapter 10

    # Revelation 13 verses 1 through 10
    # Revelation chapter 17 and verses 1 through 5
    t_str = re.sub(r' ([0-9]+) and verses ([0-9]+) through ([0-9]+)', r' \1:\2-\3', t_str)
    t_str = re.sub(r' ([0-9]+) verses ([0-9]+) through ([0-9]+)', r' \1:\2-\3', t_str)
    t_str = re.sub(r' ([0-9]+) verses ([0-9]+) to ([0-9]+)', r' \1:\2-\3', t_str)
    t_str = re.sub(r' ([0-9]+) ([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r' ([0-9]+):([0-9]+) and chapter ([0-9]+)', r' \1:\2; \3', t_str)

    t_str = re.sub(r', chapter ([0-9]+), verse ([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r' chapter ([0-9]+), verse ([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r' chapter ([0-9]+) verses ([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r' chapter ([0-9]+), verses ([0-9]+)', r' \1:\2', t_str)
    # Isaiah chapter 28 you are looking at verse 7
    t_str = re.sub(r' chapter ([0-9]+) you are looking at verse ([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r' chapter ([0-9]+) and you looking at verse ([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r' chapter ([0-9]+), and looking at verse ([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r' chapter ([0-9]+) and looking at verse ([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r' chapter ([0-9]+) looking at verse ([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r' ([0-9]+) looking at verse ([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r' chapter ([0-9]+) let s look at verse ([0-9]+)', r' \1:\2', t_str)
    # Isaiah chapter 63 were looking at verse 1
    t_str = re.sub(r' chapter ([0-9]+) were looking at verse ([0-9]+)', r' \1:\2', t_str)
    # chapter 14 in looking at verse 19
    t_str = re.sub(r' chapter ([0-9]+) in looking at verse ([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r'chapter ([0-9]+) and we\'ll be reading verse ([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r'chapter ([0-9]+) and we ll be reading verse ([0-9]+)', r' \1:\2', t_str)
    # 2 Peter, chapter 1, and we're glancing at verses 2 and 3 and 4
    t_str = re.sub(r', chapter ([0-9]+), and we re glancing at verses ([0-9]+) and ([0-9]+) and ([0-9]+)',
                   r' \1:\2, \3, \4', t_str)
    t_str = re.sub(r', chapter ([0-9]+), and we re glancing at verses ([0-9]+) and ([0-9]+)', r' \1:\2, \3', t_str)
    t_str = re.sub(r', chapter ([0-9]+), and we re glancing at verse ([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r'Jude verse ([0-9]+)', r'Jude 1:\1', t_str)
    t_str = re.sub(r'([0-9]+) through ([0-9]+)', r'\1-\2', t_str)
    # verse 15 through to verse 25
    t_str = re.sub(r'([0-9]+) through to verse ([0-9]+)', r'\1-\2', t_str)
    # Ezekiel chapter 8 and verses 16 and 17
    t_str = re.sub(r' ([0-9]+) and verses ([0-9]+) and ([0-9]+)', r' \1:\2; 1\:\3', t_str)
    t_str = re.sub(r'chapter ([0-9]+) and ([0-9]+)', r'\1:\2', t_str)
    t_str = re.sub(r'chapter ([0-9]+):([0-9]+), ([0-9]+)', r'\1:\2, \3', t_str)

    # from verse
    # 13 and verse 14

    t_str = re.sub(r'go down to verse ([0-9]+)', r'BibleBook x:\1', t_str)
    # t_str = re.sub(r'', r'', t_str)

    # from verse 1 through to verse 12
    t_str = re.sub(r'from verse ([0-9]+) through to verse ([0-9]+)', r' verse \1-\2', t_str)
    t_str = re.sub(r'([0-9]+) from verse ([0-9]+) onward', r'\1:\2', t_str)
    # daniel chapter 8 beginning then in verse 5
    t_str = re.sub(r'chapter ([0-9]+) beginning then in verse ([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r'chapter ([0-9]+) beginning in verse ([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r'chapter ([0-9]+) beginning then verse ([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r'([0-9]+) beginning then verse ([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r'([0-9]+) beginning verse ([0-9]+)', r' \1:\2', t_str)
    # Daniel chapter 8 and this starts in verse 9
    t_str = re.sub(r'([0-9]+) and this starts verse ([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r'([0-9]+) and this starts in verse ([0-9]+)', r' \1:\2', t_str)
    # Luke 21 and we re gonna look here at verse 20
    t_str = re.sub(r'([0-9]+) and we re gonna look here at verse ([0-9]+)', r' \1:\2', t_str)
    # 20 we ll read through to verse 22
    t_str = re.sub(r'([0-9]+) we ll read through to verse ([0-9]+)', r' \1-\2', t_str)
    # Luke 21 starting verse 20
    t_str = re.sub(r'([0-9]+) starting verse ([0-9]+)', r' \1:\2', t_str)
    # Psalms chapter 34 and starting verse 11
    t_str = re.sub(r'chapter ([0-9]+) and starting verse ([0-9]+)', r' \1:\2', t_str)
    # daniel 6 so verses 1-5    2020-03-01
    t_str = re.sub(r'([0-9]+) so verses ([0-9]+)-([0-9]+)', r' \1:\2-\3', t_str)

    t_str = re.sub(r'([0-9]+) all the way to ([0-9]+)', r'\1-\2', t_str)
    t_str = re.sub(r' ([0-9]+) verses ([0-9]+) ([0-9]+) and ([0-9]+)', r'\1:\2, \3, \4', t_str)
    t_str = re.sub(r' verses ([0-9]+)-([0-9]+)', r' BibleBook x:\1-\2', t_str)
    t_str = re.sub(r' verse ([0-9]+) to ([0-9]+)', r' BibleBook x:\1-\2', t_str)
    t_str = re.sub(r' verses ([0-9]+) to ([0-9]+)', r' BibleBook x:\1-\2', t_str)
    t_str = re.sub(r' verses ([0-9]+)', r' BibleBook x:\1', t_str)
    t_str = re.sub(r'of ([0-9]+)', r'BibleBook \1', t_str)
    # Daniel 6 let s begin verse 1-5    2020-03-01
    t_str = re.sub(r'([0-9]+) let s begin verse ([0-9]+)-([0-9]+)', r' \1:\2-\3', t_str)
    # in 2:24   2020-03-01
    t_str = re.sub(r' in ([0-9]+):([0-9]+)', r' BibleBook \1:\2', t_str)
    # Revelation 13 a verse 15  # 2020-03-01
    t_str = re.sub(r' ([0-9]+) a verse ([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r' both chapter ([0-9]+):([0-9]+)', r' BibleBook \1:\2', t_str)
    # that verse 4 18:x
    t_str = re.sub(r' that verse ([0-9]+) ([0-9]+):x', r' that BibleBook \2:\1', t_str)
    t_str = re.sub(r' in ([0-9]+):x', r' that BibleBook \1:x', t_str)

    # Revelation 18:x:1-5
    t_str = re.sub(r' ([0-9]+):x:([0-9]+)-([0-9]+)', r' \1:\2-\3', t_str)
    t_str = re.sub(r' BibleBook ([0-9]+):x:([0-9]+)', r' BibleBook \1:\2', t_str)
    t_str = re.sub(r' ([0-9]+):x:([0-9]+)', r' \1:\2', t_str)
    t_str = re.sub(r' begin ([0-9]+):x', r' BibleBook \1:x', t_str)
    t_str = re.sub(r' review ([0-9]+):x', r' BibleBook \1:x', t_str)
    # Matthew 28, 19 and 20 2023-08-03
    t_str = re.sub(r'([a-z]+) ([0-9]+), ([0-9]+) and ([0-9]+)', r'\1 \2:\3, \4', t_str)
    # John 15, verse eight 2023-08-03
    t_str = re.sub(r'([a-z]+) ([0-9]+), verse one', r'\1 \2:1', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+), verse two', r'\1 \2:2', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+), verse three', r'\1 \2:3', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+), verse four', r'\1 \2:4', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+), verse five', r'\1 \2:5', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+), verse six', r'\1 \2:6', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+), verse seven', r'\1 \2:7', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+), verse eight', r'\1 \2:8', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+), verse nine', r'\1 \2:9', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+), verse ten', r'\1 \2:10', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+) verse one', r'\1 \2:1', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+) verse two', r'\1 \2:2', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+) verse three', r'\1 \2:3', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+) verse four', r'\1 \2:4', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+) verse five', r'\1 \2:5', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+) verse six', r'\1 \2:6', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+) verse seven', r'\1 \2:7', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+) verse eight', r'\1 \2:8', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+) verse nine', r'\1 \2:9', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+) verse ten', r'\1 \2:10', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+), verse 1', r'\1 \2:1', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+), verse 2', r'\1 \2:2', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+), verse 3', r'\1 \2:3', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+), verse 4', r'\1 \2:4', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+), verse 5', r'\1 \2:5', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+), verse 6', r'\1 \2:6', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+), verse 7', r'\1 \2:7', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+), verse 8', r'\1 \2:8', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+), verse 9', r'\1 \2:9', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+), verse 10', r'\1 \2:10', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+) verse 1', r'\1 \2:1', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+) verse 2', r'\1 \2:2', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+) verse 3', r'\1 \2:3', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+) verse 4', r'\1 \2:4', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+) verse 5', r'\1 \2:5', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+) verse 6', r'\1 \2:6', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+) verse 7', r'\1 \2:7', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+) verse 8', r'\1 \2:8', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+) verse 9', r'\1 \2:9', t_str)
    t_str = re.sub(r'([a-z]+) ([0-9]+) verse 10', r'\1 \2:10', t_str)
    # Revelation 17, 1 to 3
    t_str = re.sub(r'([a-z]+) ([0-9]+), ([0-9]+) to ([0-9]+)', r'\1 \2:\3-\4', t_str)
    write_list_to_file_with_line_breaks(f_in_txt,
                                        t_str,
                                        outfile_encoding)

    # print('404 t_str: ' + t_str)
    return ()  # prepare_input_text_file_from_speech_input


def get_file_path_and_name(f_in):  # 2019-09-19
    file_path_name = Path(os.path.dirname(os.path.abspath(f_in)))
    input_file_base_name = os.path.basename(f_in)
    input_file_path_and_name = file_path_name / input_file_base_name

    return input_file_path_and_name  # get_file_path_and_name


def delete_ref_after_last_digit(i_str):
    # 2019-10-19 1 Timothy 6:17-19 COL
    out_str = i_str
    ix_last_digit = get_index_of_last_digit_in_string(out_str)
    out_str = out_str[0:ix_last_digit]

    return out_str  # delete_reference_after_last_digit


def delete_ref_after_last_digit_in_ref(i_str):
    # print('1124: ' + i_str)
    # Upp 14:9G2532 the thirdG51 => Upp 14:9
    # A good formatted reference may be either
    # a. BookName xxx:yyy, or
    # b. BookName xxx:yyy-zzz
    # Note that the chapter xxx may have maximum 3 positions, and verses yyy/zzz may have maximum 3 positions
    # There should not be any character after last digit in verse reference
    out_str = i_str
    if i_str.__contains__(COLON):
        ix_colon = i_str.index(COLON)
        rest_str = i_str[ix_colon+1:]
        # check each character if it is a digit
        ix = 0
        while ix < len(rest_str):
            test_char = rest_str[ix:ix+1]
            if test_char.isnumeric() or test_char == '-':
                ix = ix + 1
            else:
                break

        out_str = out_str[0:ix_colon +1] + rest_str[0:ix] + NEWLINE

    return out_str  # delete_ref_after_last_digit_in_ref


def delete_ref_after_dot(i_str=str):
    # 'Revelation 12:3 .            3'
    if i_str.__contains__(DOT):
        ix_dot = i_str.index(DOT)
        out_str = i_str[0:ix_dot].rstrip()
    else:
        out_str = i_str
    out_str = out_str.replace(')', NOSPACE).replace('(', NOSPACE)
    if out_str.__contains__(THREESPACE):
        out_str = re.sub(' +', ' ', out_str)
        # 2019-10-10 'Matt     10:28' => 'Matt 10.28'

    return out_str  # remove_waste_after_dot_in_bible_reference


def split_bible_reference_by_comma2(i_str):
    # Test cases: 'Hebr 8:13, 9:15', 'Matt 24:11,24'
    # 2019-11-10 New Test case  'Ps 121:4-7 * Den 16 juli, 1897\n'
    # 'John 12:31, 32: cast out

    out_list = []
    t_str = i_str.replace('[', NOSPACE).replace(']', NOSPACE).replace(NEWLINE, NOSPACE).strip().lstrip()
    num_comma = t_str.count(COMMA)  # Genesis 3:7, 21       '1 Krön 11, 12:23-40 25'
    # Should tolerate 'Psalms 39:6  COL, p'
    if num_comma == 0:  # Not more that one verse in reference
        out_list.append(t_str + NEWLINE)
    else:
        if t_str.__contains__(COLON):
            ix_comma = t_str.index(COMMA)
            ix_colon = t_str.index(COLON)
            if ix_comma < ix_colon:  # Bad reference, return input string
                out_list.append(i_str + NEWLINE)
                return out_list
            str_up_to_colon = t_str[0:ix_colon]
            ix_end_book_name = str_up_to_colon.rfind(ONESPACE)
            book = str_up_to_colon[0:ix_end_book_name]
            t_verses = t_str[ix_colon + 1:]
            # Strip off all spaces between COMMA    # 2019-10-14 Matt 24:11,24
            t_verses = re.sub(r', ', r',', t_verses)
            if not t_verses.__contains__(COMMA):
                out_list.append(i_str + NEWLINE)
                return out_list
            else:
                ix_comma = t_verses.index(COMMA)  # 2019-10-14 Matt 24:11,24
                v_now = t_verses[0:ix_comma]  # 1 Samuel 17:11,24,25
                t_verses = t_verses[ix_comma + 1:]
                i = 0
                while i < num_comma:
                    if not hasNumbers(v_now):
                        return out_list
                    out_list.append(str_up_to_colon + COLON + v_now + NEWLINE)
                    if t_verses.__contains__(COMMA):
                        ix_comma = t_verses.index(COMMA)
                        v_now = t_verses[0:ix_comma]
                        t_verses = t_verses[ix_comma + 1:].strip()
                        # if t_verses.__contains__(COLON):
                        #     out_list.append(book + ONESPACE + t_verses + NEWLINE)
                        #     return out_list
                    else:  # We have come to last element
                        if t_verses.__contains__(COLON):  # 2019-10-10 'Hebr 8:13, 9:15'
                            # '9:15'
                            # '26  Note: Acts 1'
                            out_list.append(book + ONESPACE + t_verses + NEWLINE)
                            return out_list
                        else:
                            v_now = t_verses
                    i = i + 1
                out_list.append(str_up_to_colon + COLON + v_now + NEWLINE)

    return out_list  # split_bible_reference_by_comma


#
# def split_bible_reference_by_semicolon(i_str):
#     # Decommissioned 2021-02-15
#     out_list = []
#     t_str = i_str.replace('[', NOSPACE).replace(']', NOSPACE).replace(NEWLINE, NOSPACE).strip()
#     num_sem_col = t_str.count(';')
#     if num_sem_col == 0:
#         out_list.append(t_str + NEWLINE)
#     else:
#         if t_str.__contains__(COLON):
#             ix_colon = t_str.index(COLON)  # '2 Peter 2:5; 3:3-13'     'Genesis 5; 9:29'
#             ix_semicolon = t_str.index(';')
#             if ix_semicolon < ix_colon:
#                 # 'Revelation 16; 18:20–24'
#                 book = t_str[0:t_str.find(ONESPACE)]
#                 ref = t_str[0:ix_semicolon]
#                 out_list.append(ref + NEWLINE)
#                 rest = t_str[ix_semicolon + 1:].strip()
#                 ref = book + ONESPACE + rest
#                 out_list.append(ref + NEWLINE)
#                 return out_list
#             str_up_to_colon = t_str[0:ix_colon]
#             ix_end_book_name = str_up_to_colon.rfind(ONESPACE)
#             book = str_up_to_colon[0:ix_end_book_name]
#             # book = i_str[0:i_str.index(ONESPACE)]
#
#             t_str = t_str[ix_end_book_name + 1:]
#             # i_str = i_str[i_str.index(ONESPACE)+1:]
#             if not t_str.__contains__(COLON):  # 'Revelation 20:11; GC, p. 666.1'
#                 out_list.append(i_str + NEWLINE)
#                 return out_list
#             else:
#                 ix_semi_colon = t_str.index(';')  # 'Genesis 5; 9:29'
#                 ref = t_str[0:ix_semi_colon]
#                 t_str = t_str[t_str.index(SEMICOLON) + 1:].strip()
#                 i = 0
#                 while i < num_sem_col:
#                     out_list.append(book + ONESPACE + ref + NEWLINE)
#                     if t_str.__contains__(SEMICOLON):
#                         ix_semi_colon = t_str.index(SEMICOLON)
#                         ref = t_str[0:ix_semi_colon]
#                         t_str = t_str[ix_semi_colon + 1:].strip()
#                         if not t_str.__contains__(COLON):  # 'Revelation 20:11; GC, p. 666.1'
#                             return out_list
#                     i = i + 1
#                 if t_str.__contains__(COLON):  # 'Revelation 20:11; GC, p. 666.1'
#                     out_list.append(book + ONESPACE + t_str + NEWLINE)
#         else:
#             # Ezekiel 17; 31    => Ezekiel 17 + Ezekiel 31
#             ix_semicolon = t_str.index(SEMICOLON)
#             book = t_str[0:t_str.find(ONESPACE)]
#             ref = ref = t_str[0:ix_semicolon]
#             out_list.append(ref + NEWLINE)
#             rest = t_str[ix_semicolon + 1:].strip()
#             ref = book + ONESPACE + rest
#             out_list.append(ref + NEWLINE)
#             return out_list
#
#     return out_list  # split_by_semicolon


def modify_2_consecutive_verses_to_range2(i_str=str):
    # 'Luk 4:18, 19' => 'Luk 4:18-19'
    # Should also tolerate ' Nehemiah 1:6, 7 .    4'
    # Should also tolerate: 'Psalms 39:6  COL, p'
    # Skip Nehemiah 1:6-7
    # Skip Genesis 10:32, 11:1
    # Ezekiel 28:4, 5, 8

    out_string = i_str
    out_string = out_string.replace('\\n\'', ONESPACE).replace(')', ONESPACE).replace('\'', NOSPACE).replace('\\x0',
                                                                                                             ONESPACE).rstrip()
    out_string2 = out_string.replace(ONESPACE, NOSPACE)

    try:
        if (out_string2.__contains__(COLON)) and \
                not (i_str.__contains__(SEMICOLON)) and \
                not (i_str.__contains__(NEWLINE)
                ):
            ix_colon = i_str.index(COLON)
            book_and_chapter = i_str[0:ix_colon + 1]
            if out_string2.__contains__(COMMA):
                # ix_comma = out_string2.index(COMMA)
                verses = out_string2[out_string2.index(COLON) + 1:]
                v_list = verses.split(COMMA)
                v_now_str = v_list[0]
                if v_now_str.__contains__(DASH) or v_now_str.__contains__(DASH):
                    return out_string
                i = 1
                v_now = int(v_now_str)
                t_str = book_and_chapter + v_now.__str__()

                while i < v_list.__len__():
                    v_next_str = v_list[i]
                    if v_next_str.__contains__(DASH) or v_next_str.__contains__(DASH):
                        t_str = t_str + ', ' + v_next_str
                    else:
                        v_next = int(v_next_str)
                        if int(v_next_str) - int(v_now_str) == 1:  # Consecutive verses
                            t_str = t_str + DASH + v_next_str
                        else:
                            t_str = t_str + ', ' + v_next_str
                    i = i + 1
                    v_now_str = v_next_str
            else:
                # There is just one verse in reference
                return out_string
        else:
            t_str = i_str
    except:
        t_str = i_str

    return t_str  # modify_2verses_to_range2


def get_file_header_text(f_in, num_ref_str, i_execution_mode):  # 2019-10-08
    datestamp = str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))  # current date and time
    datestamp2 = str(datetime.datetime.now().strftime('%Y-%m-%d--%H-%M-%S'))  # 2019-10-21

    # print('776 i_file_path_and_name: ' + i_file_path_and_name)

    if i_execution_mode == 'english_text':
        file_header = LINESEPARATOR + NEWLINE + \
                      VBAR + NEWLINE + \
                      VBAR + 'Bible Reference Finder - Result' + NEWLINE + \
                      VBAR + NEWLINE + \
                      VBAR + 'Infile: ' + f_in.__str__() + NEWLINE + \
                      VBAR + 'Date: ' + datestamp + NEWLINE + \
                      VBAR + NEWLINE + \
                      VBAR + '# References: ' + num_ref_str + NEWLINE + \
                      VBAR + NEWLINE + \
                      LINESEPARATOR + NEWLINE

    elif i_execution_mode == 'swedish_text':
        file_header = LINESEPARATOR + NEWLINE + \
                      VBAR + NEWLINE + \
                      VBAR + 'Bible Reference Finder - Result' + NEWLINE + \
                      VBAR + NEWLINE + \
                      VBAR + 'Infile: ' + f_in.__str__() + NEWLINE + \
                      VBAR + 'Date: ' + datestamp + NEWLINE + \
                      VBAR + NEWLINE + \
                      VBAR + '# References: ' + num_ref_str + NEWLINE + \
                      VBAR + NEWLINE + \
                      LINESEPARATOR + NEWLINE

        # '| Infile: ' + os.path.basename(i_file_path_and_name) + '_Bibel-ref-Swe(' + datestamp2 + ').txt' + NEWLINE + \

    return file_header  # get_file_header_text


def readjust_to_normalized_names(i_str=str, i_execution_mode=str):
    # English
    if i_execution_mode == 'english_text':
        if i_str.startswith('1 Jnx '):
            return '1 John ' + i_str[6:]
        elif i_str.startswith('2 Jnx '):
            return '2 John ' + i_str[6:]
        elif i_str.startswith('3 Jnx '):
            return '3 John ' + i_str[6:]
        # elif out_string.startswith('Jude'):
        #     if hasNumbers(out_string):
        #         return 'Jude 1:' +out_string[5:]

    elif i_execution_mode == 'swedish_text':
        # Swedish
        if i_str.startswith('1 Jnx '):
            return '1 Joh ' + i_str[6:]
        elif i_str.startswith('2 Jnx '):
            return '2 Joh ' + i_str[6:]
        elif i_str.startswith('3 Jnx '):
            return '3 Joh ' + i_str[6:]
        elif i_str.startswith('Filem'):
            return 'Filem 1:' + i_str[7:]
        # elif out_string.startswith('Jud'):
        #     return 'Jud. 1:' +out_string[5:]
    return i_str  # readjust_to_normalized_names


def readjust_to_normalized_names2(i_str=str, i_execution_mode=str):
    # English
    if i_execution_mode == 'english_text':
        if i_str.startswith('Jude'):
            # if hasNumbers(i_str):
            #     return 'Jude 1:' + i_str[5:]
            pass
        elif i_str.startswith('Philemon'):
            if hasNumbers(i_str):
                if i_str.__contains__(':'):
                    return i_str
                else:
                    return 'Philemon 1:' + i_str[9:]
    elif i_execution_mode == 'swedish_text':
        # Swedish
        if i_str.startswith('Jud'):
            pass
            # return 'Jud. 1:' + i_str[5:]

        elif i_str.startswith('Filemon'):
            if hasNumbers(i_str):
                return 'Filemon 1:' + i_str[8:]

    return i_str  # readjust_to_normalized_names2


def get_docx_file_content_as_string(f_in_docx, infile_encoding):
    # print('845 i_file: ' + i_file)

    doc = docx.Document(f_in_docx)
    full_text = []
    for para in doc.paragraphs:
        # print('850 para: ' + para.text)
        full_text.append(para.text)

    return NEWLINE.join(full_text)


def prepare_input_text_file_from_pptx_input(f_in_pptx, i_input_text_file_name, infile_encoding, outfile_encoding):
    # 2020-01-25
    t_pptx_str: str
    t_pptx_str = ''

    prs = Presentation(f_in_pptx)
    # print('1448 outfile_encoding: ' + outfile_encoding)
    # print('----------------------')
    for slide in prs.slides:
        for shape in slide.shapes:
            if hasattr(shape, 'text'):
                # print(shape.text)
                t_pptx_str = t_pptx_str + ONESPACE + shape.text
    # print('1455 ' + t_pptx_str)

    t_pptx_str = t_pptx_str.replace('~', '')
    t_pptx_str = t_pptx_str.replace('“', '')
    t_pptx_str = t_pptx_str.replace('”', '')
    t_pptx_str = t_pptx_str.replace('Â', '')
    t_pptx_str = t_pptx_str.replace('^', '')
    t_pptx_str = t_pptx_str.replace('', '')

    # Take care of Swedish charactes 2023-07-21
    t_pptx_str = t_pptx_str.replace('Ã¥', 'å') # 2023-07-21
    # t_pptx_str = t_pptx_str.replace('Ã¥', 'å')
    t_pptx_str = t_pptx_str.replace('Ã¥', 'å')
    t_pptx_str = t_pptx_str.replace('Ã', 'Å')

    t_pptx_str = t_pptx_str.replace('Ã¤', 'ä')
    t_pptx_str = t_pptx_str.replace('Ã„', 'Ä')
    t_pptx_str = t_pptx_str.replace('Ã¤', 'ä')
    t_pptx_str = t_pptx_str.replace('Ã„', 'Ä')


    t_pptx_str = t_pptx_str.replace('bÃ¶', 'ö')  # 2023-07-21
    t_pptx_str = t_pptx_str.replace('Ã-', 'Ö')

    t_pptx_str = t_pptx_str.replace('Å¶', 'ö')
    t_pptx_str = t_pptx_str.replace('Ã-', 'Ö')
    # t_pptx_str = t_pptx_str.replace('\uf0e8', '')
    # t_pptx_str = t_pptx_str.replace('\u2019', '')
    # t_pptx_str = t_pptx_str.replace('\u25c4', '')
    # t_pptx_str = t_pptx_str.replace('\u2500', '')
    # t_pptx_str = t_pptx_str.replace('\u25ba', '')
    # t_pptx_str = t_pptx_str.replace('\u2013', '')
    # t_pptx_str = t_pptx_str.replace('\\x0', '')
    # t_pptx_str = t_pptx_str.replace('', '')
    # t_pptx_str = t_pptx_str.replace('', '')

    write_list_to_file_with_line_breaks(i_input_text_file_name,
                                        t_pptx_str,
                                        outfile_encoding)

    return ()  # prepare_input_text_file_from_pptx_input


def prepare_input_text_file_from_docx_input(i_file, i_input_text_file_name, infile_encoding, i_execution_mode,
                                            outfile_encoding):  # 2019-09-20

    t_docx_str = get_file_header_text(i_file, '1', i_execution_mode) + get_docx_file_content_as_string(i_file,
                                                                                                       infile_encoding)
    write_list_to_file_with_line_breaks(i_input_text_file_name,
                                        t_docx_str,
                                        outfile_encoding)

    return ()  # prepare_input_text_file_from_docx_input


def get_pdf_file_content_as_string(f_in_pdf, i_execution_mode):
    # 2024-01-09 Several changes to this function after updating PyPDF2 to version > 3.0.0
    document_content_list = []
    infile_encoding = get_file_encoding(i_execution_mode)
    # pdf_file_obj = open(i_file, 'rb', int(infile_encoding))
    pdf_file_obj = open(f_in_pdf, 'rb')

    # creating a pdf reader object
    # pdf_reader = PyPDF2.PdfReader(pdf_file_obj)
    # 2024-01-27
    pdf_reader = PyPDF2.PdfFileReader(pdf_file_obj)

    # printing number of pages in pdf file
    # print(pdfReader.numPages)
    # num_pages = pdf_reader.getNumPages()
    num_pages = pdf_reader.numPages
    # num_pages = pdf_reader.pages.__len__()
    page = 0

    while page < num_pages:
        # creating a page object
        # page_obj = pdf_reader.getPage(page)
        page_obj = pdf_reader.pages[page]
        # extracting text from page

        page_content = page_obj.extractText()
        # 2024-01-27 page_content = page_obj.extract_text()
        # page_content = page_obj.extract_text()
        # print('922: ' + page_content)
        document_content_list.append(page_content + ONESPACE)
        page = page + 1

    pdf_file_obj.close()
    document_content_str = ''.join(document_content_list).__str__()
    # print('1160: ' + document_content_str)
    # document_content_str = replace_characters_in_former_pdf_file(document_content_str) # 2024-03-19 Moved to calling procedure

    return document_content_str  # get_pdf_file_content_as_string


def remove_char_in_txt_file(i_str):
    # Remove  bullet points '<New line>Number) '
    # '\n', '13) '
    # '\n', '14) '
    # '\n', '51) '
    # i_str = re.sub(r'Å Â¯Å Â¬', r' ', i_str) # 2024-03-17
    i_str = i_str.replace('–', '-') # 2024-01-27
    i_str = re.sub(r'â\\x80\\x93', r'-', i_str)  # 2024-01-27
    i_str = re.sub(r'\[([0-9]+)\]', r'', i_str)  # 2021-01-12
    i_str = re.sub(r'\\n\', \'([0-9]+)\)', r' \1)', i_str)  # 2021-02-27
    i_str = re.sub(r'\n', r' ', i_str)
    i_str = re.sub(r'\' \\n\'\' \, ', r' ', i_str)  # >>' \n', <<
    i_str = re.sub(r'\'', r' ', i_str)
    i_str = re.sub(r'\\n\, ', r' ', i_str)
    i_str = re.sub(r'â\\x80', r'', i_str)
    i_str = re.sub(r'\\x80', r' ', i_str)
    i_str = re.sub(r'\\x82', r' ', i_str)
    # i_str = re.sub(r'\\x84', r' ', i_str)
    i_str = re.sub(r'\\x85', r' ', i_str)
    i_str = re.sub(r'\\x91', r' ', i_str)
    i_str = re.sub(r'\\x92s', r' ', i_str)
    i_str = re.sub(r'\\x92', r' ', i_str)
    i_str = re.sub(r'\\x93', r' ', i_str)
    i_str = re.sub(r'\\x94', r' ', i_str)
    i_str = re.sub(r'\\x95', r' ', i_str)
    i_str = re.sub(r'\\x96', r'-', i_str)
    i_str = re.sub(r'\\x97', r' ', i_str)
    i_str = re.sub(r'\\x99', r' ', i_str)
    i_str = re.sub(r'\\x0c', r' ', i_str)
    i_str = re.sub(r'\\x9c', r' ', i_str)  # 2019-12-23
    i_str = re.sub(r'\\x9d', r' ', i_str)  # 2019-12-23
    i_str = re.sub(r'\\x9d', r' ', i_str)  # 2020-02-25
    i_str = re.sub(r'\\xa0', r' ', i_str)  # 2020-01-25
    i_str = re.sub(r'\\n , ', r' ', i_str)
    i_str = re.sub(r'   ', r' ', i_str)
    i_str = re.sub(r'!', r' ', i_str)
    i_str = re.sub(r'\'', r' ', i_str)
    i_str = re.sub(r' \\n ,  ', r' ', i_str)
    i_str = re.sub(r'\?', r' ', i_str)
    i_str = re.sub(r'{', r'', i_str)
    i_str = re.sub(r'}', r'', i_str)
    i_str = re.sub(r'\[', r'', i_str)
    i_str = re.sub(r'\]', r'', i_str)
    i_str = re.sub(r' #([0-9]+)', r' ', i_str)
    i_str = re.sub(r'_', r' ', i_str)
    i_str = i_str.replace(' Note: ', ONESPACE)
    i_str = i_str.replace('â ', DASH)
    i_str = i_str.replace('¦', NOSPACE)
    i_str = i_str.replace('–', '-')  # 2019-12-23 Different types of DASH!
    i_str = i_str.replace('“', '')  # 2020-01-25
    i_str = i_str.replace('~', '')  # 2020-01-25
    i_str = re.sub(r'([0-9]+)–([0-9]+)', r'\1-\2', i_str)  # 2020-09-05
    i_str = re.sub(r'([0-9]+) - ([0-9]+)', r'\1-\2', i_str)  # 2020-09-05
    i_str = re.sub(r'([0-9]+)  -  ([0-9]+)', r'\1-\2', i_str)  # 2020-09-05
    i_str = i_str.replace('˝', ONESPACE)
    i_str = i_str.replace(' ”', ONESPACE)  # 2021-01-12
    i_str = re.sub(r' \\n",  ', r' ', i_str)
    i_str = re.sub(r'\\uf0a7', r'', i_str)
    i_str = i_str.replace(TWOSPACE, ONESPACE)  # 2021-04-24
    i_str = i_str.replace('; etc.', '')  # 2021-04-24
    # i_str = re.sub(r'([0-9]+)\.([0-9]+)', r'\1 .\2', i_str)  # 2021-04-24
    i_str = i_str.replace('\"', '')  # 2021-04-24
    i_str = re.sub(r'([0-9]+) -- ([0-9]+)', r'\1-\2', i_str)  # 2021-05-14
    i_str = re.sub(r'([0-9]+) --([0-9]+)', r'\1-\2', i_str)  # 2021-05-14
    i_str = re.sub(r'([0-9]+)-- ([0-9]+)', r'\1-\2', i_str)  # 2021-05-14
    i_str = re.sub(r'([0-9]+)--([0-9]+)', r'\1-\2', i_str)  # 2021-05-14

    # 2023-07-21
    i_str = i_str.replace('Ã¥', 'å')
    i_str = i_str.replace('Ã', 'Å')

    i_str = i_str.replace('Å¤', 'ä')
    i_str = i_str.replace('Ã„', 'Ä')
    i_str = i_str.replace('Ã\\x84', 'Ä')

    i_str = i_str.replace('Å¶', 'ö')
    i_str = i_str.replace('Ã-', 'Ö')

    i_str = i_str.replace('\\x98', '')
    i_str = i_str.replace('ï\\x83', '')
    i_str = i_str.replace('Â\\xad', '')
    i_str = i_str.replace('aÌ\\x8a', 'å')
    i_str = i_str.replace('oÌ\\x88', 'ö')
    i_str = i_str.replace('aÌ\\x88', 'ä')
    i_str = i_str.replace('oÌ\\x88', 'ö')
    i_str = i_str.replace('ï\\x81\\x8a', '')

    i_str = i_str.replace('Å\\x84', 'Ä')
    # i_str = i_str.replace('xx', 'xx')


    # print('978: ' + i_str)
    # Bullet list ' 47) '   => ''
    # i_str = i_str.replace(' ([0-9]+)\) ', r'')     # 2019-12-21 Takes away  too much. Any better ideas?

    return i_str  # remove_char_in_txt_file


def prepare_input_text_file_from_pdf_input(f_in: str,
                                           input_text_file_name: str,
                                           infile_encoding: str,
                                           i_execution_mode: str,
                                           outfile_encoding: str):  # 2019-09-20

    i_str = get_file_header_text(f_in, '1', i_execution_mode) + get_pdf_file_content_as_string(f_in, i_execution_mode)
    # print('1231 i_str: ' + i_str)

    i_str = replace_characters_in_former_pdf_file(i_str)

    out_encoding = get_file_encoding(i_execution_mode)
    # 2020-10-23
    out_encoding = 'utf-8'
    # ('1079 Writing to: ' + input_text_file_name + '  With encoding: ' + out_encoding)

    f = open(input_text_file_name, 'w', encoding=out_encoding)
    f.write(i_str.__str__().
            replace(NEWLINE, NOSPACE).replace(TAB, NOSPACE)
            )
    f.close()
    # print('900: ' + i_str)
    return ()  # prepare_input_text_file_from_pdf_input


def hasNumbers(i_string):
    return any(char.isdigit() for char in i_string)


def replace_characters_in_former_pdf_file(i_str):  # 2019-09-19
    # print('1284 ' + i_str)

    # Remove numbered list 2024-03-10
    i_str = re.sub(r'\n([0-9])\.', r' ', i_str)  # 2024-03-19
    # i_str = re.sub(r'\n([0-9])\.', r' ', i_str)  # 2024-03-19

    # Remove all line breaks
    i_str = re.sub(r'\n', r' ', i_str)    # 2024-03-19

    # Remove footnotes ".ﬂ 14 "
    i_str = re.sub(r'\.ﬂ ([0-9]+)', r' ', i_str)  # 2024-03-17
    i_str = re.sub(r'\.ﬂ([0-9]+)', r' ', i_str)  # 2024-03-17
    i_str = re.sub(r'\.ﬂ1 ([0-9]+)', r' ', i_str)  # 2024-03-17
    i_str = re.sub(r'\.ﬂ1([0-9]+)', r' ', i_str)  # 2024-03-17


    i_str = re.sub(r'™', r' ', i_str)
    i_str = re.sub(r'Œ', r'-', i_str)
    i_str = re.sub(r'ﬁ', r' ', i_str)
    i_str = re.sub(r'_', r' ', i_str)
    i_str = re.sub(r'\, \'\uf0b7', r' ', i_str)  # #, '\uf0b7
    # i_str = re.sub(r'\\n\'', r' ', i_str)
    i_str = re.sub(r'\(', r' ', i_str)  # 2019-10-01
    i_str = re.sub(r'{', r' ', i_str)  # 2019-10-14
    i_str = re.sub(r'}', r' ', i_str)  # 2019-10-14
    i_str = re.sub(r'\]', r'', i_str)  # 2021-02-15
    i_str = re.sub(r'\[', r'', i_str)  # 2021-02-15
    i_str = re.sub(r'Ð', r'-', i_str)  # 2021-02-15
    i_str = re.sub(r' \. ', r' ', i_str)  # 2024-01-30

    # 3 l i 3 digit chapter
    i_str = re.sub(r'lll:', r'111:', i_str)  # 2024-02-20

    # 2 l in 3 digit chapter
    i_str = re.sub(r'll([0-9]):', r'1 1 \1:', i_str) # 2024-02-20
    i_str = re.sub(r'l([0-9])l:', r'1 \1 1:', i_str)  # 2024-02-20
    i_str = re.sub(r'([0-9])ll:', r'\1 1 1:', i_str)  # 2024-02-20
    i_str = re.sub(r'([0-9]) ([0-9]) ([0-9]+):', r'\1\2\3:', i_str)  # 2024-02-20

    # 1 l in 3 digit chapter
    i_str = re.sub(r'l([0-9])([0-9]):', r'1 \1 \2:', i_str) # 2024-02-20
    i_str = re.sub(r'([0-9])l([0-9]):', r'\1 1 \2:', i_str)  # 2024-02-17
    i_str = re.sub(r'([0-9])([0-9])l:', r'\1 \2 1:', i_str)  # 2024-02-17
    i_str = re.sub(r'([0-9]) ([0-9]) ([0-9]):', r'\1\2\3:', i_str) # 2024-02-20

    # 2 l i 2 digit chapter
    i_str = re.sub(r'll:', r'11:', i_str)  # 2024-02-20

    # 1 l in 2 digit chapter
    i_str = re.sub(r'l([0-9]):', r'1 \1:', i_str)  # 2024-02-20
    i_str = re.sub(r'([0-9])l:', r'\1 1:', i_str)  # 2024-02-17
    i_str = re.sub(r'([0-9]) ([0-9]):', r'\1\2:', i_str)  # 2024-02-20

    # It is now assumed that chapters have been cleaned from "l" and replaced by "1"

    # 3 l in 3 digit verse 2024-02-17 Funkar
    i_str = re.sub(r'([0-9]+):lll', r'\1:111', i_str)  # 2024-02-20

    # 2 l in 3 digit verse 2024-02-21 Funkar
    i_str = re.sub(r'([0-9]+):ll([0-9])', r'\1:1 1 \2', i_str)  # 2024-02-20
    i_str = re.sub(r'([0-9]+):l([0-9])l', r'\1:1 \2 1', i_str)  # 2024-02-20
    i_str = re.sub(r'([0-9]+):([0-9])ll', r'\1:\2 1 1', i_str)  # 2024-02-20
    i_str = re.sub(r'([0-9]):([0-9]) ([0-9]) ([0-9])', r'\1:\2\3\4', i_str)  # 2024-02-20

    # 1 l in 3 digit verse 2024-02-20 verkar funka
    i_str = re.sub(r'([0-9]+):l([0-9])([0-9])', r'\1:1 \2 \3', i_str)  # 2024-02-20
    i_str = re.sub(r'([0-9]+):([0-9])l([0-9])', r'\1:\2 1 \3', i_str)  # 2024-02-20
    i_str = re.sub(r'([0-9]+):([0-9])([0-9])l', r'\1:\2 \3 1', i_str)  # 2024-02-20
    i_str = re.sub(r'([0-9]+):([0-9]+) ([0-9]) ([0-9])', r'\1:\2\3\4', i_str)


    # 2 l in 2 digit verse 2024-02-17 Funkar
    i_str = re.sub(r'([0-9]+):ll', r'\1:11', i_str)  # 2024-02-17

    # 1 l in 2 digit verse Funkar
    i_str = re.sub(r'([0-9]+):l([0-9])', r'\1:1 \2', i_str)  # 2024-02-21
    i_str = re.sub(r'([0-9]+):([0-9])l', r'\1:\2 1', i_str)  # 2024-02-21
    i_str = re.sub(r'([0-9]):([0-9]) ([0-9])', r'\1:\2\3', i_str)  # 2024-02-20

    # 1 l in 1 digit verse Rev 14:l is defined
    i_str = re.sub(r'([0-9]+):l', r'\1:1', i_str)  # 2024-02-21


    # It is now assumed that chapter and first verse after colon have been eliminated from "l" and replaced by "1"

    # 3 l in 3 digit verse after dash Funkar
    i_str = re.sub(r'([0-9]+):([0-9]+)-lll', r'\1:\2-111', i_str)  # 2024-02-21

    # 2 l in 3 digit verse after dash 2024-02-17 Funkar
    i_str = re.sub(r'([0-9]+):([0-9]+)-ll([0-9])', r'\1:\2-1 1 \3', i_str)  # 2024-02-21
    i_str = re.sub(r'([0-9]+):([0-9]+)-l([0-9])l', r'\1:\2-1 \3 1', i_str)  # 2024-02-21
    i_str = re.sub(r'([0-9]+):([0-9]+)-([0-9])ll', r'\1:\2-\3 1 1', i_str)  # 2024-02-21
    i_str = re.sub(r'([0-9]+):([0-9]+)-([0-9]) ([0-9]) ([0-9])', r'\1:\2-\3\4\5', i_str)  # 2024-02-21


    # 1 l in 3 digit verse after dash 2024-02-17 Funkar
    i_str = re.sub(r'([0-9]+):([0-9]+)-l([0-9])([0-9])', r'\1:\2-1 \3 \4', i_str)  # 2024-02-21
    i_str = re.sub(r'([0-9]+):([0-9]+)-([0-9])l([0-9])', r'\1:\2-\3 1 \4', i_str)  # 2024-02-21
    i_str = re.sub(r'([0-9]+):([0-9]+)-([0-9])([0-9])l', r'\1:\2-\3 \4 1', i_str)  # 2024-02-21
    i_str = re.sub(r'([0-9]+):([0-9]+)-([0-9]) ([0-9]) ([0-9])', r'\1:\2-\3\4\5', i_str)  # 2024-02-21

    # 1 l in 3 digit verse before dash 2024-02-21  Funkar
    i_str = re.sub(r'([0-9]+):l-([0-9]+)', r'\1:1 -\2', i_str)  # 2024-02-21
    i_str = re.sub(r'([0-9]+):([0-9]+)l-([0-9]+)', r'\1:\2 1 -\3', i_str)  # 2024-02-21
    i_str = re.sub(r'([0-9]+):([0-9]+) -([0-9])', r'\1:\2-\3', i_str)  # 2024-02-21

    i_str = re.sub(r'\n', r' ', i_str)
    i_str = re.sub(r'\' \\n\'\' \, ', r' ', i_str)  # >>' \n', <<
    i_str = re.sub(r'\'', r' ', i_str)
    i_str = i_str.replace('\ï¬', ONESPACE)
    i_str = i_str.replace('\\x82', ' ')
    i_str = i_str.replace('˜', ' ')  # 2020-01-30
    i_str = i_str.replace('ˇ', ' ')  # 2020-01-30
    i_str = i_str.replace('ˆ', ' ')  # 2020-01-30
    i_str = i_str.replace('˛', ' ')  # 2020-01-30
    i_str = i_str.replace('Š', ' ')  # 2020-01-30
    i_str = i_str.replace('˙', ' ')  # 2020-01-30
    i_str = i_str.replace(TWOSPACE, ONESPACE)  # 2020-03-08
    i_str = i_str.replace(TWOSPACE, ONESPACE)  # 2020-03-08
    i_str = i_str.replace('(', '')
    i_str = i_str.replace(')', ' ')
    i_str = i_str.replace('˝', ONESPACE)
    i_str = i_str.replace('”', ONESPACE)  # 2021-01-12
    i_str = i_str.replace('˘', ' ')  # 2022-01-14
    i_str = re.sub(r'ï¬', r' ', i_str)     # 2024-03-17

    # i_str = i_str.replace('˜', ' ')     # 2020-01-30
    # i_str = i_str.replace('˜', ' ')     # 2020-01-30

    return i_str  # replace_characters_in_former_pdf_file


# def delete_see_also_in_ref01(i_str):
#     # 2019-10-08    Revelation 15:5-8; see also 22:11
#     i_str = re.sub(r'([0-9]+); see also ([0-9]+):([0-9]+)', r'\1; \2:\3', i_str)
#     i_str = re.sub(r'([0-9]+), see also ([0-9]+):([0-9]+)', r'\1; \2:\3', i_str)
#     i_str = re.sub(r'([0-9]+); also ([0-9]+):([0-9]+)', r'\1; \2:\3', i_str)
#     i_str = re.sub(r'([0-9]+), also ([0-9]+):([0-9]+)', r'\1; \2:\3', i_str)
#     i_str = re.sub(r'([0-9]+) also ([0-9]+):([0-9]+)', r'\1; \2:\3', i_str)
#
#     return i_str  # remove_see_also_in_bible_references01
#
#
# def delete_see_also_in_ref02(i_str):
#     # ('John 6:40, see also 39, 44, 54')
#     i_str = re.sub(r'([0-9]+), see also ([0-9]+)', r'\1, \2', i_str)
#
#     return i_str  # remove_see_also_in_bible_references02


# def delete_and_in_ref(i_str):
#     # 'Genesis 1:31 and 2:1' => 'Genesis 1:31; 2:1'
#     # 'Matthew 23:37 and 38'
#     # Matthew 23:37-38 and verses 21:1-38
#     # Romans 1:16 & 12:2
#     i_str = re.sub(r'([0-9]+):([0-9]+)-([0-9]+) and verses ([0-9]+):([0-9]+)-([0-9]+)', r'\1:\2-\3; \4:\5-\6', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+) and verses ([0-9]+):([0-9]+)', r'\1:\2; \3:\4', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+) and verses ([0-9]+)-([0-9]+)', r'\1:\2, \3-\4', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+) and verse ([0-9]+):([0-9]+)', r'\1:\2; \3:\4', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+) and verse ([0-9]+)-([0-9]+)', r'\1:\2, \3:\4', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+)-([0-9]+) and ([0-9]+):([0-9]+)-([0-9]+)', r'\1:\2-\3; \4:\5-\6', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+) and ([0-9]+):([0-9]+)', r'\1:\2; \3:\4', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+) and ([0-9]+)-([0-9]+)', r'\1:\2, \3:\4', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+) and ([0-9]+)', r'\1:\2, \3', i_str)  # 2019-10-12
#     i_str = re.sub(r'([0-9]+) and ([0-9]+)', r'\1, \2', i_str)  # 2019-12-14
#     i_str = re.sub(r'([0-9]+):([0-9]+) & ([0-9]+):([0-9]+)', r'\1:\2; \3:\4', i_str)  # 2019-12-23
#     i_str = re.sub(r'([0-9]+):([0-9]+); and ([0-9]+):([0-9]+)', r'\1:\2; \3:\4', i_str)  # 2020-01-30
#     i_str = re.sub(r' ([0-9]+):([0-9]+) and chapter ([0-9]+)', r' \1:\2; \3', i_str)  # 2020-01-03
#     i_str = re.sub(r' ([0-9]+):([0-9]+) verses ([0-9]+)-([0-9]+)', r' \1:\2, \3-\4', i_str)  # 2020-02-03
#     i_str = re.sub(r' ([0-9]+):([0-9]+) verse ([0-9]+)', r' \1:\2, \3', i_str)  # 2020-02-03
#
#     return i_str  # delete_and_in_ref


# def delete_apochrya_books_in_ref(i_str):
#     # 2020-02-03 Enoch 91:16
#     i_str = re.sub(r'Enoch ([0-9]+):([0-9]+)', r' ', i_str)
#
#     return i_str  # delete_apochrya_books_in_ref


def delete_chapter_in_ref(i_str):  # 2019-12-23
    i_str = re.sub(r' chapter ([0-9]+):([0-9]+)', r' \1:\2', i_str)
    i_str = re.sub(r' Chapter ([0-9]+):([0-9]+)', r' \1:\2', i_str)

    return i_str  # delete_chapter_in_ref


def delete_to_in_ref(i_str):  # 2019-10-12
    i_str = re.sub(r'([0-9]+):([0-9]+) to ([0-9]+):([0-9]+)', r'\1:\2-\3:\4', i_str)
    i_str = re.sub(r'([0-9]+):([0-9]+) to ([0-9]+)', r'\1:\2-\3', i_str)

    return i_str  # delete_to_in_ref


def delete_och_in_ref(i_str):  # 2019-10-08    'Genesis 1:31 och 2:1' => 'Genesis 1:31; 2:1'
    i_str = re.sub(r'([0-9]+):([0-9]+) och ([0-9]+):([0-9]+)', r'\1:\2; \3:\4', i_str)
    i_str = re.sub(r'([0-9]+):([0-9]+) och ([0-9]+)', r'\1:\2, \3',
                   i_str)  # Esra 7:6 och 10   2019-10-10  Esra 9:1 och 2
    i_str = re.sub(r'([0-9]+):([0-9]+)-([0-9]+) och ([0-9]+):([0-9]+)-([0-9]+)', r'\1:\2-\3; \4:\5-\6',
                   i_str)  # Apg 2:42-47 och 4:32-37   Mika 2:8-11 och 3:8-12
    i_str = re.sub(r'([0-9]+):([0-9]+) och ([0-9]+):([0-9]+)', r'\1:\2; \3:\4', i_str)  # 1 Mos 1:28 och 2:15
    i_str = re.sub(r'([0-9]+):([0-9]+)-([0-9]+) och ([0-9]+):([0-9]+)', r'\1:\2-\3; \4:\5',
                   i_str)  # Jes 3:13-15 och 5:7
    i_str = re.sub(r'([0-9]+), ([0-9]+) och ([0-9]+)', r'\1, \2, \3', i_str)
    i_str = re.sub(r'([A-Ö])([a-ö]+) ([0-9]+):([0-9]+) och ([A-Ö])([a-ö]+) ([0-9]+):([0-9]+)',
                   r'\1\2 \3:\4; \5\6 \7:\8', i_str)
    i_str = re.sub(r'([0-9]+):([0-9]+) beskriver även ([0-9]+):([0-9]+)', r'\1:\2; \3:\4', i_str)

    return i_str  # remove_och_in_bible_references


# def delete_with_in_ref(i_str):  # 2019-10-08    'Genesis 1:31 with 3:11' => 'Genesis 1:31; 3:11'
#     i_str = re.sub(r'([0-9]+):([0-9]+) with ([0-9]+):([0-9]+)', r'\1:\2; \3:\4', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+)-([0-9]+) with ([0-9]+):([0-9]+)-([0-9]+)', r'\1:\2-\3; \4:\5-\6', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+)-([0-9]+) with ([0-9]+):([0-9]+)', r'\1:\2-\3; \4:\5', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+) with ([0-9]+):([0-9]+)-([0-9]+)', r'\1:\2; \3:\4-\5', i_str)
#
#     return i_str  # remove_with_in_bible_references


# def delete_space_in_ref1(i_str):  # 1 Joh. 1:5- 7 => 1 Joh. 1:5-7
#     # 2020-10-14 Now in database file
#     i_str = re.sub(r'([0-9]+):([0-9]+)- ([0-9]+)', r'\1:\2-\3', i_str)
#
#     return i_str  # remove_space_in_bible_verse1


# def delete_space_in_ref2(i_str):  # (Rom. 7:7, 9 - 12) => (Rom. 7:7, 9-12)   2018-05-01 16:16
#     # 2020-10-14 Now in database file
#     i_str = re.sub(r', ([0-9]+) - ([0-9]+)', r', \1-\2', i_str)  # , ([0-9]+) - ([0-9]+)\) => , \1-\2)
#
#     return i_str  # remove_space_in_bible_verse2
#
#
# def delete_space_in_ref3(i_str):
#     # (Hebr. 12:1 - 2) => (Hebr. 12:1-2)
#     i_str = re.sub(r'([0-9]+): ([0-9]+) - ([0-9]+)', r'\1:\2-\3', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+) - ([0-9]+)', r'\1:\2-\3', i_str)
#     i_str = re.sub(r'([0-9]+) : ([0-9]+) - ([0-9]+)', r'\1:\2-\3', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+) -  ([0-9]+)', r'\1:\2-\3', i_str)
#     i_str = re.sub(r'([0-9]+) : ([0-9]+)', r'\1:\2', i_str)
#     # Upp 1 4:14    => Upp 1 4:14
#     i_str = re.sub(r'([0-9]+) ([0-9]+):([0-9]+)', r'\1\2:\3', i_str)
#     # i_str = re.sub(r'([0-9]+) - ([0-9]+)', r'\2-\3', i_str)
#
#     return i_str  # remove_space_in_bible_verse3
#
#
# def delete_space_in_ref4(i_str):  # (Hebr. 12 :1) => (Hebr. 12:1)
#     i_str = re.sub(r'([0-9]+) :([0-9]+)', r'\1:\2', i_str)  # ([0-9]+:[0-9]+) - ([0-9]+) => \1:\2-\3
#
#     return i_str  # remove_space_in_bible_verse4
#
#
# def delete_space_in_ref5(i_str):  # (Hebr. 12: 24) => (Hebr. 12:24)
#     i_str = re.sub(r'([0-9]+): ([0-9]+)', r'\1:\2', i_str)  # ([0-9]+:[0-9]+) - ([0-9]+) => \1:\2-\3
#
#     return i_str  # remove_space_in_bible_verse5
#
#
# def delete_space_in_ref6(i_str):  # Genesis 2:2-  3
#     i_str = re.sub(r'([0-9]+):([0-9]+)-  ([0-9]+)', r'\1:\2-\3', i_str)
#
#     return i_str  # remove_space_in_bible_verse6
#
#
# def delete_space_in_ref7(i_str):
#     # Jes. 43:3 -15
#     i_str = re.sub(r'([0-9]+):([0-9]+) -([0-9]+)', r'\1:\2-\3', i_str)
#
#     return i_str  # delete_space_in_ref7
#
#
# def delete_space_in_ref8(i_str):
#     # 'Daniel 11:44b-45 and 12:1  -45'    => '# Daniel 11:44b-45 and 12:1  -45'
#     i_str = re.sub(r'([0-9]+):([0-9]+)  -([0-9]+)', r'\1:\2-\3', i_str)
#
#     return i_str  # delete_space_in_ref8
#
#
# def delete_space_in_ref9(i_str):
#     # Isaiah 37:31, 32   -36    => Isaiah 37:31, 32-36
#     i_str = re.sub(r'([0-9]+), ([0-9]+)   -([0-9]+)', r'\1, \2-\3', i_str)
#
#     return i_str  # delete_space_in_ref9
#
#
# def delete_cf_in_ref(i_str):
#     # 2020-02-02 Dan. 3:1; cf. 2:31 => Dan. 3:1; 2:31
#     # i_str = re.sub(r'([0-9]+):([0-9]+); cf\. ([0-9]+):([0-9]+)', r'\1:\2; \3:\4', i_str)
#     i_str = re.sub(r' cf\. ', r' ', i_str)
#     i_str = re.sub(r' cf ', r' ', i_str)
#
#     return i_str  # delete_cf_in_ref
#
#
# def add_space_in_ref1(i_str):  # James 1:14,15 => James 1:14, 15
#     i_str = re.sub(r'([0-9]+):([0-9]+),([0-9]+)', r'\1:\2, \3', i_str)
#
#     return i_str  # add_space_in_bible_verse1
#
#
# def add_space_in_ref2(i_str):  # 1 Corinthians 15:42, 52,53 => 1 Corinthians 15:42, 52, 53
#     i_str = re.sub(r'([0-9]+),([0-9]+)', r'\1, \2', i_str)
#
#     return i_str
#
#
# def add_space_in_ref3(i_str):  # 1 Corinthians 15:42, 52,53,54 => 1 Corinthians 15:42, 52, 53, 54
#     i_str = re.sub(r'([0-9]+), ([0-9]+), ([0-9]+),([0-9]+)', r'\1, \2, \3, \4', i_str)
#
#     return i_str
#
#
# def add_space_in_ref4(i_str):  # 1 Corinthians15:42 => 1 Corinthians 15:42
#     i_str = re.sub(r'([a-ö]+)([0-9]+):([0-9]+)', r'\1 \2:\3', i_str)
#
#     return i_str
#
#
# def add_space_in_ref5(i_str):
#     # Joh.1:1     =>  Joh. 1:1          2019-11-10 08:12:50
#     # Joh.1:1-3     =>  Joh. 1:1-3      2019-11-10 08:12:50
#     i_str = re.sub(r'([a-ö]+)\.([0-9]+):([0-9]+)', r'\1. \2:\3', i_str)
#     i_str = re.sub(r'([a-ö]+)\.([0-9]+):([0-9]+)-([0-9]+)', r'\1. \2:\3-\4', i_str)
#
#     return i_str
#
#
# def delete_dot_in_ref1(i_str):
#     # (Joh. 14:16-21.) => (Joh. 14:16-21)
#     # (Proverbs. 4: 23)
#     i_str = re.sub(r' ([0-9]+):([0-9]+)-([0-9]+)\.', r' \1:\2-\3', i_str)
#     i_str = re.sub(r'\. ([0-9]+):([0-9]+)', r' \1:\2', i_str)
#     i_str = re.sub(r'\. ([0-9]+): ([0-9]+)', r' \1:\2', i_str)
#
#     return i_str
#
#
# def delete_dot_in_bible_verse2(i_str):  # (Neh. 4:3, 1, 4.) => (Neh. 4:3, 1, 4)
#     import re
#     i_str = re.sub(r'([0-9]+), ([0-9]+), ([0-9]+)\.', r'\1, \2, \3', i_str)
#
#     return i_str
#
#
# def delete_dot_in_ref3(i_str):  # (Vers 143.) => (Vers 143)
#     import re
#     i_str = re.sub(r'Vers ([0-9]+)\.', r'Vers \1', i_str)
#
#     return i_str
#
#
# def delete_dot_in_ref4(i_str):
#     # (1 Kor. 16:1-2.) => (1 Kor. 16:1-2)
#     import re
#     i_str = re.sub(r'([0-9]+)-([0-9]+)\.', r'\1-\2', i_str)
#
#     return i_str
#
#
# def modify_dot_to_comma_in_ref(i_str):
#     # (Jn 1:29.36) => (Jn 1:29, 36)   # 2019-12-16
#     import re
#     i_str = re.sub(r'([0-9]+):([0-9]+)\.([0-9]+)', r'\1:\2, \3', i_str)
#
#     return i_str
#
#
# def change_colon_to_semicolon1(i_str):  # Hab. 2:14: Joh. 17:3   => Hab. 2:14; Joh. 17:3
#     import re
#     i_str = re.sub(r'([0-9]+): ([A-Z][a-z]+)', r'\1; \2', i_str)  # ([0-9]+): ([A-Z][a-z]+)
#
#     return i_str
#
#
# def change_colon_to_semicolon2(i_str):  # Hab. 2:14: 7:3 => Hab. 2:14; 7:3
#     i_str = re.sub(r':([0-9]+):([0-9]+): ([0-9]+)', r':\1:\2; \3', i_str)  # ([0-9]+): ([A-Z][a-z]+)
#
#     return i_str
#
#
# def change_colon_to_semicolon3(i_str):  # Gal. 3:29: 1 Pet. 1:4  => Gal. 3:29; 1 Pet. 1:4
#     i_str = re.sub(r':([0-9]+): ([0-9]+) ([A-Z])', r':\1; \2 \3', i_str)
#
#     return i_str
#
#
# def add_comma_in_bible_verse(i_str):  # 2019-10-08 'Esra 6:15 18, och sedan, ca 6'
#     i_str = re.sub(r'([0-9]+):([0-9]+) ([0-9]+)', r'\1:\2, \3', i_str)
#
#     return i_str
#
#
# def add_comma_in_ref(i_str):
#     i_str = re.sub(r'([0-9]+):([0-9]+) ([0-9]+)', r'\1:\2, \3', i_str)
#
#     return i_str
#
#
# def delete_numbers_and_parenthesis(i_str):  # 2019-11-10
#     # text1 (75) text2      =>  text1 text2
#     i_str = re.sub(r' \(([0-9]+)\) ', r' ', i_str)
#
#     return i_str
#
#
# def delete_multiple_dashes_in_ref(i_str):
#     # 'Daniel 4:24---27'  => 'Daniel 4:24-27'
#     i_str = re.sub(r'([0-9]+):([0-9]+)---([0-9]+)', r'\1:\2-\3', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+)--([0-9]+)', r'\1:\2-\3', i_str)
#
#     return i_str  # delete_multiple_dashes_in_ref
#
#
# def delete_a_b_c_d_e_f_in_ref(i_str):
#     # Daniel 11:44b-45  => Daniel 11:44-45
#     # Daniel 11:44a  => Daniel 11:44a
#     i_str = re.sub(r'([0-9]+):([0-9]+)a', r'\1:\2', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+)b', r'\1:\2', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+)c', r'\1:\2', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+)d', r'\1:\2', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+)e', r'\1:\2', i_str)
#
#     i_str = re.sub(r'([0-9]+):([0-9]+):A', r'\1:\2', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+):B', r'\1:\2', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+):C', r'\1:\2', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+):D', r'\1:\2', i_str)
#     i_str = re.sub(r'([0-9]+):([0-9]+):E', r'\1:\2', i_str)
#
#     i_str = re.sub(r'([0-9]+):([0-9]+)ff', r'\1:\2', i_str)  # 2020-01-25
#     i_str = re.sub(r'([0-9]+):([0-9]+)f', r'\1:\2', i_str)  # 2020-01-25
#
#     return i_str  # delete_a_b_c_d_e_f_in_ref
#
#
# def delete_ff_in_ref(i_str):
#     # 'Psalms 78:12ff'    =>  'Psalms 78:12ff'
#     i_str = re.sub(r'([0-9]+):([0-9]+)ff', r'\1:\2', i_str)
#
#     return i_str  # delete_ff_in_ref
#
#
# def delete_difficult_words_in_text(t_str: str):
#     # 2020-01-30
#     t_str = re.sub(r'([0-9]+) B\.C\.', r' ', t_str)
#     t_str = re.sub(r'([0-9]+) A\.C\.', r' ', t_str)
#     t_str = re.sub(r'MONDAY ([0-9]+)page ([0-9]+)', r' ', t_str)
#     t_str = re.sub(r'TUESDAY ([0-9]+)page ([0-9]+)', r' ', t_str)
#     t_str = re.sub(r'WEDNESDAY ([0-9]+)page ([0-9]+)', r' ', t_str)
#     t_str = re.sub(r'THURSDAY ([0-9]+)page ([0-9]+)', r' ', t_str)
#     t_str = re.sub(r'FRIDAY ([0-9]+)page ([0-9]+)', r' ', t_str)
#     t_str = re.sub(r'MONDAY page ([0-9]+)', r' ', t_str)
#     t_str = re.sub(r'TUESDAY page ([0-9]+)', r' ', t_str)
#     t_str = re.sub(r'WEDNESDAY page ([0-9]+)', r' ', t_str)
#     t_str = re.sub(r'THURSDAY page ([0-9]+)', r' ', t_str)
#     t_str = re.sub(r'FRIDAY page ([0-9]+)', r' ', t_str)
#     t_str = re.sub(r'page ([0-9]+)', r' ', t_str)
#     t_str = re.sub(r'p\. ([0-9]+)', r' ', t_str)
#     t_str = re.sub(r't e ac h e r s c o m m e n t s ([0-9]+)', r' ', t_str)
#
#     return t_str  # delete_difficult_words_in_text


def normalize_chapter_and_verse(i_str: str, input_from_speech: bool):
    # if input_from_speech:
    #     print('1245 input_from_speech = True')

    f = Path(script_directory) / '..' / 'database' / 'normalize_chapter_and_verse_list.txt'
    normalize_chapter_and_verse_list = get_2_tuple_as_list(f, False)
    for item in normalize_chapter_and_verse_list:
        # print('1642 ' + item.__str__())
        # print('1643 ' + i_str)
        i_str = re.sub(item[0], item[1], i_str)
        # print('2145: ' + item[0] + ' ' + item[1] + ' ' + i_str)

    if input_from_speech:
        # print('1283')
        i_str = prepare_incomplete_references_from_speech(i_str)  # 2020-03-03
    else:
        i_str = prepare_incomplete_references_from_text(i_str)  # 2022-12-11

    return i_str  # normalize_chapter_and_verse


def normalize_bible_book_names(i_string, i_execution_mode):
    t_string = i_string

    if i_execution_mode == 'english_text':
        t_string = normalize_eng_bible_book_names(t_string)  # 2019-10-01 Added
    elif i_execution_mode == 'swedish_text':
        t_string = normalize_swe_bible_book_names(t_string)  # 2019-10-01 Added

    return t_string  # normalize_bible_book_names


# https://stackoverflow.com/questions/33545863/python-find-position-of-last-digit-in-string
def get_index_of_last_digit_in_string(i_string: str):
    if i_string.__len__() > MAX_BIBLE_REFERENCE_LENGTH:
        i_string = i_string[0:MAX_BIBLE_REFERENCE_LENGTH]
    try:
        index = next(i for i, j in list(enumerate(i_string, 1))[::-1] if j.isdigit())
        return index

    except StopIteration:
        return 0


def get_file_base_name_end_with_date_stamp2(i_file_name, i_execution_mode):  # 2019-12-01
    t_file_base_name = os.path.basename(i_file_name)
    timestr = time.strftime('(%Y-%m-%d--%H%M%S)')
    index_of_dot = t_file_base_name.rindex(DOT)  # 2021-05-29 15:28:34
    if i_execution_mode == 'english_text':
        output_file_name = t_file_base_name[0:index_of_dot] + '_Bible-ref-Eng' + timestr + '.txt'
    elif i_execution_mode == 'swedish_text':
        output_file_name = t_file_base_name[0:index_of_dot] + '_Bible-ref-Swe' + timestr + '.txt'

    # index_of_dot = t_file_base_name.rindex(DOT)     # 2019-10-13 14:51:05
    # if i_execution_mode == 'english_text':
    #     output_file_name = t_file_base_name[0:index_of_dot] + '_Bible-ref-Eng' + timestr + '.txt'
    # elif i_execution_mode == 'swedish_text':
    #     output_file_name = t_file_base_name[0:index_of_dot] + '_Bible-ref-Swe' + timestr + '.txt'

    return output_file_name  # get_file_base_name_end_with_date_stamp2


def get_file_base_name_end_with_date_stamp(i_file_name):  # 2019-09-12 Updated
    t_file_base_name = os.path.basename(i_file_name)
    time_str = time.strftime('(%Y%m%d-%H%M%S)')
    index_of_dot = t_file_base_name.index(DOT)  # 2019-12-14 09:18:02
    output_file_name = t_file_base_name[0:index_of_dot] + '_References' + time_str + t_file_base_name[index_of_dot:]

    return output_file_name  # get_file_base_name_end_with_date_stamp


def get_file_content(i_file_path_and_name):  # 2019-09-09
    # print('1368 Reading file: ' + i_file_path_and_name + '  With encoding: ' + ENCODING)
    with open(i_file_path_and_name, 'r', encoding=ENCODING) as f:  # 2019-09-19 encoding='latin-1'
        file_line_list = f.readlines()
    f.close()

    return file_line_list  # get_file_content


def write_list_to_file(i_output_file_path_and_name, t_lines_list, outfile_encoding):  # 2019-09-06
    # t_lines_list is a list of list
    # 2020-10-23
    outfile_encoding = 'utf-8'
    # print('1576 Writing file: ' + i_output_file_path_and_name + '  With encoding: ' + outfile_encoding)
    with open(i_output_file_path_and_name, mode='wt',
              encoding=outfile_encoding) as myfile:  # 2019-09-19 removed 'latin-1'
        for t_line_str in t_lines_list:
            for word in t_line_str:
                myfile.write(word)
    myfile.close()

    return ()  # write_list_to_file


def backup_file(i_input_file_path_and_name, i_output_file_path_and_name, outfile_encoding):  # 2019-09-09
    t_input_file_content_list = get_file_content(i_input_file_path_and_name)
    write_list_to_file(i_output_file_path_and_name,
                       t_input_file_content_list,
                       outfile_encoding)
    return ()  # def backup_file


def get_input_file_folder_and_name(i_file_name):  # 2019-09-06 New
    # file_path_name = os.path.dirname(os.path.abspath(i_file_name))
    file_path_name = EXECUTION_FOLDER
    input_file_base_name = os.path.basename(i_file_name)
    input_file_path_and_name = file_path_name / input_file_base_name

    return input_file_path_and_name  # get_input_file_folder_and_name


def delete_ref_after_star(i_single_reference: str):  # 2019-11-11
    if i_single_reference.__contains__('*'):
        out_single_reference = i_single_reference[0:i_single_reference.index('*')]
    else:
        out_single_reference = i_single_reference
    return out_single_reference  # delete_reference_after_star


def delete_reference_second_colon(i_ref: str):  # 2019-12-23
    # 'Acts 17:30, 31: txt'
    # Check if there is any NUMBER after the LAST COLON. If not, truncate text at the colon
    if i_ref.count(COLON) > 1:
        ix_last_colon = i_ref.rindex(COLON)
        rest = i_ref[ix_last_colon + 1:]
        if not hasNumbers(rest):
            out_ref = i_ref[0:ix_last_colon] + NEWLINE
        else:
            out_ref = i_ref
    else:
        out_ref = i_ref

    # if i_single_reference.__contains__(COLON):
    #     # There is at least one COLON
    #     ix_1_colon = i_single_reference.index(COLON)
    #     rest = i_single_reference[ix_1_colon + 1:]
    #     if rest.__contains__(COLON):
    #         # There is a second COLON
    #         ix_2_colon = rest.index(COLON)
    #         out_single_reference = i_single_reference[0:ix_1_colon + 1] + rest[0:ix_2_colon] + NEWLINE
    #     else:
    #         # There is just ONE COLON
    #         out_single_reference = i_single_reference
    # else:
    #     out_single_reference = i_single_reference

    return out_ref  # delete_reference_second_colon


def delete_ref_after_comma(i_str):
    # 2019-10-17 'Ezra 5:1-5, Haggai 1' => 'Ezra 5:1-5'
    # 'Revelation 14:9-11, 14-18'
    t_str = str
    t_str = i_str
    if t_str.__contains__(COMMA):
        ix_last_comma = i_str.rfind(COMMA)
        if t_str.__len__() > ix_last_comma + 3:
            tmp = t_str[ix_last_comma + 2: ix_last_comma + 3]
            if not hasNumbers(tmp):
                t_str = t_str[0:ix_last_comma]

    return t_str  # delete_reference_after_comma


def get_input_and_output_backup_file_and_folder_names(i_file_name, sub_folder_for_logfiles):  # 2019-09-06 New
    file_path_name = EXECUTION_FOLDER
    input_file_base_name = os.path.basename(i_file_name)
    input_file_path_and_name = file_path_name / input_file_base_name
    output_file_path_and_name = file_path_name / '..' / sub_folder_for_logfiles / get_file_base_name_end_with_date_stamp(
        i_file_name)

    return [input_file_path_and_name, output_file_path_and_name];


def get_b_ref_part(i_line_str: str, i_execution_mode: str, bible_book_hit_list=[]):
    # Pretext 1 John 3:1, 2, 3, 4, 5 Text		['Pretext', 'John 3:1, 2, 3, 4, 5', 'Text'] 2019-09-10 OK
    # Pretext 1 John 3:1-2 Text			        'Pretext', '1 John 3:1-2', 'Text'           2019-09-10 OK
    # Pretext 1 John 3:1-2, 4-5 Text			'Pretext', '1 John 3:1-2, 4-5', 'Text'      2019-09-10 OK
    return_list = []

    t_book = NOSPACE
    reference_str = NOSPACE
    t_line_str = str
    t_line_str = i_line_str

    # print('1342: i_line length: ' + i_line_str.__len__().__str__() )

    # f = Path(script_directory) / 'database' / 'normalized_swe_book_names_list.txt'
    # normalized_swe_book_names_list = get_file_rows_as_list(f)
    #
    # if i_execution_mode == 'swedish_text':
    #     book_list = normalized_swe_book_names_list
    # else:
    #     book_list = bible_book_hit_list

    if t_line_str.__contains__(COLON):
        t_colon_index = t_line_str.index(COLON)
        t_book_part = t_line_str[0:t_colon_index]
        t_chapter_start_index = t_book_part.rfind(ONESPACE)
        t_chapter = t_book_part[t_chapter_start_index:]

        for book_name in bible_book_hit_list:
            # if t_line_str.__contains__(book_name):      # 2019-10-17 changed from i_line_str to t_line_str
            if t_line_str.startswith(book_name):  # 2020-02-25
                t_book = book_name
                break

        pretext_end_index = t_line_str.index(t_book)
        pre_text_str = t_line_str[0:pretext_end_index - 1]

        last_verse_index = get_index_of_last_digit_in_string(t_line_str)
        post_text_str = t_line_str[last_verse_index + 1:]

        reference_str = t_line_str[pretext_end_index:last_verse_index]

        return_list = [pre_text_str, reference_str, post_text_str]
    else:
        # print('1367: ' + i_line_str)
        if i_line_str.startswith('verse '):
            verse_no = i_line_str[i_line_str.index(ONESPACE) + 1:]
            t_line_str = 'Book x:' + verse_no
            # print('1373: ' + reference_str)
        # Handle the book of Jude 2019-12-22
        if i_line_str.__contains__('Jude'):
            # print('875: ' + i_line_str)
            reference_str = i_line_str

        else:
            # print('1412: ' + t_line_str)
            reference_str = readjust_to_normalized_names(t_line_str[0:get_index_of_last_digit_in_string(t_line_str)],
                                                         i_execution_mode)
            # print('1414: ' + reference_str)
            # 2020-02-27 Comment next two lines
            # if new_reference_str != t_line_str:
            #     reference_str = new_reference_str

            # 2020-02-27 Comment 4 lines below
            # if hasNumbers(i_line_str):
            #     # print('          Unresolved: ' + i_line_str)
            #     if not reference_str.startswith('Book'):
            #         reference_str = '          Unresolved: ' + i_line_str[0:get_index_of_last_digit_in_string(t_line_str)]

    # 2021-02-27 Moved to a later step
    # reference_str = readjust_to_normalized_names(reference_str, i_execution_mode)
    # print('1426: ' + reference_str)

    return reference_str  # get_bible_reference_part


def get_bible_reference_parts(i_line_str, normalized_eng_book_names_list=[]):
    # Pretext 1 John 3:1, 2, 3, 4, 5 Text		['Pretext', 'John 3:1, 2, 3, 4, 5', 'Text'] 2019-09-10 OK
    # Pretext 1 John 3:1-2 Text			        'Pretext', '1 John 3:1-2', 'Text'           2019-09-10 OK
    # Pretext 1 John 3:1-2, 4-5 Text			'Pretext', '1 John 3:1-2, 4-5', 'Text'      2019-09-10 OK

    return_list = []
    t_line_str = i_line_str

    if t_line_str[0] == '÷':
        t_line_str = t_line_str[1:]

    t_colon_index = t_line_str.index(COLON)

    t_book_part = t_line_str[0:t_colon_index]
    t_chapter_start_index = t_book_part.rfind(ONESPACE)
    t_chapter = t_book_part[t_chapter_start_index:]

    for eng_book_name in normalized_eng_book_names_list:
        if i_line_str.__contains__(eng_book_name):
            t_book = eng_book_name

    pre_text_end_index = t_line_str.index(t_book)
    pre_text_str = t_line_str[0:pre_text_end_index - 1]

    last_verse_index = get_index_of_last_digit_in_string(t_line_str)
    post_text_str = t_line_str[last_verse_index + 1:]
    reference_str = t_line_str[pre_text_end_index:last_verse_index]
    return_list = [pre_text_str, reference_str, post_text_str]

    return return_list


def get_bible_reference_parts_simple(i_line_str):
    # This procedure assumes that a verse reference starts at the beginning of a line
    # b, c, v, t
    # discerned. {1 Corinthians 2:14} And we
    #     Example					Type
    # =======					====
    # Pretext Amos 2:14 Text			        “N C:V”
    # Pretext 1 Joh 271:271 Text			    ”#1 N1 C:V”		#1.[1-5]
    # Pretext 1. Mos. 5:5 Text			        ”#1. N1 C:V”
    # Pretext 1Kor 183:183 Text			        ”#1N1 C:V”
    # Pretext Första Samuelsboken 54:54 Text	”Första N1 C:V”
    # Pretext Andra Joh 283:283 Text			“Andra N2 C:V”
    # Pretext Tredje Moseboken 22:22 Text		”Tredje N1 C:V”
    # Pretext Fjärde Mos. 31:31 Text			”Fjärde N1 C:V”
    # Pretext Femte Mos. 39:39 Text			    ”Femte N1 C:V”
    # Pretext Brevet till Filemon 246:246 Text	”Brevet till N1 C:V'
    # Pretext Höga V 100:100 Text			    ”N1 N2 C:V”
    # Pretext Jobs bok 86:86 Text			    “N1 N2 C:V”
    #
    # Pretext 1 John 3:1, 2, 3, 4, 5 Text		#1 N1 C:V1, V2, V3, V4, V5
    # Pretext 1 John 3:1-2 Text			        #1 N1 C:V1-V2
    # Pretext 1 John 3:1-2, 4-5 Text			#1 N1 C:V1-V2, V3-V4

    t_line = i_line_str

    if t_line[0] == '÷':
        t_line = t_line[1:]

    t_colon_index = t_line.index(COLON)

    t_book_part = t_line[0:t_colon_index]
    t_chapter_start_index = t_book_part.rfind(ONESPACE)
    t_chapter = t_book_part[t_chapter_start_index:]

    if i_line_str.__contains__('Brevet till Filemon'):
        t_book = 'Brevet till Filemon'
    else:
        t_book = t_book_part[0:t_chapter_start_index]

    t_verse_to_end = t_line[t_colon_index:]

    t_verse_start_index = t_verse_to_end.index(ONESPACE)
    t_verse = t_verse_to_end[1:t_verse_start_index]

    t_text = t_verse_to_end[t_verse_start_index + 1:]
    t_text = remove_new_line_character_from_end(t_text)

    return t_book, int(t_chapter), int(t_verse), t_text


def remove_new_line_character_from_end(i_text):
    if i_text[-1] == NEWLINE:
        i_text = i_text[0:-1]

    return i_text  # remove_new_line_character_from_end


def get_input_and_output_backup_file_names(i_file_name, sub_folder_for_logfiles):  # 2019-09-06 New
    # file_path_name = os.path.dirname(os.path.abspath(i_file_name))
    file_path_name = EXECUTION_FOLDER
    input_file_base_name = os.path.basename(i_file_name)
    input_file_path_and_name = file_path_name / input_file_base_name
    output_file_path_and_name = file_path_name / '..' / sub_folder_for_logfiles / get_file_base_name_end_with_date_stamp(
        i_file_name)

    return [input_file_path_and_name, output_file_path_and_name]


def hasNumbers(i_string):
    return any(char.isdigit() for char in i_string)


def write_list_to_file_with_line_breaks(i_output_file_path_and_name: str,
                                        i_list: [],
                                        outfile_encoding: str):
    outfile_encoding = ENCODING  # 2020-10-01
    # print('1785 Writing to: ' + i_output_file_path_and_name + '  With encoding: ' + outfile_encoding)
    # print('1786: i_list: ' + i_list.__str__())
    with open(i_output_file_path_and_name, mode='wt', encoding=outfile_encoding) as myfile:
        for line in i_list:
            # t_string_list =  re.split(r'[~\r\n]+', i_str)
            myfile.write(line)
    myfile.close()

    return ()  # write_string_to_file_with_line_breaks


def get_file_backup_path_and_name(i_file_name, sub_folder_for_logfiles):  # 2019-09-19
    file_path_name = EXECUTION_FOLDER
    output_file_path_and_name = file_path_name / '..' / sub_folder_for_logfiles / get_file_base_name_end_with_date_stamp(
        i_file_name)

    # print('1766 output_file_path_and_name: ' + output_file_path_and_name)

    return output_file_path_and_name
