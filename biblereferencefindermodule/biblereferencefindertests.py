#!/usr/bin/env python
__author__ = "Sune Gustafsson"
__copyright__ = "Copyright 2019, Sune Gustafsson"
__credits__ = ["Sune Gustafsson"]
__license__ = "GPL"
__version__ = "1.0.1"
__date__ = "2019-09-13 08:47:13"
__maintainer__ = "Sune Gustafsson"
__email__ = "ehssugu@gmail.com"
__status__ = "Test"

import os
import re
import unittest
from pathlib import Path

from biblereferencefinder import post_process_difficult_references, get_file_rows_as_list, \
    get_bible_book_name_from_b_ref, get_chapter_no_from_b_ref, get_verse_no_from_b_ref, get_bible_book_references_split3
from biblereferencefindereng import get_1_line_in_file_as_list, prepare_eng_text_for_replacement, \
    add_bible_book_name_before_incomplete_reference, remove_eng_reference_suffix, \
    get_kjv_number_of_verses_per_chapter_list, remove_eng_reference_prefix
from biblereferencefindershared import normalize_bible_book_names, delete_reference_second_colon, \
    get_last_verse_number_in_bible_reference, get_bible_reference_parts_simple, modify_2_consecutive_verses_to_range2, \
    delete_ref_after_last_digit, remove_char_in_txt_file, get_2_tuple_as_list, split_b_ref_by_semicolon2, \
    split_reference_crossing_chapter_border, split_b_ref_by_comma3, normalize_chapter_and_verse, \
    remove_non_digits_at_end_of_reference, delete_ref_after_last_digit_in_ref, replace_characters_in_former_pdf_file

script_directory = os.path.dirname(os.path.realpath(__file__))


def process_1John(i_str):
    out_str = prepare_eng_text_for_replacement(i_str)
    # i_str = remove_eng_reference_prefix(i_str)  # 2019-10-09
    out_str = remove_eng_reference_suffix(out_str)  # 2019-10-17
    # print('1335: ' + i_str)

    f_test_normalized_eng_bible_book_names_list = Path(
        script_directory) / '..' / 'database' / 'test_normalized_eng_bible_book_names_list.txt'
    normalized_eng_bible_book_names_list = get_2_tuple_as_list(f_test_normalized_eng_bible_book_names_list,
                                                               new_line=False)
    for item in normalized_eng_bible_book_names_list:
        if item[0].__contains__('1 John') and out_str.__contains__('1 John'):
            # print('155: ' + item[0] + ' ==> ' + item[1])
            # print('157: ' + out_str)
            pass
        out_str = re.sub(item[0], item[1], out_str)
        # if out_str.__contains__('1 Jn'):
        #     print('159: ' + out_str)
        #     pass

        # a = '1 John ([0-9]+):([0-9]+)'
        # b = '1 Jn \1:\2'
        # i_str = re.sub(a, b, i_str)
        # i_str = re.sub(r'1 John ([0-9]+):([0-9]+)', r'1 Jn \1:\2', i_str)

    return out_str  # normalize_eng_bible_book_names


def find_string(txt, str1):
    return txt.find(str1, txt.find(str1) + 1)


class TestStringMethods(unittest.TestCase):

    def test_get_bible_book_references_split2_07(self):
        i_execution_mode = 'swedish_text'
        f = Path(script_directory) / '..' / 'database' / 'normalized_swe_book_names_list.txt'
        bible_book_hit_list = get_file_rows_as_list(f, 'utf-8')
        self.assertEqual(
            ['Dan 12:1-3\n', 'Dan 12:4-5\n', 'Joh 1:1-5\n'],
            get_bible_book_references_split3(['Dan 12:1-3, 4-5; Joh 1:1-5\n'], i_execution_mode, bible_book_hit_list)
        )

    def test_get_bible_book_references_split2_06(self):
        i_execution_mode = 'swedish_text'
        f = Path(script_directory) / '..' / 'database' / 'normalized_swe_book_names_list.txt'
        bible_book_hit_list = get_file_rows_as_list(f, 'utf-8')
        self.assertEqual(
            ['Dan 12:1-3\n', 'Joh 1:1-5\n'],
            get_bible_book_references_split3(['Dan 12:1-3, Joh 1:1-5\n'], i_execution_mode, bible_book_hit_list)
        )

    def test_get_bible_book_references_split2_05(self):
        i_execution_mode = 'swedish_text'
        f = Path(script_directory) / '..' / 'database' / 'normalized_swe_book_names_list.txt'
        bible_book_hit_list = get_file_rows_as_list(f, 'utf-8')
        self.assertEqual(
            ['Dan 12:1-3\n', 'Dan 12:1-5\n'],
            get_bible_book_references_split3(['Dan 12:1-3, 1-5\n'], i_execution_mode, bible_book_hit_list)
        )

    def test_get_bible_book_references_split2_04(self):
        i_execution_mode = 'swedish_text'
        f = Path(script_directory) / '..' / 'database' / 'normalized_swe_book_names_list.txt'
        bible_book_hit_list = get_file_rows_as_list(f, 'utf-8')
        self.assertEqual(
            ['Dan 12:3\n', 'Dan 12:1-5\n'],
            get_bible_book_references_split3(['Dan 12:3, 1-5\n'], i_execution_mode, bible_book_hit_list)
        )

    def test_get_bible_book_references_split2_03(self):
        i_execution_mode = 'swedish_text'
        f = Path(script_directory) / '..' / 'database' / 'normalized_swe_book_names_list.txt'
        bible_book_hit_list = get_file_rows_as_list(f, 'utf-8')
        self.assertEqual(
            ['Dan 12:3\n', 'Dan 12:5\n'],
            get_bible_book_references_split3(['Dan 12:3, 5\n'], i_execution_mode, bible_book_hit_list)
        )

    def test_get_bible_book_references_split2_02(self):
        i_execution_mode = 'swedish_text'
        f = Path(script_directory) / '..' / 'database' / 'normalized_swe_book_names_list.txt'
        bible_book_hit_list = get_file_rows_as_list(f, 'utf-8')
        self.assertEqual(
            ['Dan 12:2\n', 'Dan 1:5\n'],
            get_bible_book_references_split3(['Dan 12:2; 1:5\n'], i_execution_mode, bible_book_hit_list)
        )

    def test_get_bible_book_references_split2_01(self):
        i_execution_mode = 'swedish_text'
        f = Path(script_directory) / '..' / 'database' / 'normalized_swe_book_names_list.txt'
        bible_book_hit_list = get_file_rows_as_list(f, 'utf-8')
        self.assertEqual(
            ['Dan 12:1\n'],
            get_bible_book_references_split3(['Dan 12:1\n'], i_execution_mode, bible_book_hit_list)
        )

    def test_replace_characters_in_former_pdf_file_01(self):
        input_from_speech = False

        self.assertEqual(
            'Zech 12:11',
            replace_characters_in_former_pdf_file('Zech 12:ll')
        )

    def test_replace_characters_in_former_pdf_file_02(self):
        input_from_speech = False

        self.assertEqual(
            'Joel 2:32; 3:1-17',
            replace_characters_in_former_pdf_file('Joel 2:32; 3:l-17')
        )

    def test_replace_characters_in_former_pdf_file_03(self):
        input_from_speech = False

        self.assertEqual(
            'Dan 11:45',
            replace_characters_in_former_pdf_file('Dan 1l:45')
        )

    def test_replace_characters_in_former_pdf_file_04(self):
        input_from_speech = False

        self.assertEqual(
            'Rev 14:1 is defined',
            replace_characters_in_former_pdf_file('Rev 14:l is defined')
        )

    def test_normalize_chapter_and_verse_08(self):
        input_from_speech = False

        self.assertEqual(
            'Lev 17:7',
            normalize_chapter_and_verse('Lev. 17.7', input_from_speech)
        )

    def test_normalize_chapter_and_verse_07(self):
        input_from_speech = False

        self.assertEqual(
            '2 Cor 12:7-9',
            normalize_chapter_and_verse('2 Cor. 12.7-9', input_from_speech)
        )

    def test_normalize_chapter_and_verse_06(self):
        input_from_speech = False

        self.assertEqual(
            'Matt. 16:13-17',
            normalize_chapter_and_verse('Matt.16.13-17', input_from_speech)
        )

    def test_normalize_chapter_and_verse_05(self):
        input_from_speech = False

        self.assertEqual(
            'Matt 16:13-17',
            normalize_chapter_and_verse('Matt. 16.13-17', input_from_speech)
        )

    def test_normalize_chapter_and_verse_04(self):
        input_from_speech = False

        self.assertEqual(
            '2 Chron. 11:15',
            normalize_chapter_and_verse('2 Chron.11.15', input_from_speech)
        )

    def test_normalize_chapter_and_verse_03(self):
        input_from_speech = False

        self.assertEqual(
            '2 Chron 11:15',
            normalize_chapter_and_verse('2 Chron. 11.15', input_from_speech)
        )

    def test_normalize_chapter_and_verse_02(self):
        input_from_speech = False

        self.assertEqual(
            'Matthew 16:13-17',
            normalize_chapter_and_verse('Matthew 16.13-17', input_from_speech)
        )

    def test_normalize_chapter_and_verse_02(self):
        input_from_speech = False

        self.assertEqual(
            'Matthew 16:13',
            normalize_chapter_and_verse('Matthew 16.13', input_from_speech)
        )

    def test_normalize_chapter_and_verse_01(self):
        input_from_speech = False

        self.assertEqual(
            'Matthew 16:13',
            normalize_chapter_and_verse('Matthew 16.13', input_from_speech)
        )

    def test_delete_ref_after_last_digit_in_ref_01(self):

        self.assertEqual(
            'Upp 14:9\n',
            delete_ref_after_last_digit_in_ref('Upp 14:9G2532')
        )

    def test_delete_ref_after_last_digit_in_ref_02(self):
        self.assertEqual(
            'Upp 14:9-11\n',
            delete_ref_after_last_digit_in_ref('Upp 14:9-11G2532')
        )

    def test_add_bible_book_name_before_incomplete_reference_25(self):
        # (cf. 4:1-11) OK
        self.assertEqual(
            'Revelation 4:1; 1:11; 17:12',
            add_bible_book_name_before_incomplete_reference(' (cf 4:1; 1:11; 17:12)', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_24(self):
        # (cf. 4:1-11) OK
        self.assertEqual(
            'Revelation 4:1; 1:11',
            add_bible_book_name_before_incomplete_reference(' (cf 4:1; 1:11)', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_23(self):
        # OK
        self.assertEqual(
            'Revelation 22:16, 18, 20, 25',
            add_bible_book_name_before_incomplete_reference(' between 22:16, 18, 20, 25', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_22(self):
        # OK
        self.assertEqual(
            'Revelation 22:16, 18, 20, 25',
            add_bible_book_name_before_incomplete_reference(' and 22:16, 18, 20, 25', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_21(self):
        # OK
        self.assertEqual(
            'Revelation 22:16, 18, 20, 25',
            add_bible_book_name_before_incomplete_reference(' both 22:16, 18, 20, 25', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_20(self):
        # OK
        self.assertEqual(
            'Revelation 22:16, 18, 20, 25',
            add_bible_book_name_before_incomplete_reference(' with 22:16, 18, 20, 25', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_19(self):
        # OK
        self.assertEqual(
            'Revelation 22:16, 18, 20, 25',
            add_bible_book_name_before_incomplete_reference(' in 22:16, 18, 20, 25', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_18(self):
        # OK
        self.assertEqual(
            'Revelation 22:16, 18, 20',
            add_bible_book_name_before_incomplete_reference(' in 22:16, 18, 20', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_17(self):
        # OK
        self.assertEqual(
            'Revelation 22:16, 18',
            add_bible_book_name_before_incomplete_reference(' in 22:16, 18', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_16(self):
        # OK
        self.assertEqual(
            'Revelation 22:16-18',
            add_bible_book_name_before_incomplete_reference(' in 22:16-18', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_15(self):
        # OK
        self.assertEqual(
            'Revelation 22:16',
            add_bible_book_name_before_incomplete_reference(' in 22:16', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_14(self):
        # OK
        self.assertEqual(
            'Revelation 2:16, 18',
            add_bible_book_name_before_incomplete_reference(' (2:16, 18)', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_13(self):
        # with (4:1-11) OK
        self.assertEqual(
            'Revelation 4:1-11',
            add_bible_book_name_before_incomplete_reference(' with (4:1-11)', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_12(self):
        # with (4:1) OK
        self.assertEqual(
            'Revelation 4:1',
            add_bible_book_name_before_incomplete_reference(' with (4:1)', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_11(self):
        # (cf. 4:1-11) OK
        self.assertEqual(
            'Revelation 4:1-11',
            add_bible_book_name_before_incomplete_reference(' (cf 4:1-11)', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_10(self):
        # (cf. 4:1) OK
        self.assertEqual(
            'Revelation 4:1',
            add_bible_book_name_before_incomplete_reference(' (cf 4:1)', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_09(self):
        # OK
        self.assertEqual(
            'Revelation 8:13; 9:12; 11:14; 12:12; 13:1',
            add_bible_book_name_before_incomplete_reference(' (8:13; 9:12; 11:14; 12:12; 13:1)', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_08(self):
        # OK
        self.assertEqual(
            'Revelation 8:13; 9:12; 11:14; 12:12; 13:1',
            add_bible_book_name_before_incomplete_reference(' (8:13; 9:12; 11:14; 12:12; 13:1)', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_07(self):
        # OK
        self.assertEqual(
            'Revelation 8:13; 9:12; 11:14; 12:12',
            add_bible_book_name_before_incomplete_reference(' (8:13; 9:12; 11:14; 12:12)', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_06(self):
        # OK
        self.assertEqual(
            'Revelation 8:13; 9:12; 10:1',
            add_bible_book_name_before_incomplete_reference(' (8:13; 9:12; 10:1)', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_05(self):
        self.assertEqual(
            'Revelation 8:13; 9:12',
            add_bible_book_name_before_incomplete_reference(' (8:13; 9:12)', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_04(self):
        # OK
        self.assertEqual(
            'Revelation 2:16-3:15',
            add_bible_book_name_before_incomplete_reference(' (2:16-3:15)', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_03(self):
        # OK
        self.assertEqual(
            'Revelation 2:16-3:',
            add_bible_book_name_before_incomplete_reference(' (2:16-3:)', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_02(self):
        # OK
        self.assertEqual(
            'Revelation 22:16-',
            add_bible_book_name_before_incomplete_reference(' (22:16-)', 'Revelation')
        )

    def test_add_bible_book_name_before_incomplete_reference_01(self):
        # OK
        self.assertEqual(
            'Revelation 22:16',
            add_bible_book_name_before_incomplete_reference(' (22:16)', 'Revelation')
        )

    def test_get_verse_no_from_b_ref_01(self):
        self.assertEqual(
            '88',
            get_verse_no_from_b_ref('Psalms 119:88')
        )

    def test_get_verse_no_from_b_ref_02(self):
        self.assertEqual(
            '88-110',
            get_verse_no_from_b_ref('Psalms 119:88-110')
        )

    def test_get_verse_no_from_b_ref_03(self):
        self.assertEqual(
            '11',
            get_verse_no_from_b_ref('1 John 4:11')
        )

    def test_get_chapter_no_from_b_ref_01(self):
        self.assertEqual(
            '7',
            get_chapter_no_from_b_ref('Matt 7:10', 'Matt')
        )

    def test_get_chapter_no_from_b_ref_02(self):
        self.assertEqual(
            '10',
            get_chapter_no_from_b_ref('1 Samuel 10:35-16', '1 Samuel')
        )

    def test_get_chapter_no_from_b_ref_03(self):
        self.assertEqual(
            '4',
            get_chapter_no_from_b_ref('1 John 4:11', '1 John')
        )

    def test_get_bible_book_name_from_b_ref_01(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_eng_book_names_list = get_file_rows_as_list(f, 'utf-8')

        self.assertEqual(
            'Revelation',
            get_bible_book_name_from_b_ref('Revelation 7:1', normalized_eng_book_names_list)
        )

    def test_get_bible_book_name_from_b_ref_02(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_eng_book_names_list = get_file_rows_as_list(f, 'utf-8')

        self.assertEqual(
            'Song of Solomon',
            get_bible_book_name_from_b_ref('Song of Solomon 7:1', normalized_eng_book_names_list)
        )

    def test_get_bible_book_name_from_b_ref_03(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_eng_book_names_list = get_file_rows_as_list(f, 'utf-8')

        self.assertEqual(
            '1 John',
            get_bible_book_name_from_b_ref('1 John 4:11', normalized_eng_book_names_list)
        )

    def test_get_bible_book_name_from_b_ref_04(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_eng_book_names_list = get_file_rows_as_list(f, 'utf-8')

        self.assertEqual(
            '',
            get_bible_book_name_from_b_ref('No bible book name 7:1', normalized_eng_book_names_list)
        )

    def test_remove_non_digits_at_end_of_reference_01(self):
        self.assertEqual(
            'Revelation 6:14-17',
            remove_non_digits_at_end_of_reference('Revelation 6:14-17 in GC')
        )

    def test_remove_non_digits_at_end_of_reference_02(self):
        self.assertEqual(
            'Acts 1:9-11',
            remove_non_digits_at_end_of_reference('Acts 1:9-11; DA')
        )

    def test_remove_non_digits_at_end_of_reference_03(self):
        self.assertEqual(
            'text only text only',
            remove_non_digits_at_end_of_reference('text only text only')
        )

    def test_1john_issues_01(self):
        self.assertEqual(
            'Christians should walk, even as he walked.” 1 Jn 2:6 Jesus is our “example.” 1 Peter 2:21 ',
            process_1John(
                'Christians should walk, even as he walked.” 1 John 2:6. Jesus is our “example.” 1 Peter 2:21. ')
        )

    def test_post_process_difficult_references_014(self):
        self.assertEqual(
            'Mark 7:34',
            post_process_difficult_references('Mark 7:34 5 6')
        )

    def test_post_process_difficult_references_013(self):
        self.assertEqual(
            'Matthew 11:28-30',
            post_process_difficult_references('Matthew 11:28-30 The 144')
        )

    def test_post_process_difficult_references_012(self):
        self.assertEqual(
            'Proverbs 23:31',
            post_process_difficult_references('Proverbs 23:31 5')
        )

    def test_post_process_difficult_references_011(self):
        self.assertEqual(
            '1 Peter 2:11',
            post_process_difficult_references('1 Peter 2:11 fleshy lusts')
        )

    def test_post_process_difficult_references_010(self):
        # Daniel 8:14 if there is a 2000
        self.assertEqual(
            'Daniel 8:14',
            post_process_difficult_references('Daniel 8:14 If there is a 2000')
        )

    def test_post_process_difficult_references_009(self):
        # Revelation 12:14 as 1260
        self.assertEqual(
            'Revelation 12:14',
            post_process_difficult_references('Revelation 12:14 as 1260')
        )

    def test_post_process_difficult_references_008(self):
        # Matthew 24:37-39 38
        self.assertEqual(
            'Matthew 24:37-39',
            post_process_difficult_references('Matthew 24:37-39 38')
        )

    def test_post_process_difficult_references_007(self):
        # Matthew 24:37-39 with verses 4
        self.assertEqual(
            'Matthew 24:37-39',
            post_process_difficult_references('Matthew 24:37-39 with verses 4')
        )

    def test_post_process_difficult_references_006(self):
        # Isaiah 12:1-6 Isaiah 12
        self.assertEqual(
            'Isaiah 12:1-6',
            post_process_difficult_references('Isaiah 12:1-6 Isaiah 12')
        )

    # Matthew 6:21 ISBN 0-7684-2960
    def test_post_process_difficult_references_005(self):
        self.assertEqual(
            'Matthew 6:21',
            post_process_difficult_references('Matthew 6:21 ISBN 0-7684-2960')
        )

    def test_post_process_difficult_references_004(self):
        self.assertEqual(
            'Numbers 14:34',
            post_process_difficult_references('Numbers 14:34 Thus the 490')
        )

    def test_post_process_difficult_references_003(self):
        self.assertEqual(
            'Revelation 14:5',
            post_process_difficult_references('Revelation 14:5 about the 144')
        )

    def test_post_process_difficult_references_002(self):
        self.assertEqual(
            'Luke 22:45',
            post_process_difficult_references('Luke 22:45 SECTION 1')
        )

    def test_post_process_difficult_references_001(self):
        self.assertEqual(
            'Luke 21:45',
            post_process_difficult_references('Luke 21:45 SECTION')
        )

    # Isaiah 40:1; 44:28-45:13)
    # Genesis 2:1-25; 37:5

    def test_split_reference_by_semi_colon_32(self):
        # 2024-04-15
        f = Path(script_directory) / '..' / 'database' / 'normalized_swe_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            [
                'Hebr 1:13\n'
            ],
            split_b_ref_by_semicolon2('Dan 5; Hebr 1:13\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_31(self):
        # 2024-04-15
        f = Path(script_directory) / '..' / 'database' / 'normalized_swe_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            [
                'Hebr 1:3\n',
                'Hebr 1:13\n',
                'Hebr 8:1\n',
                'Hebr 10:12\n',
                'Hebr 10:13\n',
                'Hebr 12:1\n',
                'Acts 2:36\n'
            ],
            split_b_ref_by_semicolon2('Hebr 1:3, 13; 8:1; 10:12, 13; 12:1; Acts 2:36\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_30(self):
        # 2024-04-15
        f = Path(script_directory) / '..' / 'database' / 'normalized_swe_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            [
                'Hebr 1:3\n',
                'Hebr 1:13\n',
                'Hebr 8:1\n',
                'Hebr 10:12\n',
                'Hebr 10:13\n',
                'Hebr 12:1\n',
            ],
            split_b_ref_by_semicolon2('Hebr 1:3, 13; 8:1; 10:12, 13; 12:1\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_29(self):
        # 2021-02-27 '1 John 2:18, 22; 4:3
        f = Path(script_directory) / '..' / 'database' / 'normalized_swe_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Hebr 2:1\n',
             'Hebr 3:12-14\n',
             'Hebr 5:11-999\n'],
            split_b_ref_by_semicolon2('Hebr 2:1; 3:12-14; 5:11-999\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_28(self):
        # 2021-02-27 '1 John 2:18, 22; 4:3
        f = Path(script_directory) / '..' / 'database' / 'normalized_swe_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Hebr 2:1\n',
             'Hebr 3:12-14\n'],
            split_b_ref_by_semicolon2('Hebr 2:1; 3:12-14\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_27(self):
        # 2021-02-27 '1 John 2:18, 22; 4:3
        f = Path(script_directory) / '..' / 'database' / 'normalized_swe_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Hebr 2:1\n',
             'Hebr 3:12-14\n',
             'Hebr 5:11-999\n'],
            split_b_ref_by_semicolon2('Hebr 2:1; 3:12-14; 5:11-999\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_26(self):
        # 2021-02-27 '1 John 2:18, 22; 4:3
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Genesis 2:1-3\n'],
            split_b_ref_by_semicolon2('Genesis 1; 2:1-3\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_025(self):
        # 2021-02-27 '1 John 2:18, 22; 4:3
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Genesis 2:1-25\n',
             'Genesis 37:5\n'],
            split_b_ref_by_semicolon2('Genesis 2:1-25; 37:5\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_024(self):
        # 2021-02-27 '1 John 2:18, 22; 4:3
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['1 Jnx 2:18\n',
             '1 Jnx 2:22\n',
             '1 Jnx 4:3\n'],
            split_b_ref_by_semicolon2('1 Jnx 2:18, 22; 4:3\n', normalized_book_names_list)
        )

    # 2021-02-18 (Daniel 8:22, 23, 25; 11:2, 3, 4, 7, 14, 20, 21; 12:1).

    def test_split_reference_by_semi_colon_023(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Daniel 8:22\n',
             'Daniel 8:23\n',
             'Daniel 8:25\n',
             'Daniel 11:2\n',
             'Daniel 11:3\n',
             'Daniel 11:4\n',
             'Daniel 11:7\n',
             'Daniel 11:14\n',
             'Daniel 11:20\n',
             'Daniel 11:21\n',
             'Daniel 12:1\n'
             ],
            split_b_ref_by_semicolon2('Daniel 8:22, 23, 25; 11:2, 3, 4, 7, 14, 20, 21; 12:1\n',
                                      normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_022(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Psalms 33:20-21\n',
             'Psalms 44:5-8\n',
             'Psalms 54:1\n'],
            split_b_ref_by_semicolon2('Psalms 33:20-21; 44:5-8; 54:1\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_020(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Genesis 1:3-5\n',
             'Genesis 1:14\n',
             'Genesis 8:22\n'],
            split_b_ref_by_semicolon2('Genesis 1:3-5, 14; 8:22\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_019(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['2 Peter 2:5\n',
             '2 Peter 3:3-13\n'],
            split_b_ref_by_semicolon2('2 Peter 2:5;   3:3-13\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_018(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['1 Peter 2:5\n',
             '1 Peter 3:3-13\n'],
            split_b_ref_by_semicolon2('1 Peter 2:5;3:3-13\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_017(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Ezekiel 28:13, 15\n'],
            split_b_ref_by_semicolon2('Ezekiel 28:13, 15\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_016(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Genesis 9:29\n'],
            split_b_ref_by_semicolon2('Genesis 5; 9:29\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_015(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Revelation 18:20–24\n'],
            split_b_ref_by_semicolon2('Revelation 16; 18:20–24\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon014(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Revelation 17:1\n',
             'Revelation 18:5\n',
             'Revelation 19:1-19\n'],
            split_b_ref_by_semicolon2('Revelation 17:1;18:5;  19:1-19\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_013(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Ezekiel 28:13, 15\n'],
            split_b_ref_by_semicolon2('Ezekiel 28:13, 15\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_012(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['1 Samuel 17:11, 24, 25\n'],
            split_b_ref_by_semicolon2('1 Samuel 17:11, 24, 25\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_011(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Genesis 3:6  Jud. 1:s 14:2-3\n'],
            split_b_ref_by_semicolon2('Genesis 3:6  Jud. 1:s 14:2-3\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_010(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Romans 3:31\n',
             'Romans 6:1-2\n',
             'Romans 6:15\n'],
            split_b_ref_by_semicolon2('Romans 3:31; 6:1-2; 6:15\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_009(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Genesis 18:1-2\n',
             'Genesis 19:24\n',
             'Genesis 19:28\n'],
            split_b_ref_by_semicolon2('Genesis 18:1-2; 19:24, 28\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_008(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Genesis 7:11-12\n',
             'Genesis 8:1-5\n'],
            split_b_ref_by_semicolon2('Genesis 7:11-12; 8:1-5\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_007(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Revelation 11:1, 2, 3, 4,\n'],
            split_b_ref_by_semicolon2('Revelation 11:1, 2, 3, 4, \n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_006(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Revelation 17:1\n',
             'Revelation 18:5\n',
             'Revelation 19:1\n'],
            split_b_ref_by_semicolon2('Revelation 17:1; 18:5 19:1\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_005(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Revelation 17:1\n',
             'Revelation 18:5\n',
             'Revelation 19:1\n'],
            split_b_ref_by_semicolon2('Revelation 17:1; 18:5; 19:1\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_004(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Revelation 17:1\n',
             'Revelation 18:5\n',
             'Revelation 19:1\n'],
            split_b_ref_by_semicolon2('Revelation 17:1; 18:5; 19:1\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_003(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Revelation 17:1\n',
             'Revelation 18:5\n'],
            split_b_ref_by_semicolon2('Revelation 17:1; 18:5\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_002(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Revelation 17:1\n'],
            split_b_ref_by_semicolon2('Revelation 17:1\n', normalized_book_names_list)
        )

    def test_split_reference_by_semi_colon_001(self):
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_book_names_list = get_1_line_in_file_as_list(f)
        self.assertEqual(
            ['Ezekiel 17:31\n'],
            split_b_ref_by_semicolon2('Ezekiel 17; 31\n', normalized_book_names_list)
        )

    #########################################################

    def test_get_last_verse_number_in_bible_reference_002(self):
        kjv_number_of_verses_per_chapter_list = get_kjv_number_of_verses_per_chapter_list()
        self.assertEqual(
            '21',
            get_last_verse_number_in_bible_reference('Revelation 22', 'english_text')
        )

    def test_get_last_verse_number_in_bible_reference_001(self):
        kjv_number_of_verses_per_chapter_list = get_kjv_number_of_verses_per_chapter_list()
        self.assertEqual(
            '22',
            get_last_verse_number_in_bible_reference('Isaiah 36', 'english_text')
        )


    # def test_split_reference_by_chapter_004(self):
    # 	self.assertEqual(
    # 		['Isaiah 40:1\n',
    # 		 'Isaiah 44:28\n',
    # 		 'Isaiah 45:1-13\n'],
    # 		split_reference_crossing_chapter_border('Isaiah 40:1; 44:28-45:13', 'english_text')
    # 	)

    def test_split_reference_by_chapter_003(self):
        # Isaiah 36:21-37:20
        self.assertEqual(
            ['Isaiah 36:21-37\n'],
            split_reference_crossing_chapter_border('Isaiah 36:21-37', 'english_text')
        )

    def test_split_reference_by_chapter_002(self):
        # Isaiah 36:21-37:20
        self.assertEqual(
            ['Isaiah 36:21-22\n', 'Isaiah 37:1-20\n'],
            split_reference_crossing_chapter_border('Isaiah 36:21-37:20', 'english_text')
        )

    def test_split_reference_by_chapter_001(self):
        # Isaiah 36:21-37:20
        self.assertEqual(
            ['Isaiah 36:21\n'],
            split_reference_crossing_chapter_border('Isaiah 36:21', 'english_text')
        )

    def test_delete_reference_second_colon_004(self):
        # 'Acts 17:30, 31: txt'
        self.assertEqual(
            'Acts 17:30, 31\n',
            delete_reference_second_colon('Acts 17:30, 31: txt\n')
        )

    def test_delete_reference_second_colon_003(self):
        # Acts 17:30, 31: txt
        # Ephesians 2:4-6: loved us
        # John 32: cast out
        # Acts 17:31: repent
        self.assertEqual(
            'Ephesians 2:4-6\n',
            delete_reference_second_colon('Ephesians 2:4-6: loved us\n')
        )

    def test_delete_reference_second_colon_002(self):
        # Acts 17:30, 31: txt
        # Ephesians 2:4-6: loved us
        # John 32: cast out
        # Acts 17:31: repent
        self.assertEqual(
            'John 32: cast out\n',
            delete_reference_second_colon('John 32: cast out\n')
        )

    def test_delete_reference_second_colon_001(self):
        # Acts 17:30, 31: txt
        # Ephesians 2:4-6: loved us
        # John 32: cast out
        self.assertEqual(
            'Ephesians 2:4-6\n',
            delete_reference_second_colon('Ephesians 2:4-6: loved us\n')
        )

    # Isaiah 37:10, 12
    # Isaiah 36:18, 20
    # Isaiah 37:16-17, 20
    # Isaiah 38:14, 16

    def test_split_bible_reference_by_comma3_030(self):
        # 2019-10-17 'Ezra 5:1-5, Haggai 1' => 'Ezra 5:1-5'
        self.assertEqual(
            ['Isaiah 37:10\n', 'Isaiah 37:12\n'],
            split_b_ref_by_comma3(['Isaiah 37:10, 12\n'])
        )

    def test_split_bible_reference_by_comma3_029(self):
        # 2019-10-17 'Ezra 5:1-5, Haggai 1' => 'Ezra 5:1-5'
        self.assertEqual(
            ['Ezra 5:1-5\n', 'Ezra 6:16-26\n'],
            split_b_ref_by_comma3(['Ezra 5:1-5, Ezra 6:16-26\n'])
        )

    def test_split_bible_reference_by_comma3_028(self):
        # 2019-10-17 'Ezra 5:1-5, Haggai 1' => 'Ezra 5:1-5'
        self.assertEqual(
            ['Revelation 14:9-11\n', 'Revelation 14:14-18\n'],
            split_b_ref_by_comma3(['Revelation 14:9-11, 14-18\n'])
        )

    def test_split_bible_reference_by_comma3_027(self):
        # 2019-10-17 'Ezra 5:1-5, Haggai 1' => 'Ezra 5:1-5'
        self.assertEqual(
            ['Ezra 5:1-5\n', 'Haggai 1\n'],
            split_b_ref_by_comma3(['Ezra 5:1-5, Haggai 1\n'])
        )

    def test_split_bible_reference_by_comma3_026(self):
        # 2019-10-17 'Ezra 5:1-5, Haggai 1' => 'Ezra 5:1-5'
        self.assertEqual(
            ['Ezra 5:1-5\n'],
            split_b_ref_by_comma3(['Ezra 5:1-5\n'])
        )

    def test_split_bible_reference_by_comma3_025(self):  # 2019-11-10
        self.assertEqual(
            ['Ps 121:4-7 * Den 16 juli\n', 'Ps 121:1897\n'],
            split_b_ref_by_comma3(['Ps 121:4-7 * Den 16 juli, 1897\n'])
        )

    def test_split_bible_reference_by_comma3_024(self):
        # 2019-10-22 'Esra 6:15-18, och sedan'
        self.assertEqual(
            ['Esra 6:15-18\n', 'och sedan\n'],
            split_b_ref_by_comma3(['Esra 6:15-18, och sedan\n'])
        )

    def test_split_bible_reference_by_comma3_023(self):
        self.assertEqual(
            ['Genesis 3:7\n'],
            split_b_ref_by_comma3(['Genesis 3:7\n'])
        )

    def test_split_bible_reference_by_comma3_022(self):
        self.assertEqual(
            ['Genesis 1:11-13\n'],
            split_b_ref_by_comma3(['Genesis 1:11-13\n'])
        )

    def test_split_bible_reference_by_comma3_021(self):
        self.assertEqual(
            ['Genesis 3:7\n',
             'Genesis 3:21\n'],
            split_b_ref_by_comma3(['Genesis 3:7, 21\n'])
        )

    def test_split_bible_reference_by_comma3_020(self):
        self.assertEqual(
            ['1 Samuel 17:11\n',
             '1 Samuel 17:24\n',
             '1 Samuel 17:25\n'],
            split_b_ref_by_comma3(['1 Samuel 17:11, 24, 25\n'])
        )

    def test_split_bible_reference_by_comma3_019(self):
        self.assertEqual(
            ['Genesis 3:7-11\n',
             'Genesis 3:21\n'],
            split_b_ref_by_comma3(['Genesis 3:7-11, 21\n'])
        )

    def test_split_bible_reference_by_comma3_018(self):
        self.assertEqual(
            ['Genesis 3:7-11\n',
             'Genesis 3:21-35\n'],
            split_b_ref_by_comma3(['Genesis 3:7-11, 21-35\n'])
        )

    def test_split_bible_reference_by_comma3_017(self):
        self.assertEqual(
            ['Hes 18:1–4\n',
             'Hes 18:20\n'],
            split_b_ref_by_comma3([' Hes 18:1–4, 20\n'])
        )

    def test_split_bible_reference_by_comma3_016(self):
        self.assertEqual(
            ['Revelation 3:17\n',
             'Revelation 3:18\n',
             'DA\n',
             'p. 279\n'],
            split_b_ref_by_comma3(['Revelation 3:17, 18, DA, p. 279\n'])
        )

    def test_split_bible_reference_by_comma3_015(self):
        self.assertEqual(
            ['Hebr 8:13\n',
             'Hebr 9:15\n'],
            split_b_ref_by_comma3(['Hebr 8:13, 9:15\n'])  # There should be a SEMICOLON,
        )

    def test_split_bible_reference_by_comma3_014(self):
        self.assertEqual(
            ['Matt 24:9\n',
             'Matt 24:11\n',
             'Matt 24:24\n'],
            split_b_ref_by_comma3(['Matt 24:9,11,24\n'])
        )

    def test_split_bible_reference_by_comma3_013(self):
        self.assertEqual(
            ['Matthew 24:4-5\n',
             'Matthew 24:11\n',
             'Matthew 24:24\n'],
            split_b_ref_by_comma3(['Matthew 24:4-5, 11,24\n'])
        )

    def test_split_bible_reference_by_comma3_012(self):
        self.assertEqual(
            ['Matthew 24:4-5\n',
             'Matthew 24:11\n',
             'Matthew 24:24\n'],
            split_b_ref_by_comma3(['Matthew 24:4-5,11,24\n'])
        )

    def test_split_bible_reference_by_comma3_011(self):
        self.assertEqual(
            ['Matthew 24:4-5\n',
             'Matthew 24:7\n',
             'Matthew 24:24-28\n'],
            split_b_ref_by_comma3(['Matthew 24:4-5, 7, 24-28\n'])
        )

    def test_split_bible_reference_by_comma3_010(self):
        self.assertEqual(
            ['Hebr 8:13\n',
             'Hebr 9:15-17\n'],
            split_b_ref_by_comma3(['Hebr 8:13, 9:15-17\n'])
            # There should be a SEMICOLON,   Matthew 24:4-5, 11,24
        )

    def test_split_bible_reference_by_comma3_009(self):
        self.assertEqual(
            ['Matthew 24:4-5\n',
             'Matthew 24:11\n',
             'Matthew 24:24\n'],
            split_b_ref_by_comma3(['Matthew 24:4-5, 11,24\n'])  # Matthew 24:4-5, 11,24     Genesis 3:14, 17, 4:11
        )

    def test_split_bible_reference_by_comma3_008(self):
        self.assertEqual(
            ['Genesis 3:14\n',
             'Genesis 3:17\n',
             'Genesis 4:11\n'],
            split_b_ref_by_comma3(['Genesis 3:14, 17, 4:11\n'])  # 2019-10-14 Genesis 3:14, 17, 4:11
        )

    def test_split_bible_reference_by_comma3_007(self):
        # 2019-10-17 'Ezra 5:1-5, Haggai 1' => 'Ezra 5:1-5'
        self.assertEqual(
            ['Ezra 7:1-5\n'],
            split_b_ref_by_comma3(['Ezra 7:1-5\n'])
        )

    def test_split_bible_reference_by_comma3_006(self):
        # 2019-10-17 'Ezra 5:1-5, Haggai 1' => 'Ezra 5:1-5'
        self.assertEqual(
            ['Ezra 5:1-5\n', 'Ezra 51:11-15\n'],
            split_b_ref_by_comma3(['Ezra 5:1-5, Ezra 51:11-15\n'])
        )

    def test_split_bible_reference_by_comma3_005(self):
        # 2019-10-17 'Ezra 5:1-5, Haggai 1' => 'Ezra 5:1-5'
        self.assertEqual(
            ['Ezra 5:1\n', 'Ezra 5:11\n', 'Ezra 5:15\n'],
            split_b_ref_by_comma3(['Ezra 5:1, 11, 15\n'])
        )

    def test_split_bible_reference_by_comma3_004(self):
        # 2019-10-17 'Ezra 5:1-5, Haggai 1' => 'Ezra 5:1-5'
        self.assertEqual(
            ['Ezra 5:1-2\n', 'Ezra 5:11\n', 'Ezra 5:15\n'],
            split_b_ref_by_comma3(['Ezra 5:1-2, 11, 15\n'])
        )

    def test_split_bible_reference_by_comma3_003(self):
        # 2019-10-17 'Ezra 5:1-5, Haggai 1' => 'Ezra 5:1-5'
        self.assertEqual(
            ['Ezra 5:1-2\n', 'Ezra 5:11-22\n', 'Ezra 5:15\n'],
            split_b_ref_by_comma3(['Ezra 5:1-2, 11-22, 15\n'])
        )

    def test_split_bible_reference_by_comma3_002(self):
        # 2019-10-17 'Ezra 5:1-5, Haggai 1' => 'Ezra 5:1-5'
        self.assertEqual(
            ['Ezra 5:1-2\n', 'Ezra 5:11-22\n', 'Ex 2:15\n'],
            split_b_ref_by_comma3(['Ezra 5:1-2, 11-22, Ex 2:15\n'])
        )

    def test_split_bible_reference_by_comma3_001(self):
        # 2019-10-17 'Ezra 5:1-5, Haggai 1' => 'Ezra 5:1-5'
        self.assertEqual(
            ['Ezra 5:1-5\n', 'Haggai 1\n'],
            split_b_ref_by_comma3(['Ezra 5:1-5, Haggai 1\n'])
        )

    def test_change_eng_65_jude01(self):
        self.assertEqual(
            'Jude 1:24',
            normalize_bible_book_names('Jude 24', 'english_text')
        )

    def test_change_eng_65_jude02(self):
        self.assertEqual(
            'Jude 1:24\nJude 1:25',
            normalize_bible_book_names('Jude 24, 25', 'english_text')
        )

    def test_change_eng_65_jude03(self):
        self.assertEqual(
            'Jude 1:11',
            normalize_bible_book_names('Jude 1:11', 'english_text')
        )

    def test_remove_char_in_txt_file02(self):
        #  '1Corinthians 2:13, 14\n', '13) These things we also speak, not in words
        self.assertEqual(
            ' (Lev. 17.7, 2 Chron.',
            remove_char_in_txt_file(' (Lev. 17.7, 2 Chron.')
        )

    def test_remove_char_in_txt_file01(self):
        #  '1Corinthians 2:13, 14\n', '13) These things we also speak, not in words
        self.assertEqual(
            '1Corinthians 2:13, 14 These things we also speak, not in words ',
            remove_char_in_txt_file('1Corinthians 2:13, 14\n  These things we also speak, not in words ')
        )

    def test_modify_dot_to_comma_in_ref01(self):
        # (Jn 1:29.36) => (Jn 1:29, 36)   # 2019-12-16
        self.assertEqual(
            'Jn 1:29, 36',
            normalize_chapter_and_verse('Jn 1:29.36', input_from_speech=False)
        )

    def test_delete_multiple_dashes_in_ref01(self):
        # 'Daniel 4:24---27'  =>  'Daniel 4:24-27'
        self.assertEqual(
            'Daniel 4:24-27',
            normalize_chapter_and_verse('Daniel 4:24---27', input_from_speech=False)
        )

    def test_remove_list_prefix01(self):
        self.assertEqual(
            'Adam and Eve were given “over the fish of the sea, over the birds of the air, and over every living thing that moves on the earth. (Genesis 1:28) In order to continue enjoying their blissful life, Adam and Eve needed to choose to obey God. To',
            remove_eng_reference_prefix(
                '3. Adam and Eve were given “over the fish of the sea, over the birds of the air, and over every living thing that moves on the earth. (Genesis 1:28) 4. In order to continue enjoying their blissful life, Adam and Eve needed to choose to obey God. To')
        )

    def test_remove_list_prefix02(self):
        self.assertEqual(
            'förbundet',
            remove_eng_reference_prefix('3:e förbundet')
        )

    def test_remove_list_prefix03(self):
        self.assertEqual(
            '(1 Mos. 6–9) förbundet – Abraham (1 Mos. 12:1–3)',
            remove_eng_reference_prefix('(1 Mos. 6–9) 3:e förbundet – Abraham (1 Mos. 12:1–3)')
        )

    def test_prepare_text_for_replacement01(self):
        # 2300 days of Daniel 8:14.  Date Unknown:
        #  Daniel 8:14 and Revelation 14:6, 7.  The
        self.assertEqual(
            'Daniel 8:14 and Revelation 14:6, 7 ',
            prepare_eng_text_for_replacement('Daniel 8:14 and Revelation 14:6, 7. ')
        )

    def test_prepare_text_for_replacement02(self):
        # 2300 days of Daniel 8:14.  Date Unknown:
        #  Daniel 8:14 and Revelation 14:6, 7.  The
        self.assertEqual(
            '2300 days of Daniel 8:14  Date Unknown:',
            prepare_eng_text_for_replacement('2300 days of Daniel 8:14.  Date Unknown:')
        )

    def test_remove_all_characters_after_last_digit01(self):
        # 1 Timothy 6:17-19 COL
        self.assertEqual(
            '1 Timothy 6:17-19',
            delete_ref_after_last_digit('1 Timothy 6:17-19 COL')
        )

    def test_remove_all_characters_after_last_digit02(self):
        self.assertEqual(
            'Job 33:24',
            delete_ref_after_last_digit('Job 33:24.  Education')
        )

    def test_change_eng_book_name_2cor01(self):
        self.assertEqual(
            '2 Corinthians 11:24',
            normalize_bible_book_names('2Corinthians 11:24', 'english_text')
        )

    def test_remove_see_also_in_bible_references01(self):  # Revelation 15:5-8; see also 22:11
        self.assertEqual(
            'Revelation 15:5-8; 22:11',
            normalize_chapter_and_verse('Revelation 15:5-8; see also 22:11', input_from_speech=False)
        )

    def test_remove_see_also_in_bible_references02(self):  # John 6:40, see also 39, 44, 54
        self.assertEqual(
            'John 6:40, 39, 44, 54',
            normalize_chapter_and_verse('John 6:40, see also 39, 44, 54', input_from_speech=False)
        )

    def test_remove_to_in_bible_references01(self):  # 2019-10-08
        self.assertEqual(
            'Genesis 1:31-2:1',
            normalize_chapter_and_verse('Genesis 1:31 to 2:1', input_from_speech=False)  # Ezra 4:24 to 6:12
        )

    def test_remove_to_in_bible_references02(self):  # 2019-10-08
        self.assertEqual(
            'Genesis 1:32-45',
            normalize_chapter_and_verse('Genesis 1:32 to 45', input_from_speech=False)
        )

    def test_remove_and_in_bible_references01(self):
        self.assertEqual(
            'Genesis 1:31; 2:1',
            normalize_chapter_and_verse('Genesis 1:31 and 2:1', input_from_speech=False)
        )

    def test_remove_and_in_bible_references02(self):
        # 2019-10-08   'Matthew 23:37 and 38'
        # Daniel 3:10 and 15
        self.assertEqual(
            'Matthew 23:37, 38',
            normalize_chapter_and_verse('Matthew 23:37 and 38', input_from_speech=False)
        )

    def test_remove_and_in_bible_references03(self):  # 2019-10-12
        self.assertEqual(
            'Matthew 23:37-38; 21:1-38',
            normalize_chapter_and_verse('Matthew 23:37-38 and 21:1-38', input_from_speech=False)
        )

    def test_remove_and_in_bible_references04(self):  # 2019-10-12
        self.assertEqual(
            'Matthew 23:37-38; 21:1-38',
            normalize_chapter_and_verse('Matthew 23:37-38 and verses 21:1-38', input_from_speech=False)
        )

    def test_remove_and_in_bible_references05(self):  # 2019-10-12
        # Isaiah 17:13 and 8:7
        self.assertEqual(
            'Isaiah 17:13; 8:7',
            normalize_chapter_and_verse('Isaiah 17:13 and 8:7', input_from_speech=False)
        )

    def test_remove_and_in_bible_references06(self):
        # 2019-10-08   'Matthew 23:37 and 38'
        # Daniel 3:10 and 15
        self.assertEqual(
            'Daniel 3:10, 15',
            normalize_chapter_and_verse('Daniel 3:10 and 15', input_from_speech=False)
        )

    def delete_with_in_refxx(self):  # 2019-10-08
        self.assertEqual(
            'Genesis 2:31; 7:11',
            normalize_chapter_and_verse('Genesis 2:31 with 7:11', input_from_speech=False)
        )

    # def test_remove_waste_after_dot_in_bible_reference1(self):
    #     self.assertEqual(
    #         'Revelation 12:3',
    #         normalize_chapter_and_verse('Revelation 12:3 .            3', input_from_speech=False)
    #     )

    def test_modify_2verses_to_range01(self):  # 2019-10-04
        self.assertEqual(
            'Luk 4:18-19',
            modify_2_consecutive_verses_to_range2('Luk 4:18, 19')
        )

    def test_modify_2verses_to_range02(self):  # 2019-10-04
        self.assertEqual(
            'Luk 4:18, 20',
            modify_2_consecutive_verses_to_range2('Luk 4:18, 20')
        )

    def test_modify_2verses_to_range03(self):  # 2019-10-10
        self.assertEqual(
            'Hes 18:1–4, 20',
            modify_2_consecutive_verses_to_range2('Hes 18:1–4, 20')
        )

    def test_modify_2verses_to_range04(self):  # 2019-10-10
        self.assertEqual(
            'Matt 6:26, 28–30',
            modify_2_consecutive_verses_to_range2('Matt 6:26, 28–30')
        )

    def test_modify_2verses_to_range05(self):  # 2019-10-20   'Ezekiel 28:3, 4, 5')
        self.assertEqual(
            'Ezekiel 28:4-5, 8',
            modify_2_consecutive_verses_to_range2('Ezekiel 28:4, 5, 8')
        )

    def test_modify_2verses_to_range00(self):  # 2019-10-20   'Ezekiel 28:3, 4, 5')
        self.assertEqual(
            'Ezekiel 28:4',
            modify_2_consecutive_verses_to_range2('Ezekiel 28:4')
        )

    def test_modify_2verses_to_range06(self):  # 2019-10-25   'Ezekiel 28:3, 4, 5')
        self.assertEqual(
            'Ezekiel 28:9, 11-12',
            modify_2_consecutive_verses_to_range2('Ezekiel 28:9, 11, 12')
        )

    def test_modify_2verses_to_range07(self):  # 2019-10-25
        self.assertEqual(
            'Ezekiel 28:10-11-12',
            modify_2_consecutive_verses_to_range2('Ezekiel 28:10, 11, 12')
        )

    def test_modify_2verses_to_range08(self):  # 2019-10-25
        self.assertEqual(
            'Ezekiel 28:1, 11-12, 24',
            modify_2_consecutive_verses_to_range2('Ezekiel 28:1, 11, 12, 24')
        )

    def test_modify_2verses_to_range09(self):  # 2019-10-25
        self.assertEqual(
            'Ezekiel 28:1, 11-12, 24-25',
            modify_2_consecutive_verses_to_range2('Ezekiel 28:1, 11, 12, 24-25')
        )

    def test_modify_2verses_to_range10(self):  # 2019-10-25  '1 Thessalonians 3:12, 13'
        self.assertEqual(
            'Esra 6:15, 18, och sedan, ca 6',
            modify_2_consecutive_verses_to_range2('Esra 6:15, 18, och sedan, ca 6')
        )

    def test_modify_2verses_to_range11(self):  # 2019-10-25  '1 Thessalonians 3:12, 13; 4:14'
        self.assertEqual(
            '1 Thessalonians 3:12-13',
            modify_2_consecutive_verses_to_range2('1 Thessalonians 3:12, 13')
        )

    #
    # def test_modify_2verses_to_range00(self):    # 2019-10-25  '1 Thessalonians 3:12, 13; 4:14'
    #     self.assertEqual(
    #         '1 Thessalonians 3:12-13; 4.14',
    #         modify_2_consecutive_verses_to_range2('1 Thessalonians 3:12, 13; 4:14')
    #     )
    #

    def test_add_comma_in_bible_verse01(self):  # 2019-10-08 'Esra 6:15 18, och sedan, ca 6'
        self.assertEqual(
            'Esra 6:15, 18,',
            normalize_chapter_and_verse('Esra 6:15 18,', input_from_speech=False)
        )

    def test_add_space_in_bible_verse1(self):  # James 1:14,15 => James 1:14, 15
        self.assertEqual(
            'James 1:14, 15',
            normalize_chapter_and_verse('James 1:14,15', input_from_speech=False)
        )

    def test_add_space_in_bible_verse2(self):  # 1 Corinthians 15:42, 52,53 => 1 Corinthians 15:42, 52, 53
        self.assertEqual(
            '1 Corinthians 15:42, 52, 53',
            normalize_chapter_and_verse('1 Corinthians 15:42, 52,53', input_from_speech=False)
        )

    def test_add_space_in_bible_verse3(self):  # 1 Corinthians 15:42, 52, 53,54 => 1 Corinthians 15:42, 52, 53, 54
        self.assertEqual(
            '1 Corinthians 15:42, 52, 53, 54',
            normalize_chapter_and_verse('1 Corinthians 15:42, 52, 53,54', input_from_speech=False)
        )

    def test_add_space_in_bible_verse4(self):  # 1 Corinthians15:42 => 1 Corinthians 15:42
        self.assertEqual(
            '1 Corinthians 15:42',
            normalize_chapter_and_verse('1 Corinthians15:42', input_from_speech=False)
        )

    def test_add_space_in_bible_verse5(self):  # Joh.1:1-3     =>  Joh. 1:1-3      2019-11-10 08:12:50
        self.assertEqual(
            'Joh 1:1-3',
            normalize_chapter_and_verse('Joh.1:1-3', input_from_speech=False)
        )

    def test_remove_och_in_bible_references01(self):
        self.assertEqual(
            'Apg 2:42-47; 4:32-37',
            delete_och_in_ref('Apg 2:42-47 och 4:32-37', input_from_speech=False)
        )

    def test_remove_och_in_bible_references02(self):
        self.assertEqual(
            'Apg 2:42; 4:32-37',
            normalize_chapter_and_verse('Apg 2:42 och 4:32-37', input_from_speech=False)
        )

    def test_remove_och_in_bible_references03(self):
        self.assertEqual(
            'Apg 2:42; 4:32',
            normalize_chapter_and_verse('Apg 2:42 och 4:32', input_from_speech=False)  # 1 Mos 1:28 och 2:15
        )

    def test_remove_och_in_bible_references04(self):
        self.assertEqual(
            '1 Mos 1:28; 2:15',
            normalize_chapter_and_verse('1 Mos 1:28 och 2:15', input_from_speech=False)  # Jes 3:13-15 och 5:7
        )

    def test_remove_och_in_bible_references05(self):
        self.assertEqual(
            'Jes 3:13-15; 5:7',
            normalize_chapter_and_verse('Jes 3:13-15 och 5:7', input_from_speech=False)  # Jes 3:13-15 och 5:7
        )

    def test_remove_och_in_bible_references01(self):
        self.assertEqual(
            'Apg 2:42-47; 4:32-37',
            normalize_chapter_and_verse('Apg 2:42-47 och 4:32-37', input_from_speech=False)
        )

    def test_remove_och_in_bible_references01(self):
        self.assertEqual(
            'Apg 2:42-47; 4:32-37',
            normalize_chapter_and_verse('Apg 2:42-47 och 4:32-37', input_from_speech=False)
        )

    def test_remove_och_in_bible_references01(self):
        self.assertEqual(
            'Apg 2:42-47; 4:32-37',
            normalize_chapter_and_verse('Apg 2:42-47 och 4:32-37', input_from_speech=False)
        )

    def test_remove_space_in_bible_verse01(self):
        # 1 Joh. 1:5- 7 => 1 Joh. 1:5-7
        self.assertEqual(
            '1 Joh 1:5-7',
            normalize_chapter_and_verse('1 Joh. 1:5- 7', input_from_speech=False)
        )

    def test_remove_space_in_bible_verse02(self):
        # (Rom. 7:7, 9 - 12) => (Rom. 7:7, 9-12)
        self.assertEqual(
            'Rom 7:7, 9-12',
            normalize_chapter_and_verse('Rom. 7:7, 9 - 12', input_from_speech=False)
        )

    def test_remove_space_in_bible_verse03(self):
        # (Hebr. 12:1 - 2) => (Hebr. 12:1-2)
        self.assertEqual(
            'Hebr 12:1-2',
            normalize_chapter_and_verse('Hebr. 12:1 - 2', input_from_speech=False)
        )

    def test_remove_space_in_bible_verse04(self):
        # (Hebr. 12 :1) => (Hebr. 12:1)
        self.assertEqual(
            'Hebr 12:1',
            normalize_chapter_and_verse('Hebr. 12 :1', input_from_speech=False)
        )

    def test_remove_space_in_bible_verse05(self):
        # (Hebr. 12: 21) => (Hebr. 12:21)
        self.assertEqual(
            'Hebr 12:21',
            normalize_chapter_and_verse('Hebr. 12: 21', input_from_speech=False)
        )

    def test_remove_space_in_bible_verse06(self):
        # Genesis 2:2-  3
        # Daniel 11:44b-45 and 12:1  -45
        self.assertEqual(
            'Genesis 2:2-3',
            normalize_chapter_and_verse('Genesis 2:2-  3', input_from_speech=False)
        )

    def test_remove_space_in_bible_verse07(self):
        # Genesis 2:2-  3
        # Daniel 11:44b-45 and 12:1  -45
        # Jes. 43:3 -15
        self.assertEqual(
            'Jes 43:3-15',
            normalize_chapter_and_verse('Jes. 43:3 -15', input_from_speech=False)
        )

    def test_delete_ff_in_ref01(self):
        # 'Psalms 78:12ff'    =>  'Psalms 78:12ff'
        self.assertEqual(
            'Psalms 78:12',
            normalize_chapter_and_verse('Psalms 78:12ff', input_from_speech=False)
        )

    def test_delete_a_and_b_in_ref01(self):
        # Daniel 11:44b-45  => Daniel 11:44-45
        # Daniel 11:44a  => Daniel 11:44
        self.assertEqual(
            'Daniel 11:44',
            normalize_chapter_and_verse('Daniel 11:44a', input_from_speech=False)
        )

    def test_delete_a_and_b_in_ref02(self):
        # Daniel 11:44b-45  => Daniel 11:44-45
        # Daniel 11:44a  => Daniel 11:44
        self.assertEqual(
            'Daniel 11:44-45',
            normalize_chapter_and_verse('Daniel 11:44b-45', input_from_speech=False)
        )

    def test_remove_space_in_bible_verse08(self):
        # Genesis 2:2-  3
        # 'Daniel 11:44b-45 and 12:1  -45'    => '# Daniel 11:44b-45 and 12:1  -45'
        # Jes. 43:3 -15
        # Isaiah 37:31, 32   -36    => Isaiah 37:31, 32-36
        self.assertEqual(
            'Daniel 11:44-45, 12:1-45',
            normalize_chapter_and_verse('Daniel 11:44b-45 and 12:1  -45', input_from_speech=False)
        )

    def test_remove_space_in_bible_verse09(self):
        # Isaiah 37:31, 32   -36    => Isaiah 37:31, 32-36
        self.assertEqual(
            'Isaiah 37:31, 32-36',
            normalize_chapter_and_verse('Isaiah 37:31, 32   -36', input_from_speech=False)
        )

    def test_remove_dot_in_bible_verse01(self):
        # (Joh. 14:16-21.) => (Joh. 14:16-21)  # (Proverbs. 4: 23)
        self.assertEqual(
            'Joh 14:16-21',
            normalize_chapter_and_verse('Joh. 14:16-21.', input_from_speech=False)
        )

    def test_remove_dot_in_bible_verse00(self):
        self.assertEqual(
            'Proverbs 4:23',
            normalize_chapter_and_verse('Proverbs. 4: 23', input_from_speech=False)
        )

    def test_remove_dot_in_bible_verse02(self):  # (Neh. 4:3, 1, 4.) => (Neh. 4:3, 1, 4)
        self.assertEqual(
            'Text Neh 4:3, 1, 4 Text',
            normalize_chapter_and_verse('Text Neh. 4:3, 1, 4. Text', input_from_speech=False)
        )

    def test_remove_dot_in_bible_verse03(self):  # (Vers 143.) => (Vers 143)
        self.assertEqual(
            'Vers 143',
            normalize_chapter_and_verse('Vers 143.', input_from_speech=False)
        )

    def test_remove_dot_in_bible_verse04(self):  # (1 Kor. 16:1-2.) => (1 Kor. 16:1-2)
        self.assertEqual(
            'Text 1 Kor 14:16-21',
            normalize_chapter_and_verse('Text 1 Kor. 14:16-21.', input_from_speech=False)
        )

    #####   Swedish Bible book test cases start  ####

    def test_get_verse_parts_0_book_nanme_prefix_text_after(self):
        self.assertEqual(
            ('Amos', 126, 127, 'Text after')
            , get_bible_reference_parts_simple('Amos 126:127 Text after'))

    def test_get_verse_parts_1_book_prefix_text_after(self):
        self.assertEqual(
            ('1 Joh', 126, 127, 'Text after')
            , get_bible_reference_parts_simple('1 Joh 126:127 Text after'))

    def test_get_verse_parts_2_book_prefix_text_after(self):
        self.assertEqual(
            ('1 Joh', 126, 127, 'Text after')
            , get_bible_reference_parts_simple('1 Joh 126:127 Text after'))

    def test_get_verse_parts_3_book_prefix_text_after(self):
        self.assertEqual(
            ('Brevet till Filemon', 126, 127, 'Text after')
            , get_bible_reference_parts_simple('Brevet till Filemon 126:127 Text after'))

    def test_get_verse_parts_3_book_prefix_text_before_and_after(self):
        self.assertEqual(
            ('Brevet till Filemon', 126, 127, 'Text after'),
            get_bible_reference_parts_simple('Text before Brevet till Filemon 126:127 Text after'))

    def test_get_verse_parts_3_book_prefix_text_before_and_after(self):
        self.assertEqual(
            ('Brevet till Filemon', 126, 127, 'Text after'),
            get_bible_reference_parts_simple('Text before Brevet till Filemon 126:127 Text after'))

    # def test_get_verse_parts_verse_range1(self):
    #     self.assertEqual(
    #         ['Pretext', '1 Jn 3:1', 'Text'],
    #         get_bible_reference_parts('Pretext 1 Jn 3:1 Text')
    #     )
    #
    # def test_get_verse_parts_verse_range2(self):
    #     self.assertEqual(
    #         ['Pretext', '1 Jn 3:1-2', 'Text'],
    #         get_bible_reference_parts('Pretext 1 Jn 3:1-2 Text')
    #     )
    #
    # def test_get_verse_parts_verse_range3(self):
    #     self.assertEqual(
    #         ['Pretext', '1 Jn 3:1, 2, 3, 4, 5', 'Text'],
    #         get_bible_reference_parts('Pretext 1 Jn 3:1, 2, 3, 4, 5 Text')
    #     )

    # def test_get_verse_parts_verse_range4(self):
    #     self.assertEqual(
    #         ['Pretext', '1 Jn 3:1-2, 4-5', 'Text'],
    #         get_bible_reference_parts('Pretext 1 Jn 3:1-2, 4-5 Text')
    #     )

    # def test_get_verse_parts_verse_range5(self):
    #     self.assertEqual(
    #         ['Pretext', '2 Jn 3:1-2, 4-5', 'Text'],
    #         get_bible_reference_parts('Pretext 2 Jn 3:1-2, 4-5 Text')
    #     )

    def test_change_colon_to_semicolon1(self):  # Hab. 2:14: Joh. 17:3   => Hab. 2:14; Joh. 17:3
        self.assertEqual(
            'Hab 2:14; Joh 17:3',
            normalize_chapter_and_verse('Hab. 2:14: Joh. 17:3', input_from_speech=False)
        )

    # def test_change_colon_to_semicolon2(self):  # Hab. 2:14: 7:3 => Hab. 2:14; 7:3
    #     self.assertEqual(
    #         'Hab 2:14; 7:3',
    #         normalize_chapter_and_verse('Hab. 2:14: 7:3', input_from_speech=False)
    #     )

    def test_change_colon_to_semicolon3(self):  # Gal. 3:29: 1 Pet. 1:4  => Gal. 3:29; 1 Pet. 1:4
        self.assertEqual(
            'Gal 3:29:1 Pet 1:4',
            normalize_chapter_and_verse('Gal. 3:29: 1 Pet. 1:4', input_from_speech=False)
        )

    def test_get_verse_parts_0_book_nanme_prefix_text_after(self):
        self.assertEqual(
            ('Amos', 126, 127, 'Text after')
            , get_bible_reference_parts_simple('Amos 126:127 Text after'))

    def test_get_verse_parts_1_book_prefix_text_after(self):
        self.assertEqual(
            ('1 Joh', 126, 127, 'Text after')
            , get_bible_reference_parts_simple('1 Joh 126:127 Text after'))

    def test_get_verse_parts_2_book_prefix_text_after(self):
        self.assertEqual(
            ('1 Joh', 126, 127, 'Text after')
            , get_bible_reference_parts_simple('1 Joh 126:127 Text after'))

    def test_change_eng_book_name_matthew1(self):
        self.assertEqual(
            'Matthew 17:23',
            normalize_bible_book_names('Matt. 17:23', 'english_text')
        )


######## END ENGLISH TEST CASES###############################


if __name__ == '__main__':
    # get_bible_book_number_from_swedish_names('Johannes')
    unittest.main()
