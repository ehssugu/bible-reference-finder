
import re
import sys
import os
from biblereferencefindershared import NEWLINE, LINESEPARATOR, VBAR, SEMICOLON, COLON, get_file_path_and_name, get_2_tuple_as_list, write_list_to_file, get_file_base_name_end_with_date_stamp2


start_string = '|                  Bible Reference Normalizer Results'
end_string = '|                                  End'
ONESPACE = ' '
TWOSPACE = '  '
THREESPACE = '   '


def get_database_lists(f_replace_swe_esword_book_names_list, f_replace_swe_dot_book_names_list):
    # Get e-Sword replace list
    replace_swe_esword_book_names_list = get_2_tuple_as_list(f_replace_swe_esword_book_names_list, False)
    replace_swe_dot_book_names_list = get_2_tuple_as_list(f_replace_swe_dot_book_names_list, False)

    return replace_swe_esword_book_names_list, replace_swe_dot_book_names_list  # get_database_lists


def get_file_content_as_list(i_file_path_and_name, i_infile_encoding):
    # 2020-03-10
    count = 0
    out_file_list = []
    # print('816 i_infile_encoding: ' + i_infile_encoding)
    # print('817 Reading file: ' + i_file_path_and_name + '  With encoding: ' + i_infile_encoding)

    with open(i_file_path_and_name, encoding=i_infile_encoding) as f:
        for line in f:
            count += 1
            if not (line.startswith('1234556789')):

                line = re.sub(r'“', r'xxx', line)
                line = re.sub(r'”', r'xxx', line)
                line = re.sub(r'“', r'"', line)
                line = re.sub(r'”', r'"', line)
                line = re.sub(r'', r'...', line)
                line = re.sub(r'', r'"', line)
                line = re.sub(r' ', r'" ', line)
                line = re.sub(r' ', r' "', line)
                line = re.sub(r'. ', r'" ', line)
                line = re.sub(r' ', r' "', line)
                line = re.sub(r' ', r'- ', line)
                line = re.sub(r'”', r'x"', line)
                line = re.sub(r' ', r'"', line)
                line = re.sub(r' ', r'"', line)
                line = re.sub(r'xx', r'xx', line)
                # 2020-11-08 Added Swedish character conversion
                line = line.replace('Ã¥', 'å')
                line = line.replace('Ã¤', 'ä')
                line = line.replace('Ã¶', 'ö')
                line = line.replace('Ã...', 'Å')
                line = line.replace('Ã\x84', 'Ä')
                line = line.replace('Ã\x96', 'Ö')
                line = line.replace('â\x80\x9d', '"')
                line = line.replace('Â', '')
                line = line.replace('  ', ' ')

                out_file_list.append(line)
    f.close()
    # print('850 out_file_list: ' + out_file_list.__str__())

    return out_file_list  # get_file_content_as_list


def print_start_program(start_string):
    print(NEWLINE +
          LINESEPARATOR + NEWLINE +
          start_string + NEWLINE +
          LINESEPARATOR + NEWLINE + VBAR)
    return ()


def print_end_program(end_string):
    print(LINESEPARATOR + NEWLINE +
          end_string + NEWLINE +
          LINESEPARATOR)
    return ()


if __name__ == '__main__':

    f_input_default = Path(script_directory) / 'biblereferencenormalizer_input.txt'
    f_output_default = Path(script_directory) / 'biblereferencenormalizer_output.txt'
    f_replace_swe_esword_book_names_list = Path(script_directory) / 'database' / 'replace_swe_esword_book_names_list.txt'
    f_replace_swe_dot_book_names_list = Path(script_directory) / 'database' / 'replace_swe_dot_book_names_list.txt'
    infile_encoding = 'iso-8859-1'
    outfile_encoding = 'utf-8'
    i_execution_mode = 'swedish_text'

    print_start_program(start_string)

    argumentsList = sys.argv
    # print('| argumentsList: ' + argumentsList.__str__())
    # print('ArgLen: ' + argumentsList.__len__().__str__())

    if argumentsList.__len__() == 4:
        # py_file = argumentsList[0]
        f_in = argumentsList[1]
        f_in_encoding = argumentsList[2]
        folder_for_logfiles = argumentsList[3]
    else:
        f_in = f_input_default
        f_in_encoding = infile_encoding

    # Read infile as a list of strings
    f_in_content_list = get_file_content_as_list(get_file_path_and_name(f_in), f_in_encoding)

    # Get data from database files
    replace_swe_esword_book_names_list, replace_swe_dot_book_names_list = get_database_lists(f_replace_swe_esword_book_names_list, f_replace_swe_dot_book_names_list)

    # Normalize e-Sword Swedish bible references
    print(VBAR + 'Normalizing Swedish e-Sword Bible references')
    found = False
    f_in_content_list_new01 = []
    for line in f_in_content_list:
        txt_str = line
        for item in replace_swe_esword_book_names_list:
            if line.__contains__(item[0]):
                txt_str = txt_str.replace(item[0], item[1])
                f_in_content_list_new01.append(txt_str)
                found = True
        if not found:
            f_in_content_list_new01.append(line)
        found = False

    # Normalize dot Swedish bible references
    print(VBAR + 'Normalizing Swedish dot Bible references')
    found = False
    f_in_content_list_new02 = []
    for line in f_in_content_list_new01:
        txt_str = line
        for item in replace_swe_dot_book_names_list:
            if line.__contains__(item[0]):
                txt_str = txt_str.replace(item[0], item[1])
                f_in_content_list_new02.append(txt_str)
                found = True
        if not found:
            f_in_content_list_new02.append(line)
        found = False

    # Write output in default output file
    print(VBAR + 'Creating default output file')
    write_list_to_file(f_output_default, f_in_content_list_new02, outfile_encoding)

    # Write result also in the folder where input file exist
    print(VBAR + 'Creating last output file')
    f_input_folder = get_file_base_name_end_with_date_stamp2(os.path.basename(f_in), i_execution_mode)
    write_list_to_file(os.path.dirname(f_in) + '\\' + f_input_folder, f_in_content_list_new02, outfile_encoding)

    print_end_program (end_string)
