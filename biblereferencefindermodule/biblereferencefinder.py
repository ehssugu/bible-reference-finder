#!/usr/bin/env python
import os
import re
import sys
import time
from pathlib import PureWindowsPath, Path

from biblereferencefindershared import NEWLINE, LINESEPARATOR, SEMICOLON, COLON, VBAR, ONESPACE
from biblereferencefindershared import get_file_path_and_name, get_file_header_text, \
    prepare_input_text_file_from_pdf_input, get_file_content_as_str
from biblereferencefindershared import normalize_chapter_and_verse, write_list_to_file_with_line_breaks, \
    normalize_bible_book_names, ENCODING, MAX_BIBLE_REFERENCE_LENGTH, get_index_of_last_digit_in_string, get_b_ref_part, \
    readjust_to_normalized_names2, readjust_to_normalized_names, delete_ref_after_last_digit, delete_ref_after_dot, \
    modify_2_consecutive_verses_to_range2, delete_ref_after_comma, delete_ref_after_star, hasNumbers, \
    split_b_ref_by_comma3, get_file_backup_path_and_name, get_file_base_name_end_with_date_stamp2, \
    sub_folder_for_logfiles, write_list_to_file, prepare_input_text_file_from_docx_input, delete_reference_second_colon, \
    prepare_input_text_file_from_pptx_input, prepare_input_text_file_from_speech_input, \
    split_reference_crossing_chapter_border, split_b_ref_by_semicolon2, post_process_difficult_references, \
    print_start_program, print_end_program, prepare_input_text_file_from_txt_input, delete_ref_after_last_digit_in_ref

__author__ = 'Sune Gustafsson'
__copyright__ = 'Copyright 2019, Sune Gustafsson'
__credits__ = ['Sune Gustafsson']
__license__ = 'GPL'
__version__ = '1.0.1'
__date__ = '2019-10-02 08:08:48'
__maintainer__ = 'Sune Gustafsson'
__email__ = 'ehssugu@gmail.com'
__status__ = 'Test'

# TODO
# TODO 2021-02-13 Make new implementation of split reference by SEMICOLON
# TODO 2021-02-13 Make implementation for crossing chapter boundaries for SWEDISH bible
# TODO 2021-01-26 the gospel still is being preached (Rev. 10:8–11:14) and intercession is => Rev 10:8-x, Rev 11:1-14
#
#

MAXVERSES = 176  # The longest chapter in Bible has this amount of verses

script_directory = os.path.dirname(os.path.realpath(__file__))
TWOSPACE = '  '
THREESPACE = '   '
start_string = 'Bible Reference Finder - Start'
end_string = 'Bible Reference Finder - End'
f_norm_chapter_and_verses = Path(
    script_directory) / '..' / 'output' / 'biblereferencefinder_output01_NormChapAndVer.txt'  # Contains the original text, but the CHAPTERS and VERSES have been NORMALIZED
f_norm_book_chapter_verse = Path(
    script_directory) / '..' / 'output' / 'biblereferencefinder_output02_NormBookNames.txt'  # Contains the original text, but the BIBLE BOOK NAMES have been NORMALIZED


def get_verse_no_from_b_ref(b_ref: str):
    out_verse = '0'
    # It is assumed that the Bible reference i well formatted
    # Matthew 11:35-36
    # 1 Samuel 6:18

    if b_ref.__contains__(':'):
        ix_colon = b_ref.index(COLON)

        out_verse = b_ref[ix_colon + 1:]

    return out_verse


def get_chapter_no_from_b_ref(b_ref: str, current_bible_book_name: str):
    # It is assumed that the Bible reference i well formatted
    # Matthew 11:35-36
    # 1 Samuel 6:18
    out_chapter = '0'

    if b_ref.__contains__(':'):
        ix_colon = b_ref.index(COLON)
        ref_up_to_colon = b_ref[0:ix_colon]

        out_chapter = ref_up_to_colon[current_bible_book_name.__len__() + 1:]

    return out_chapter


def get_bible_book_name_from_b_ref(i_string: str, normalized_eng_book_names_list: []):
    out_bible_index_list = []
    bible_book_name = ''

    if i_string.startswith('1 John'):
        return '1 John'
    elif i_string.startswith('2 John'):
        return '2 John'
    elif i_string.startswith('3 John'):
        return '3 John'
    else:
        for book_item in normalized_eng_book_names_list:
            for match in re.finditer(book_item, i_string):
                start_index = match.start()
                end_ix = match.end()
                bible_book_name = book_item[start_index:end_ix]

    return bible_book_name  # get_bible_book_name_from_b_ref


def replace_fake_bible_book_names_with_prevoius(bible_book_references_split_list, normalized_eng_book_names_list):
    # The purpose of this procedure is to replace a reference "BibleBook x:3" with "Daniel 7:3", assuming that the previous reference was for example "Daniel 7:1"
    # This means that we need to loop through the list of references and save the previous Bible book name and chapter.
    # When the "BibleBook x:3" shows up, the previous BibleBook name shall replace "BibleBook" and "x" shall be replaced by previous chapter
    # The overall problem is to solve references like this "(verse 10-12)" where the complete reference has been givin earlier in the text

    real_bible_book_names_list = []
    current_bible_book_name = 'None'
    prevoius_chapter = '0'

    for b_ref in bible_book_references_split_list:

        if not b_ref.startswith('|===='):

            previous_bible_book_name = current_bible_book_name

            # Revelation 7:1
            # 1 Chronicles 7:1-11

            current_bible_book_name = get_bible_book_name_from_b_ref(b_ref, normalized_eng_book_names_list)

            if current_bible_book_name == 'BibleBook':
                current_bible_book_name = previous_bible_book_name

                #2024-01-27 Check if we have a real chapter or just "x" for chapter in b_ref
                ix_colon = b_ref.index(":")
                chapter = b_ref[len('BibleBook')+1:ix_colon]

                if chapter.isnumeric():
                    current_chapter = chapter
                else:
                    current_chapter = prevoius_chapter

                current_verse = get_verse_no_from_b_ref(b_ref)
                real_bible_book_names_list.append(
                    current_bible_book_name + ONESPACE + current_chapter + COLON + current_verse)
                pass
            else:
                prevoius_chapter = get_chapter_no_from_b_ref(b_ref, current_bible_book_name)
                real_bible_book_names_list.append(b_ref)
        else:
            real_bible_book_names_list.append(b_ref)

    return real_bible_book_names_list


def get_file_rows_as_list(f_name, f_encoding):
    # 2020-10-03
    # This function assumes that the file data is organized in the following way
    # # <Text> Data that should be ignored
    # <Text-A><\n>
    # <Text-B><\n>
    #
    # Output is returned in a list with following format
    # [
    # [Text-A][Text-B]...[Text-N]
    # ]
    out_list = []

    with open(f_name, 'r', encoding=ENCODING) as myfile:
        file_lines = myfile.readlines()

        for line in file_lines:
            if not line.startswith('#'):
                out_list.append(line.rstrip())

    myfile.close()

    return out_list  # get_file_rows_as_list


def merge_ixs_of_b_verses_and_verses(verse_index_list=[], reference_list=[]):
    # verse_index_list = [[s,e], [s,e], ... ]

    merged_index_list = []
    merged_index_list = reference_list
    i = 0
    for item in verse_index_list:
        merged_index_list.append(item[0])

    merged_index_list.sort()

    return merged_index_list


def get_ixs_of_b_verses(i_string):
    out_verse_index_list = []

    i = 1
    while i <= MAXVERSES:
        search_string = 'verse ' + i.__str__()
        for match in re.finditer(search_string, i_string):
            start_ix = match.start()
            end_ix = match.end()
            add_item_list = [start_ix, end_ix]
            out_verse_index_list.append(add_item_list)
        i = i + 1

    out_verse_index_list.sort()

    return out_verse_index_list  # get_ixs_of_b_verses


def get_b_ref_ixs(i_string: str, i_lang_book_standard_names_list: [], input_from_speech: bool):
    out_bible_index_list = []

    for book_item in i_lang_book_standard_names_list:
        # for match in re.finditer(eng_book_name, out_string):
        for match in re.finditer(book_item, i_string):
            start_index = match.start()
            # end_ix = match.end()
            # if book_item.startswith('Jud'):
            #     print('159: ' + book_item + ONESPACE + start_index.__str__())
            # Remove false positives e.g. 'EGW Book Acts of the Apostles'
            if not input_from_speech:
                if i_string[start_index:match.end() + 12].__contains__(COLON) or \
                        i_string[start_index:match.end() + 10].__contains__(SEMICOLON) or \
                        i_string.__contains__('Jude'):
                    out_bible_index_list.append(start_index)
            else:
                if hasNumbers(i_string[start_index:match.end() + 10]):
                    out_bible_index_list.append(start_index)

    out_bible_index_list.sort()

    return out_bible_index_list  # get_bible_ref_ixs


def get_verse_str(i_str: str):
    # verse 1 xxx
    # verse 1, x
    # verse 12 x
    # verse 12, x
    # verse 123 x
    # verse 123,
    # verse 1-12

    # print('137: ' + i_str)
    rest = i_str[i_str.index(ONESPACE) + 1:]
    if rest.__contains__(','):
        ix_comma = rest.index(',')
        out_str = 'verse ' + rest[0:ix_comma]
    else:
        if rest.__contains__(ONESPACE):
            ix_space = rest.index(ONESPACE)
            out_str = 'verse ' + rest[0:ix_space]
        else:
            out_str = 'verse ' + rest

    return out_str


def cleanup_single_ref(single_reference: str, normalized_book_hit_list: [], i_execution_mode: str):
    single_reference = get_b_ref_part(single_reference, i_execution_mode, normalized_book_hit_list)
    single_reference = delete_ref_after_last_digit(single_reference)
    single_reference = delete_ref_after_dot(single_reference)
    single_reference = modify_2_consecutive_verses_to_range2(single_reference)
    single_reference = delete_ref_after_comma(single_reference)
    single_reference = delete_ref_after_star(single_reference)
    # single_reference = delete_reference_second_colon(single_reference)

    single_reference = post_process_difficult_references(single_reference)
    single_reference = readjust_to_normalized_names(single_reference)
    single_reference = readjust_to_normalized_names2(single_reference)

    return single_reference  # cleanup_single_ref


def get_b_book_ref(f_norm_book_names, normalized_book_hit_list, input_from_speech: bool,
                   infile_encoding: str, execution_mode: str):
    b_book_ref_ix_list = []  # Each element contains the position for start of a Bible Book Chapter and Verse Reference
    t_string = str  # Contains the input file as long string, with '\' stripped off
    out_file_ref_list = []  # Contains the end result as a list of strings

    # print('163 Reading: ' + i_norm_book_chapter_verse_file_name + '  With encoding: ' + infile_encoding)
    with open(f_norm_book_names, encoding=infile_encoding) as f:
        t_string = f.read().replace(NEWLINE, '')
    f.close()
    b_book_ref_ix_list = get_b_ref_ixs(t_string, normalized_book_hit_list,
                                       input_from_speech)

    if input_from_speech:
        m_l = []
        verse_list = get_ixs_of_b_verses(t_string)
        # print('105 ' + verse_list.__str__())

        b_book_ref_ix_list = merge_ixs_of_b_verses_and_verses(verse_list,
                                                              b_book_ref_ix_list)
        # for item in m_l:
        # print(t_string[item: item + 10])
        # if t_string[item: item + 10].startswith('verse'):
        #     print('173: ' + get_verse_str(t_string[item: item + 10]))
        #     print('144: ' + m_l.__str__())

    number_of_indexes = b_book_ref_ix_list.__len__()
    # print(NEWLINE +'Found # of BIBLE References: ' + number_of_indexes.__str__() + NEWLINE)
    if number_of_indexes == 1:
        start_ix = b_book_ref_ix_list[0]
        end_ix = t_string.__len__()
        # print('352' +start_index.__str__(), end_index.__str__())
        if end_ix - start_ix >= MAX_BIBLE_REFERENCE_LENGTH:
            end_ix = start_ix + MAX_BIBLE_REFERENCE_LENGTH

        single_ref = t_string[start_ix:end_ix]

        if single_ref.__contains__(')'):  # 2019-11-10
            single_ref = single_ref[0:t_string.index(')')]

        single_ref = cleanup_single_ref(single_ref, normalized_book_hit_list, execution_mode)
        if single_ref.__contains__(':'):
            out_file_ref_list.append(single_ref + NEWLINE)

    elif number_of_indexes > 1:
        end_ix = b_book_ref_ix_list[number_of_indexes - 1]
        t_string_len = t_string.__len__()

        i = 1  # Start with second element i.e. index =1
        while i < number_of_indexes:
            start_ix = b_book_ref_ix_list[i - 1]
            end_ix = b_book_ref_ix_list[i]
            # print('352' +start_index.__str__(), end_index.__str__())
            if end_ix - start_ix >= MAX_BIBLE_REFERENCE_LENGTH:
                end_ix = start_ix + MAX_BIBLE_REFERENCE_LENGTH
            ix_last_digit = get_index_of_last_digit_in_string(t_string[start_ix:start_ix + end_ix])

            if start_ix + ix_last_digit < end_ix:
                end_ix = start_ix + ix_last_digit

            single_ref = t_string[start_ix:end_ix]
            # print('222 single reference len: ' + single_reference.__len__().__str__())

            if single_ref.__len__() > 0:
                if single_ref.__contains__(')'):  # 2019-11-10
                    single_ref = single_ref[0:single_ref.index(')')]

                single_ref = cleanup_single_ref(single_ref, normalized_book_hit_list, execution_mode)
                if single_ref.__len__() > 1:
                    if single_ref.__contains__(':'):
                        out_file_ref_list.append(single_ref + NEWLINE)
            i = i + 1

        # Get the LAST reference from last string segment
        start_ix = b_book_ref_ix_list[number_of_indexes - 1]
        single_ref = t_string[start_ix:t_string_len]

        # Strip to max length
        single_ref = single_ref[0:MAX_BIBLE_REFERENCE_LENGTH]

        if hasNumbers(single_ref):
            end_ix = get_index_of_last_digit_in_string(single_ref)
        else:
            end_ix = t_string_len

        single_ref = cleanup_single_ref(single_ref, normalized_book_hit_list, execution_mode)

        if single_ref.__contains__(':'):
            out_file_ref_list.append(single_ref + NEWLINE)

    return out_file_ref_list  # get_b_book_ref

def get_bible_book_references_split3(i_bible_book_ref_list, i_execution_mode, bible_book_hit_list): # 2024-07-16
    out_b_book_ref_list1 = []
    out_b_book_ref_list2 = []
    # kjv_number_of_verses_per_chapter_list = get_kjv_number_of_verses_per_chapter_list()
    # fb98_srb16_number_of_verses_per_chapter_list = get_FB98_SRB16_number_of_verses_per_chapter_list()

    for b_ref in i_bible_book_ref_list:
        # Split references crossing chapter border
        # 2021-02-13
        t_list = []
        t_list = split_reference_crossing_chapter_border(b_ref.__str__(), i_execution_mode)
        if t_list.__len__() > 1:
            i = 0
            while i < t_list.__len__():
                out_b_book_ref_list1.append(t_list[i])
                i = i + 1

        else:
            # Split by SEMICOLON
            t_list_split_by_semicolon = []
            t_list_split_by_semicolon = split_b_ref_by_semicolon2(b_ref.__str__(),
                                               bible_book_hit_list)  # New implementation 2021-02-15
            # if t_list.__len__() >= 1:  # 2021-05-29
            #     i = 0
            #     while i < t_list.__len__():
            #         out_b_book_ref_list1.append(t_list[i])
            #         i = i + 1
            # # print('103: ' +out_bible_book_references_list.__str__())
            # else:

            # Split by COMMA
            t_list_split_by_comma = split_b_ref_by_comma3(t_list_split_by_semicolon)  # 2021-02-15 New implementation
            if t_list_split_by_comma.__len__() > 0:
                i = 0
                while i < t_list_split_by_comma.__len__():
                    out_b_book_ref_list1.append(t_list_split_by_comma[i])
                    i = i + 1

    # Go through the list of references and cleanup any remaining endings
    for item in out_b_book_ref_list1:
        # 'Acts 17:30, 31: txt'
        # Ephesians 2:4-6: loved us
        # John 32: cast out
        ref = delete_reference_second_colon(item)
        ref = readjust_to_normalized_names(ref, i_execution_mode)
        ref = readjust_to_normalized_names2(ref, i_execution_mode)
        ref = delete_ref_after_last_digit_in_ref(ref)  # 2022-12-25
        # print('210: ' + item)
        out_b_book_ref_list2.append(ref)  # 1 Thessalonians 4:15-5:1 does not work

    return out_b_book_ref_list2  # get_bible_book_references_split_03


# def get_bible_book_references_split2(i_bible_book_ref_list, i_execution_mode, bible_book_hit_list):
#     out_b_book_ref_list1 = []
#     out_b_book_ref_list2 = []
#     kjv_number_of_verses_per_chapter_list = get_kjv_number_of_verses_per_chapter_list()
#
#     for b_ref in i_bible_book_ref_list:
#         # Split references crossing chapter border
#         # 2021-02-13
#         t_list = []
#         t_list = split_reference_crossing_chapter_border(b_ref.__str__(), i_execution_mode,
#                                                          kjv_number_of_verses_per_chapter_list)
#         if t_list.__len__() > 1:
#             i = 0
#             while i < t_list.__len__():
#                 out_b_book_ref_list1.append(t_list[i])
#                 i = i + 1
#
#         else:
#             # Split by SEMICOLON
#             t_list = split_b_ref_by_semicolon2(b_ref.__str__(),
#                                                bible_book_hit_list)  # New implementation 2021-02-15
#             if t_list.__len__() >= 1:  # 2021-05-29
#                 i = 0
#                 while i < t_list.__len__():
#                     out_b_book_ref_list1.append(t_list[i])
#                     i = i + 1
#             # print('103: ' +out_bible_book_references_list.__str__())
#             else:
#                 # Split by COMMA
#                 t_list = split_b_ref_by_comma3(b_ref.__str__())  # 2021-02-15 New implementation
#                 if t_list.__len__() > 0:
#                     i = 0
#                     while i < t_list.__len__():
#                         out_b_book_ref_list1.append(t_list[i])
#                         i = i + 1
#
#     # Go through the list of references and cleanup any remaining endings
#     for item in out_b_book_ref_list1:
#         # 'Acts 17:30, 31: txt'
#         # Ephesians 2:4-6: loved us
#         # John 32: cast out
#         ref = delete_reference_second_colon(item)
#         ref = readjust_to_normalized_names(ref, i_execution_mode)
#         ref = readjust_to_normalized_names2(ref, i_execution_mode)
#         ref = delete_ref_after_last_digit_in_ref(ref)  # 2022-12-25
#         # print('210: ' + item)
#         out_b_book_ref_list2.append(ref)  # 1 Thessalonians 4:15-5:1 does not work
#
#     return out_b_book_ref_list2  # get_bible_book_references_split


class MyFileClass:
    def __init__(self, filename, last_mod, path_and_filename):
        self.filename = filename
        self.last_mod = last_mod
        self.path_and_filename = path_and_filename


def build_file_list(in_directory, extension):
    file_name_mod_path_list = []
    for root, dirs, files in os.walk(in_directory):
        for file in files:

            if file.endswith(extension):
                # print(file)
                filename = os.path.basename(file)
                path_on_windows = PureWindowsPath(root)
                # print('path_on_windows: ', path_on_windows)
                path_and_filename = Path(path_on_windows, filename)
                # print('p&fn: ', path_and_filename)
                last_mod = time.localtime(os.path.getmtime(path_and_filename))
                # print('last_mod: ', last_mod)

                filename_found = False
                for item in file_name_mod_path_list:
                    if item.filename == filename:
                        filename_found = True
                        if item.last_mod < last_mod:
                            item.last_mod = last_mod
                            item.path_and_filename = path_and_filename
                if not filename_found:
                    file_name_mod_path_list.append(MyFileClass(filename, last_mod, path_and_filename))

    return file_name_mod_path_list  # build_file_list


def get_bible_references_multiple_files(input_execution_folder, folder_for_logfiles, input_file_language,
                                        infile_encoding,
                                        input_from_speech):
    f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
    normalized_eng_book_names_list = get_file_rows_as_list(f, 'utf-8')

    if input_from_speech:
        print('344 input_from_speech is True')
    else:
        print('346 input_from_speech is False')

    file_type_list = ['.docx', '.pdf', '.pptx']
    for file_type_item in file_type_list:
        # Create a list of files to process
        file_name_mod_path_list = build_file_list(input_execution_folder, file_type_item)
        # Process each file individually
        for file in file_name_mod_path_list:
            get_bible_references_single_file(file.path_and_filename, folder_for_logfiles, input_file_language,
                                             infile_encoding, input_from_speech)

    return ()  # get_bible_references_multiple_files


def get_bible_references_single_file(f_in: str, folder_for_logfiles: str, input_file_language: str,
                                     infile_encoding: str, input_from_speech: bool, f_out_03_no_split_std,
                                     f_out_04_split: str):
    # if input_from_speech:
    #     print('362 input_from_speech is True')
    # else:
    #     print('364 input_from_speech is False')

    bible_book_hit_list = []
    normalized_chapter_and_verse_str = []
    bible_book_references_list = []
    bible_book_references_split_list = []
    input_file_content_str = str
    input_file_content_str = str
    normalized_book_chapter_and_verse_str = str
    num_references_str = str
    cont = True

    f_in_txt = Path(script_directory) / '..' / 'output'/ 'biblereferencefinder_input.txt'  # Contains the original text with the wanted Bible references to extract
    print(VBAR + 'Input file: ' + f_in.__str__())
    # print(' Folder for logfiles: ' + folder_for_logfiles)
    print(VBAR + 'Input file language: ' + input_file_language)
    print(VBAR + 'Input file encoding: ' + infile_encoding)
    print(VBAR + 'Input from speech: ' + input_from_speech.__str__())
    # print(LINESEPARATOR)

    # outfile_encoding = infile_encoding 2022-02-01
    outfile_encoding = 'utf-8'
    if os.path.basename(f_in).endswith('.pdf'):
        prepare_input_text_file_from_pdf_input(f_in, f_in_txt, infile_encoding, input_file_language,
                                               outfile_encoding)

    elif os.path.basename(f_in).endswith('.docx'):
        prepare_input_text_file_from_docx_input(f_in, f_in_txt, infile_encoding,
                                                input_file_language, outfile_encoding)

    elif os.path.basename(f_in).endswith('.pptx'):
        # Trying to correct Swedish PPTX issues 2023-07-21
        # print('503 input_file_language: ' + input_file_language)
        if input_file_language == 'swedish_text':
            outfile_encoding = 'iso-8859-1'
        else:
            outfile_encoding = 'utf-8'

        prepare_input_text_file_from_pptx_input(f_in, f_in_txt, infile_encoding, outfile_encoding)

    elif os.path.basename(f_in).endswith('.txt') and input_from_speech:
        prepare_input_text_file_from_speech_input(f_in, f_in_txt, infile_encoding,
                                                  input_file_language, outfile_encoding)

    elif os.path.basename(f_in).endswith('.txt'):
        infile_encoding = 'utf-8'
        prepare_input_text_file_from_txt_input(f_in, f_in_txt, infile_encoding, outfile_encoding)

    else:
        print('No valid file type given as input. Program will exit.')
        cont = False
        return 'Error. No valid file type given as input. Program will exit.'

    if input_file_language == 'english_text':
        outfile_encoding = 'utf-8'
        f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
        normalized_eng_book_names_list = get_file_rows_as_list(f, 'utf-8')
        bible_book_hit_list = normalized_eng_book_names_list

    elif input_file_language == 'swedish_text':
        outfile_encoding = 'iso-8859-1'
        f = Path(script_directory) / '..' / 'database' / 'normalized_swe_book_names_list.txt'
        normalized_swe_book_names_list = get_file_rows_as_list(f, 'utf-8')
        bible_book_hit_list = normalized_swe_book_names_list

    #  Read INPUT FILE as text
    # infile_encoding = 'latin1' # test 2023-01-11
    infile_encoding = 'utf-8'  # test 2023-07-21
    input_file_content_str = get_file_content_as_str(get_file_path_and_name(f_in_txt), infile_encoding)


    # Normalize all CHAPTERS and VERSES in references and write to file
    normalized_chapter_and_verse_str = normalize_chapter_and_verse(input_file_content_str, input_from_speech)
    write_list_to_file_with_line_breaks(get_file_path_and_name(f_norm_chapter_and_verses),
                                        normalized_chapter_and_verse_str,
                                        outfile_encoding)

    # Normalize all BIBLE BOOK NAMES in references and write to file
    normalized_book_chapter_and_verse_str = normalize_bible_book_names(normalized_chapter_and_verse_str,
                                                                       input_file_language)

    write_list_to_file_with_line_breaks(get_file_path_and_name(f_norm_book_chapter_verse),
                                        normalized_book_chapter_and_verse_str,
                                        outfile_encoding)

    # Get all BIBLE BOOK REFERENCES from the NORMALIZED file and write to final output file
    bible_book_references_list = get_b_book_ref(f_norm_book_chapter_verse,
                                                bible_book_hit_list,
                                                input_from_speech,
                                                infile_encoding,
                                                input_file_language)

    # if input_from_speech:
    #     verse_list = get_indexes_of_bible_verses(normalized_book_chapter_and_verse_str)
    #     print('346 ' + verse_list.__str__())
    #     for item in verse_list:
    #         start_ix = item[0]
    #         end_ix = item[1]
    #         print('350: ' + normalized_book_chapter_and_verse_str[start_ix:end_ix])

    write_list_to_file_with_line_breaks(get_file_path_and_name(f_out_03_no_split_std),
                                        bible_book_references_list,
                                        outfile_encoding)

    # Split BIBLE REFERENCES by SEMICOLON and COMMA and write to file
    bible_book_references_split_list = get_bible_book_references_split3(bible_book_references_list,
                                                                        input_file_language, bible_book_hit_list)

    num_references_str = bible_book_references_split_list.__len__().__str__()
    print(VBAR + '# References: ' + num_references_str)
    print(VBAR)
    # print('335: ' + bible_book_references_split_list.__str__())

    bible_book_references_split_list = [get_file_header_text(f_in, num_references_str,
                                                             input_file_language)] + bible_book_references_split_list + [
                                           LINESEPARATOR]

    write_list_to_file_with_line_breaks(get_file_path_and_name(f_out_04_split),
                                        bible_book_references_split_list,
                                        outfile_encoding)

    # Go through the BibleReferences and replace fake BibleBook name with BibleBook name from previous bible reference
    bible_book_references_real_list = []
    bible_book_references_real_list = replace_fake_bible_book_names_with_prevoius(bible_book_references_split_list,
                                                                                  bible_book_hit_list)

    write_list_to_file_with_line_breaks(get_file_path_and_name(f_out_05_real),
                                        bible_book_references_real_list,
                                        outfile_encoding)

    # Log final result in logfile with file name starting with same name as input file
    # log_file_name = os.path.basename(input_file_name) + '.txt'
    # f = get_file_backup_path_and_name(os.path.basename(input_file_name) + '.txt', sub_folder_for_logfiles)
    write_list_to_file_with_line_breaks(
        get_file_backup_path_and_name(os.path.basename(f_in) + '.txt', sub_folder_for_logfiles),
        bible_book_references_real_list,
        outfile_encoding)

    # Write result also in the folder where the input file is located
    file_name_in_start_folder = get_file_base_name_end_with_date_stamp2(os.path.basename(f_in),
                                                                        input_file_language)

    write_list_to_file(Path(os.path.dirname(f_in)) / file_name_in_start_folder,
                       bible_book_references_real_list, outfile_encoding)

    return ()  # get_bible_references_single_file


if __name__ == '__main__':

    print_start_program(start_string)

    # Test settings only
    # i_Eng_Bookname = 'Matthew'
    # print('Bible book number: ', getBibleBookNumber(i_Eng_Bookname))
    # print('Bible book name:   ', getSwedishBibleBookName(getBibleBookNumber (i_Eng_Bookname)))

    t_verses = []

    f_input_text = Path(
        script_directory) / '..' / 'input' / 'biblereferencefinder_input.txt'  # Contains the original text with the wanted Bible references to extract
    f_out_03_no_split_std = Path(
        script_directory) / '..' / 'output' / 'biblereferencefinder_output03_No_split.txt'  # Contains the FINAL output from this program. One Bible Reference per line
    f_out_04_split = Path(script_directory) / '..' / 'output' / 'biblereferencefinder_output04_Split.txt'
    f_out_05_real = Path(script_directory) / '..' / 'output' / 'biblereferencefinder_output05_Real.txt'
    # config_file_name = 'CONFIG.txt'
    input_from_speech_str: str  # Used when preparing input file from speech text

    argumentsList = sys.argv
    # print('| argumentsList: ' + argumentsList.__str__())
    # print('ArgLen: ' + argumentsList.__len__().__str__())

    # Index			            Description			        Example
    # 0. BRFpython: 		    py-file				        .../biblereferencefinder.py
    # 1. %1:			        Input file			        .../Test.txt / VOID
    # 2. BRFlogFolder:		    Sub-folder for logfiles     Logfiles
    # 3. BRFinputLang:		    Language			        english_text / swedish_text
    # 4. BRFinfileEncoding:     Input file encoding		    iso-8859-1 / utf-8
    # 5. BRFinputFolder: 	    Folder for multiple files	.../TestFolder / VOID
    # 6. BRFinputFromSpeech:	Input from speech		    True / False

    # Set default values for input attributes

    folder_for_logfiles = 'Logfiles'
    # infile_encoding = 'utf-8'  # 2019-10-04 Works with PDF as input
    infile_encoding = 'iso-8859-1'  # 2019-10-04 Does not work with PDF as input
    # infile_encoding = 'latin-1'  # 2019-10-04 Does not work with PDF as input
    # input_file_name = input_text_file_name

    cont = False
    single_file = False
    multiple_file = False

    if argumentsList.__len__() == 6:
        # print(VBAR + '554: argumentsList: ' + argumentsList.__str__())
        # py_file = argumentsList[0]
        f_in = argumentsList[1]
        folder_for_logfiles = argumentsList[2]
        input_file_language = argumentsList[3]
        infile_encoding = argumentsList[4]
        input_execution_folder = argumentsList[5]
        # input_from_speech_str = argumentsList[6]
        cont = True
    elif argumentsList.__len__() == 7:
        # print('564: argumentsList: ' + argumentsList.__str__())
        # py_file = argumentsList[0]
        f_in = argumentsList[1]  # VOID for the multiple case
        folder_for_logfiles = argumentsList[2]
        input_file_language = argumentsList[3]
        infile_encoding = argumentsList[4]
        input_execution_folder = argumentsList[5]
        input_from_speech_str = argumentsList[6]
        if input_from_speech_str == 'True':
            input_from_speech = True
        else:
            input_from_speech = False

        # if input_from_speech:
        #     print('589 input_from_speech is True')
        # else:
        #     print('591 input_from_speech is False')

        if input_execution_folder != 'VOID':
            multiple_file = True
        else:
            multiple_file = False
        cont = True
    else:
        # TODO For test purpose only. Remember to reset after test is finished
        # TODO Remember to reset after test is finished
        # f_in = Path(script_directory) / '..' / 'input' / 'biblereferencefinder_input.txt'
        f_in = 'C:\\References\\Python\\bible-reference-finder\\Test Data\\test.txt'
        # f_in = 'C:\\References\\Python\\bible-reference-finder\\Test Data\\test.pdf'
        # f_in = 'C:\\Sune-X\\Presentations\\Bengt Dahlöf-Pres\\Vad säger Bibelns profetior om vår tid.pptx'
        input_file_language = 'swedish_text'
        # input_file_language = 'english_text'

        # input_from_speech = True
        input_from_speech = False
        # f_in = 'C:\\References\\ComputerTools\Python\\bible-reference-finder\\DanielCh1-Speech.txt'
        # f_in = 'X:\\(FG2G) From Glory To Glory\\From Glory to Glory - Orginal Files\\Stephen Wallace - From Glory to Glory.docx'
        # f_in = Path(script_directory) / 'biblereferencefinder_input.txt'
        # i_execution_mode = 'swedish_text'
        # f_in = 'C:\\References\\ComputerTools\\Python\\bible-reference-finder\\Test.pptx'
        # i_execution_mode = 'english_text'
        # input_from_speech = False
        # input_from_speech = True

        # input_execution_folder = 'X:\\(FG2G) From Glory To Glory\\From Glory to Glory - Word Text from Audio Del 01-37\\Test'
        # input_execution_folder = 'C:\\Slask\\Test'

        # multiple_file = True
        # single_file = True

        # if input_from_speech:
        #     print('607 input_from_speech is True')
        # else:
        #     print('609 input_from_speech is False')

        # infile_encoding = "iso-8859-1"
        infile_encoding = "utf-8"

    if not multiple_file:
        # print('605: calling get_bible_references_single_file: ' + f_in)
        # print('732 input_file_language: ' + input_file_language)
        get_bible_references_single_file(f_in, folder_for_logfiles, input_file_language, infile_encoding,
                                         input_from_speech, f_out_03_no_split_std, f_out_04_split)

    else:
        # print('609: calling get_bible_references_multiple_files: ' + f_in)
        get_bible_references_multiple_files(input_execution_folder, folder_for_logfiles, input_file_language,
                                            infile_encoding, input_from_speech)

    print_end_program(end_string)
