#!/usr/bin/env python
__author__ = 'Sune Gustafsson'
__copyright__ = 'Copyright 2019, Sune Gustafsson'
__credits__ = ['Sune Gustafsson']
__license__ = 'GPL'
__version__ = '1.0.1'
__date__ = '2019-09-13 08:47:13'
__maintainer__ = 'Sune Gustafsson'
__email__ = 'ehssugu@gmail.com'
__status__ = 'Test'

import re
import os
from pathlib import Path
from biblereferencefindereng import get_2_tuple_as_list

ONESPACE = ' '
TWOSPACE = '  '
THREESPACE = '   '
ENCODING = "UTF-8"

script_directory = os.path.dirname(os.path.realpath(__file__))

def get_1_line_in_file_as_list(f_name):
    # 2020-10-03
    # This function assumes that the file data is organized in the following way
    # # <Text> Data that should be ignored
    # <Text-A><\n>
    # <Text-B><\n>
    #
    # Output is returned in a list with following format
    # [
    # [Text-A][Text-B]...[Text-N]
    # ]
    out_list = []

    with open(f_name, 'r', encoding=ENCODING) as myfile:
        file_lines = myfile.readlines()

        for line in file_lines:
            if not line.startswith('#'):
                out_list.append(line.rstrip())

    myfile.close()

    return out_list # get_file_rows_as_list

def get_FB98_SRB16_number_of_verses_per_chapter_list():

    f = Path(script_directory) / '..' / 'database' / 'fb98_srb16_number_of_verses_per_chapter_list.txt'
    fb98_srb16_number_of_verses_per_chapter_list = get_1_line_in_file_as_list(f)

    return fb98_srb16_number_of_verses_per_chapter_list

# def remove_swe_list_prefix(i_str = str):
#     # 2019-10-09 ' 10. T'
#     # Hebr. 11:24, 25. 22 Medan Moses var
#     i_str = i_str.replace(TWOSPACE, ONESPACE)
#     i_str = re.sub(r' ([0-9]+)\. ([A-Ö]+)', r'\2', i_str)
#     i_str = re.sub(r' ([0-9]+)\. ([0-9]+)', r'\2', i_str)
#     i_str = re.sub(r'([0-9]+):a ([a-z]+)', r'\2', i_str)
#     i_str = re.sub(r'([0-9]+):e ([a-z]+)', r'\2', i_str)
#
#     return i_str    # remove_swe_list_prefix


# normalized_swe_book_names_list = [
# 2020-10-23 In database file
#     # If bible books not are in this list they will not be found
#     '1 Mos', '2 Mos', '3 Mos', '4 Mos', '5 Mos',
#     'Jos', 'Dom', 'Rut', '1 Sam', '2 Sam',
#     '1 Kung', '2 Kung', '1 Krön', '2 Krön', 'Esra',
#     'Neh', 'Est', 'Job', 'Ps', 'Ords',
#     'Pred', 'Höga V', 'Jes', 'Jer', 'Klag',
#     'Hes', 'Dan', 'Hos', 'Joel', 'Amos',
#     'Ob', 'Jona', 'Mika', 'Nah', 'Hab',
#     'Sef', 'Hagg', 'Sak', 'Mal',
#     'Matt', 'Mark', 'Luk', 'Joh', 'Apg',
#     'Rom', '1 Kor', '2 Kor', 'Gal', 'Ef',
#     'Fil', 'Kol', '1 Thess', '2 Thess', '1 Tim',
#     '2 Tim', 'Tit', 'Filem', 'Hebr', 'Jak',
#     '1 Pet', '2 Pet', '1 Jh', '2 Jh', '3 Jh',
#     'Jud', 'Upp', 'Unresolved', 'Book'
# ]

#
# t_replace_swe_lists = [
#     ['1Mos. ', '1 Mos '],
#     ['2Mos. ', '2 Mos '],
#     ['3Mos. ', '3 Mos '],
#     ['4Mos. ', '4 Mos '],
#     ['5Mos. ', '5 Mos '],
#     ['1 Mos. ', '1 Mos '],
#     ['2 Mos. ', '2 Mos '],
#     ['3 Mos. ', '3 Mos '],
#     ['4 Mos. ', '4 Mos '],
#     ['5 Mos. ', '5 Mos '],
#     ['1. Mos. ', '1 Mos '],  # 2018-05-24
#     ['2. Mos. ', '2 Mos '],  # 2018-05-24
#     ['3. Mos. ', '3 Mos '],  # 2018-05-24
#     ['4. Mos. ', '4 Mos '],  # 2018-05-24
#     ['5. Mos. ', '5 Mos '],  # 2018-05-24
#     ['Första Moseboken ', '1 Mos '],
#     ['Förste Moseboken ', '1 Mos '],  # 2018-04-12 13:07
#     ['Andra Moseboken ', '2 Mos '],
#     ['Tredje Moseboken ', '3 Mos '],
#     ['Fjärde Moseboken ', '4 Mos '],
#     ['Femte Moseboken ', '5 Mos '],
#     ['Första Mos. ', '1 Mos '],  # 2018-04-12 13:07
#     ['Andra Mos. ', '2 Mos '],
#     ['Tredje Mos. ', '3 Mos '],
#     ['Fjärde Mos. ', '4 Mos '],
#     ['Femte Mos. ', '5 Mos '],
#     ['1 Moseboken ', '1 Mos '],
#     ['2 Moseboken ', '2 Mos '],
#     ['3 Moseboken ', '3 Mos '],
#     ['4 Moseboken ', '4 Mos '],
#     ['5 Moseboken ', '5 Mos '],
#     ['1 Mosebok ', '1 Mos '],
#     ['2 Mosebok ', '2 Mos '],
#     ['3 Mosebok ', '3 Mos '],
#     ['4 Mosebok ', '4 Mos '],
#     ['5 Mosebok ', '5 Mos '],
#     # ['1 Mos ', '1 Mos '],
#     # ['2 Mos ', '2 Mos '],
#     # ['3 Mos ', '3 Mos '],
#     # ['4 Mos ', '4 Mos '],
#     # ['5 Mos ', '5 Mos '],
#     ['Jos. ', 'Jos '],
#     # ['Josua ', 'Jos '],           # 2018-05-24 Regex
#     ['Dom. ', 'Dom '],
#     ['Domarboken ', 'Dom '],
#     # ['Rut ', 'Rut '],
#     ['Ruts bok ', 'Rut '],
#     ['1 Sam. ', '1 Sam '],
#     ['2 Sam. ', '2 Sam '],
#     ['Första Samuelsboken ', '1 Sam '],
#     ['Förste Samuelsboken ', '1 Sam '],
#     ['Andra Samuelsboken ', '2 Sam '],
#     ['Första Sam. ', '1 Sam '],
#     ['Andra Sam. ', '2 Sam '],
#     ['1 Samuelsboken ', '1 Sam '],
#     ['2 Samuelsboken ', '2 Sam '],
#     ['1 Kung. ', '1 Kung '],
#     ['2 Kung. ', '2 Kung '],
#     ['Första Kungaboken ', '1 Kung '],
#     ['Förste Kungaboken ', '1 Kung '],
#     ['Andra Kungaboken ', '2 Kung '],
#     ['Första Kon. ', '1 Kung '],
#     ['Andra Kon. ', '2 Kung '],
#     ['1 Kungaboken ', '1 Kung '],
#     ['2 Kungaboken ', '2 Kung '],
#     ['Första Krönikeboken ', '1 Krön '],
#     ['Förste Krönikeboken ', '1 Krön '],
#     ['Andra Krönikeboken ', '2 Krön '],
#     ['Första Krön. ', '1 Krön '],
#     ['Andra Krön. ', '2 Krön '],
#     ['1 Krön. ', '1 Krön '],
#     ['2 Krön. ', '2 Krön '],
#     ['1 Krönikeboken ', '1 Krön '],
#     ['2 Krönikeboken ', '2 Krön '],
#     ['Esr. ', 'Esr '],
#     # ['Esra ', 'Esr '],            # 2018-04-18 Fix this with reg-ex
#     ['Neh. ', 'Neh '],
#     # ['Nehemja ', 'Neh '],         # 2018-04-18 Fix this with reg-ex
#     ['Est. ', 'Est '],
#     ['Esters bok ', 'Est '],
#     ['Est. ', 'Est '],
#     ['Jobs bok ', 'Job '],  # 2018-05-24
#     ['Job. ', 'Job '],
#     ['Ps. ', 'Ps '],
#     ['Psaltaren ', 'Ps '],
#     ['Psalm ', 'Ps '],
#     ['Ords. ', 'Ords '],
#     ['Ordspråksboken ', 'Ords '],
#     ['Ordspr. ', 'Ords.'],  # 2018-05-24
#     ['Pred. ', 'Pred '],
#     ['Predikaren ', 'Pred '],
#     ['Höga v ', 'Höga v '],
#     ['Höga v. ', 'Höga v '],
#     ['Höga visan ', 'Höga v '],
#     ['Höga Visan ', 'Höga v '],  # 2018-05-24
#     ['Jes. ', 'Jes '],
#     # ['Jesaja ', 'Jes '],          # 2018-04-18 Fix this with reg-ex
#     ['Jer. ', 'Jer '],
#     # ['Jeremia ', 'Jer '],         # 2018-04-18 Fix this with reg-ex
#     ['Klag. ', 'Klag '],
#     ['Klagovisorna ', 'Klag '],
#     ['Hes. ', 'Hes '],
#     # ['Hesekiel ', 'Hes '],        # 2018-04-18 Fix this with reg-ex
#     ['Dan. ', 'Dan '],
#     # ['Daniel ', 'Dan '],          # 2018-04-18 Fix this with reg-ex
#     # ['Hos ', 'Hos '],
#     # ['Hosea ', 'Hos '],           # 2018-04-18 Fix this with reg-ex
#     # ['Joel ', 'Joel '],
#     ['Am. ', 'Amos '],  # 2018-04-18 Fix this with reg-ex
#     # ['Amos ', 'Amos '],
#     ['Ob. ', 'Ob '],
#     # ['Obadja ', 'Ob '],           # 2018-04-18 Fix this with reg-ex
#     ['Jon ', 'Jon '],
#     # ['Jona ', 'Jon '],            # 2018-04-18 Fix this with reg-ex
#     ['Mik. ', 'Mik '],
#     # ['Mika ', 'Mik '],            # 2018-04-18 Fix this with reg-ex
#     ['Nah. ', 'Nah '],
#     # ['Nahum ', 'Nah '],           # 2018-04-18 Fix this with reg-ex
#     ['Hab. ', 'Hab '],
#     # ['Habbakuk ', 'Hab '],        # 2018-04-18 Fix this with reg-ex
#     ['Sef ', 'Sef '],
#     # ['Sefanja ', 'Sef '],         # 2018-04-18 Fix this with reg-ex
#     ['Hagg. ', 'Hagg '],
#     # ['Haggai ', 'Hagg '],         # 2018-04-18 Fix this with reg-ex
#     ['Sak. ', 'Sak '],
#     # ['Sakarja ', 'Sak '],         # 2018-04-18 Fix this with reg-ex
#     ['Mal. ', 'Mal '],
#     # ['Malaki ', 'Mal '],          # 2018-04-18 Fix this with reg-ex
#     ['Matt. ', 'Matt '],
#     ['Matteusevangeliet ', 'Matt '],
#     ['Mark. ', 'Mark '],
#     ['Markusevangeliet ', 'Mark '],
#     ['Luk. ', 'Luk '],
#     ['Lukasevangeliet ', 'Luk '],
#     ['Joh. ', 'Joh '],
#     ['Johannesevangeliet ', 'Joh '],
#     ['Apg ', 'Apg '],
#     ['Apg. ', 'Apg '],
#     ['Apostlagärningarna ', 'Apg '],
#     ['Rom. ', 'Rom '],
#     ['Romarbrevet ', 'Rom '],
#     ['1 Kor. ', '1 Kor '],
#     ['2 Kor. ', '2 Kor '],
#     ['1Kor. ', '1 Kor '],
#     ['2Kor. ', '2 Kor '],
#     ['Förste Korintierbrevet ', '1 Kor '],
#     ['Första Korintierbrevet ', '1 Kor '],
#     ['Andra Korintierbrevet ', '2 Kor '],
#     ['Första Kor.', '1 Kor '],
#     ['Andra Kor.', '2 Kor '],
#     ['1 Korintierbrevet ', '1 Kor '],
#     ['2 Korintierbrevet ', '2 Kor '],
#     ['Gal. ', 'Gal '],
#     ['Galaterbrevet ', 'Gal '],
#     ['Ef. ', 'Ef '],
#     ['Efesierbrevet ', 'Ef '],
#     ['Efésierbrevet ', 'Ef '],
#     ['Fil. ', 'Fil '],
#     ['Filipperbrevet ', 'Fil '],
#     ['Kol. ', 'Kol '],
#     ['Kolosserbrevet ', 'Kol '],
#
#     ['Första Tessalonikerbrevet ', '1 Thess '],
#     ['Förste Tessalonikerbrevet ', '1 Thess '],
#     ['Andra Tessalonikerbrevet ', '2 Thess '],
#     ['Första Tess. ', '1 Thess '],
#     ['Andra Tess. ', '2 Thess '],
#     ['1 Tessalonikerbrevet ', '1 Thess '],
#     ['2 Tessalonikerbrevet ', '2 Thess '],
#     ['1 Thess. ', '1 Thess '],
#     ['2 Thess. ', '2 Thess '],
#     ['1 Tim. ', '1 Tim '],
#     ['2 Tim. ', '2 Tim '],
#     ['1Tim. ', '1 Tim '],  # 2018-06-01
#     ['2Tim. ', '2 Tim '],  # 2018-06-01
#     ['1Timoteus ', '1 Tim '],  # 2018-06-01
#     ['2Timoteus ', '2 Tim '],  # 2018-06-01
#     ['1 Timoteusbrevet ', '1 Tim '],
#     ['2 Timoteusbrevet ', '2 Tim '],
#     ['1 Timoteus ', '1 Tim '],
#     ['2 Timoteus ', '2 Tim '],
#     ['Förste Timoteusbrevet ', '1 Tim '],
#     ['Första Timoteusbrevet ', '1 Tim '],
#     ['Andra Timoteusbrevet ', '2 Tim '],
#     ['Första Tim. ', '1 Tim '],
#     ['Andra Tim. ', '2 Tim '],
#     ['Tit. ', 'Tit '],
#     ['Titusbrevet ', 'Tit '],
#     ['Filem. ', 'Filem '],
#     ['Brevet till Filemon ', 'Filem '],
#     ['Heb. ', 'Hebr '],
#     ['Hebr. ', 'Hebr '],
#     ['Hebreerbrevet ', 'Hebr '],
#     ['Jak. ', 'Jak '],
#     ['Jakobsbrevet ', 'Jak '],
#     ['1 Pet. ', '1 Pet '],
#     ['2 Pet. ', '2 Pet '],
#     ['1. Pet. ', '1 Pet '],
#     ['2. Pet. ', '2 Pet '],  # 2018-05-07
#     ['1Petr ', '1 Pet '],
#     ['2Petr ', '2 Pet '],
#     ['1Pet ', '1 Pet '],
#     ['2Pet ', '2 Pet '],
#     ['1Pet. ', '1 Pet '],
#     ['2Pet. ', '2 Pet '],
#     ['1Petr. ', '1 Pet '],
#     ['2Petr. ', '2 Pet '],
#     ['1 Petr. ', '1 Pet '],
#     ['2 Petr. ', '2 Pet '],
#     ['1 Petri ', '1 Pet '],
#     ['2 Petri ', '2 Pet '],
#     ['1 Petri. ', '1 Pet '],
#     ['2 Petri. ', '2 Pet '],
#     ['1Petri ', '1 Pet '],  # 2018-06-01
#     ['2Petri ', '2 Pet '],  # 2018-06-01
#     ['1 Peter ', '1 Pet '],
#     ['2 Peter ', '2 Pet '],
#     ['Första Petrusbrevet ', '1 Pet '],
#     ['Förste Petrusbrevet ', '1 Pet '],
#     ['Andra Petrusbrevet ', '2 Pet '],
#     ['Första Petr. ', '1 Pet '],
#     ['Andra Petr. ', '2 Pet '],
#     ['1 Petrusbrevet ', '1 Pet '],
#     ['2 Petrusbrevet ', '2 Pet '],
#     ['1 Joh ', ' 1 Jh '],
#     ['2 Joh ', ' 2 Jh '],
#     ['3 Joh ', ' 3 Jh '],
#     ['1Joh. ', ' 1 Jh '],
#     ['2Joh. ', ' 2 Jh '],
#     ['3Joh. ', ' 3 Jh '],
#     ['1 Joh. ', ' 1 Jh '],
#     ['2 Joh. ', ' 2 Jh '],
#     ['3 Joh. ', ' 3 Jh '],
#     ['1 joh ', ' 1 Jh '],
#     ['2 joh ', ' 2 Jh '],
#     ['3 joh ', ' 3 Jh '],
#     ['Första Johannesbrevet ', ' 1 Jh '],
#     ['Förste Johannesbrevet ', ' 1 Jh '],
#     ['Andra Johannesbrevet ', ' 2 Jh '],
#     ['Tredje Johannesbrevet ', ' 3 Jh '],
#     ['Första Joh.', ' 1 Jh '],
#     ['Andra Joh.', ' 2 Jh '],
#     ['Tredje Joh.', ' 3 Jh '],
#     ['1 Johannesbrevet ', ' 1 Jh '],
#     ['2 Johannesbrevet ', ' 2 Jh '],
#     ['3 Johannesbrevet ', ' 3 Jh '],
#     ['Jud. ', 'Jud '],
#     ['Judasbrevet ', 'Jud '],
#     ['Upp. ', 'Upp '],
#     ['Uppenbarelseboken ', 'Upp ']
# ]

#
# def normalize_swe_01_genesis(i_str):
#     i_str = re.sub(r'1 Mos. ([0-9]+):([0-9]+)', r'1 Mos \1:\2', i_str)
#     i_str = re.sub(r'1 Mosebok ([0-9]+):([0-9]+)', r'1 Mos \1:\2', i_str)
#     i_str = re.sub(r'1 Moseboken ([0-9]+):([0-9]+)', r'1 Mos \1:\2', i_str)
#     i_str = re.sub(r'1. Mos. ([0-9]+):([0-9]+)', r'1 Mos \1:\2', i_str)
#     i_str = re.sub(r'1Mos. ([0-9]+):([0-9]+)', r'1 Mos \1:\2', i_str)
#     i_str = re.sub(r'Första Mos. ([0-9]+):([0-9]+)', r'1 Mos \1:\2', i_str)
#     i_str = re.sub(r'Första Moseboken ([0-9]+):([0-9]+)', r'1 Mos \1:\2', i_str)
#     i_str = re.sub(r'Förste Moseboken ([0-9]+):([0-9]+)', r'1 Mos \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_02_exodus(i_str):
#     i_str = re.sub(r'1 Mos. ([0-9]+):([0-9]+)', r'1 Mos \1:\2', i_str)
#     i_str = re.sub(r'2 Mos. ([0-9]+):([0-9]+)', r'2 Mos \1:\2', i_str)
#     i_str = re.sub(r'2 Mosebok ([0-9]+):([0-9]+)', r'2 Mos \1:\2', i_str)
#     i_str = re.sub(r'2 Moseboken ([0-9]+):([0-9]+)', r'2 Mos \1:\2', i_str)
#     i_str = re.sub(r'2. Mos. ([0-9]+):([0-9]+)', r'2 Mos \1:\2', i_str)  # 2018-05-24
#     i_str = re.sub(r'2Mos. ([0-9]+):([0-9]+)', r'2 Mos \1:\2', i_str)
#     i_str = re.sub(r'Andra Mos. ([0-9]+):([0-9]+)', r'2 Mos \1:\2', i_str)
#     i_str = re.sub(r'Andra Moseboken ([0-9]+):([0-9]+)', r'2 Mos \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_03_leviticus(i_str):
#     i_str = re.sub(r'1 Mos. ([0-9]+):([0-9]+)', r'1 Mos \1:\2', i_str)
#     i_str = re.sub(r'3 Mos. ([0-9]+):([0-9]+)', r'3 Mos \1:\2', i_str)
#     i_str = re.sub(r'3 Mosebok ([0-9]+):([0-9]+)', r'3 Mos \1:\2', i_str)
#     i_str = re.sub(r'3 Moseboken ([0-9]+):([0-9]+)', r'3 Mos \1:\2', i_str)
#     i_str = re.sub(r'3. Mos. ([0-9]+):([0-9]+)', r'3 Mos \1:\2', i_str)  # 2018-05-24
#     i_str = re.sub(r'3Mos. ([0-9]+):([0-9]+)', r'3 Mos \1:\2', i_str)
#     i_str = re.sub(r'Tredje Mos. ([0-9]+):([0-9]+)', r'3 Mos \1:\2', i_str)
#     i_str = re.sub(r'Tredje Moseboken ([0-9]+):([0-9]+)', r'3 Mos \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_04_numbers(i_str):
#     i_str = re.sub(r'1 Mos. ([0-9]+):([0-9]+)', r'1 Mos \1:\2', i_str)
#     i_str = re.sub(r'4 Mos. ([0-9]+):([0-9]+)', r'4 Mos \1:\2', i_str)
#     i_str = re.sub(r'4 Mosebok ([0-9]+):([0-9]+)', r'4 Mos \1:\2', i_str)
#     i_str = re.sub(r'4 Moseboken ([0-9]+):([0-9]+)', r'4 Mos \1:\2', i_str)
#     i_str = re.sub(r'4. Mos. ([0-9]+):([0-9]+)', r'4 Mos \1:\2', i_str)  # 2018-05-24
#     i_str = re.sub(r'4Mos. ([0-9]+):([0-9]+)', r'4 Mos \1:\2', i_str)
#     i_str = re.sub(r'Fjärde Mos. ([0-9]+):([0-9]+)', r'4 Mos \1:\2', i_str)
#     i_str = re.sub(r'Fjärde Moseboken ([0-9]+):([0-9]+)', r'4 Mos \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_05_deuteronomy(i_str):
#     i_str = re.sub(r'1 Mos. ([0-9]+):([0-9]+)', r'1 Mos \1:\2', i_str)
#     i_str = re.sub(r'5 Mos. ([0-9]+):([0-9]+)', r'5 Mos \1:\2', i_str)
#     i_str = re.sub(r'5 Mosebok ([0-9]+):([0-9]+)', r'5 Mos \1:\2', i_str)
#     i_str = re.sub(r'5 Moseboken ([0-9]+):([0-9]+)', r'5 Mos \1:\2', i_str)
#     i_str = re.sub(r'5. Mos. ([0-9]+):([0-9]+)', r'5 Mos \1:\2', i_str)
#     i_str = re.sub(r'5Mos. ([0-9]+):([0-9]+)', r'5 Mos \1:\2', i_str)
#     i_str = re.sub(r'Femte Mos. ([0-9]+):([0-9]+)', r'5 Mos \1:\2', i_str)
#     i_str = re.sub(r'Femte Moseboken ([0-9]+):([0-9]+)', r'5 Mos \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_06_joshua(i_str):
#     i_str = re.sub(r'Jos. ([0-9]+):([0-9]+)', r'Jos \1:\2', i_str)
#     i_str = re.sub(r'Josua ([0-9]+):([0-9]+)', r'Jos \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_07_judges(i_str):
#     i_str = re.sub(r'Dom. ([0-9]+):([0-9]+)', r'Dom \1:\2', i_str)
#     i_str = re.sub(r'Domarboken ([0-9]+):([0-9]+)', r'Dom \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_08_ruth(i_str):
#     i_str = re.sub(r'Rt\. ([0-9]+):([0-9]+)', r'Rut \1:\2', i_str)
#     i_str = re.sub(r'Rut ([0-9]+):([0-9]+)', r'Rut \1:\2', i_str)
#     i_str = re.sub(r'Ruts bok ([0-9]+):([0-9]+)', r'Rut \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_09_1samuel(i_str):
#     i_str = re.sub(r'1 Sam. ([0-9]+):([0-9]+)', r'1 Sam \1:\2', i_str)
#     i_str = re.sub(r'1 Samuelsboken ([0-9]+):([0-9]+)', r'1 Sam \1:\2', i_str)
#     i_str = re.sub(r'Första Sam. ([0-9]+):([0-9]+)', r'1 Sam \1:\2', i_str)
#     i_str = re.sub(r'Första Samuelsboken ([0-9]+):([0-9]+)', r'1 Sam \1:\2', i_str)
#     i_str = re.sub(r'Förste Samuelsboken ([0-9]+):([0-9]+)', r'1 Sam \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_10_2samuel(i_str):
#     i_str = re.sub(r'2 Sam. ([0-9]+):([0-9]+)', r'2 Sam \1:\2', i_str)
#     i_str = re.sub(r'2 Samuelsboken ([0-9]+):([0-9]+)', r'2 Sam \1:\2', i_str)
#     i_str = re.sub(r'Andra Sam. ([0-9]+):([0-9]+)', r'2 Sam \1:\2', i_str)
#     i_str = re.sub(r'Andra Samuelsboken ([0-9]+):([0-9]+)', r'2 Sam \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_11_1kings(i_str):
#     i_str = re.sub(r'I Kung\. ([0-9]+):([0-9]+)', r'1 Kung \1:\2', i_str)
#     i_str = re.sub(r'I Kung ([0-9]+):([0-9]+)', r'1 Kung \1:\2', i_str)
#     i_str = re.sub(r'1 Kung. ([0-9]+):([0-9]+)', r'1 Kung \1:\2', i_str)
#     i_str = re.sub(r'1 Kungaboken ([0-9]+):([0-9]+)', r'1 Kung \1:\2', i_str)
#     i_str = re.sub(r'Första Kon. ([0-9]+):([0-9]+)', r'1 Kung \1:\2', i_str)
#     i_str = re.sub(r'Första Kungaboken ([0-9]+):([0-9]+)', r'1 Kung \1:\2', i_str)
#     i_str = re.sub(r'Förste Kungaboken ([0-9]+):([0-9]+)', r'1 Kung \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_12_2kings(i_str):
#     i_str = re.sub(r'II Kung\. ([0-9]+):([0-9]+)', r'2 Kung \1:\2', i_str)
#     i_str = re.sub(r'II Kung ([0-9]+):([0-9]+)', r'2 Kung \1:\2', i_str)
#     i_str = re.sub(r'2 Kung. ([0-9]+):([0-9]+)', r'2 Kung \1:\2', i_str)
#     i_str = re.sub(r'2 Kungaboken ([0-9]+):([0-9]+)', r'2 Kung \1:\2', i_str)
#     i_str = re.sub(r'Andra Kon. ([0-9]+):([0-9]+)', r'2 Kung \1:\2', i_str)
#     i_str = re.sub(r'Andra Kungaboken ([0-9]+):([0-9]+)', r'2 Kung \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_13_1chronicles(i_str):
#     i_str = re.sub(r'I Krön\. ([0-9]+):([0-9]+)', r'1 Krön \1:\2', i_str)
#     i_str = re.sub(r'I Krön ([0-9]+):([0-9]+)', r'1 Krön \1:\2', i_str)
#     i_str = re.sub(r'1 Krön\. ([0-9]+):([0-9]+)', r'1 Krön \1:\2', i_str)
#     i_str = re.sub(r'1 Krönikeboken ([0-9]+):([0-9]+)', r'1 Krön \1:\2', i_str)
#     i_str = re.sub(r'Första Krön\. ([0-9]+):([0-9]+)', r'1 Krön \1:\2', i_str)
#     i_str = re.sub(r'Första Krönikeboken ([0-9]+):([0-9]+)', r'1 Krön \1:\2', i_str)
#     i_str = re.sub(r'Förste Krönikeboken ([0-9]+):([0-9]+)', r'1 Krön \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_14_2chronicles(i_str):
#     i_str = re.sub(r'II Krön\. ([0-9]+):([0-9]+)', r'2 Krön \1:\2', i_str)
#     i_str = re.sub(r'II Krön ([0-9]+):([0-9]+)', r'2 Krön \1:\2', i_str)
#     i_str = re.sub(r'2 Krön. ([0-9]+):([0-9]+)', r'2 Krön \1:\2', i_str)
#     i_str = re.sub(r'2 Krönikeboken ([0-9]+):([0-9]+)', r'2 Krön \1:\2', i_str)
#     i_str = re.sub(r'Andra Krön. ([0-9]+):([0-9]+)', r'2 Krön \1:\2', i_str)
#     i_str = re.sub(r'Andra Krönikeboken ([0-9]+):([0-9]+)', r'2 Krön \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_15_ezra(i_str):
#     i_str = re.sub(r'Es\. ([0-9]+):([0-9]+)', r'Esra \1:\2', i_str)
#     i_str = re.sub(r'Esra ([0-9]+):([0-9]+)', r'Esr \1:\2', i_str)
#     i_str = re.sub(r'Esr. ([0-9]+):([0-9]+)', r'Esr \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_16_nehemiah(i_str):
#     i_str = re.sub(r'Neh. ([0-9]+):([0-9]+)', r'Neh \1:\2', i_str)
#     i_str = re.sub(r'Nehemja ([0-9]+):([0-9]+)', r'Neh \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_17_esther(i_str):
#     i_str = re.sub(r'Est. ([0-9]+):([0-9]+)', r'Est \1:\2', i_str)
#     i_str = re.sub(r'Ester ([0-9]+):([0-9]+)', r'Est \1:\2', i_str)
#     i_str = re.sub(r'Esters bok ([0-9]+):([0-9]+)', r'Est \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_18_job(i_str):
#     i_str = re.sub(r'Jb\. ([0-9]+):([0-9]+)', r'Job \1:\2', i_str)
#     i_str = re.sub(r'Job ([0-9]+):([0-9]+)', r'Job \1:\2', i_str)
#     i_str = re.sub(r'Jobs bok ([0-9]+):([0-9]+)', r'Job \1:\2', i_str)  # 2018-05-24
#     return i_str
#
#
# def normalize_swe_19_psalms(i_str):
#     i_str = re.sub(r'Ps. ([0-9]+):([0-9]+)', r'Ps \1:\2', i_str)
#     i_str = re.sub(r'Psalm ([0-9]+):([0-9]+)', r'Ps \1:\2', i_str)
#     i_str = re.sub(r'Psaltaren ([0-9]+):([0-9]+)', r'Ps \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_20_proverbs(i_str):
#     i_str = re.sub(r'Ords. ([0-9]+):([0-9]+)', r'Ords \1:\2', i_str)
#     i_str = re.sub(r'Ordspråksboken ([0-9]+):([0-9]+)', r'Ords \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_21_ecclesiastes(i_str):
#     i_str = re.sub(r'Höga v ([0-9]+):([0-9]+)', r'Höga v \1:\2', i_str)
#     i_str = re.sub(r'Höga v. ([0-9]+):([0-9]+)', r'Höga v \1:\2', i_str)
#     i_str = re.sub(r'Höga visan ([0-9]+):([0-9]+)', r'Höga v \1:\2', i_str)
#     i_str = re.sub(r'Höga Visan ([0-9]+):([0-9]+)', r'Höga v \1:\2', i_str)  # 2018-05-24
#     return i_str
#
#
# def normalize_swe_22_songofsolomon(i_str):
#     i_str = re.sub(r'Pred. ([0-9]+):([0-9]+)', r'Pred \1:\2', i_str)
#     i_str = re.sub(r'Predikaren ([0-9]+):([0-9]+)', r'Pred \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_23_isaiah(i_str):
#     i_str = re.sub(r'Jes\. ([0-9]+):([0-9]+)', r'Jes \1:\2', i_str)
#     i_str = re.sub(r'Jesaja ([0-9]+):([0-9]+)', r'Jes \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_24_jeremiah(i_str):
#     i_str = re.sub(r'Jer. ([0-9]+):([0-9]+)', r'Jer \1:\2', i_str)
#     i_str = re.sub(r'Jeremia ([0-9]+):([0-9]+)', r'Jer \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_25_lamentations(i_str):
#     i_str = re.sub(r'Klag. ([0-9]+):([0-9]+)', r'Klag \1:\2', i_str)
#     i_str = re.sub(r'Klagovisorna ([0-9]+):([0-9]+)', r'Klag \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_26_ezekiel(i_str):
#     i_str = re.sub(r'Hes. ([0-9]+):([0-9]+)', r'Hes \1:\2', i_str)
#     i_str = re.sub(r'Hesekial ([0-9]+):([0-9]+)', r'Hes \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_27_daniel(i_str):
#     i_str = re.sub(r'Dan. ([0-9]+):([0-9]+)', r'Dan \1:\2', i_str)
#     i_str = re.sub(r'Daniel ([0-9]+):([0-9]+)', r'Dan \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_28_hosea(i_str):
#     i_str = re.sub(r'Hos\. ([0-9]+):([0-9]+)', r'Hos \1:\2', i_str)
#     i_str = re.sub(r'Hosea ([0-9]+):([0-9]+)', r'Hos \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_29_joel(i_str):
#     i_str = re.sub(r'Jo\. ([0-9]+):([0-9]+)', r'Joel \1:\2', i_str)
#     i_str = re.sub(r'Joel ([0-9]+):([0-9]+)', r'Joel \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_30_amos(i_str):
#     i_str = re.sub(r'Am. ([0-9]+):([0-9]+)', r'Amos \1:\2', i_str)
#     i_str = re.sub(r'Amos ([0-9]+):([0-9]+)', r'Amos \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_31_obadiah(i_str):
#     i_str = re.sub(r'Ob\. ([0-9]+):([0-9]+)', r'Ob \1:\2', i_str)
#     i_str = re.sub(r'Obadja ([0-9]+):([0-9]+)', r'Ob \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_32_jonah(i_str):
#     i_str = re.sub(r'Jon\. ([0-9]+):([0-9]+)', r'Jona \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_33_micah(i_str):
#     i_str = re.sub(r'Mika\. ([0-9]+):([0-9]+)', r'Mika \1:\2', i_str)
#     i_str = re.sub(r'Mik\. ([0-9]+):([0-9]+)', r'Mika \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_34_nahum(i_str):
#     i_str = re.sub(r'Nah\. ([0-9]+):([0-9]+)', r'Nah \1:\2', i_str)
#     i_str = re.sub(r'Nahum ([0-9]+):([0-9]+)', r'Nah \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_35_habakkuk(i_str):
#     i_str = re.sub(r'Hab\. ([0-9]+):([0-9]+)', r'Hab \1:\2', i_str)
#     i_str = re.sub(r'Habbackuk ([0-9]+):([0-9]+)', r'Hab \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_36_zephaniah(i_str):
#     i_str = re.sub(r'Sef\. ([0-9]+):([0-9]+)', r'Sef \1:\2', i_str)
#     i_str = re.sub(r'Sefanja ([0-9]+):([0-9]+)', r'Sef \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_37_haggai(i_str):
#     i_str = re.sub(r'Hagg\. ([0-9]+):([0-9]+)', r'Hagg \1:\2', i_str)
#     i_str = re.sub(r'Haggai ([0-9]+):([0-9]+)', r'Hagg \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_38_zechariah(i_str):
#     i_str = re.sub(r'Sak\. ([0-9]+):([0-9]+)', r'Sak \1:\2', i_str)
#     i_str = re.sub(r'Sakarja ([0-9]+):([0-9]+)', r'Sak \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_39_malachi(i_str):
#     i_str = re.sub(r'Mal\. ([0-9]+):([0-9]+)', r'Mal \1:\2', i_str)
#     i_str = re.sub(r'Malaki ([0-9]+):([0-9]+)', r'Mal \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_40_matthew(i_str):
#     i_str = re.sub(r'Matt\. ([0-9]+):([0-9]+)', r'Matt \1:\2', i_str)
#     i_str = re.sub(r'Matt ([0-9]+):([0-9]+)', r'Matt \1:\2', i_str)
#     i_str = re.sub(r'Matteus ([0-9]+):([0-9]+)', r'Matt \1:\2', i_str)
#     i_str = re.sub(r'Matteus Evangelium ([0-9]+):([0-9]+)', r'Matt \1:\2', i_str)
#     i_str = re.sub(r'Mat ([0-9]+):([0-9]+)', r'Matt \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_41_mark(i_str):
#     i_str = re.sub(r'Mark\. ([0-9]+):([0-9]+)', r'Mark \1:\2', i_str)
#     i_str = re.sub(r'Markus Evangelium ([0-9]+):([0-9]+)', r'Mark \1:\2', i_str)
#     i_str = re.sub(r'Markus ([0-9]+):([0-9]+)', r'Mark \1:\2', i_str)
#     i_str = re.sub(r'Mar ([0-9]+):([0-9]+)', r'Mark \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_42_luke(i_str):
#     i_str = re.sub(r'Luk\. ([0-9]+):([0-9]+)', r'Luk \1:\2', i_str)
#     i_str = re.sub(r'Lk ([0-9]+):([0-9]+)', r'Luk \1:\2', i_str)
#     i_str = re.sub(r'Lk\. ([0-9]+):([0-9]+)', r'Luk \1:\2', i_str)
#     i_str = re.sub(r'Lukas ([0-9]+):([0-9]+)', r'Luk \1:\2', i_str)
#     i_str = re.sub(r'Lukas Evangelium ([0-9]+):([0-9]+)', r'Luk \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_43_john(i_str):
#     i_str = re.sub(r'Joh\. ([0-9]+):([0-9]+)',       r' Joh \1:\2', i_str)
#     i_str = re.sub(r'Johannes ([0-9]+):([0-9]+)',    r' Joh \1:\2', i_str)
#     i_str = re.sub(r'Johannes Evangelium ([0-9]+):([0-9]+)',    r' Joh \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_44_acts(i_str):
#     i_str = re.sub(r'Apg. ([0-9]+):([0-9]+)', r'Apg \1:\2', i_str)
#     i_str = re.sub(r'Apostlagärningarna ([0-9]+):([0-9]+)', r'Apg \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_45_romans(i_str):
#     i_str = re.sub(r'Rom. ([0-9]+):([0-9]+)', r'Rom \1:\2', i_str)
#     i_str = re.sub(r'Romarbrevet ([0-9]+):([0-9]+)', r'Rom \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_46_1cor(i_str):
#     i_str = re.sub(r'I Kor ([0-9]+):([0-9]+)', r'1 Kor \1:\2', i_str)
#     i_str = re.sub(r'I Kor\. ([0-9]+):([0-9]+)', r'1 Kor \1:\2', i_str)
#     i_str = re.sub(r'1 Kor\. ([0-9]+):([0-9]+)', r'1 Kor \1:\2', i_str)
#     i_str = re.sub(r'1 Kor. ([0-9]+):([0-9]+)', r'1 Kor \1:\2', i_str)
#     i_str = re.sub(r'1 Korintierbrevet ([0-9]+):([0-9]+)', r'1 Kor \1:\2', i_str)
#     i_str = re.sub(r'1Kor. ([0-9]+):([0-9]+)', r'1 Kor \1:\2', i_str)
#     i_str = re.sub(r'Första Kor.([0-9]+):([0-9]+)', r'1 Kor \1:\2', i_str)
#     i_str = re.sub(r'Första Korintierbrevet ([0-9]+):([0-9]+)', r'1 Kor \1:\2', i_str)
#     i_str = re.sub(r'Förste Korintierbrevet ([0-9]+):([0-9]+)', r'1 Kor \1:\2', i_str)
#     i_str = re.sub(r'1Ko ([0-9]+):([0-9]+)', r'1 Kor \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_47_2cor(i_str):
#     i_str = re.sub(r'II Kor ([0-9]+):([0-9]+)', r'2 Kor \1:\2', i_str)
#     i_str = re.sub(r'II Kor\. ([0-9]+):([0-9]+)', r'2 Kor \1:\2', i_str)
#     i_str = re.sub(r'2 Kor\. ([0-9]+):([0-9]+)', r'2 Kor \1:\2', i_str)
#     i_str = re.sub(r'2 Kor. ([0-9]+):([0-9]+)', r'2 Kor \1:\2', i_str)
#     i_str = re.sub(r'2 Korintierbrevet ([0-9]+):([0-9]+)', r'2 Kor \1:\2', i_str)
#     i_str = re.sub(r'2Kor. ([0-9]+):([0-9]+)', r'2 Kor \1:\2', i_str)
#     i_str = re.sub(r'Andra Kor.([0-9]+):([0-9]+)', r'2 Kor \1:\2', i_str)
#     i_str = re.sub(r'Andra Korintierbrevet ([0-9]+):([0-9]+)', r'2 Kor \1:\2', i_str)
#     i_str = re.sub(r'2Ko ([0-9]+):([0-9]+)', r'2 Kor \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_48_galatians(i_str):
#     i_str = re.sub(r'Gal\. ([0-9]+):([0-9]+)', r'Gal \1:\2', i_str)
#     i_str = re.sub(r'Gal. ([0-9]+):([0-9]+)', r'Gal \1:\2', i_str)
#     i_str = re.sub(r'Galaterbrevet ([0-9]+):([0-9]+)', r'Gal \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_49_ephesians(i_str):
#     i_str = re.sub(r'Ef\. ([0-9]+):([0-9]+)', r'Ef \1:\2', i_str)
#     i_str = re.sub(r'Ef. ([0-9]+):([0-9]+)', r'Ef \1:\2', i_str)
#     i_str = re.sub(r'Efesierbrevet ([0-9]+):([0-9]+)', r'Ef \1:\2', i_str)
#     i_str = re.sub(r'Efésierbrevet ([0-9]+):([0-9]+)', r'Ef \1:\2', i_str)
#     i_str = re.sub(r'Efe ([0-9]+):([0-9]+)', r'Ef \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_50_philippians(i_str):
#     i_str = re.sub(r'Fil. ([0-9]+):([0-9]+)', r'Fil \1:\2', i_str)
#     i_str = re.sub(r'Filipperbrevet ([0-9]+):([0-9]+)', r'Fil \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_51_colossians(i_str):
#     i_str = re.sub(r'Kol. ([0-9]+):([0-9]+)', r'Kol \1:\2', i_str)
#     i_str = re.sub(r'Kolosserbrevet ([0-9]+):([0-9]+)', r'Kol \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_52_1thess(i_str):
#     i_str = re.sub(r'I Thess\. ([0-9]+):([0-9]+)', r'1 Thess \1:\2', i_str)
#     i_str = re.sub(r'1 Thess\. ([0-9]+):([0-9]+)', r'1 Thess \1:\2', i_str)
#     i_str = re.sub(r'1 Thessalonikebrevet ([0-9]+):([0-9]+)', r'1 Thess \1:\2', i_str)
#     i_str = re.sub(r'1 Tessalonikebrevet ([0-9]+):([0-9]+)', r'1 Thess \1:\2', i_str)
#     i_str = re.sub(r'1 Tess ([0-9]+):([0-9]+)', r'1 Thess \1:\2', i_str)
#     i_str = re.sub(r'1 Tessalonikerbrevet ([0-9]+):([0-9]+)', r'1 Thess \1:\2', i_str)
#     i_str = re.sub(r'1 Thess. ([0-9]+):([0-9]+)', r'1 Thess \1:\2', i_str)
#     i_str = re.sub(r'Första Tess. ([0-9]+):([0-9]+)', r'1 Thess \1:\2', i_str)
#     i_str = re.sub(r'Första Tessalonikerbrevet ([0-9]+):([0-9]+)', r'1 Thess \1:\2', i_str)
#     i_str = re.sub(r'Förste Tessalonikerbrevet ([0-9]+):([0-9]+)', r'1 Thess \1:\2', i_str)
#     i_str = re.sub(r'1Te ([0-9]+):([0-9]+)', r'1 Thess \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_53_2thess(i_str):
#     i_str = re.sub(r'II Thess\. ([0-9]+):([0-9]+)', r'2 Thess \1:\2', i_str)
#     i_str = re.sub(r'2 Thess\. ([0-9]+):([0-9]+)', r'2 Thess \1:\2', i_str)
#     i_str = re.sub(r'2 Thessalonikebrevet ([0-9]+):([0-9]+)', r'2 Thess \1:\2', i_str)
#     i_str = re.sub(r'2 Tessalonikebrevet ([0-9]+):([0-9]+)', r'2 Thess \1:\2', i_str)
#     i_str = re.sub(r'2 Tess ([0-9]+):([0-9]+)', r'2 Thess \1:\2', i_str)
#     i_str = re.sub(r'2 Tessalonikerbrevet ([0-9]+):([0-9]+)', r'2 Thess \1:\2', i_str)
#     i_str = re.sub(r'2 Thess. ([0-9]+):([0-9]+)', r'2 Thess \1:\2', i_str)
#     i_str = re.sub(r'Andra Tess. ([0-9]+):([0-9]+)', r'2 Thess \1:\2', i_str)
#     i_str = re.sub(r'Andra Tessalonikerbrevet ([0-9]+):([0-9]+)', r'2 Thess \1:\2', i_str)
#     i_str = re.sub(r'2Te ([0-9]+):([0-9]+)', r'2 Thess \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_54_1timothy(i_str):
#     i_str = re.sub(r'1 Tim. ([0-9]+):([0-9]+)', r'1 Tim \1:\2', i_str)
#     i_str = re.sub(r'1 Tim\. ([0-9]+):([0-9]+)', r'1 Tim \1:\2', i_str)
#     i_str = re.sub(r'1 Timoteus ([0-9]+):([0-9]+)', r'1 Tim \1:\2', i_str)
#     i_str = re.sub(r'1 Timoteusbrevet ([0-9]+):([0-9]+)', r'1 Tim \1:\2', i_str)
#     i_str = re.sub(r'1 Timoteusbrevet ([0-9]+):([0-9]+)', r'1 Tim \1:\2', i_str)
#     i_str = re.sub(r'1Tim. ([0-9]+):([0-9]+)', r'1 Tim \1:\2', i_str)  # 2018-06-01
#     i_str = re.sub(r'1Timoteus ([0-9]+):([0-9]+)', r'1 Tim \1:\2', i_str)  # 2018-06-01
#     i_str = re.sub(r'Första Tim. ([0-9]+):([0-9]+)', r'1 Tim \1:\2', i_str)
#     i_str = re.sub(r'Första Timoteusbrevet ([0-9]+):([0-9]+)', r'1 Tim \1:\2', i_str)
#     i_str = re.sub(r'Förste Timoteusbrevet ([0-9]+):([0-9]+)', r'1 Tim \1:\2', i_str)
#     i_str = re.sub(r'I Tim\. ([0-9]+):([0-9]+)', r'1 Tim \1:\2', i_str)
#     i_str = re.sub(r'I Timoteusbrevet ([0-9]+):([0-9]+)', r'1 Tim \1:\2', i_str)
#     i_str = re.sub(r'1Ti ([0-9]+):([0-9]+)', r'1 Tim \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_55_2timothy(i_str):
#     i_str = re.sub(r'2 Tim. ([0-9]+):([0-9]+)', r'2 Tim \1:\2', i_str)
#     i_str = re.sub(r'2 Tim\. ([0-9]+):([0-9]+)', r'2 Tim \1:\2', i_str)
#     i_str = re.sub(r'2 Timoteus ([0-9]+):([0-9]+)', r'2 Tim \1:\2', i_str)
#     i_str = re.sub(r'2 Timoteusbrevet ([0-9]+):([0-9]+)', r'2 Tim \1:\2', i_str)
#     i_str = re.sub(r'2 Timoteusbrevet ([0-9]+):([0-9]+)', r'2 Tim \1:\2', i_str)
#     i_str = re.sub(r'2Tim. ([0-9]+):([0-9]+)', r'2 Tim \1:\2', i_str)  # 2018-06-01
#     i_str = re.sub(r'2Timoteus ([0-9]+):([0-9]+)', r'2 Tim \1:\2', i_str)  # 2018-06-01
#     i_str = re.sub(r'Andra Tim. ([0-9]+):([0-9]+)', r'2 Tim \1:\2', i_str)
#     i_str = re.sub(r'Andra Timoteusbrevet ([0-9]+):([0-9]+)', r'2 Tim \1:\2', i_str)
#     i_str = re.sub(r'II Tim\. ([0-9]+):([0-9]+)', r'2 Tim \1:\2', i_str)
#     i_str = re.sub(r'II Timoteusbrevet ([0-9]+):([0-9]+)', r'2 Tim \1:\2', i_str)
#     i_str = re.sub(r'2Ti ([0-9]+):([0-9]+)', r'2 Tim \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_56_titus(i_str):
#     i_str = re.sub(r'Tit. ([0-9]+):([0-9]+)', r'Tit \1:\2', i_str)
#     i_str = re.sub(r'Titusbrevet ([0-9]+):([0-9]+)', r'Tit \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_57_philemon(i_str):
#     i_str = re.sub(r'Brevet till Filemon ([0-9]+):([0-9]+)', r'Filem \1:\2', i_str)
#     i_str = re.sub(r'Filem. ([0-9]+):([0-9]+)', r'Filem \1:\2', i_str)
#     i_str = re.sub(r'Flm ([0-9]+):([0-9]+)', r'Filem \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_58_hebrews(i_str):
#     i_str = re.sub(r'Heb. ([0-9]+):([0-9]+)', r'Hebr \1:\2', i_str)
#     i_str = re.sub(r'Hebr. ([0-9]+):([0-9]+)', r'Hebr \1:\2', i_str)
#     i_str = re.sub(r'Hebreerbrevet ([0-9]+):([0-9]+)', r'Hebr \1:\2', i_str)
#     i_str = re.sub(r'Heb ([0-9]+):([0-9]+)', r'Hebr \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_59_james(i_str):
#     i_str = re.sub(r'Jak. ([0-9]+):([0-9]+)', r'Jak \1:\2', i_str)
#     i_str = re.sub(r'Jakobsbrevet ([0-9]+):([0-9]+)', r'Jak \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_60_1peter(i_str):
#     i_str = re.sub(r'1 Pet. ([0-9]+):([0-9]+)', r'1 Pet \1:\2', i_str)
#     i_str = re.sub(r'1 Peter ([0-9]+):([0-9]+)', r'1 Pet \1:\2', i_str)
#     i_str = re.sub(r'1 Petr. ([0-9]+):([0-9]+)', r'1 Pet \1:\2', i_str)
#     i_str = re.sub(r'1 Petri ([0-9]+):([0-9]+)', r'1 Pet \1:\2', i_str)
#     i_str = re.sub(r'1 Petri. ([0-9]+):([0-9]+)', r'1 Pet \1:\2', i_str)
#     i_str = re.sub(r'1 Petrusbrevet ([0-9]+):([0-9]+)', r'1 Pet \1:\2', i_str)
#     i_str = re.sub(r'1. Pet. ([0-9]+):([0-9]+)', r'1 Pet \1:\2', i_str)
#     i_str = re.sub(r'1Pet ([0-9]+):([0-9]+)', r'1 Pet \1:\2', i_str)
#     i_str = re.sub(r'1Pet. ([0-9]+):([0-9]+)', r'1 Pet \1:\2', i_str)
#     i_str = re.sub(r'1Petr ([0-9]+):([0-9]+)', r'1 Pet \1:\2', i_str)
#     i_str = re.sub(r'1Petr. ([0-9]+):([0-9]+)', r'1 Pet \1:\2', i_str)
#     i_str = re.sub(r'1Petri ([0-9]+):([0-9]+)', r'1 Pet \1:\2', i_str)  # 2018-06-01
#     i_str = re.sub(r'Första Petr. ([0-9]+):([0-9]+)', r'1 Pet \1:\2', i_str)
#     i_str = re.sub(r'Första Petrusbrevet ([0-9]+):([0-9]+)', r'1 Pet \1:\2', i_str)
#     i_str = re.sub(r'Förste Petrusbrevet ([0-9]+):([0-9]+)', r'1 Pet \1:\2', i_str)
#     i_str = re.sub(r'1Pe ([0-9]+):([0-9]+)', r'1 Pet \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_61_2peter(i_str):
#     i_str = re.sub(r'2 Pet. ([0-9]+):([0-9]+)', r'2 Pet \1:\2', i_str)
#     i_str = re.sub(r'2 Peter ([0-9]+):([0-9]+)', r'2 Pet \1:\2', i_str)
#     i_str = re.sub(r'2 Petr. ([0-9]+):([0-9]+)', r'2 Pet \1:\2', i_str)
#     i_str = re.sub(r'2 Petri ([0-9]+):([0-9]+)', r'2 Pet \1:\2', i_str)
#     i_str = re.sub(r'2 Petri. ([0-9]+):([0-9]+)', r'2 Pet \1:\2', i_str)
#     i_str = re.sub(r'2 Petrusbrevet ([0-9]+):([0-9]+)', r'2 Pet \1:\2', i_str)
#     i_str = re.sub(r'2. Pet. ([0-9]+):([0-9]+)', r'2 Pet \1:\2', i_str)  # 2018-05-07
#     i_str = re.sub(r'2Pet ([0-9]+):([0-9]+)', r'2 Pet \1:\2', i_str)
#     i_str = re.sub(r'2Pet. ([0-9]+):([0-9]+)', r'2 Pet \1:\2', i_str)
#     i_str = re.sub(r'2Petr ([0-9]+):([0-9]+)', r'2 Pet \1:\2', i_str)
#     i_str = re.sub(r'2Petr. ([0-9]+):([0-9]+)', r'2 Pet \1:\2', i_str)
#     i_str = re.sub(r'2Petri ([0-9]+):([0-9]+)', r'2 Pet \1:\2', i_str)  # 2018-06-01
#     i_str = re.sub(r'Andra Petr. ([0-9]+):([0-9]+)', r'2 Pet \1:\2', i_str)
#     i_str = re.sub(r'Andra Petrusbrevet ([0-9]+):([0-9]+)', r'2 Pet \1:\2', i_str)
#     i_str = re.sub(r'2Pe ([0-9]+):([0-9]+)', r'2 Pet \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_62_1john(i_str):
#     i_str = re.sub(r'1 Joh ([0-9]+):([0-9]+)', r' 1 Jh \1:\2', i_str)
#     i_str = re.sub(r'1 joh ([0-9]+):([0-9]+)', r' 1 Jh \1:\2', i_str)
#     i_str = re.sub(r'1 Joh. ([0-9]+):([0-9]+)', r' 1 Jh \1:\2', i_str)
#     i_str = re.sub(r'1 Johannesbrevet ([0-9]+):([0-9]+)', r' 1 Jh \1:\2', i_str)
#     i_str = re.sub(r'1Jh ([0-9]+):([0-9]+)', r' 1 Jh \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_63_2john(i_str):
#     i_str = re.sub(r'2 Joh ([0-9]+):([0-9]+)', r' 2 Jh \1:\2', i_str)
#     i_str = re.sub(r'2 joh ([0-9]+):([0-9]+)', r' 2 Jh \1:\2', i_str)
#     i_str = re.sub(r'2 Joh. ([0-9]+):([0-9]+)', r' 2 Jh \1:\2', i_str)
#     i_str = re.sub(r'2 Johannesbrevet ([0-9]+):([0-9]+)', r' 2 Jh \1:\2', i_str)
#     i_str = re.sub(r'2Joh. ([0-9]+):([0-9]+)', r' 2 Jh \1:\2', i_str)
#     i_str = re.sub(r'Andra Joh.([0-9]+):([0-9]+)', r' 2 Jh \1:\2', i_str)
#     i_str = re.sub(r'Andra Johannesbrevet ([0-9]+):([0-9]+)', r' 2 Jh \1:\2', i_str)
#     i_str = re.sub(r'2Jh ([0-9]+):([0-9]+)', r' 2 Jh \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_64_3john(i_str):
#     i_str = re.sub(r'3 Joh ([0-9]+):([0-9]+)', r' 3 Jh \1:\2', i_str)
#     i_str = re.sub(r'3 joh ([0-9]+):([0-9]+)', r' 3 Jh \1:\2', i_str)
#     i_str = re.sub(r'3 Joh. ([0-9]+):([0-9]+)', r' 3 Jh \1:\2', i_str)
#     i_str = re.sub(r'3 Johannesbrevet ([0-9]+):([0-9]+)', r' 3 Jh \1:\2', i_str)
#     i_str = re.sub(r'3Joh. ([0-9]+):([0-9]+)', r' 3 Jh \1:\2', i_str)
#     i_str = re.sub(r'Tredje Joh.([0-9]+):([0-9]+)', r' 3 Jh \1:\2', i_str)
#     i_str = re.sub(r'Tredje Johannesbrevet ([0-9]+):([0-9]+)', r' 3 Jh \1:\2', i_str)
#     i_str = re.sub(r'3Jh ([0-9]+):([0-9]+)', r' 3 Jh \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_65_jude(i_str):
#     # Add chapter '1' if missing
#     # Jude 24, 25   =>      Jude 1:24\nJude 1:25
#     # Jude 24   => Jude 1:24
#     i_str = re.sub(r'Jud. ([0-9]+):([0-9]+)', r'Jud \1:\2', i_str)
#     i_str = re.sub(r'Judasbrevet ([0-9]+):([0-9]+)', r'Jud \1:\2', i_str)
#     return i_str
#
#
# def normalize_swe_66_revelation(i_str):
#     i_str = re.sub(r'Upp. ([0-9]+):([0-9]+)', r'Upp \1:\2', i_str)
#     i_str = re.sub(r'Uppenbarelseboken ([0-9]+):([0-9]+)', r'Upp \1:\2', i_str)
#     return i_str
#
#
# def remove_swe_reference_suffix(i_str):       # 2019-10-15
#     i_str = re.sub(r'([0-9]+) nuB', r'\1', i_str)
#     i_str = re.sub(r'([0-9]+) nuBibeln', r'\1', i_str)
#     i_str = re.sub(r'([0-9]+) SFB', r'\1', i_str)
#     i_str = re.sub(r'([0-9]+) SFB15', r'\1', i_str)
#     i_str = re.sub(r'([0-9]+) Folkbibeln 2015', r'\1', i_str)
#     i_str = re.sub(r'([0-9]+) SFB98', r'\1', i_str)
#     i_str = re.sub(r'([0-9]+) Bibel 2000', r'\1', i_str)
#     i_str = re.sub(r'([0-9]+) Bibel 1917', r'\1', i_str)
#     i_str = re.sub(r'([0-9]+) SRB', r'\1', i_str)
#     i_str = re.sub(r'([0-9]+) Svenska kärnbibeln', r'\1', i_str)
#     i_str = re.sub(r'Kapitel ([0-9]+).', r'', i_str)              # 2019-11-10
#     return i_str



def normalize_swe_bible_book_names(i_str):
    f_normalize_swe_bible_book_names_list = Path(script_directory) / '..' / 'database' / 'normalized_swe_bible_book_names_list.txt'
    normalized_swe_bible_book_names_list = get_2_tuple_as_list(f_normalize_swe_bible_book_names_list, False)
    for item in normalized_swe_bible_book_names_list:
        # print(item[0])
        i_str = re.sub(item[0], item[1], i_str)

    f_replace_swe_esword_book_names_list = Path(script_directory) / '..' / 'database' / 'replace_swe_esword_book_names_list.txt'    # 2022-12-25
    replace_swe_esword_book_names_list = get_2_tuple_as_list(f_replace_swe_esword_book_names_list, False)
    for item in replace_swe_esword_book_names_list:
        i_str = re.sub(item[0], item[1], i_str)

    # print('975 i_str: ' +i_str)
    return i_str
