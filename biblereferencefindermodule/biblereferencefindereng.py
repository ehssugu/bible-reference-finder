#!/usr/bin/env python
__author__ = 'Sune Gustafsson'
__copyright__ = 'Copyright 2019, Sune Gustafsson'
__credits__ = ['Sune Gustafsson']
__license__ = 'GPL'
__version__ = '1.0.1'
__date__ = '2019-10-02 07:51:55'
__maintainer__ = 'Sune Gustafsson'
__email__ = 'ehssugu@gmail.com'
__status__ = 'Working'

import re
from pathlib import Path
import os

script_directory = os.path.dirname(os.path.realpath(__file__))
ONESPACE = ' '
TWOSPACE = '  '
THREESPACE = '   '
ENCODING = "UTF-8"


def get_1_line_in_file_as_list(f_name):
    # 2020-10-03
    # This function assumes that the file data is organized in the following way
    # # <Text> Data that should be ignored
    # <Text-A><\n>
    # <Text-B><\n>
    #
    # Output is returned in a list with following format
    # [
    # [Text-A][Text-B]...[Text-N]
    # ]
    out_list = []

    with open(f_name, 'r', encoding=ENCODING) as myfile:
        file_lines = myfile.readlines()

        for line in file_lines:
            if not line.startswith('#'):
                out_list.append(line.rstrip())

    myfile.close()

    return out_list # get_file_rows_as_list


def add_bible_book_name_before_incomplete_reference(i_str, real_bible_book_name):
    # Experimental "(1:9–20)" ==> (Revelation 1:9-20"

    # with (22:16-18) OK
    i_str = re.sub(r' with \(([0-9]+):([0-9]+)-([0-9]+)\)',
                   r'BibleTempBook \1:\2-\3',
                   i_str)

    # with (22:16) OK
    i_str = re.sub(r' with \(([0-9]+):([0-9]+)\)',
                   r'BibleTempBook \1:\2',
                   i_str)

    # (cf. 4:2; 17:3; 21:10) OK
    i_str = re.sub(r' \(cf ([0-9]+):([0-9]+); ([0-9]+):([0-9]+); ([0-9]+):([0-9]+)\)',
                   r'BibleTempBook \1:\2; \3:\4; \5:\6',
                   i_str)

    # (cf. 4:2; 17:3) OK
    i_str = re.sub(r' \(cf ([0-9]+):([0-9]+); ([0-9]+):([0-9]+)\)',
                   r'BibleTempBook \1:\2; \3:\4',
                   i_str)

    # (cf. 22:16-18) OK
    i_str = re.sub(r' \(cf ([0-9]+):([0-9]+)-([0-9]+)\)',
                   r'BibleTempBook \1:\2-\3',
                   i_str)

    # (cf. 22:16) OK
    i_str = re.sub(r' \(cf ([0-9]+):([0-9]+)\)',
                   r'BibleTempBook \1:\2',
                   i_str)

    # (22:16) OK
    i_str = re.sub(r' \(([0-9]+):([0-9]+)\)',
                   r'BibleTempBook \1:\2',
                   i_str)

    # (22:16-) OK
    i_str = re.sub(r' \(([0-9]+):([0-9]+)-\)',
                   r'BibleTempBook \1:\2-',
                   i_str)

    # (1:9–3:) OK
    i_str = re.sub(r' \(([0-9]+):([0-9]+)-([0-9]+):\)',
                   r'BibleTempBook \1:\2-\3:',
                   i_str)

    # (22:16, 18) OK
    i_str = re.sub(r' \(([0-9]+):([0-9]+), ([0-9]+)\)',
                   r'BibleTempBook \1:\2, \3',
                   i_str)

    # (1:9–3:5) OK
    i_str = re.sub(r' \(([0-9]+):([0-9]+)-([0-9]+):([0-9]+)\)',
                   r'BibleTempBook \1:\2-\3:\4',
                   i_str)

    # (8:13; 9:12; 11:14; 12:12; 13:1) OK
    i_str = re.sub(r' \(([0-9]+):([0-9]+); ([0-9]+):([0-9]+); ([0-9]+):([0-9]+); ([0-9]+):([0-9]+); ([0-9]+):([0-9]+)\)',
                   r'BibleTempBook \1:\2; \3:\4; \5:\6; \7:\8; \9:\10',
                   i_str)

    # (8:13; 9:12; 11:14; 12:12) OK
    i_str = re.sub(r' \(([0-9]+):([0-9]+); ([0-9]+):([0-9]+); ([0-9]+):([0-9]+); ([0-9]+):([0-9]+)\)',
                   r'BibleTempBook \1:\2; \3:\4; \5:\6; \7:\8',
                   i_str)

    # (8:13; 9:12; 11:14) OK
    i_str = re.sub(r' \(([0-9]+):([0-9]+); ([0-9]+):([0-9]+); ([0-9]+):([0-9]+)\)',
                   r'BibleTempBook \1:\2; \3:\4; \5:\6',
                   i_str)
    # (8:13; 9:12) OK
    i_str = re.sub(r' \(([0-9]+):([0-9]+); ([0-9]+):([0-9]+)\)',
                   r'BibleTempBook \1:\2; \3:\4',
                   i_str)

    # between 22:6, 12, 14, 16 OK
    i_str = re.sub(r' between ([0-9]+):([0-9]+), ([0-9]+), ([0-9]+), , ([0-9]+)',
                   r'BibleTempBook \1:\2, \3, \4, \5',
                   i_str)

    # between 22:8, 12, 14 OK
    i_str = re.sub(r' between ([0-9]+):([0-9]+), ([0-9]+), ([0-9]+)',
                   r'BibleTempBook \1:\2, \3, \4',
                   i_str)

    # between 22:8, 12 OK
    i_str = re.sub(r' between ([0-9]+):([0-9]+), ([0-9]+)',
                   r'BibleTempBook \1:\2, \3',
                   i_str)

    # between 22:6-8 OK
    i_str = re.sub(r' between ([0-9]+):([0-9]+)-([0-9]+)',
                   r'BibleTempBook \1:\2-\3',
                   i_str)

    # in 22:6 OK
    i_str = re.sub(r' in ([0-9]+):([0-9]+)',
                   r'BibleTempBook \1:\2',
                   i_str)

    # in 22:6, 12, 14, 16 OK
    i_str = re.sub(r' in ([0-9]+):([0-9]+), ([0-9]+), ([0-9]+), , ([0-9]+)',
                   r'BibleTempBook \1:\2, \3, \4, \5',
                   i_str)

    # in 22:8, 12, 14 OK
    i_str = re.sub(r' in ([0-9]+):([0-9]+), ([0-9]+), ([0-9]+)',
                   r'BibleTempBook \1:\2, \3, \4',
                   i_str)

    # in 22:8, 12 OK
    i_str = re.sub(r' in ([0-9]+):([0-9]+), ([0-9]+)',
                   r'BibleTempBook \1:\2, \3',
                   i_str)

    # in 22:6-8 OK
    i_str = re.sub(r' in ([0-9]+):([0-9]+)-([0-9]+)',
                   r'BibleTempBook \1:\2-\3',
                   i_str)

    # in 22:6 OK
    i_str = re.sub(r' in ([0-9]+):([0-9]+)',
                   r'BibleTempBook \1:\2',
                   i_str)

    # with 22:6, 12, 14, 16 OK
    i_str = re.sub(r' with ([0-9]+):([0-9]+), ([0-9]+), ([0-9]+), ([0-9]+)',
                   r'BibleTempBook \1:\2, \3, \4, \5',
                   i_str)

    # with 22:8, 12, 14 OK
    i_str = re.sub(r' with ([0-9]+):([0-9]+), ([0-9]+), ([0-9]+)',
                   r'BibleTempBook \1:\2, \3, \4',
                   i_str)

    # with 22:8, 12 OK
    i_str = re.sub(r' with ([0-9]+):([0-9]+), ([0-9]+)',
                   r'BibleTempBook \1:\2, \3',
                   i_str)

    # with 22:6-8 OK
    i_str = re.sub(r' with ([0-9]+):([0-9]+)-([0-9]+)',
                   r'BibleTempBook \1:\2-\3',
                   i_str)

    # with 22:6 OK
    i_str = re.sub(r' with ([0-9]+):([0-9]+)',
                   r'BibleTempBook \1:\2',
                   i_str)

    # both 22:6, 12, 14, 16 OK
    i_str = re.sub(r' both ([0-9]+):([0-9]+), ([0-9]+), ([0-9]+), ([0-9]+)',
                   r'BibleTempBook \1:\2, \3, \4, \5',
                   i_str)

    # both 22:8, 12, 14 OK
    i_str = re.sub(r' both ([0-9]+):([0-9]+), ([0-9]+), ([0-9]+)',
                   r'BibleTempBook \1:\2, \3, \4',
                   i_str)

    # both 22:8, 12 OK
    i_str = re.sub(r' both ([0-9]+):([0-9]+), ([0-9]+)',
                   r'BibleTempBook \1:\2, \3',
                   i_str)

    # both 22:6-8 OK
    i_str = re.sub(r' both ([0-9]+):([0-9]+)-([0-9]+)',
                   r'BibleTempBook \1:\2-\3',
                   i_str)

    # and 22:6 OK
    i_str = re.sub(r' and ([0-9]+):([0-9]+)',
                   r'BibleTempBook \1:\2',
                   i_str)

    # and 22:6, 12, 14, 16 OK
    i_str = re.sub(r' and ([0-9]+):([0-9]+), ([0-9]+), ([0-9]+), ([0-9]+)',
                   r'BibleTempBook \1:\2, \3, \4, \5',
                   i_str)

    # and 22:8, 12, 14 OK
    i_str = re.sub(r' and ([0-9]+):([0-9]+), ([0-9]+), ([0-9]+)',
                   r'BibleTempBook \1:\2, \3, \4',
                   i_str)

    # and 22:8, 12 OK
    i_str = re.sub(r' and ([0-9]+):([0-9]+), ([0-9]+)',
                   r'BibleTempBook \1:\2, \3',
                   i_str)

    # and 22:6-8 OK
    i_str = re.sub(r' and ([0-9]+):([0-9]+)-([0-9]+)',
                   r'BibleTempBook \1:\2-\3',
                   i_str)

    # and 22:6 OK
    i_str = re.sub(r' and ([0-9]+):([0-9]+)',
                   r'BibleTempBook \1:\2',
                   i_str)

    # TODO 2022-02-02 Add Real Bible Book Name in this statement
    i_str = i_str.replace('BibleTempBook', real_bible_book_name)

    return i_str





def get_2_tuple_as_list(i_file_path_and_name, new_line: bool):
    # 2020-10-03
    # This function assumes that the file data is organized in the following way
    # # <Text> Data that should be ignored
    # <Text-A><\t><Text-B><\n>
    #
    # Output is returned in a list with following format
    # [
    # [Text-A]['\n'Text-B]
    # ...
    # [Text-A]['\n'Text-B]
    # ]
    # OR
    # [
    # [Text-A][Text-B]
    # ...
    # [Text-A][Text-B]
    # ]

    out_list = []
    line_list = []

    if new_line:
        b_prefix = '\n'
    else:
        b_prefix = ''

    with open(i_file_path_and_name, 'r', encoding=ENCODING) as myfile:
        file_lines = myfile.readlines()

        for line in file_lines:
            if not line.startswith('#'):
                if line.__contains__('\t'):
                    a = line[0:line.index('\t')]
                    b = b_prefix + line[line.index('\t') + 1:line.index('\n')]
                    line_list.append(a)
                    line_list.append(b)
                    out_list.append(line_list)
                    # print('1278 line: ' + line)
                else:
                    print("1280 Error in file: " + i_file_path_and_name + ' Line: ' + line)
                line_list = []

    myfile.close()
    # print("186 t_text: " +t_text.__str__())

    return out_list  # get_2_tuple_as_list

def get_kjv_number_of_verses_per_chapter_list():

    f = Path(script_directory) / '..' / 'database' / 'kjv_number_of_verses_per_chapter_list.txt'
    kjv_number_of_verses_per_chapter_list = get_1_line_in_file_as_list(f)

    return kjv_number_of_verses_per_chapter_list


def prepare_eng_text_for_replacement(i_str: str):
    f_eng_text_for_replacement_list = Path(script_directory) / '..' / 'database' / 'eng_text_for_replacement_list.txt'
    eng_text_for_replacement_list = get_2_tuple_as_list(f_eng_text_for_replacement_list, False)
    for item in eng_text_for_replacement_list:
        i_str = re.sub(item[0], item[1], i_str)
    #  'Daniel 8:14 and Revelation 14:6, 7.  The'  => 'Daniel 8:14 and Revelation 14:6, 7  The'
    # #  Daniel 8:14 and Revelation 14:6, 7.  The
    # i_str = re.sub(r'([0-9]+):([0-9]+), ([0-9]+)\.', r'\1:\2, \3', i_str)
    # i_str = re.sub(r'([0-9]+):([0-9]+)\. ', r'\1:\2 ', i_str)




    # TODO 2022-02-01. Experimental for some books with incomplete references
    real_bible_book_name = 'Revelation'
    # For test purpose only
    # real_bible_book_name = ' BibleTempBook'
    # i_str = add_bible_book_name_before_incomplete_reference(i_str, real_bible_book_name)

    return i_str  # prepare_text_for_replacement


def remove_eng_reference_prefix(i_str = str):
    f_eng_reference_prefix_list = Path(script_directory) / '..' / 'database' / 'eng_reference_prefix_list.txt'
    eng_reference_prefix_list = get_2_tuple_as_list(f_eng_reference_prefix_list, False)
    for item in eng_reference_prefix_list:
        i_str = re.sub(item[0], item[1], i_str)
    # i_str = prepare_text_for_replacement(i_str)
    # i_str = i_str.replace(TWOSPACE, ONESPACE)
    # i_str = re.sub(r'([0-9]+)\. ([A-Ö]+)', r'\2', i_str)
    # i_str = re.sub(r'([0-9]+)\. ([0-9]+)', r'\2', i_str)
    # i_str = re.sub(r'([0-9]+):a ([a-z]+)', r'\2', i_str)
    # i_str = re.sub(r'([0-9]+):e ([a-z]+)', r'\2', i_str)

    return i_str


def remove_eng_reference_suffix(i_str):
    f_eng_reference_suffix_list = Path(script_directory) / '..' / 'database' / 'eng_reference_suffix_list.txt'
    eng_reference_suffix_list = get_2_tuple_as_list(f_eng_reference_suffix_list, False)
    for item in eng_reference_suffix_list:
        i_str = re.sub(item[0], item[1], i_str)
    # i_str = re.sub(r'([0-9]+) KJV', r'\1', i_str)
    # i_str = re.sub(r'([0-9]+) NIV', r'\1', i_str)
    # i_str = re.sub(r'([0-9]+) NLT', r'\1', i_str)
    # i_str = re.sub(r'([0-9]+) NKJV', r'\1', i_str)
    # i_str = re.sub(r'([0-9]+) RSV', r'\1', i_str)
    # i_str = re.sub(r'([0-9]+) YLT', r'\1', i_str)

    return i_str


def normalize_eng_bible_book_names(in_str):
    out_str = prepare_eng_text_for_replacement(in_str)
    # i_str = remove_eng_reference_prefix(i_str)  # 2019-10-09
    out_str = remove_eng_reference_suffix(out_str)  # 2019-10-17
    # print('1335: ' + i_str)

    f_normalized_eng_bible_book_names_list = Path(script_directory) / '..' / 'database' / 'normalized_eng_bible_book_names_list.txt'
    normalized_eng_bible_book_names_list = get_2_tuple_as_list(f_normalized_eng_bible_book_names_list, new_line = False)

    for item in normalized_eng_bible_book_names_list:
        # if item[0].__contains__('1 John') and out_str.__contains__('1 John'):
        #     print('155: ' + item[0] + ' ==> ' + item[1])
        #     print('157: ' + out_str)
        out_str = re.sub(item[0], item[1], out_str)
        # if out_str.__contains__('1 Jnx'):
        #     print('159: ' + out_str)

    # if out_str.__contains__('1 Jnx'):
    #     print('165: ' + out_str)
    if out_str.__contains__('Jude 1:1:'):
        out_str = re.sub(r'Jude 1:1:', r'Jude 1:', out_str)

    return out_str  # normalize_eng_bible_book_names


def get_english_standard_book_name(i_book_number):
    f = Path(script_directory) / '..' / 'database' / 'normalized_eng_book_names_list.txt'
    normalized_eng_book_names_list = get_1_line_in_file_as_list(f)

    return normalized_eng_book_names_list[i_book_number - 1]
